#!/usr/bin/env python
# coding: utf-8

import os

from rich import print
from rich.panel import Panel
from rich.columns import Columns
from datetime import datetime


EXCLUDE_DIR = [".git", "__pycache__"]
EXCLUDE_FILES = [".pyc"]

parents = [p for p in os.listdir(os.environ["DHA_BASE"]) if not p in EXCLUDE_DIR]


def convert_date(timestamp):
    d = datetime.utcfromtimestamp(timestamp)
    formated_date = d.strftime("%d %b %Y")
    return formated_date


def file_info(file):
    if isinstance(file, str):
        info = os.stat(file)
    else:
        info = file.stat()
    return f"[green]Last Modified: {convert_date(info.st_mtime)}[/green], [yellow]File size: {info.st_size} Bytes[/yellow]"


repo_structure = []
for parent in parents:
    # lets do them later
    if os.path.isfile(parent):
        continue
    files = []
    todo = [os.path.relpath(os.path.join(parent, p)) for p in os.listdir(parent)]
    while todo:
        entry = todo.pop(0)
        if not os.path.isdir(entry):
            if not any(pattern in entry for pattern in EXCLUDE_FILES):
                files.append(f"{entry}: {file_info(entry)}")
        else:
            todo = [
                os.path.relpath(os.path.join(entry, e))
                for e in os.listdir(entry)
                if not e in EXCLUDE_DIR
            ] + todo
    if files:
        repo_structure.append(
            Panel("\n".join(files), expand=False, title=parent, border_style="blue", padding=(1, 2))
        )

# do single files here
repo_structure.append(
    Panel(
        "\n".join(f"{f}: {file_info(f)}" for f in parents if os.path.isfile(f)),
        expand=False,
        title="Other files",
        border_style="blue",
        padding=(1, 2),
    )
)

print(Columns(repo_structure, title="Common repo structure", expand=True))
