#!/bin/bash
# enter docker container interactively
echo "Please enter docker container via 'htdocker cmssw/cc7:x86_64' and source this file."

# get common base dirs
DHA_BASE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && /bin/pwd )"
DHA_DATA="/net/scratch/cms/dihiggs"

# set inference base dirs
DHI_BASE=${DHA_BASE}/inference
DHI_DATA=${DHA_DATA}/inference

# install inference repo
cd ${DHI_BASE}
export DHI_COMBINE_STANDALONE=False
source setup.sh default

setup_combine_harvester() {
    # add CombineHarvester to CMSSW installation of inference repo and recompile
    cd ${DHI_SOFTWARE}/${CMSSW_VERSION}/src || return 1
    # add Harvester to CMSSW source
    git clone https://github.com/cms-analysis/CombineHarvester.git CombineHarvester || return 1
    # recompile with maximum number cores
    scram b -j $(grep -c ^processor /proc/cpuinfo) || return 1
}

if [[ ! -f ${DHI_SOFTWARE}/${CMSSW_VERSION}/src/.done ]]
then # if inference installation not complete
    if setup_combine_harvester
    then
        # if done touch file
        touch ${DHI_SOFTWARE}/${CMSSW_VERSION}/src/.done
    else
        echo "Combine Harvester Setup failed!"
    fi
fi

# switch back to original directory
cd $DHA_BASE