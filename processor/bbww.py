# coding: utf-8

import numpy as np
import awkward as ak


btag_wp = {
    "2016": {"medium": 0.3093, "tight": 0.7221},
    "2017": {"medium": 0.3033, "tight": 0.7489},
    "2018": {"medium": 0.2770, "tight": 0.7264},
}

# muons
def presel_mu(muons):
    muon_selection = (
        muons.looseId
        & (muons.pt >= 5.0)
        & (abs(muons.eta) <= 2.4)
        & (abs(muons.dxy) <= 0.05)
        & (abs(muons.dz) <= 0.1)
        & (muons.miniPFRelIso_all <= 0.4)
        & (muons.sip3d <= 8.0)
    )
    presel_muons = muons[muon_selection]
    return presel_muons


def conept_mu(muons):
    abspdgid = abs(muons.pdgId)
    condition = ((abspdgid != 11) & (abspdgid != 13)) | (
        (abspdgid == 13) & muons.mediumId & (muons.mvaTTH > 0.5)
    )
    return ak.where(condition, muons.pt, 0.9 * muons.pt * (1.0 + muons.jetRelIso))


def smooth_bflav(muons, year):
    jetpt = 0.9 * muons.pt * (1 + muons.jetRelIso)
    x = np.minimum(np.maximum(0.0, jetpt - 20.0) / (45.0 - 20.0), 1.0)
    if year == "2016":
        return x * 0.0614 + (1 - x) * 0.3093
    elif year == "2017":
        return x * 0.0521 + (1 - x) * 0.3033
    elif year == "2018":
        return x * 0.0494 + (1 - x) * 0.2770
    else:
        raise Exception("choose a valid year!")


def fakeable_mu(presel_muons, **kwargs):
    year = kwargs.pop("year")
    btag = ak.fill_none(presel_muons.matched_jet.btagDeepFlavB, np.array(0, np.float32), axis=-1)
    fakeable_muon_selection = (
        (conept_mu(presel_muons) >= 10.0)
        & (btag <= btag_wp[year]["medium"])
        & (
            (presel_muons.mvaTTH >= 0.5)
            | ((presel_muons.jetRelIso <= 0.8) & (btag < smooth_bflav(presel_muons, year=year)))
        )
    )
    fakeable_muons = presel_muons[fakeable_muon_selection]
    return fakeable_muons


# electrons
def presel_ele(electrons):
    ele_selection = (
        electrons.mvaFall17V2noIso_WPL
        & (electrons.pt >= 7.0)
        & (abs(electrons.eta) <= 2.5)
        & (abs(electrons.dxy) <= 0.05)
        & (abs(electrons.dz) <= 0.1)
        & (electrons.miniPFRelIso_all <= 0.4)
        & (electrons.sip3d <= 8.0)
        & (electrons.lostHits <= 1)
    )
    good_electrons = electrons[ele_selection]
    return good_electrons


def presel_fat(fatjets, **kwargs):
    year = kwargs.pop("year")
    fatjets_selection = (
        (fatjets.pt > 200)
        & (np.abs(fatjets.eta) < 2.4)
        & (fatjets.subjets.counts == 2)
        & (fatjets.subjets.pt > 20).all()
        & (np.abs(fatjets.subjets.eta) < 2.4).all()
        & (
            (fatjets.subjets.pt > 30) * (fatjets.subjets.btagDeepB > btag_wp[year]["medium"])
        ).any()  # FIXME: missing double-b tagger: (fatjets.btagDeepB > 0.6)
        & ((fatjets.mass > 30) * (fatjets.mass < 210))
        & ((fatjets.tau2 / fatjets.tau1) < 0.75)
    )
    return fatjets[fatjets_selection]


def conept_ele(electrons):
    abspdgid = abs(electrons.pdgId)
    condition = ((abspdgid != 11) & (abspdgid != 13)) | (
        (abspdgid == 11) & (electrons.mvaTTH > 0.30)
    )
    return ak.where(condition, electrons.pt, 0.9 * electrons.pt * (1.0 + electrons.jetRelIso))


def fakeable_ele(presel_electrons, **kwargs):
    year = kwargs.pop("year")
    abseta = abs(presel_electrons.eta + presel_electrons.deltaEtaSC)
    btag = ak.fill_none(
        presel_electrons.matched_jet.btagDeepFlavB, np.array(0, np.float32), axis=-1
    )
    fakeable_ele_selection = (
        (conept_ele(presel_electrons) >= 10.0)
        & (
            ((abseta <= 1.479) & (presel_electrons.sieie <= 0.011))
            | ((abseta > 1.479) & (presel_electrons.sieie <= 0.030))
        )
        & (presel_electrons.hoe <= 0.10)
        & (presel_electrons.eInvMinusPInv >= -0.04)
        & (
            (presel_electrons.mvaTTH >= 0.30)
            | ((presel_electrons.jetRelIso < 0.7) & (presel_electrons.mvaFall17V2noIso_WP90))
        )
        & (presel_electrons.lostHits == 0)
        & (presel_electrons.convVeto)
        & (btag <= btag_wp[year]["tight"])
        & ((presel_electrons.mvaTTH < 0.30) | (btag <= btag_wp[year]["medium"]))
    )
    fakeable_electrons = presel_electrons[fakeable_ele_selection]
    return fakeable_electrons
