# coding: utf-8

import re
from typing import Dict, Tuple

import awkward as ak
import numpy as np
from coffea.nanoevents.methods.base import NanoEventsArray
from coffea.processor.helpers import Weights

from tools.numpy import sigmaclip_mask


class GeneratorHelper:
    def __init__(self, events: NanoEventsArray, weights: Weights):
        self.events = events
        self.weights = weights
        self.ones = np.ones(len(events), dtype=np.float32)

    def PSWeight(self, lfn: str) -> None:
        w = ak.to_numpy(self.events.PSWeight)

        if int(re.search(r"NanoAODv(\d+)", lfn).group(1)) < 7:
            try:
                w = w * ak.to_numpy(self.events.LHEWeight.originalXWGTUP)[:, None]
            except AttributeError:
                return

        if w.shape[1:] == (4,):
            self.weights.add("PSWeight_ISR", self.ones, weightUp=w[..., 2], weightDown=w[..., 0])
            self.weights.add("PSWeight_FSR", self.ones, weightUp=w[..., 3], weightDown=w[..., 1])
        else:
            assert w.shape[1:] == (1,)
            self.weights.add("PSWeight_ISR", self.ones, weightUp=self.ones, weightDown=self.ones)
            self.weights.add("PSWeight_FSR", self.ones, weightUp=self.ones, weightDown=self.ones)

    def ScaleWeight(self) -> None:
        """
        Twiki: https://twiki.cern.ch/twiki/bin/viewauth/CMS/TopSystematics#Factorization_and_renormalizatio

        __doc__:
        ['LHE scale variation weights (w_var / w_nominal)',
        ' [0] is renscfact=0.5d0 facscfact=0.5d0 ',
        ' [1] is renscfact=0.5d0 facscfact=1d0 ',
        ' [2] is renscfact=0.5d0 facscfact=2d0 ',
        ' [3] is renscfact=1d0 facscfact=0.5d0 ',
        ' [4] is renscfact=1d0 facscfact=1d0 ',
        ' [5] is renscfact=1d0 facscfact=2d0 ',
        ' [6] is renscfact=2d0 facscfact=0.5d0 ',
        ' [7] is renscfact=2d0 facscfact=1d0 ',
        ' [8] is renscfact=2d0 facscfact=2d0 ']
        """
        if hasattr(self.events, "LHEScaleWeight"):
            w = ak.to_numpy(self.events.LHEScaleWeight)
            assert np.all(ak.to_numpy(ak.num(self.events.LHEScaleWeight)) == w.shape[1])
        else:
            w = self.ones[:, None][:, :0]  # empty wegihts

        if w.shape[1] == 9:
            # norm by 'nominal' values w[..., 4]
            # clipping between 0..10 was discussed on skype with Florian and Karl                #
            # because of some malicious files eg.:
            # /ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM
            # /ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM
            # ...

            w0 = w[..., 4]
            for suffix, up, down in [
                ("Fact", 5, 3),
                ("Renorm", 7, 1),
                ("Mixed", 8, 0),
            ]:
                self.weights.add(
                    f"ScaleWeight_{suffix}",
                    self.ones,
                    weightUp=np.clip(
                        np.divide(w[..., up], w0, out=np.ones_like(w0), where=w0 != 0),
                        -10.0,
                        10.0,
                    ),
                    weightDown=np.clip(
                        np.divide(w[..., down], w0, out=np.ones_like(w0), where=w0 != 0),
                        -10.0,
                        10.0,
                    ),
                )
        elif w.shape[1] == 8:
            for suffix, up, down in [
                ("Fact", 4, 3),
                ("Renorm", 6, 1),
                ("Mixed", 7, 0),
            ]:
                self.weights.add(
                    f"ScaleWeight_{suffix}",
                    self.ones,
                    weightUp=w[..., up],
                    weightDown=w[..., down],
                )
        else:
            for suffix in ["Fact", "Renorm", "Mixed"]:
                self.weights.add(
                    f"ScaleWeight_{suffix}",
                    self.ones,
                    weightUp=self.ones,
                    weightDown=self.ones,
                )

        # add envelope
        all_shifts = [
            self.weights._modifiers[f"ScaleWeight_{suffix}{shift}"]
            for suffix in ("Fact", "Renorm", "Mixed")
            for shift in ("Up", "Down")
        ]
        self.weights.add(
            "ScaleWeight_Envelope",
            self.ones,
            weightUp=np.maximum.reduce(all_shifts),
            weightDown=np.minimum.reduce(all_shifts),
        )
        # assert that maximum and minimum are correct (Up >= Down):
        assert np.all(
            self.weights._modifiers["ScaleWeight_EnvelopeUp"]
            >= self.weights._modifiers["ScaleWeight_EnvelopeDown"]
        )

    def gen_weight(
        self,
        output: Dict[str, object],
        dataset_key: Tuple[str, str],
        datasets: Dict[str, np.ndarray] = {},
        *,
        gen_weight_thresh: float = 20,
    ) -> None:
        dataset_name, dataset_shift = dataset_key
        datasets = dict(datasets)
        assert dataset_name not in datasets
        datasets[dataset_name] = datasets.pop(None, 1)

        gw = ak.to_numpy(self.events.Generator.weight)[:]
        badgw = sigmaclip_mask(gw, gen_weight_thresh, gen_weight_thresh)
        gw = np.where(badgw, 0.0, gw)

        # See:
        # https://gitlab.cern.ch/cms-hh-bbww/cms-hh-to-bbww/-/blob/master/Legacy/datasets.md#L292-294
        # and
        # https://git.rwth-aachen.de/3pia/cms_analyses/common/-/issues/79
        if dataset_name.startswith(("THQ", "THW")):
            gw *= ak.to_numpy(self.events.LHEReweightingWeight[:, 11])

        self.weights.add("gen_weight", gw)

        variations = {None}
        if dataset_shift == "nominal":
            variations.update(self.weights.variations)

        for name, factor in datasets.items():
            output["count_bad_gen_weights"][name, dataset_shift] = badgw.sum()

            for shift in variations:
                output["sum_gen_weights"][name, shift or dataset_shift] = np.sum(
                    self.weights.weight(modifier=shift) * factor
                )
