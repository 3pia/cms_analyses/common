# -*- coding: utf-8 -*-

from functools import partial
from typing import FrozenSet, Iterable, Optional, Union

import numpy as np
from hist import Hist
from tqdm.auto import tqdm

from utils.bh5 import Histogram as Hist5

# Available top systematics
TopSystematics: FrozenSet = frozenset(
    {
        "UnderlyingEvent",
        "TopMass",
        "GluonMove",
        "QCDbased",
        "erdON",
        "MEPSMatchingScale",
    }
)


def topvariation(
    hist: Hist,
    src: str,
    dst: Optional[str] = None,
    symmetrize: bool = True,
    scale: float = 1.0,
) -> Hist:
    """
    Returns:
    Hist including `dst{Up,down}`  variations from `src`

    If `src` is directly available, it is intrepreted as `dstUp` and `dstDown` is set to
    `nominal` or the reflection (about `nominal`) of `src` depending on `symmetrize`
    , otherise `src{Up,Down}` are used for `dst{Up,down}`.

    The output `dst{Up,down}` are `scale` (w.r.t nominal), i.e. negative vales flip and 0 disables
    the shift.
    """
    if dst is None:
        dst = src

    hnew = Hist5.regrow(
        hist,
        {"systematic": [f"{dst}Up", f"{dst}Down"]},
        copy=False,
    )
    newUpIdx = hnew.axes["systematic"].index(f"{dst}Up")
    newDownIdx = hnew.axes["systematic"].index(f"{dst}Down")

    hv = hist.view(True)
    ttIdx = hist.axes["process"].index("tt")
    syst = hist.axes["systematic"]
    assert f"{dst}Up" not in syst
    assert f"{dst}Down" not in syst

    nomIdx = syst.index("nominal")
    if src in syst:
        upIdx = downIdx = syst.index(src)
        upScale = scale
        downScale = -scale if symmetrize else 0
    else:
        upIdx = syst.index(f"{src}Up")
        downIdx = syst.index(f"{src}Down")
        upScale = downScale = scale
    if len(syst) in {upIdx, downIdx}:
        return hnew

    tt = hv[ttIdx, ...]
    nominal = tt[:, nomIdx, :]
    up = tt[:, upIdx, :]
    down = tt[:, downIdx, :]

    # fmt: off
    # nominal
    hnew.view(True)[ttIdx, :, newUpIdx, :]["value"] = nominal["value"] + upScale * (up["value"] - nominal["value"])
    hnew.view(True)[ttIdx, :, newDownIdx, :]["value"] = nominal["value"] + downScale * (down["value"] - nominal["value"])

    # variance propagation
    hnew.view(True)[ttIdx, :, newUpIdx, :]["variance"] = nominal["variance"] + upScale**2 * (up["variance"] + nominal["variance"])
    hnew.view(True)[ttIdx, :, newDownIdx, :]["variance"] = nominal["variance"] + downScale**2 * (down["variance"] + nominal["variance"])
    # fmt: on
    return hnew


def topvariations(
    hist: Hist, progress: Optional[dict] = dict(desc="topvariations"), symmetrize: bool = True
) -> Hist:
    """
    Returns a new histogram containing only the top variation systematic uncertainties

    Can calculate the following variations:

    UnderlyingEvent:
        Directly inferred from up/down variations of the datasets

    TopMass (3 GeV shift):
        Up = 175.5 GeV
        Down = 169.5 GeV

    ColorReconnection:
        for cr in GluonMove, QCDbased, erdON:
            Up = cr
            Down = (nominal - (up - nomina)) if symmetrize else nominal

    MEPSMatchingScale:
        Directly inferred from up/down variations of the datasets
    """

    variations = [
        # dataset name(s), systematic name, scale factor
        dict(src="TuneCP5", dst="UnderlyingEvent"),
        dict(src="mtop", dst="TopMass", scale=1.0 / 3.0),  # 3 GeV shift -> 1 GeV shift
        dict(src="GluonMove"),
        dict(src="QCDbased"),
        dict(src="erdON"),
        dict(src="HDamp", dst="MEPSMatchingScale"),
    ]
    systematics = [(var.get("dst", var["src"]) + d) for var in variations for d in ["Up", "Down"]]

    # clone histogram, create new shifts
    out = Hist5.regrow(
        hist,
        {"systematic": systematics},
        copy=False,
    )

    # get view of histograms
    hv = hist.view(True)
    ov = out.view(True)

    # set new shifts to nominal
    ov[:, :, :, :] = hv[:, :, [hist.axes[2].index("nominal")], :]

    # set new shifts for ttbar to 0
    ov[[out.axes[0].index("tt")], :, :, :] *= 0

    # set new shifts for ttbar to explicit value
    for kwargs in (tq := tqdm(variations, desc="topvariation")):
        tq.set_postfix(**kwargs)
        out += topvariation(hist=hist, symmetrize=symmetrize, **kwargs)
    return out
