import numpy as np
import numba


@numba.njit
def tukey(a: np.ndarray):
    assert a.shape == (3,)
    return np.median(np.array([a[0], a[1], 3 * a[1] - 2 * a[2]]))


@numba.njit
def median(
    data: np.ndarray, window_size: int, use_tukey: bool = True, use_smaller: bool = True
) -> np.ndarray:
    assert window_size > 0
    assert window_size == int(window_size)
    assert window_size & 1  # must be odd
    assert data.ndim == 1

    offset = (window_size - 1) // 2

    out = data.copy()
    for i in range(offset, len(data) - offset):
        out[i] = np.median(data[i - offset : i + offset + 1])

    if window_size == 3 and use_tukey and len(data) >= 3:
        # important: already uses partial result
        out[0] = tukey(out[:3])
        out[-1] = tukey(out[-3:][::-1])
    elif window_size > 3 and use_smaller:
        for i in range(1, offset):
            pos = offset - i
            num = window_size - 2 * i
            if num <= len(data):
                out[pos] = np.median(data[:num])
                out[-pos - 1] = np.median(data[-num:])

    return out


@numba.njit
def qipol(data: np.ndarray) -> np.ndarray:
    assert data.ndim == 1
    assert data.shape[0] > 0

    out = data.copy()

    for i in range(2, len(data) - 2):
        c = data[i]
        if c != data[i - 1] or c != data[i + 1]:
            continue
        l = data[i - 2] - c
        r = data[i + 2] - c
        if l * r <= 0:
            continue
        k = -1 if abs(r) > abs(l) else 1
        out[i] = -0.5 * data[i - 2 * k] + c / 0.75 + data[i + 2 * k] / 6.0
        out[i + k] = 0.5 * (data[i + 2 * k] - data[i - 2 * k]) + c

    return out


@numba.njit
def hanning(data: np.ndarray, window_size: int) -> np.ndarray:
    assert window_size > 0
    assert window_size == int(window_size)
    assert window_size & 1
    assert data.ndim == 1
    assert data.shape[0] >= window_size

    offset = (window_size - 1) // 2

    kernel = np.hanning(window_size + 2)[1:-1]
    kernel /= kernel.sum()

    out = np.convolve(data, kernel)

    # fix boundaries
    if offset:
        out = out[offset:-offset]
        out[:offset] = data[:offset]
        out[-offset:] = data[-offset:]

    return out


@numba.njit
def _root_step(data: np.ndarray) -> np.ndarray:
    """recipe: 353'QH"""
    # see https://www.desy.de/~sschmitt/blobel/blobel_smooth.pdf
    data = median(data, 3)
    data = median(data, 5)
    data = median(data, 3, use_tukey=False)
    data = qipol(data)
    data = hanning(data, 3)
    return data


@numba.njit
def root_eqiv(data: np.ndarray, rounds: int) -> np.ndarray:
    """logic of TH1::SmoothArray (used by TH1::Smooth), equivalent reimplementation"""
    assert rounds > 0
    assert rounds == int(rounds)

    assert data.ndim == 1
    assert data.shape[0] >= 3

    for r in range(rounds):

        part = _root_step(data)
        resi = _root_step(data - part)

        if data.min() < 0:
            data = part + resi
        else:
            data = np.maximum(part + resi, 0)

    return data


@numba.njit
def root_full(data: np.ndarray, rounds: int) -> np.ndarray:
    """logic of TH1::SmoothArray (used by TH1::Smooth), plaint translation"""
    assert rounds > 0
    assert rounds == int(rounds)

    assert data.ndim == 1
    assert data.shape[0] >= 3

    return _TH1_SmoothArray(len(data), data.copy(), rounds)


# https://github.com/root-project/root/blob/8cc176c9ee7dde3b72501a2b657534bc6bd10418/hist/hist/src/TH1.cxx#L6751-L6859
@numba.njit
def _TH1_SmoothArray(nn: int, xx: np.ndarray, ntimes: int):
    """plain trainslation of the original TH1::SmoothArray"""
    assert xx.shape == (nn,)
    assert nn >= 3

    ii: int
    hh = np.zeros(6)

    for _pass in range(0, ntimes):
        # first copy original data into temp array
        zz = xx.copy()

        for noent in range(0, 2):  # run algorithm two times

            #  do 353 i.e. running median 3, 5, and 3 in a single loop
            for kk in range(0, 3):
                yy = zz.copy()

                medianType = 3 if (kk != 1) else 5
                ifirst = 1 if (kk != 1) else 2
                ilast = nn - 1 if (kk != 1) else nn - 2
                # nn2 = nn - ik - 1;
                # do all elements beside the first and last point for median 3
                #  and first two and last 2 for median 5
                for ii in range(ifirst, ilast):
                    assert ii - ifirst >= 0
                    for jj in range(0, medianType):
                        hh[jj] = yy[ii - ifirst + jj]
                    zz[ii] = np.median(hh[:medianType])

                if kk == 0:  # first median 3
                    # first point
                    hh[0] = zz[1]
                    hh[1] = zz[0]
                    hh[2] = 3 * zz[1] - 2 * zz[2]
                    zz[0] = np.median(hh[:3])
                    # last point
                    hh[0] = zz[nn - 2]
                    hh[1] = zz[nn - 1]
                    hh[2] = 3 * zz[nn - 2] - 2 * zz[nn - 3]
                    zz[nn - 1] = np.median(hh[:3])

                if kk == 1:  #  median 5
                    for ii in range(0, 3):
                        hh[ii] = yy[ii]

                    zz[1] = np.median(hh[:3])
                    # last two points
                    for ii in range(0, 3):
                        hh[ii] = yy[nn - 3 + ii]

                    zz[nn - 2] = np.median(hh[:3])

            yy = zz.copy()

            # quadratic interpolation for flat segments
            for ii in range(2, nn - 2):

                if zz[ii - 1] != zz[ii]:
                    continue
                if zz[ii] != zz[ii + 1]:
                    continue
                hh[0] = zz[ii - 2] - zz[ii]
                hh[1] = zz[ii + 2] - zz[ii]
                if hh[0] * hh[1] <= 0:
                    continue
                jk = 1
                if abs(hh[1]) > abs(hh[0]):
                    jk = -1
                yy[ii] = -0.5 * zz[ii - 2 * jk] + zz[ii] / 0.75 + zz[ii + 2 * jk] / 6.0
                yy[ii + jk] = 0.5 * (zz[ii + 2 * jk] - zz[ii - 2 * jk]) + zz[ii]

            # running means
            # std::copy(zz.begin(), zz.end(), yy.begin());
            for ii in range(1, nn - 1):
                zz[ii] = 0.25 * yy[ii - 1] + 0.5 * yy[ii] + 0.25 * yy[ii + 1]
            zz[0] = yy[0]
            zz[nn - 1] = yy[nn - 1]

            if noent == 0:

                # save computed values
                rr = zz.copy()

                # COMPUTE  residuals
                for ii in range(0, nn):
                    zz[ii] = xx[ii] - zz[ii]

        # end loop on noent
        xmin = xx[:nn].min()
        for ii in range(0, nn):
            if xmin < 0:
                xx[ii] = rr[ii] + zz[ii]
            # make smoothing defined positive - not better using 0 ?
            else:
                xx[ii] = max((rr[ii] + zz[ii]), 0.0)

    return xx


if __name__ == "__main__":
    n = 1
    a = np.exp(np.random.normal(size=10))

    print(n, a)
    try:
        import ROOT
    except ImportError:
        pass
    else:

        def root_orig(data: np.ndarray, rounds: int) -> np.ndarray:
            assert data.ndim == 1
            out = np.array(data, dtype=np.float64, copy=True)
            ROOT.TH1.SmoothArray(len(out), out, rounds)
            return out.astype(data.dtype, copy=False)

        # a = np.array([3, 1, 0, 0])

        def test():
            print(a)
            print(root_eqiv(a, n))
            print(root_full(a, n))
            print(root_orig(a, n))

        test()
