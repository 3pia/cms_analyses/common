from functools import partial, reduce
from operator import add, mul
from typing import Sequence

import numpy as np
from scipy.stats import rv_continuous, beta as beta_dist
from scipy.special import loggamma, poch

import mpmath

loggamma = np.vectorize(mpmath.loggamma, otypes=[float])
hyp2f1 = np.vectorize(mpmath.hyp2f1, otypes=[float])
hyp3f2 = np.vectorize(mpmath.hyp3f2, otypes=[float])

# simpler pochhammer implementations for scalar (smallish) positive integer *m*
def _poch(x, m: int):
    assert 0 < m
    assert m == int(m)
    ret = np.copy(x)
    for i in range(1, m):
        ret *= x + i
    return ret


# implementation of the beta-quotient distribution (quotient of two beta distributions)#
#   useful for efficency ratios, i.e. scale factors
class betaq_gen(rv_continuous):
    def _rvs(self, alpha1, beta1, alpha2, beta2, size=None, random_state=None):
        d1, d2 = beta_dist(alpha1, beta1), beta_dist(alpha2, beta2)
        return d1.rvs(size, random_state) / d2.rvs(size, random_state)

    def _pdf(self, x, alpha1, beta1, alpha2, beta2):
        return self._xdf(x, alpha1, beta1, alpha2, beta2, cum=False)

    def _cdf(self, x, alpha1, beta1, alpha2, beta2):
        return self._xdf(x, alpha1, beta1, alpha2, beta2, cum=True)

    # all following methods are taken from: https://www.someweekendreading.blog/beta-ratios/
    def _xdf(self, x, alpha1, beta1, alpha2, beta2, cum=False):
        hi = 1 < x
        alphas = alpha1 + alpha2
        xi = np.where(hi, 1 / x, x)
        alpha21 = np.where(hi, alpha2, alpha1)
        beta12 = np.where(hi, beta1, beta2)
        beta21 = np.where(hi, beta2, beta1)

        ls = 0
        # beta(alphas, beta12) / (beta(alpha1, beta1) * beta(alpha2, beta2))
        ls += loggamma(alphas)
        ls += loggamma(beta12)
        ls += loggamma(beta1 + alpha1)
        ls += loggamma(beta2 + alpha2)
        ls -= loggamma(alpha1)
        ls -= loggamma(alpha2)
        ls -= loggamma(beta1)
        ls -= loggamma(beta2)
        ls -= loggamma(beta12 + alphas)

        # fraction
        ls += alpha21 * np.log(xi)
        ls -= np.log(alpha21 if cum else x)

        # hyp*
        v = np.exp(ls)
        if cum:
            v *= hyp3f2(alpha21, alphas, 1 - beta21, 1 + alpha21, alphas + beta12, xi)
        else:
            v *= hyp2f1(alphas, 1 - beta21, alphas + beta12, xi)

        if cum:
            return np.clip(np.where(hi, 1 - v, v), 0, 1)
        else:
            return np.maximum(v, 0.0)

    def _munp(self, n, alpha1, beta1, alpha2, beta2):
        p = _poch if n == int(n) and 0 < n < 100 else poch
        return p(alpha1, n) * p(alpha2 + beta2 - n, n) / (p(alpha1 + beta1, n) * p(alpha2 - n, n))


betaq = betaq_gen(
    name="betaq", a=0, longname="beta quotient distribution", shapes="alpha1, beta1, alpha2, beta2"
)

