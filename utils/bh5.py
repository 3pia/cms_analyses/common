# -*- coding: utf-8 -*-

from io import BytesIO
from tempfile import NamedTemporaryFile
from weakref import finalize
import numpy as np
import boost_histogram as bh
from boost_histogram._internal.axis import BaseCategory
from hist import Hist, axis as Haxis
import h5py
from itertools import product, starmap
from utils.util import view_sdtype_flat
from hdf5plugin import Blosc


def _mk_sdtype(*names, dtype=np.float64):
    return np.dtype([(n, dtype) for n in names])


def regrow_axis(values, ax0, *axN):
    assert ax0.traits.growth is True
    ret = type(ax0)(values, growth=True)
    ret._ax.metadata = ax0._ax.metadata
    return ret


class Histogram:
    _axesTuple = Haxis.NamedAxesTuple
    _storageCls2dtype = {
        bh.storage.Double: np.float64,
        bh.storage.Int64: np.int64,
        bh.storage.AtomicInt64: np.int64,
        bh.storage.Weight: _mk_sdtype(
            "value",
            "variance",
        ),
        bh.storage.Mean: _mk_sdtype(
            "count",
            "value",
            "variance",
        ),
        bh.storage.WeightedMean: _mk_sdtype(
            "sum_of_weights",
            "sum_of_weights_squared",
            "value",
            "_sum_of_weighted_deltas_squared",
        ),
    }

    # tested with: 146 datasets, 420 categories, 113 shifts, 402 (regular) bins
    # reported values: time to write/read in s, size in GB
    # dict(compression="lzf") # 35/63, 3.327
    # Blosc(cname="blosclz", clevel=1) # 33/21, 13.594
    # Blosc(cname="blosclz", clevel=9) # 45/25, 3.464
    # Blosc(cname="blosclz", clevel=9, shuffle=0) # 19/11, 4.156
    # Blosc(cname="blosclz", clevel=9, shuffle=0) + shuffle=True # 59/63, 3.036
    # Blosc(cname="lz4", clevel=1) # 22/22, 3.563
    # Blosc(cname="lz4", clevel=9) # 27/23, 3.025
    # Blosc(cname="lz4", clevel=9, shuffle=0) # 17/13, 2.541
    # Blosc(cname="lz4", clevel=9, shuffle=2) # 51/44, 4.802
    # Blosc(cname="lz4hc", clevel=1) # 136/24, 2.836
    # Blosc(cname="lz4hc", clevel=9) # 452/21, 2.586
    # Blosc(cname="snappy", clevel=1) # 23/30, 4.692
    # Blosc(cname="snappy", clevel=9) # 24/31, 4.511
    # Blosc(cname="snappy", clevel=9, shuffle=0) # 10/20, 4.164
    # Blosc(cname="zlib", clevel=1) # 131/68, 2.820
    # Blosc(cname="zlib", clevel=9) # 2677/80 2.383
    # Blosc(cname="zstd", clevel=1) # 53/21, 2.949
    # Blosc(cname="zstd", clevel=6) # 174/26, 2.204
    # Blosc(cname="zstd", clevel=9) #
    # LZ4() # 7/13, 41.508? no compression?!
    # FciDecomp() # 7/13, 41.508? no compression?!
    # BitShuffle() # 45/33, 6.526
    default_ds_opts = Blosc(cname="lz4", clevel=9, shuffle=0)
    # default_ds_opts = dict(compression=32001, compression_opts=(0, 0, 0, 0, 9, 0, 1))

    def __init__(
        self, *axes, storage=bh.storage.Double(), metadata=None, group=None, ds_opts=None, f_opts={}
    ):
        assert all(isinstance(ax, bh.axis.Axis) for ax in axes)
        assert type(storage) in self._storageCls2dtype

        self.axes = self._axesTuple(axes)
        self.metadata = metadata
        self._storage_type = type(storage)

        # prepare dataset creation
        if group is None:
            group = BytesIO()
        elif isinstance(group, bytes):
            group = BytesIO(group)
        if group is True:
            self._src = NamedTemporaryFile(prefix="bh5_", suffix=".h5", buffering=0, mode="rb")
            finalize(self, self._src.close)
            self._h5f = group = h5py.File(self._src.name, "r+", **f_opts)
        elif isinstance(group, (BytesIO, str)):
            self._src = group
            self._h5f = group = h5py.File(self._src, "r+", **f_opts)
        if ds_opts is None:
            ds_opts = self.default_ds_opts

        self.group = group
        self.dtype = self._storageCls2dtype[type(storage)]

        # create dataset
        self._h5d = self.group.require_dataset(
            name="data",
            shape=self.axes.extent,
            dtype=self.dtype,
            exact=True,
            maxshape=tuple(None if ax.traits.growth else ax.extent for ax in self.axes),
            chunks=tuple(map(_get_chunks, self.axes)),
            fillvalue=np.zeros(1, dtype=self.dtype)[0],
            fletcher32=True,
            **ds_opts,
        )

    @property
    def raw(self) -> bytes:
        self.flush()
        if isinstance(self._src, BytesIO):
            return self._src.getvalue()
        elif isinstance(self._src, str):
            with open(self._src, mode="rb") as f:
                return f.read()
        else:
            self._src.seek(0)
            return self._src.read()

    def view(self, flow=False):
        if flow or not self.has_flow:
            return self._h5d
        else:
            raise NotImplementedError("not supported")

    def __getitem__(self, key):
        raise NotImplementedError()
        indices, fields = self._expand_key(key)
        if fields:
            raise NotImplementedError("getting fields is not supported")
        print("qq", indices)
        regrow = {}
        for i, (ax, idx) in enumerate(zip(self.axes, indices)):
            if idx == slice(None):
                continue
            if isinstance(ax, BaseCategory):
                if isinstance(idx, slice):
                    raise NotImplementedError(f"can't slice categorical axis {ax}")
                elif isinstance(idx, (tuple, list)):
                    regrow[i] = [ax[j] for j in idx]
                else:
                    raise NotImplementedError(f"selection type on {ax} not supported")
            else:
                raise NotImplementedError(f"selection on {ax} not supported")
        return self.regrow(regrow, cls=Hist)

    def __setitem__(self, key, value):
        assert isinstance(value, np.ndarray)
        indices, fields = self._expand_key(key)
        if fields:
            raise NotImplementedError("setting fields is not supported")
        self._h5d[tuple(indices)] = value

    def grow(self, categories):
        self._adopt(Histogram._mod_axes(self, categories))

    def __iadd__(self, other):
        self._adopt(other.axes)
        return self._hiadd(other)

    def copy(self):
        return self._copy(None, empty=False)

    def reset(self):
        return self._copy(None, empty=True)

    def fill(self, *args, **kwargs):
        self += self._copy(cls=Hist, empty="ungrow").fill(*args, **kwargs)

    @property
    def ndim(self):
        return len(self.axes)

    @property
    def size(self):
        return self._h5d.size

    @property
    def shape(self):
        return self.axes.size

    # extra functions
    @property
    def has_flow(self):
        return self.axes.size != self.axes.extent

    @classmethod
    def from_hist(cls, hist, **kwargs):
        ret = cls(*hist.axes, storage=hist._storage_type(), metadata=hist.metadata, **kwargs)
        ret[:] = hist.view(flow=True)
        return ret

    def to_hist(self, cls=Hist):
        assert issubclass(cls, bh.Histogram)
        return self._copy(cls)

    def flush(self):
        self._h5d.file.flush()

    def regrow(self, categories=(), cls=None, copy=True):
        if categories is min:
            categories = {
                i: []
                for i, ax in enumerate(self.axes)
                if isinstance(ax, BaseCategory) and ax.traits.growth
            }
        axes = Histogram._mod_axes(self, categories)
        if cls is None:
            cls = type(self)

        ret = cls(*axes, storage=self._storage_type(), metadata=self.metadata)
        if copy:
            Histogram._hiadd(ret, self)

        return ret

    # helper functions
    def _mod_axes(self, categories):
        if not isinstance(categories, dict):
            categories = dict(enumerate(categories))

        axes = list(self.axes)
        for key, cats in categories.items():
            if cats is None:
                continue
            idx = Histogram._expand_k(self, key)
            axes[idx] = regrow_axis(cats, axes[idx])

        return axes

    def _adopt(self, axes):
        assert len(self.axes) == len(axes)
        self.axes = self._axesTuple(starmap(_merge_axis, zip(self.axes, axes)))
        if self._h5d.shape != self.axes.extent:
            self._h5d.resize(self.axes.extent)

    def _hiadd(self, other, set=False):
        dst = self.view(flow=True)
        src = other.view(flow=True)
        for il, ir in starmap(zip, product(*starmap(_merge_slices, zip(self.axes, other.axes)))):
            dst[il] = src[ir] if set else _siadd(dst[il], src[ir])
        return self

    def _copy(self, cls, empty=False):
        kwargs = dict(storage=self._storage_type(), metadata=self.metadata)
        if cls is None:
            cls = type(self)
            if not empty and self._inline:
                kwargs["group"] = self.raw
        if empty == "ungrow":
            axes = tuple(
                regrow_axis([], ax) if isinstance(ax, BaseCategory) and ax.traits.growth else ax
                for ax in self.axes
            )
        else:
            axes = self.axes
        ret = cls(*axes, **kwargs)
        if not empty and "group" not in kwargs:
            ret.view(flow=True)[...] = self.view(flow=True)[...]
        return ret

    def __reduce__(self):
        if not self._inline:
            raise NotImplementedError(f"can't pickle {type(self)!r} with external group")
            # self = self.copy()  # workaround: on the fly copy
        return (
            self._make,
            (
                tuple(self.axes),
                dict(
                    storage=self._storage_type(),
                    metadata=self.metadata,
                    group=self.raw,
                ),
            ),
        )

    @classmethod
    def _make(cls, axes, kwargs):
        return cls(*axes, **kwargs)

    @property
    def _inline(self):
        return hasattr(self, "_src")

    def _expand_key(self, key):
        # dictify
        if not isinstance(key, dict):
            if not isinstance(key, tuple):
                key = (key,)
            assert key.count(...) <= 1
            if ... in key:
                rev = key[::-1]
                rev = key[: rev.index(...)]
                key = dict(enumerate(key[: key.index(...)]))
                key.update((-i, v) for i, v in enumerate(rev, start=1))
            else:
                key = dict(enumerate(key))

        # build indices
        indices = [slice(None)] * self.ndim
        for k, v in key.items():
            indices[Histogram._expand_k(self, k)] = v

        for i, idx in enumerate(indices):
            if callable(idx):
                indices[i] = idx(self.axes[i])
            elif isinstance(idx, str):
                indices[i] = self.axes[i].index(idx)

        return indices, []

    def _expand_k(self, k):
        if isinstance(k, bh.axis.Axis):
            if k in self.axes:
                return tuple(self.axes).index(k)
            else:
                raise ValueError(f"axis {k!r} foreign to {self!r}")
        if isinstance(k, str):
            if k in self.axes.name:
                return self.axes.name.index(k)
            else:
                raise ValueError(f"unknown key {k!r} among {self.axes!r}")
        elif isinstance(k, int):
            return k
        else:
            raise ValueError(f"key {k!r} not understood")


def _siadd(a, b):
    assert a.shape == b.shape
    assert a.dtype == b.dtype
    if a.size:
        av = view_sdtype_flat(np.asarray(a))
        bv = view_sdtype_flat(np.asarray(b))
        np.add(av, bv, out=av)
    return a


def _get_chunks(ax):
    if ax.traits.growth:
        ret = getattr(ax, "chunks", None)
        if ret is None:
            ret = 1 if isinstance(ax, BaseCategory) else 100
        elif ret is max:
            ret = ax.extent
        return ret
    else:
        return ax.extent


def _merge_axis(ax0, ax1):
    if ax0 == ax1:
        return ax0
    if not any(isinstance(a, type(b)) for a, b in [(ax0, ax1), (ax1, ax0)]):
        raise ValueError(f"incompatible axes types for merge: {type(ax0)} & {type(ax1)}")
    if not ax0.traits.growth:
        raise ValueError("original axis can't grow")
    if isinstance(ax0, BaseCategory):
        return regrow_axis(list(ax0) + [c for c in ax1 if c not in ax0], ax0, ax1)
    else:
        raise NotImplementedError(f"axis merge for {type(ax0)} not supported")


def _merge_slices(ax0, ax1):
    if ax0 == ax1:
        return [(slice(None), slice(None))]
    if isinstance(ax0, BaseCategory):
        if len(ax1) == 1:
            idx = ax0.index(ax1[0])
            return [(idx, 0)] if idx < len(ax0) else []
        keys = sorted(set(ax0) & set(ax1), key=ax0.index)
        if not keys:
            return []
        return [
            tuple(run[0])
            if len(run) == 1
            else tuple(slice(s, e + 1) for s, e in zip(run[0], run[-1]))
            for run in _runs(np.c_[ax0.index(keys), ax1.index(keys)])
        ]
    else:
        raise NotImplementedError(f"axis merge for {type(ax0)} not supported")


def _runs(iterable, cmp=lambda a, b: np.all(a + 1 == b)):
    ret = []
    for item in iterable:
        if ret and cmp(ret[-1][-1], item):
            ret[-1].append(item)
        else:
            ret.append([item])
    return ret


def rebin(hist, axis, idx=None, keep_metadata=True):
    if idx is None:
        idx = hist.axes.name.index(axis.name)
    orig = hist.axes[idx]
    idx = tuple(hist.axes).index(orig)

    if isinstance(axis, (range, slice)):
        n = (axis.stop - axis.start) // axis.step
        axis = Haxis.Regular(n, axis.start, axis.stop, name=orig.name)
    elif isinstance(axis, (tuple, list)) or (isinstance(axis, np.ndarray) and axis.ndim == 1):
        axis = Haxis.Variable(axis, name=orig.name)
    if keep_metadata:
        axis._ax.metadata = orig._ax.metadata

    assert not isinstance(orig, BaseCategory), "can't replace a category axis"
    assert not isinstance(axis, BaseCategory), "can't insert a category axis"
    assert set(axis.edges) <= set(orig.edges), "can't insert new edges"

    # slicing helper
    def pos(*args):
        assert 0 < len(args) <= 3
        if len(args) > 1:
            args = (slice(*args),)
        return (slice(None),) * idx + args

    # data getting
    view = hist.view(True)
    offset = int(orig.traits.underflow)

    def get(*args):
        assert 0 < len(args) < 3
        return view[pos(*(a if a is None else offset + orig.index(a) for a in args))].sum(axis=idx)

    # create new histogram
    axes = list(hist.axes)
    axes[idx] = axis
    hnew = type(hist)(*axes, storage=hist._storage_type(), metadata=hist.metadata)

    # regular bins
    vnew = hnew.view()
    for i, lr in enumerate(axis):
        vnew[pos(i)] = get(*lr)

    # under-/overflow bins
    vnew = hnew.view(True)
    if axis.traits.underflow:
        vnew[pos(0)] = get(None, axis.edges[0])
    if axis.traits.overflow:
        vnew[pos(-1)] = get(axis.edges[-1], None)

    return hnew
