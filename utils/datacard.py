from __future__ import annotations

import fnmatch
import os
import re
from abc import abstractmethod
from collections.abc import MutableMapping
from enum import IntEnum
from functools import cached_property
from numbers import Number
from operator import itemgetter
from typing import Dict, List, Optional, Tuple, Union

import hist
import law
import numpy as np
import uproot3
from rich.console import Console
from tqdm.auto import tqdm

from tools.evil import pin
from utils.aci import Analysis, Campaign, Process
from utils.deprecate import deprecated_import
from utils.export import export1d_hist
from utils.hist import reax
from utils.processes import ProcessesMixin
from utils.shape import Shape
from utils.uproot import bulk_write


def auto_repr(cls):
    def repr(self):
        return f"{self.__class__.__name__}({', '.join([f'{k}={v}' for k, v in self.__dict__.items() if not k.startswith('_')])})"

    cls.__repr__ = repr
    return cls


class DIR(IntEnum):
    DOWN = 0
    UP = 1


@auto_repr
class Datacard(ProcessesMixin):
    """Example usage:

    class StatModel(Datacard):
        # add custom lines to the end of the datacard
        @property
        def custom_lines(self):
            return ["* autoMCStats 10 0 1"]

        def build_systematics(self):

            # add many systs with individual strengths
            self.add_systematics(
                names={
                    "CMS_lumi_13TeV_2017": 1.02,
                    # correlated lumi
                    "CMS_lumi_13TeV_XYFact": 1.008,
                    "CMS_lumi_13TeV_LScale": 1.003,
                    "CMS_lumi_13TeV_BBDefl": 1.004,
                    "CMS_lumi_13TeV_DynBeta": 1.005,
                    "CMS_lumi_13TeV_CurrCalib": 1.003,
                    "CMS_lumi_13TeV_Ghosts": 1.001,
                },
                type="lnN",
                processes=self.processes,
            )

            # or like this
            self.add_systematics(
                names=["lnNSys4", "lnNSys5"],
                type="lnN",
                strengths=[1.1, 1.2],
                processes=self.background_processes,
            )

            or many with the same strength
            self.add_systematics(
                names=["lnNSys6", "lnNSys7"],
                type="lnN",
                strengths=1.1,
                processes=self.background_processes,
            )

            or a single nuisance
            self.add_systematics(
                names="lnNSys8",
                type="lnN",
                strengths=1.1,
                processes=["DY"],
            )

            # also wildcards
            self.add_systematics(
                names="CMS_JES*",
                type="shape",
                strength=1.0,
                processes=self.processes,
            )

    # can also define 2 Statmodel
    class StatModelRateParam(StatModel):
        # add custom lines to the end of the datacard
        @property
        def custom_lines(self):
            return ["* autoMCStats 10 0 1", "CMS_rateParam_TT rateParam * tt 1.0 [0.0,3.0]"]

    # add signals to both
    inject(processes=[("ggHH_kl_0_kt_1_2B2VTo2L2Nu", -2), ("ggHH_kl_1_kt_1_2B2VTo2L2Nu", -1), ("ggHH_kl_5_kt_1_2B2VTo2L2Nu", 0)], where=[StatModel, StatModelRateParam])

    # add backgrounds to both
    inject(processes=[("st", 1), ("tt", 2), ("vv", 3), ("vvv", 4)], where=[StatModel, StatModelRateParam])

    Run by hand:

    card = StatModel(
        analysis="bbww_dl",
        category="mumu_diHiggs",
        variable="dnn_score_max",
        hists_path="/net/scratch/cms/dihiggs/store/bbww_dl/Run2_pp_13TeV_2017/Rebin/dev1/rebinned_hists.root",
        analysis_inst=...,
        campaign_inst=...,
    )
    card.dump("./")
    """

    _processes = set()
    sep = os.linesep
    dashes = 80 * "-"
    min_yield = 0.05

    def __init__(self, hists: hist.Hist, task: law.Task) -> None:
        if not self._processes:
            raise Exception("Processes have to be defined!")

        analysis = task.analysis_choice
        variable = task.var
        category = task.category
        analysis_inst = task.analysis_inst
        campaign_inst = task.campaign_inst
        verbose = task.verbose

        # e.g.: category = "ee_2b"
        channel = category.split("_")[0]
        _nuisances_names = []
        _valid_processes = []
        _nuisances_renaming = RDict()
        _newhists = {}
        console = Console()
        year = campaign_inst.aux["year"]
        pin(locals())

    @classmethod
    def requires(cls, task):
        return task.base_requires()

    @classmethod
    def trim_binning(cls, year, variable, category, bins, minl, task) -> List[float]:
        """
        no trimming implementation, returning original binning
        """
        return bins

    @classmethod
    def calculate_binnings(cls, variable, h, task) -> Dict[str, List[float]]:
        """
        Calculate binning for each variable and category:
        e.g.:
            return {
                "cat1": [0.0, 0.25, 0.5, 0,75, 1.0]
                # these two need utils.util.AsteriskDict
                "*_cat?2": [0.0, 0.3, 0.7, 1.0],
                "*": [0.0, 1.0]
            }
        """
        binnings = {}
        for c in tqdm(
            h.axes["category"],
            desc=f"calculate binning - {variable.name}",
            unit="category",
            leave=False,
        ):
            num, start, stop = variable.binning
            binning = np.linspace(start, stop, num=num+1)
            binnings[c] = binning.tolist()

        return binnings

    @classmethod
    def rebin(cls, variable, h, binnings, task) -> Dict[str, hist.Hist]:
        hists = {}
        for c in tqdm(
            h.axes["category"],
            desc=f"rebin - {variable.name}",
            unit="category",
            leave=False,
        ):
            hnew = reax(
                h[:, c, ...],
                -1,
                hist.axis.Variable(
                    binnings[c],
                    name=variable.name,
                    label=variable.x_title,
                )
            )
            hists[c] = hnew
        return hists

    def rate(self, process, systematic="nominal") -> float:
        h = self.hists[
            process,
            systematic,
            :,
        ]
        arr = h.view(flow=False)["value"]
        # we remove negative bins later anyhow
        return np.sum(arr[arr >= 0.0])

    @property
    def observation(self) -> int:
        return -1

    @property
    def bin(self) -> str:
        return f"{self.analysis}_{self.year}_{self.category}"

    @property
    def available_systematics(self) -> List[str]:
        return [*self.hists.axes["systematic"]]

    @property
    def available_processes(self) -> List[str]:
        return [*self.hists.axes["process"]]

    @cached_property
    def spaces(self) -> int:
        return (
            max(
                *[
                    len(self.rprocesses[p])
                    for p in self.processes
                    + [
                        "data_obs",
                    ]
                ],
                len(self.bin),
                *[
                    len(" ".join([self.rnuisances[name], type]))
                    for name, type in self._nuisances_names
                ],
            )
            + 5
        )

    @property
    def nominal_pattern(self) -> str:
        return Shape.ch_template_nom.format(variable=str(self.variable)).replace("$BIN", self.bin)

    @property
    def sys_pattern(self) -> str:
        return Shape.ch_template_sys.format(variable=str(self.variable)).replace("$BIN", self.bin)

    @property
    def lumi(self) -> Optional[float]:
        # Lumi to scale the datacard to in pb-1
        return None

    @property
    def custom_lines(self) -> List[str]:
        return []

    def decorrelate(
        self,
        systematic: str,
        processes: Union[List[str], Dict[str, str]],
        drop: bool = True,
    ) -> List[str]:
        """
        Decorrelate the nuisance parameters.

        Parameters
        ----------
        systematic : str
            The systematic to decorrelate by given processes.

        processes : Union[List[str], Dict[str, str]]
            Processes used for decorellation. If a list is given, processes are just decorrelated.
            If a dict is given, the key is the process and the value is used to modify the systematic's name.
            The name of the systematic is modified to: `systematic_{process}`.

        drop : bool
            If True, the systematic is dropped at the end. Otherwise a remaining systematic stays, correlated over all processes.
        """
        if isinstance(processes, list):
            processes = {p: p for p in processes}
        assert isinstance(processes, dict)
        lines = [
            f"nuisance edit rename {process} * {systematic} {systematic}_{processes.get(process, process)} ifexists"
            for process in processes.keys()
        ]
        if drop:
            lines.append(f"nuisance edit drop * * {systematic} ifexists")
        return lines

    def reluncs(
        self,
        process: Union[str, Process],
        uncs: Optional[Union[List[str], List[Tuple[str]]]] = None,
    ) -> Union[Tuple[float], Dict[str, Tuple]]:
        """
        All:

        reluncs("tt")
        >>> {'mtop': (0.9730090410695392, 1.0278686159469077),
            'pdf': (0.9578484178128307, 1.0421515821871694),
            'scale': (0.9648937193421179, 1.023768875637203)}

        Individual uncertainties:

        reluncs("tt", uncs=["mtop", "pdf"])
        >>> {'mtop': (0.9730090410695392, 1.0278686159469077),
            'pdf': (0.9578484178128307, 1.0421515821871694)}

        or

        reluncs("tt", uncs="mtop")
        >>> (0.9730090410695392, 1.0278686159469077)

        Combine uncertainties:

        reluncs("tt", uncs=[("mtop", "pdf")])
        >>> {('mtop', 'pdf'): (0.9499473502712643, 1.0505313332070108)}

        Mix all:

        reluncs("tt", uncs=[("mtop", "pdf"), "scale"])
        >>> {'scale': (0.9648937193421179, 1.023768875637203),
            ('mtop', 'pdf'): (0.9499473502712643, 1.0505313332070108)}
        """
        from scinum import DOWN, UP

        import utils.aci as aci

        if isinstance(process, str):
            process = self.analysis_inst.processes.get(process)

        assert isinstance(process, aci.Process)
        xsec = process.xsecs.get(13.0, None)

        if xsec is None:
            xsec = 0.0
            ignore = set()
            for _, p in process.walk_processes():
                if p.name not in ignore and (xs := p.xsecs.get(13, None)) and xs.uncertainties:
                    # ignore all child processes from now on
                    children = set()
                    for _, c in p.walk_processes():
                        children.add(c.name)
                    ignore |= children
                    xsec += p.xsecs.get(13)

        if not uncs:
            uncs = list(xsec.uncertainties.keys())

        if isinstance(uncs, str):
            return tuple(
                xsec.get(direction=direction, names=uncs, factor=True) for direction in (DOWN, UP)
            )
        uncs = list(uncs)
        return {
            name: tuple(
                xsec.get(direction=direction, names=name, factor=True) for direction in (DOWN, UP)
            )
            for name in uncs
        }

    def nuisance_idx(self, name: str) -> List[int]:
        return list(map(itemgetter(0), self._nuisances_names)).index(name)

    def add_systematics(
        self,
        names: Union[List[str], Dict[str, int]],
        type: str,
        processes: List[str],
        suffix: Tuple[str] = ("Up", "Down"),
        strength: Optional[Union[int, List[int]]] = None,
        collapse: bool = False,
    ) -> None:
        up, down = suffix
        # remove non-valid processes
        if not (set(processes) <= set(self.available_processes)):
            self.console.log(
                f"Skip unknown process(es): {set(processes) - set(self.available_processes)} for nuisance(s) {names}."
            )
        if isinstance(names, str):
            rpl = lambda x: x.replace(up, "").replace(down, "")
            names = fnmatch.filter(set(map(rpl, self.available_systematics)), names)
        if isinstance(names, dict):
            names, strengths = list(names.keys()), list(names.values())
        if strength:
            if isinstance(strength, Number):
                strengths = len(names) * (strength,)
            else:
                strengths = strength
        # at this point names has to be a valid list
        assert isinstance(names, (list, tuple))
        assert len(names) == len(strengths)
        for name, strength in zip(names, strengths):
            _processes = [p for p in processes if p in self.vps]
            if isinstance(strength, Number):
                strength = 2 * (strength,)
            assert len(strength) == 2
            # first check if systematic is empty, only for shape
            if type == "shape":
                name_up = name + up
                name_down = name + down
                # verify that the shape exists
                if not all(name in self.available_systematics for name in (name_up, name_down)):
                    self.console.log(
                        "[bold red on white]WARNING:[/]"
                        f"Shape {name} does not have both, up and down shift, dropped"
                    )
                    continue
                # track malformed shift namings for combine...
                if up != "Up":
                    self._nuisances_renaming[name_up] = name + "Up"
                if down != "Down":
                    self._nuisances_renaming[name_down] = name + "Down"

                processes_ind = self.hists.axes[0].index(_processes)
                nominal_ind = self.hists.axes[1].index("nominal")
                h_nom = self.hists.view()[processes_ind, nominal_ind, :]
                h_up = self.hists.view()[processes_ind, self.hists.axes[1].index(name_up), :]
                h_down = self.hists.view()[processes_ind, self.hists.axes[1].index(name_down), :]
                if collapse and np.all(h_up == h_nom) and np.all(h_down == h_nom):
                    self.console.log(f"Collapsing Systematic {name} (nominal == up == down)")
                    continue
                to_be_removed = []
                for p in _processes:
                    if (self.rate(p, name_up) <= 0.0) or (self.rate(p, name_down) <= 0.0):
                        self.console.log(
                            f"Remove Systematic (NULL Yield): {name} for process {p} (up: {self.rate(p, name_up)}, down: {self.rate(p, name_down)})."
                        )
                        to_be_removed.append(p)
                _processes = list(set(_processes) - set(to_be_removed))
            if not _processes:
                continue
            self._nuisances_names.append((name, type))
            # get index:
            nuisance_idx = self.nuisance_idx(name)
            process_idx = [self.vps.index(p) for p in _processes]
            self._nuisances_arr = np.concatenate(
                (self._nuisances_arr, np.zeros((1, self._nuisances_arr.shape[1], 2))), axis=0
            )
            self._nuisances_arr[nuisance_idx, process_idx, DIR.DOWN] = strength[DIR.DOWN]
            self._nuisances_arr[nuisance_idx, process_idx, DIR.UP] = strength[DIR.UP]

    @abstractmethod
    def build_systematics(self):
        pass

    # hook for renamings
    # rename processes
    @property
    def rprocesses(self) -> RDict:
        return RDict()

    # rename nuisances
    @property
    def rnuisances(self) -> RDict:
        return RDict()

    @property
    def data_name(self) -> str:
        # corresponds to the process name of data in the hist.Hist
        return "data"

    def check_processes(self) -> None:
        i = 0
        for p, id in zip(self.processes, self.processes_ids):
            # id > 0, we don't want to remove signal
            if (id > 0) and ((r := self.rate(p)) <= self.min_yield):
                self.console.log(
                    f"Remove: process {p} has negative or no contribution (rate: {r}, tresh: {self.min_yield})"
                )
                i += 1
            else:
                self._valid_processes.append((p, id - i))

    @property
    def vps(self) -> List[str]:
        # valid processes
        return [tpl[0] for tpl in self._valid_processes]

    @property
    def vids(self) -> List[int]:
        # valid processes ids
        return [tpl[1] for tpl in self._valid_processes]

    def fix_histograms(self) -> None:
        from rich.panel import Panel

        from config.constants import LAMBDA0

        valid = [n for n, t in self._nuisances_names if "shape" in t] + ["nominal"]
        summary = {
            "Hists with empty bins": 0,
            "Hists with nan bins": 0,
            "Hists with extremely high difference to nominal": 0,
            "Hists with extremely low difference to nominal": 0,
        }
        for p in tqdm(self.vps, total=len(self.vps), desc="Fixing histograms"):
            nom = self.hists[p, "nominal", :].project(self.variable).view(flow=False)
            nom = nom["value"]
            nom[nom <= 0] = 0.0
            for s in self.available_systematics:
                if not any([s.startswith(v) for v in valid]):
                    continue
                syst = self._nuisances_renaming[s]
                h = export1d_hist(self.hists[p, s, :].project(self.variable))
                w = np.array(h)
                if np.all(w <= 0):
                    continue
                w2 = h.allvariances

                # SAFEGUARDS:
                # fix negative and empty bins for sumw and sumw2
                bad = w[1:-1] <= 0.0
                w[1:-1][bad] = 0.0
                val = 1e-5 * min(1, max(1e-10, np.sum(w)))
                nval = 1e-5 * min(1, max(1e-10, np.sum(nom)))

                fallback_w2 = ((np.sum(w2) / np.sum(w)) ** 2) * LAMBDA0

                keys = f"(category={self.category}, process={p}, systematic={s})"
                if empties := bad.sum():
                    msg = f"{empties}/{len(bad)} bin(s) are empty {keys} are set to: {val}."
                    msg += f"\n* The corresponding values in nominal histogram are: {nom[bad]}."
                    if self.verbose:
                        tqdm.write(msg)
                    if (nval / val >= 10) or (nval / val <= 0.1):
                        if self.verbose:
                            tqdm.write(
                                "WARNING: At least faktor 10(!) "
                                f"difference to nominal: {nval} and nuisance: {val}!"
                            )
                    w[1:-1][bad] = val
                    w2[1:-1][bad] = val
                    # variance can be taken from negative bin content, should only be substituted if 0
                    # w2[1:-1][w[1:-1] == 0.0] = np.nanmax([val, fallback_w2])
                    summary["Hists with empty bins"] += 1
                # check for NaN's
                if m := np.isnan(w).sum():
                    if self.verbose:
                        tqdm.write(f"WARNING: Found {m.sum()} {np.nan}'s {keys}!")
                    summary["Hists with nan bins"] += 1
                # check extremely high or low shifts compared to nominal bins
                if (m := ((nom > 0) & (w[1:-1] > 100 * nom))).sum():
                    if self.verbose:
                        tqdm.write(
                            f"Extreme high values for {keys}, setting to "
                            f"100 * nominal: {w[1:-1][m]} -> {nom[m] * 100}"
                        )
                    w[1:-1][m] = nom[m] * 100
                    summary["Hists with extremely high difference to nominal"] += 1
                # Do we want to check this too?
                if (m := ((nom > 0) & (w[1:-1] < 1 / 100 * nom))).sum():
                    if self.verbose:
                        tqdm.write(
                            f"Extreme low values for {keys}, setting to 1/100 * nominal: "
                            f"{w[1:-1][m]} -> {1/100 * nom[m]}"
                        )
                    w[1:-1][m] = 1 / 100 * nom[m]
                    summary["Hists with extremely low difference to nominal"] += 1
                h[:] = w.tolist()
                h._fSumw2 = w2
                h._fTsumw2 = w2[1:-1].sum()
                h._fTsumw = w[1:-1].sum()
                syst = re.sub(
                    "(.+?)(Up|Down)$",
                    lambda x: f"{self.rnuisances[x.group(1)]}{x.group(2)}",
                    syst,
                )
                n = Shape.template.format(
                    category=self.bin,
                    process=self.rprocesses[p],
                    variable=self.variable,
                    systematic=syst,
                )
                self._newhists[n] = h
        sm = f"[bold underline]Summary fixes for {len(self._newhists)} histograms:[/]"
        for k, v in summary.items():
            sm += f"\n* {k} = [magenta]{v}[/] ([magenta]{100*v/len(self._newhists):.2f}[/]%)"
        self.console.log(Panel(sm, expand=False, border_style="green"))
        assert summary["Hists with nan bins"] == 0

    def build(self) -> None:
        # check that processes are available in histograms
        if len((missing := (set(self.processes) - set(self.available_processes)))) > 0:
            raise Exception(f"Missing {len(missing)} processes: {missing}")

        # now check if there is any process with no contribution
        self.check_processes()

        if self.data_name != "data_obs":
            self.console.log(
                f"For combine you probably want to rename the data process in the future, I will do it for you now: [bold]{self.data_name} :right_arrow: 'data_obs'"
            )
        # just add data to output histograms
        self._newhists[
            Shape.template.format(
                category=self.bin,
                process="data_obs",
                variable=self.variable,
                systematic="nominal",
            )
        ] = export1d_hist(self.hists[self.data_name, "nominal", :].project(self.variable))

        # now check if there is no data
        if (r := self.rate(self.data_name)) <= 0.0:
            self.console.log(f"No data, no fun! (rate: {r})")

        # axis = 0: nuisance name
        # axis = 1: valid (!) processess
        # axis = 2: up/down strength
        self._nuisances_arr = np.zeros((0, len(self.vps), 2))

        self.build_systematics()

        # in the end fix histograms, i.e. negative bins
        self.fix_histograms()

    def dump(self, directory: str, txt: str = "datacard.txt", shapes: str = "shapes.root") -> None:
        txt_path = os.path.join(directory, txt)
        shapes_path = os.path.join(directory, shapes)
        card = open(txt_path, "w")
        # first write header
        card.write(f"# {repr(self)}{self.sep}")
        # build:
        #  - check histograms
        #  - fix histograms
        #  - build systematics
        self.build()
        card.write(f"imax 1 number of bins{self.sep}")
        card.write(f"jmax * number of processes minus 1{self.sep}")
        card.write(f"kmax * number of nuisance parameters{self.sep}")
        card.write(f"{self.dashes}{self.sep}")
        card.write(
            f"shapes * {self.bin} {shapes} {self.nominal_pattern} {self.sys_pattern}{self.sep}"
        )
        card.write(f"{self.dashes}{self.sep}")
        card.write(f"bin {self.bin}{self.sep}")
        card.write(f"observation {self.observation}{self.sep}")
        card.write(f"{self.dashes}{self.sep}")
        card.write("bin".ljust(self.spaces))  # category
        card.write("".join([f"{self.bin.ljust(self.spaces)}"] * len(self.vps)) + f"{self.sep}")
        card.write("process".ljust(self.spaces))  # process names
        card.write(
            "".join([f"{self.rprocesses[p]}".ljust(self.spaces) for p in self.vps]) + f"{self.sep}"
        )
        card.write("process".ljust(self.spaces))  # process ids
        card.write("".join([f"{i}".ljust(self.spaces) for i in self.vids]) + f"{self.sep}")
        card.write("rate".ljust(self.spaces))
        card.write(
            "".join([f"{self.rate(p)}".ljust(self.spaces) for p in self.vps]) + f"{self.sep}"
        )
        card.write(f"{self.dashes}{self.sep}")
        if nn := set(self._nuisances_names):
            for name, type in sorted(nn, key=lambda x: (x[1], x[0])):
                nidx = self.nuisance_idx(name)
                card.write(f"{self.rnuisances[name]} {type}".ljust(self.spaces))
                for pidx in range(len(self.vps)):
                    s = self._nuisances_arr[nidx, pidx, ...]
                    assert s.size == 2
                    if not np.any(s):
                        card.write("-".ljust(self.spaces))
                        continue
                    if np.any(s) and (s[DIR.DOWN] == s[DIR.UP]):
                        card.write(f"{round(s[DIR.DOWN], 4)}".ljust(self.spaces))
                        continue
                    else:
                        card.write(
                            f"{round(s[DIR.DOWN], 4)}/{round(s[DIR.UP], 4)}".ljust(self.spaces)
                        )
                card.write(f"{self.sep}")

        # at the end: add custom lines and lumi scale
        if self.lumi is not None:
            assert self.lumi > 0.0
            scale = self.lumi / self.campaign_inst.aux["lumi"]
            lumi_projection = [
                f"lumi_projection rateParam * * {scale}",
                "nuisance edit freeze lumi_projection",
            ]
            card.write(self.sep.join(lumi_projection) + f"{self.sep}")
        if self.custom_lines:
            if self.lumi is not None:
                assert "lumi_projection" not in self.sep.join(
                    self.custom_lines
                ), "Name 'lumi_projection' is already reserved!"
            card.write(self.sep.join(self.custom_lines))
        card.close()

        # finally dump new/fixed histograms
        with uproot3.recreate(shapes_path) as root_file:
            bulk_write(
                root_file,
                tqdm(self._newhists.items(), total=len(self._newhists), desc="write to file"),
            )


class RDict(MutableMapping):
    def __init__(self, txt_file="", **kwargs):
        if isinstance(txt_file, str) and txt_file.endswith(".txt"):
            with open(txt_file) as f:
                for line in f:
                    if l := line.strip():
                        k, v = l.split("=")
                        self.__dict__[k] = v
        self.__dict__.update(**kwargs)

    def __setitem__(self, key, value):
        self.__dict__[key] = value

    def __getitem__(self, key):
        if key not in self.__dict__:
            return key
        else:
            return self.__dict__[key]

    def __delitem__(self, key):
        del self.__dict__[key]

    def __iter__(self):
        return iter(self.__dict__)

    def __len__(self):
        return len(self.__dict__)


thresh_rebin = deprecated_import("utils.binning.thresh_rebin")
