from collections import defaultdict
from typing import Callable, Iterable, Optional, Union

import hist
import matplotlib
import matplotlib.pyplot as plt
import mplhep as hep
import numpy as np
from coffea.hist.plot import clopper_pearson_interval, normal_interval, poisson_interval
from matplotlib.font_manager import FontProperties
from matplotlib.legend import Legend
from matplotlib.offsetbox import AnchoredOffsetbox, HPacker, TextArea
from matplotlib.text import Text
from matplotlib.ticker import AutoMinorLocator

from utils.util import linsplit

valid_x_axis = (hist.axis.Regular, hist.axis.Variable, hist.axis.Integer)


def _xaxis_ticks(ax, *axis):
    if all(isinstance(a, hist.axis.Integer) for a in axis):
        ax.xaxis.get_major_locator().set_params(integer=True)
        delta = int(np.diff(ax.xaxis.get_majorticklocs()).mean())
    else:
        delta = np.inf
    if 1 < delta:
        ax.xaxis.set_minor_locator(AutoMinorLocator(n=None if delta > 5 else delta))


def plot1d(
    h,
    ax=None,
    clear=True,
    overlay=None,
    stack=False,
    overflow=False,
    line_opts=None,
    fill_opts=None,
    error_opts=None,
    legend_opts=dict(loc="upper left", bbox_to_anchor=(1.04, 1), borderaxespad=0),
    density=False,
    binwnorm=None,
    order=None,
    legend_labels={},
    legend_title=None,
    log=False,
    yrange=[],  # [min, max] -> [1e-3, 1e6] (only min: [1e-3, None])
    x_label="",
    errmethod=np.sqrt,
):
    if ax is None:
        ax = plt.gca()
    else:
        if not isinstance(ax, plt.Axes):
            raise ValueError("ax must be a matplotlib Axes object")
        if clear:
            ax.clear()

    if line_opts is None and fill_opts is None and error_opts is None:
        if stack:
            fill_opts = {}
        else:
            line_opts = {}
            error_opts = {}

    if overlay is not None:
        assert len(h.axes) == 2
        overlay = h.axes[overlay]
        assert isinstance(overlay, hist.axis.StrCategory)
        (axis,) = tuple(ax for ax in h.axes if isinstance(ax, valid_x_axis))

    else:
        assert len(h.axes) == 1
        (axis,) = h.axes

    if not isinstance(axis, valid_x_axis):
        raise NotImplementedError("Plot a sparse axis (e.g. bar chart)")

    else:
        ax.set_xlabel(x_label or axis.label)
        if binwnorm:
            ax.set_ylabel("Entries / bin")
        elif h.metadata:
            ax.set_ylabel(h.metadata.get("y_title", "Entries / bin"))
        else:
            ax.set_ylabel("Entries / bin")

        edges = axis.edges
        if order is None:
            identifiers = [*overlay] if overlay is not None else [None]
        else:
            identifiers = order

        plot_info = {
            "identifier": identifiers,
            "label": [legend_labels[i] for i in identifiers] if legend_labels else identifiers,
            "sumw": [],
            "sumw2": [],
        }
        for i, identifier in enumerate(identifiers):
            if identifier is None:
                sumw = h.view(flow=overflow)["value"]
                sumw2 = h.view(flow=overflow)["variance"]
            else:
                sumw = h[identifier, ...].view(flow=overflow)["value"]
                sumw2 = h[identifier, ...].view(flow=overflow)["variance"]

            plot_info["sumw"].append(sumw)
            plot_info["sumw2"].append(sumw2)

        kwargs = None
        if line_opts is not None and error_opts is None:
            _error = None
        else:
            _error = list(np.array(plot_info["sumw2"]))
        if fill_opts is not None:
            histtype = "fill"
            kwargs = fill_opts
        elif error_opts is not None and line_opts is None:
            histtype = "errorbar"
            kwargs = error_opts.copy()
            kwargs.setdefault("xerr", None)
        else:
            histtype = "step"
            kwargs = line_opts
        if kwargs is None:
            kwargs = {}

        if _error is not None:
            _error = [errmethod(e) for e in _error]

        hep.histplot(
            plot_info["sumw"],
            edges,
            label=plot_info["label"],
            yerr=_error,
            histtype=histtype,
            ax=ax,
            density=density,
            binwnorm=binwnorm,
            stack=stack,
            **kwargs,
        )

        if stack and error_opts is not None:
            stack_sumw = np.sum(plot_info["sumw"], axis=0)
            stack_sumw2 = np.sum(plot_info["sumw2"], axis=0)
            err = poisson_interval(stack_sumw, stack_sumw2)
            if binwnorm is not None:
                err *= binwnorm / np.diff(edges)[None, :]
            opts = {
                "step": "post",
                "label": "Sum unc.",
                "hatch": "///",
                "facecolor": "none",
                "edgecolor": (0, 0, 0, 0.5),
                "linewidth": 0,
                "zorder": 1.1,  # needed to overlay error above bin content (to be checked in mplhep)
            }
            opts.update(error_opts)
            ax.fill_between(
                x=edges, y1=np.r_[err[0, :], err[0, -1]], y2=np.r_[err[1, :], err[1, -1]], **opts
            )

        if log:
            ax.set_yscale("log")
        ax.autoscale(axis="x", tight=True)
        ax.autoscale(axis="y", tight=False)
        if not log:
            ax.set_ylim(0, None)
        if yrange:
            ax.set_ylim(*yrange)

        _xaxis_ticks(ax, axis)

        if legend_opts is not None:
            if legend_title is None and overlay is not None and overlay.label:
                legend_title = overlay.label
            ax.legend(title=legend_title, **legend_opts)

    return ax


def plotratio(
    num,
    denom,
    ax=None,
    clear=True,
    overflow=False,
    error_opts=None,
    denom_fill_opts=None,
    guide_opts=None,
    unc="clopper-pearson",
    label=None,
    ratio_yticks=[],  # [start, stop, step] -> [0.5, 1.5, 0.1]
    x_label="",
):
    if ax is None:
        fig, ax = plt.subplots(1, 1)
    else:
        if not isinstance(ax, plt.Axes):
            raise ValueError("ax must be a matplotlib Axes object")
        if clear:
            ax.clear()

    if error_opts is None and denom_fill_opts is None and guide_opts is None:
        error_opts = {}
        denom_fill_opts = {}

    (naxis,) = num.axes
    (daxis,) = denom.axes
    assert isinstance(naxis, valid_x_axis)
    assert isinstance(daxis, valid_x_axis)
    assert all(naxis.edges == daxis.edges)

    ax.set_xlabel(x_label or naxis.label)
    ax.set_ylabel("Data / Exp.")
    edges = naxis.edges
    centers = naxis.centers

    sumw_num = num.view(flow=overflow)["value"]
    sumw2_num = num.view(flow=overflow)["variance"]
    sumw_denom = denom.view(flow=overflow)["value"]
    sumw2_denom = denom.view(flow=overflow)["variance"]

    rsumw = sumw_num / sumw_denom
    if unc == "clopper-pearson":
        rsumw_err = np.abs(clopper_pearson_interval(sumw_num, sumw_denom) - rsumw)
    elif unc == "poisson-ratio":
        # poisson ratio n/m is equivalent to binomial n/(n+m)
        rsumw_err = np.abs(clopper_pearson_interval(sumw_num, sumw_num + sumw_denom) - rsumw)
    elif unc == "num":
        rsumw_err = np.abs(poisson_interval(rsumw, sumw2_num / sumw_denom**2) - rsumw)
    elif unc == "normal":
        rsumw_err = np.abs(normal_interval(sumw_num, sumw_denom, sumw2_num, sumw2_denom))
    elif unc == "scale":
        rsumw_err = np.sqrt(sumw2_num) / sumw_denom
    else:
        raise ValueError("Unrecognized uncertainty option: %r" % unc)

    if unc == "scale":
        denom_unc = np.sqrt(sumw2_denom) / sumw_denom
        denom_unc = (1 - denom_unc, 1 + denom_unc)
    else:
        denom_unc = poisson_interval(np.ones_like(sumw_denom), sumw2_denom / sumw_denom**2)

    # fixup 0-division results for plotting
    rsumw[sumw_denom == 0] = np.nan
    rsumw[(sumw_num == 0) & (sumw_denom == 0)] = 1
    rsumw_err = np.nan_to_num(rsumw_err, nan=0, posinf=0, neginf=0)
    # denom_unc = np.nan_to_num(denom_unc, nan=0)

    if denom_fill_opts is not None:
        opts = {"facecolor": (0, 0, 0, 0.3), "linewidth": 0}
        opts.update(denom_fill_opts, fill=True)
        ax.stairs(edges=edges, baseline=denom_unc[0], values=denom_unc[1], **opts)
    if error_opts is not None:
        opts = {"label": label, "linestyle": "none"}
        opts.update(error_opts)
        emarker = opts.pop("emarker", "")
        errbar = ax.errorbar(x=centers, y=rsumw, yerr=rsumw_err, **dict(opts, linestyle="none"))
        plt.setp(errbar[1], "marker", emarker)
        if opts.get("linestyle", "") not in (None, "", " ", "none", "None"):
            ax.stairs(rsumw, edges=edges, baseline=1, fill=False, **opts)
    if guide_opts is not None:
        opts = {"linestyle": "--", "color": (0, 0, 0, 0.5), "linewidth": 1}
        opts.update(guide_opts)
        ax.axhline(1.0, **opts)

    if ratio_yticks:
        start, stop, step = ratio_yticks
        ax.set_yticks(np.arange(start, stop + step, step).tolist())
        for label in ax.yaxis.get_ticklabels()[::2]:
            label.set_visible(False)
        ax.set_ylim(start, stop)

    if clear:
        ax.autoscale(axis="x", tight=True)
        ax.set_ylim(0, None)

    _xaxis_ticks(ax, naxis, daxis)

    return ax


def plot_cov(data, labels, size=5, step=0.5, text=None):
    assert data.shape == data.shape[2::-1]
    n = size + step * data.shape[0]
    fig, ax = plt.subplots(figsize=(n, n))

    im = ax.imshow(data, cmap="PuOr", vmin=-1, vmax=1)

    # Create colorbar
    cbar = fig.colorbar(im, ax=ax)
    cbar.ax.set_ylabel("covariance", rotation=-90, va="bottom")

    # We want to show all ticks...
    ax.set_xticks(np.arange(data.shape[1]))
    ax.set_yticks(np.arange(data.shape[0]))
    # ... and label them with the respective list entries.
    ax.set_xticklabels(labels)
    ax.set_yticklabels(labels)

    # Let the horizontal axes labeling appear on top.
    ax.tick_params(top=True, bottom=False, labeltop=True, labelbottom=False)

    # Rotate the tick labels and set their alignment.
    # plt.setp(ax.get_yticklabels(), rotation=45, ha="right", rotation_mode="anchor")
    plt.setp(ax.get_xticklabels(), rotation=-45, ha="right", rotation_mode="anchor")

    # Turn spines off and create white grid.
    ax.spines[:].set_visible(False)

    # ax.set_xticks(np.arange(data.shape[1] + 1) - 0.5, minor=True)
    # ax.set_yticks(np.arange(data.shape[0] + 1) - 0.5, minor=True)
    # ax.tick_params(which="minor", bottom=False, left=False)
    # ax.grid(which="minor", color="w", linestyle="-", linewidth=3)

    if text is None:
        text = data.shape[0] <= 30
    if text:
        colors = ("black", "white")

        for idx, val in np.ndenumerate(data):
            txt = f"{val:.2f}"
            if ".00" in txt:
                continue
            ax.text(
                *idx,
                txt.replace("0.", "."),
                color=colors[abs(val) > 0.5],
                horizontalalignment="center",
                verticalalignment="center",
                size=7,
            )

    fig.tight_layout()

    return fig


def _label_text_props(halign, valign="baseline", **kwargs):
    return dict(
        verticalalignment=valign,
        horizontalalignment=halign,
        fontproperties=FontProperties(family=["Helvetica", "sans-serif"], **kwargs),
    )


def _label(ax, left, child, *, voffset=0.01):
    vside = "lower" if voffset > 0 else "upper"
    hside = "left" if left else "right"
    if ax is not None:
        child = AnchoredOffsetbox(
            loc=f"{vside} {hside}",
            bbox_to_anchor=(0 if left else 1, 1 + voffset),
            bbox_transform=ax.transAxes,
            frameon=False,
            borderpad=0,
            pad=0.01,
            child=child,
        )
        ax.add_artist(child)
    return child


def label(ax, label="Private Work", **kwargs):
    return (
        cms_label(ax, label, data=kwargs.get("data", True)),
        lumi_label(ax, **kwargs),
    )


def cms_label(ax, label="Private Work", *, data=True):
    label = HPacker(
        children=[
            TextArea(
                r"CMS" if data else r"CMS Simulation",
                textprops=_label_text_props(halign="left", size="large", weight="bold"),
            ),
            TextArea(
                label,
                textprops=_label_text_props(halign="left", style="italic"),
            ),
        ],
        align="baseline",
        pad=0,
        sep=5,
    )
    return _label(ax, True, label)


def lumi_label(
    ax, *, data=True, lumi=None, year=None, ecm=None, campaign=None, lumi_prefix_label=""
):
    if campaign:
        if lumi is None:
            lumi = campaign.aux.get("lumi", None) / 1000
        if year is None:
            year = campaign.aux.get("year", None)
        if ecm is None:
            ecm = campaign.ecm or None

    text = ""
    if lumi and data:
        text = rf"{lumi:.0f} fb${{}}^{{-1}}$"
    if year:
        if text:
            text += ", "
        text += str(year)
    if ecm:
        if text:
            text += " "
        ecm = f"{ecm:.1f}".rstrip("0").rstrip(".")
        text += f"({ecm} TeV)"

    if lumi_prefix_label:
        text = f"{lumi_prefix_label} - {text}"

    text = TextArea(text, _label_text_props("right"))
    return _label(ax, False, text)


class LegendMerger:
    def __init__(self):
        self._label2info = defaultdict(lambda: defaultdict(list))
        self._label2pos = defaultdict(int)

    def add(self, ax, order=0, position=None):
        if position is None:
            position = not order
        for pos, (handler, label) in enumerate(zip(*ax.get_legend_handles_labels())):
            self._label2info[label][order].append(handler)
            self._label2pos[label] += pos

    def _parse(self, info, sli=-1):
        return sum(
            (
                tuple(x) if isinstance(x, list) else (x,)
                for x in (v[sli] for k, v in sorted(info.items()))
            ),
            (),
        )

    def reposition(self, func, position=None):
        for key in self._label2info.keys():
            pos = func(key)
            if isinstance(pos, bool):
                if pos:
                    assert position is not None
                    pos = position
                else:
                    continue
            elif pos is None:
                continue
            self._label2pos[key] = pos

    def result(self, slice=-1, reversed=False):
        return zip(
            *(
                (self._parse(info, slice), label)
                for label, info in sorted(
                    self._label2info.items(), key=lambda li: self._label2pos[li[0]]
                )[:: (-1 if reversed else 1)]
            )
        )


def axis_margin(ax, **kwargs):
    for k, m in kwargs.items():
        assert 0 < abs(m) < 1, f"invalid {k}-margin: {m}"
        lo, ho = getattr(ax, f"get_{k}lim")()
        t = getattr(ax, f"{k}axis").get_transform()
        lt, ht = t.transform([lo, ho])
        dt = ht - lt
        m = 1 / (1 - abs(m)) - 1
        if m > 0:
            ht += m * dt
        else:
            lt -= m * dt
        ln, hn = t.inverted().transform([lt, ht])
        getattr(ax, f"set_{k}lim")(ln, hn)


def legend_fcf(legend):
    """Legend Fill-Columns-First"""
    assert isinstance(legend, Legend)
    cols = legend._legend_handle_box.get_children()
    colc = [col.get_children() for col in cols]
    entries = sum(colc, [])
    if not entries:
        return
    rows = len(colc[0])
    for i, col in enumerate(colc):
        col[:] = entries[i * rows : (i + 1) * rows]
    # remove empty columns, can't be plotted anyways
    del cols[-(-len(entries) // rows) :]


def auto_ratio_ylim(aax, ratio_max_delta=None, auto_log_thresh=10):
    if not isinstance(aax, (list, tuple, np.ndarray)):
        aax = [aax]
    ax = aax[0]
    ax.autoscale(axis="y", tight=True)
    o = np.abs(np.array(ax.get_ylim()) - 1).max()
    if ratio_max_delta is not None:
        o = min(o, ratio_max_delta)
    if o > auto_log_thresh:
        ax.set_yscale("log")
        ax.autoscale(axis="y")
        l, h = ax.get_ylim()
        ax.set_ylim(max(1e-3, l), h)
    else:
        m = 20 * 10 ** np.floor(-np.log10(o * 0.7))
        o = np.ceil(o * 1.05 * m) / m
        ax.set_ylim(max(0, 1 - o), 1 + o)

        for ax in aax:
            ax.yaxis.set_minor_locator(AutoMinorLocator())


def autoscale(
    axs: Iterable["matplotlib.axes.Axes"],
    skip: Union[Callable[[str], bool], str],
    use_sticky_edges: Optional[bool] = None,
    margins: dict = {},
    **kwargs,
):
    hide = []
    if not callable(skip):
        needle = skip
        skip = lambda label: needle in label

    for ax in axs:
        hide.extend(
            art
            for art in ax.get_children()
            if art.get_visible()
            and skip(txt.get_text() if isinstance((txt := art.get_label()), Text) else txt)
        )
    for art in hide:
        art.set_visible(False)
    for ax in axs:
        ax.relim(visible_only=True)
    if use_sticky_edges is not None:
        ax.use_sticky_edges = use_sticky_edges
    if margins:
        ax.margins(**margins)
    ax.autoscale_view(**kwargs)
    for art in hide:
        art.set_visible(True)
