# coding: utf-8

from dataclasses import dataclass
from functools import partial, reduce
import gc
import logging
from numbers import Number
import re
from abc import abstractmethod
from collections import ChainMap, defaultdict
from contextlib import contextmanager
from copy import deepcopy
from itertools import cycle, filterfalse
from operator import and_, attrgetter, itemgetter, methodcaller, or_
from time import time
from typing import (
    Any,
    Callable,
    Dict,
    Iterable,
    List,
    Literal,
    Optional,
    Sequence,
    Set,
    Tuple,
    Union,
)

# do not remove these imports
# needed explicit imports for
# variable expression 'eval' calls
import awkward as ak  # noqa
import cachetools
import hist
import numpy as np
from boost_histogram._internal.view import _to_view
from coffea.analysis_tools import PackedSelection as _PS
from coffea.analysis_tools import Weights as _W
from coffea.lookup_tools import evaluator
from coffea.nanoevents import NanoAODSchema
from coffea.processor import ProcessorABC as _pABC
from coffea.processor.accumulator import (
    AccumulatorABC,
    column_accumulator,
    defaultdict_accumulator,
    dict_accumulator,
    set_accumulator,
)
from dask.base import tokenize
from hist import Hist
from tqdm.auto import tqdm
import iminuit

import processor.util as util  # noqa
from processor.util import normalize, reduce_or
from utils.bh5 import BaseCategory
from utils.bh5 import Histogram as Hist5
from utils.hist import unify
from utils.util import MaxRSSWatch, iter_chunks, ro_view, view_sdtype_flat

from scipy.stats import beta, norm
from scipy.optimize import minimize, curve_fit, differential_evolution, newton


class Weights(_W):
    def __init__(self, size, *, dtype=None, **kwargs):
        self._dtype = dtype
        super().__init__(size, **kwargs)
        if self._dtype is not None:
            self._weight = np.asarray(self._weight, dtype=self._dtype)

    def add(self, name, weight, weightUp=None, weightDown=None, shift=False):
        if self._dtype is not None:
            weight = np.asarray(weight, dtype=self._dtype)
            if weightUp is not None:
                weightUp = np.asarray(weightUp, dtype=self._dtype)
            if weightDown is not None:
                weightDown = np.asarray(weightDown, dtype=self._dtype)
        return super().add(name, weight, weightUp=weightUp, weightDown=weightDown, shift=shift)

    def copy(self):
        return deepcopy(
            self,
            # HACK: pre-inject memos for all Dict[Any, np.ndarray] as read-only view "copies"
            {
                id(d): {k: ro_view(v) for k, v in d.items()}
                for d in [self._weights, self._modifiers]
            },
        )


class PackedSelection(_PS):
    def __init__(self, dtype="uint64"):
        assert np.issubdtype(dtype, np.unsignedinteger)
        self._dtype = np.dtype(dtype)
        self._names = []
        self._masks = []

    def _get_hl(self, num):
        bits = self._dtype.itemsize * 8
        return num // bits, num % bits

    def add(self, name, selection):
        selection = np.asarray(selection, dtype=bool)
        if name.startswith("!"):
            raise ValueError("selection name must not start with '!'")
        if not (isinstance(selection, np.ndarray) and selection.dtype == np.dtype("bool")):
            raise ValueError(
                "PackedSelection only understands numpy boolean arrays, got %r" % selection
            )
        if name in self._names:
            raise ValueError("selection name already in use")

        h, l = self._get_hl(len(self._names))
        if len(self._masks) <= h:
            self._masks.append(np.zeros(shape=selection.shape, dtype=self._dtype))
        self._masks[h] |= selection.astype(self._dtype) << l
        self._names.append(name)

    def require(self, **names):
        mr = [(0, 0) for m in self._masks]
        for name, val in names.items():
            if not isinstance(val, bool):
                raise ValueError(
                    "Please use only booleans in PackedSelection.require(), received %r for %s"
                    % (val, name)
                )
            h, l = self._get_hl(self._names.index(name))
            m, r = mr[h]
            m |= 1 << l
            r |= int(val) << l
            mr[h] = m, r
        ret = None
        for (m, r), mask in zip(mr, self._masks):
            if m:
                res = (mask & m) == r
                ret = res if ret is None else res & ret
        if ret is None:
            if self._masks:
                ret = np.full_like(self._masks[0], True, dtype=bool)
            else:
                ret = np.r_[:]
        return ret

    def all(self, *names):
        return self.require(
            **{
                (name if want else name[1:]): want
                for name, want in ((n, not n.startswith("!")) for n in names)
            }
        )

    def any(self, *names):
        return reduce_or(*(self.all(name) for name in names))

    def skip_early(self, categories: Dict[Any, List[str]], inject_missing: bool = True) -> bool:
        """
        Helper to check if the available selectors can't fulfill any of the given `categories`.
        In that case, may also `inject_missing` selectors.
        """
        # collated cuts
        if common := set(self.names).intersection(*categories.values()):
            if (mask := self.all(*common)).any():
                return False  # something fulfills common cuts
        else:
            # single category cuts
            seen = set()
            for avail in (frozenset(cat).intersection(self.names) for cat in categories.values()):
                if avail and avail not in seen:
                    seen.add(avail)
                    if (mask := self.all(*avail)).any():
                        return False  # some cactegory is fulfilled
            if not seen:
                return False  # no category has available selectors
        if inject_missing:
            assert not np.any(mask)
            for missing in (q := set().union(*categories.values()) - set(self.names)):
                self.add(missing, mask)
        return True

    def copy(self):
        return deepcopy(self)


class ProcessorABC(_pABC):
    output = "data.coffea"
    debug_dataset = "st"
    debug_uuids = None
    debug_paths = None
    schemaclass = NanoAODSchema

    @classmethod
    def requires(cls, task):
        return task.base_requires()

    @classmethod
    def live_callback(cls, accumulator):
        return {}

    @classmethod
    def save(cls, task, output, **kwargs):
        assert not callable(cls.output)
        target = task.output()
        target.parent.touch()
        if cls.output == "*.npy":
            output = {
                key: value.value if isinstance(value, column_accumulator) else value
                for key, value in output.items()
            }
            for name, value in list(output.items()):
                if not isinstance(value, np.ndarray):
                    print(
                        "output[%r]=%r expected to be %r but is %r"
                        % (name, value, np.array, type(value))
                    )
                    del output[name]
            target.dump(output, **kwargs)
            target.touch()
        else:
            target.dump(output, **kwargs)

    @classmethod
    def group_processes(cls, hists, task):
        # hook which is called in tasks/group.py
        # after: mapping_stage_2
        return hists

    def postprocess(self, accumulator):
        return accumulator

    def _preExe(self, exe, exeArgs):
        pass

    def _postExe(self, exe, exeArgs, output):
        return output


class BaseProcessor(ProcessorABC):
    individual_weights = False
    jes_shifts = False
    jec_objects = dict(Jet="AK4PFchs", FatJet="AK8PFPuppi", CorrT1METJet="AK4PFchs")
    jec_cache = cachetools.Cache(100 << 20)  # 100MB
    dataset_shifts = False
    extra_nominals = tuple()

    def __init__(self, task):
        self.publish_message = task.publish_message if task.debug else None
        self.debug = task.debug
        self.analysis_inst = task.analysis_inst
        self.campaign_inst = task.campaign_inst
        self.year = task.year
        self.corrections = task.load_corrections()

        self.dataset_axis = hist.axis.StrCategory(
            [], name="dataset", label="Primary dataset", growth=True
        )
        self.category_axis = hist.axis.StrCategory(
            [], name="category", label="Category selection", growth=True
        )
        self.syst_axis = hist.axis.StrCategory(
            [], name="systematic", label="Shift of systematic uncertainty", growth=True
        )
        self.category_axis.chunks = 10
        self.syst_axis.chunks = 10

        self._accumulator = dict_accumulator(
            {
                name: hist_accumulator(
                    Hist(
                        self.dataset_axis,
                        self.category_axis,
                        hist.axis.Regular(10, 0, 10, name=name, label="Cut index"),
                        storage=hist.storage.Weight(),
                    )
                )
                for name in ["cutflow", "cutflow_raw"]
            },
            n_events=defaultdict_accumulator(int),
            sum_gen_weights=defaultdict_accumulator(float),
            count_bad_gen_weights=defaultdict_accumulator(float),
            object_cutflow=defaultdict_accumulator(int),
        )

    @contextmanager
    def timeit(self, msg, publish=False):
        if "%" not in msg:
            msg += ": %.2fs"
        t = time()
        try:
            yield
        finally:
            msg %= time() - t
            if self.publish_message:
                if publish:
                    self.publish_message(msg)
                print(repr(self), msg)

    @property
    def accumulator(self):
        return self._accumulator

    def get_dataset(self, events):
        return self.campaign_inst.datasets.get(events.metadata["dataset"][0])

    def get_dataset_shift(self, events):
        return events.metadata["dataset"][1]

    def get_lfn(self, events, default=None):
        ds = self.get_dataset(events)
        fn = events.metadata["filename"].rsplit("/", 1)[-1]

        for lfn in ds.info[self.get_dataset_shift(events)]["aux"]["lfns"]:
            if lfn.endswith(fn):
                return lfn
        else:
            if default is not None:
                return default
            else:
                raise RuntimeError(
                    "could not find original LFN for: %s" % events.metadata["filename"]
                )

    def get_pu_key(self, events):
        ds = self.get_dataset(events)
        if ds.is_data:
            return "data"
        else:
            lfn = self.get_lfn(events, default="")
            for name, hint in ds.campaign.aux.get("pileup_lfn_scenario_hint", {}).items():
                if hint in lfn:
                    return name
            else:
                return "MC"

    def jec_shifts(self, events):
        ds = self.get_dataset(events)
        shifts = ["nominal"]
        if ds.is_mc and self.jes_shifts and self.get_dataset_shift(events) == "nominal":
            shifts += ["UnclustEn", "jer"] + self.campaign_inst.aux["jes_sources"]
            if int(self.year) == 2018:
                shifts.append("HemIssue")
        return shifts

    def jec_loop(self, events, extra_nominals=[]):
        ds = self.get_dataset(events)
        shifts = self.jec_shifts(events)
        yield from self.corrections["jet"].generate(
            events,
            run=ds.aux["run"] if ds.is_data else "mc",
            met=("METFixEE2017" if int(self.year) == 2017 else "MET"),
            shifts=shifts,
        )

    def select(self, events):
        raise NotImplementedError("%r needs to be overridden!" % self.select)
        return locals()  # pass needed select_output on as mapping


class Histogramer(BaseProcessor):
    jes_shifts = True
    dataset_shifts = True

    @property
    def variables(self):
        return [
            var for var in self.analysis_inst.variables.values if var.aux.get("histogram", True)
        ]

    def __init__(self, task):
        super().__init__(task)

        self._accumulator["histograms"] = dict_accumulator(
            {
                variable.name: hist_accumulator(
                    Hist(
                        self.dataset_axis,
                        self.category_axis,
                        self.syst_axis,
                        hist.axis.Regular(
                            variable.binning[0],
                            variable.binning[1],
                            variable.binning[2],
                            name=variable.name,
                            label=variable.x_title,
                        ),
                        metadata={
                            "name": variable.name,
                            "x_title": variable.x_title,
                            "y_title": variable.y_title,
                        },
                        storage=hist.storage.Weight(),
                    )
                )
                for variable in self.variables
            }
        )

    def category_variables(self, category):
        return self.variables

    def variable_full_shifts(self, variable):
        return True

    def process(self, events):
        output = self.accumulator.identity()
        shifts = self.jec_shifts(events)
        with MaxRSSWatch(prefix="Total:"):
            item = events.metadata
            logging.getLogger("distributed.worker").info(
                f"{item['filename']} ({item['entrystart']}-{item['entrystop']})"
            )
            for _locals in self.select(events):
                with MaxRSSWatch(
                    prefix=f"{type(self).__qualname__}, JEC loop:",
                    suffix=f"Shift: {_locals['unc'], _locals['shift']}",
                ):
                    self.histogram(_locals=_locals, output=output, jec_shifts=shifts)
                del _locals
                gc.collect()
        return output

    def histogram(
        self,
        _locals: Dict,
        output: dict_accumulator,
        jec_shifts: Iterable = set(),
    ) -> dict_accumulator:
        categories = _locals["categories"]
        selection = _locals["selection"]
        weights = _locals["weights"]
        dsname, dsshift = tuple(_locals["events"].metadata["dataset"])

        # determine jes_shift
        jes_shift = str(_locals["unc"])
        if _locals["shift"] is not None:
            jes_shift += _locals["shift"].capitalize()

        # collect cutflow output
        if jes_shift == "nominal":
            output += _locals["output"]

        datasets = _locals.get("datasets", {})
        datasets.setdefault(None, 1)

        # flip cat->vars mapping
        var2cats = defaultdict(set)
        for category, cuts in categories.items():
            if np.any(selection.all(*cuts)):
                for var in self.category_variables(category):
                    var2cats[var].add(category)

        # get variations
        variations = {jes_shift: None}
        if jes_shift == "nominal":
            if dsshift == "nominal":
                variations.update({var: var for var in sorted(weights.variations)})
            else:
                variations = {dsshift: None}
        else:
            assert dsshift == "nominal"  # self.jec_loop assures this

        for variable, variable_categories in var2cats.items():
            # preallocate
            full_shifts = self.variable_full_shifts(variable)
            evars = list(variations.keys()) if full_shifts else ["nominal"]
            variable_categories = sorted(variable_categories)

            hacc = output["histograms"][variable.name]
            hacc.hint(meta=_locals["events"].metadata)  # needed for save hist_server usage
            hacc.alloc(
                dataset=sorted(set(ds or dsname for ds in datasets.keys())),
                category=sorted(categories.keys()),
                systematic=evars,
            )

            # value
            try:
                val = eval(
                    variable.expression,
                    globals(),
                    ChainMap({}, _locals),
                )
            except Exception as e:
                raise RuntimeError(
                    f"variable {variable.name} (expr: {variable.expression}) erred in {self.get_lfn(_locals['events'])}"
                ) from e

            for dsn, dsfactor in datasets.items():
                fargs = {}
                fargs["dataset"] = dsn or dsname

                for cats in (
                    iter_chunks(variable_categories, 100)
                    if full_shifts and len(variations) > 1
                    else [variable_categories]
                ):
                    with hacc.temp(
                        # dataset=[fargs["dataset"]],
                        category=cats,
                        systematic=evars,  #
                    ) as hist:
                        for variation, modifier in variations.items():
                            if full_shifts or variation == "nominal":
                                weight = weights.weight(modifier=modifier) * dsfactor
                                fargs["systematic"] = variation

                                for category in cats:
                                    cut = selection.all(*categories[category])

                                    fargs["category"] = category
                                    fargs["weight"] = weight[cut]
                                    fargs[variable.name] = val[cut]
                                    hist.fill(**fargs)

        # cutflow histograms
        if jes_shift == "nominal" == dsshift:
            w = weights.weight()
            with output["cutflow"].temp(dataset=[dsname], category=categories) as hist, output[
                "cutflow_raw"
            ].temp(dataset=[dsname], category=categories) as hist_raw:
                for category, cuts in categories.items():
                    for i in range(len(cuts) + 1):
                        if i:
                            cut = selection.all(*cuts[:i])
                            w_cut = np.sum(w * cut if i else w)
                            n_cut = np.sum(cut) if i else w.size
                        else:
                            w_cut = np.sum(w)
                            n_cut = w.size
                        kw = dict(dataset=dsname, category=category)
                        hist.fill(**kw, cutflow=i, weight=w_cut)
                        hist_raw.fill(**kw, cutflow_raw=i, weight=n_cut)

        return output

    def _preExe(self, exe, exeArgs):
        if client := exeArgs.get("client", None):
            hists = self._accumulator["histograms"]
            merger = sorted(
                addr
                for addr in client.ncores().keys()
                if any(ip in addr for ip in exeArgs["merge_workers"])
            )
            assert merger, "no merge workers online!?"

            for var in self.variables:
                hists[var.name].compress()
                if self.variable_full_shifts(var):
                    hists[var.name] = hists[var.name].to_remote(client, workers=merger)

    def _postExe(self, exe, exeArgs, output):
        if "client" in exeArgs:
            hists = output["histograms"]
            for key, hacc in hists.items():
                hists[key] = hacc.to_local()
        return output

    @staticmethod
    def filter_mc_proc(
        analysis_inst,
        hin,
        skip_mc=(),
        proc_data="data",
    ):
        mc_proc = list(hin.axes["process"])
        mc_proc.remove(proc_data)
        if callable(skip_mc):
            mc_proc = list(filter(skip_mc, mc_proc))
        else:
            Pget = analysis_inst.processes.get

            skip_mc = list(map(Pget, skip_mc))
            mc_proc = [
                proc
                for proc in mc_proc
                if Pget(proc) not in skip_mc and not any(root.has_process(proc) for root in skip_mc)
            ]
        return mc_proc

    @classmethod
    def data_mc_substract(
        cls,
        analysis_inst,
        hin,
        mapping,
        proc_name=None,
        proc_data="data",
        data_syst=(),
        skip_mc=(),
        eject=True,
        overwrite=False,
    ):
        assert hin.axes.name[:2] == ("process", "category")
        if proc_name is None:
            proc_name = mapping.__name__
        assert isinstance(proc_name, str)

        if not overwrite:
            # would we need to overwrite?
            overwrite = proc_name in hin.axes["process"]
            assert (
                # we dont want to overwrite
                not overwrite
                # but overwriteing zeros is ok
                or not hin.view(True)[hin.axes["process"].index(proc_name)]["value"].any()
            ), f"process already present: {proc_name}"

        if callable(mapping):
            mapping = {k: mapping(k) for k in hin.axes["category"] if mapping(k)}
        assert not (set(mapping.keys()) - set(hin.axes["category"])), "not all src cat present"
        # sort, sind hdf5 needs it
        mapping = dict(sorted(mapping.items(), key=lambda a: hin.axes[1].index(a[0])))
        cat_src = list(mapping.keys())
        cat_dst = list(mapping.values())

        has_syst = "systematic" in hin.axes.name
        if has_syst:
            assert hin.axes.name[2] == "systematic"
            if callable(data_syst):
                data_syst = list(filter(data_syst, hin.axes["systematic"]))
        else:
            data_syst = ()

        mc_proc = cls.filter_mc_proc(analysis_inst, hin, skip_mc=skip_mc, proc_data=proc_data)

        # get indices
        idx_data = hin.axes["process"].index(proc_data)
        idx_mc = sorted(hin.axes["process"].index(mc_proc))
        idx_cat = hin.axes["category"].index(cat_src)

        # get data/mc for new hist
        hinv = hin.view(flow=True)
        if has_syst:
            data = hinv[idx_data, idx_cat, hin.axes[2].index("nominal")][:, None, ...]
        else:
            data = hinv[idx_data, idx_cat]
        # TODO: optimize slice order/directly reduce
        mc = _to_view(hinv[idx_mc][:, idx_cat])
        mc = mc.sum(axis=0, where=np.isfinite(mc["value"]) & np.isfinite(mc["variance"]))

        # build new hist
        hnew = Hist5.regrow(hin, {"process": [proc_name], "category": cat_dst}, copy=False)

        # (inplace) subtract & inject
        out = np.empty_like(mc)
        np.subtract(data["value"], mc["value"], out=out["value"])
        np.add(data["variance"], mc["variance"], out=out["variance"])
        hnew[proc_name, ...] = out
        del out, data

        # fixup data systematics
        for syst in data_syst:
            # get data systematics
            idx_syst = hin.axes[2].index(syst)
            data = hinv[idx_data, idx_cat, idx_syst, ...].copy()

            #  (inplace) subtract & inject
            data["value"] -= mc[:, idx_syst, ...]["value"]
            data["variance"] += mc[:, idx_syst, ...]["variance"]
            hnew[proc_name, :, syst, ...] = data
            del data

        # build output hist
        if eject:
            cat_new = list(hin.axes["category"])
            for src, dst in mapping.items():
                if src in cat_new:
                    cat_new.remove(src)
                if dst not in cat_new:
                    cat_new.append(dst)

            hout = Hist5.regrow(hin, {"category": cat_new}, copy=True)
        else:
            hout = hin.copy()

        if overwrite and proc_name in hout.axes["process"]:
            Hist5._hiadd(hout, hnew, set=True)
        else:
            hout += hnew

        return hout

    @classmethod
    def regroup(cls, hist, groups={}):
        cats = [c for c in hist.axes["category"] if not re.search(groups.get(None, "x^"), c)]
        # add regrouped categories
        regroups = []
        for dst, pat in groups.items():
            if dst is None:
                continue
            cats_new = set()
            for cat in cats:
                rem = re.sub(pat, dst, cat)
                if rem != cat:
                    assert rem not in cats, f"new category {rem} already existing"
                    cats_new.add(rem)
            regroups.append((dst, pat, cats.copy()))
            cats.extend(sorted(cats_new))

        # regrow histograms once
        h = Hist5.regrow(hist, dict(category=cats))
        hv = h.view(True).view(np.ndarray)

        for dst, pat, cats in tqdm(regroups, desc="regroup", leave=False):
            for cat in tqdm(cats, unit="category", leave=False):
                rem = re.sub(pat, dst, cat)
                if rem != cat:
                    a = view_sdtype_flat(hv[:, h.axes["category"].index(rem)])
                    a[...] += view_sdtype_flat(hv[:, h.axes["category"].index(cat)])
        return h


class ArrayExporter(BaseProcessor):
    output = "*.npy"
    dtype = None
    sep = "_"
    jes_shifts = False

    def __init__(self, task):
        super().__init__(task)

        self._accumulator["arrays"] = dict_accumulator()

    @abstractmethod
    def arrays(self, select_output):
        """
        select_output is the output of self.select
        this function should return an dict of numpy arrays, the "weight" key is reserved
        """
        pass

    def datasets_arrays(self, select_output, dataset):
        """
        select_output is the output of self.select
        dataset is one of the keys of select_output["datasets"] (with default {None: 1})
        this function should return a dict of numpy arrays, that are "dataset" dependent
        """
        return {}

    def categories(self, select_output) -> Dict[str, Union[np.ndarray, slice]]:
        selection = select_output.get("selection")
        categories = select_output.get("categories")
        if selection and categories:
            ret = {cat: selection.all(*cuts) for cat, cuts in categories.items()}
            return {cat: mask for cat, mask in ret.items() if mask.any()}
        else:
            return {"all": slice(None)}

    def process(self, events):
        with MaxRSSWatch(prefix=f"{type(self).__qualname__}:"):
            output = self.accumulator.identity()
            _locals = next(self.select(events))
            assert _locals["unc"] == "nominal"
            categories = self.categories(_locals)
            output = _locals["output"]
            weights = _locals["weights"]
            datasets = _locals.get("datasets", {})
            datasets.setdefault(None, 1)

            def fetch(func, *args):
                arr = func(ChainMap({}, _locals), *args)
                assert all(not a.dtype.hasobject for a in arr.values())
                if self.dtype:
                    arr = {key: array.astype(self.dtype) for key, array in arr.items()}
                return arr

            if categories:
                arrays = fetch(self.arrays)
                weight = weights.weight()

                for category, cut in categories.items():

                    def pack(arrs):
                        assert "weight" not in arrs
                        return {
                            key: array_accumulator(array[cut, ...]) for key, array in arrs.items()
                        }

                    data = pack(arrays)
                    for dsn, dsfactor in datasets.items():
                        if dsn is None:
                            dsid = self.get_dataset(events).id
                        else:
                            dsid = self.campaign_inst.datasets.get(dsn).id

                        w = weight * dsfactor
                        w = w[cut]
                        w = w.astype(self.dtype if self.dtype else weight.dtype)

                        # make it fortran-contiguous since we slice it apart later anyway
                        w = np.stack([np.full(w.shape, dsid, dtype=w.dtype), w], axis=0).T
                        if w.shape[0]:
                            assert w[0, 0] == dsid
                        else:
                            break  # nothing passed the cut, abort this category

                        dat = data.copy()
                        dat.update(pack(fetch(self.datasets_arrays, dsn)))
                        dat["weight"] = array_accumulator(w)

                        output["arrays"] += {category: dict_accumulator(dat)}

            return output

    def postprocess(self, output):
        dsids, weights = np.array(
            sorted(
                (
                    (
                        (ds := self.campaign_inst.datasets.get(dsname)).id,
                        1.0 if ds.is_data else (1.0 / sum_gen_weight),
                    )
                    for (dsname, dsshift), sum_gen_weight in output["sum_gen_weights"].items()
                    if dsshift == "nominal"
                ),
                key=itemgetter(0),
            )
        ).T

        dsids = np.ascontiguousarray(dsids)
        weights = np.ascontiguousarray(weights)

        for arrays in output["arrays"].values():
            dsid, weight = arrays["weight"].value.T
            arrays["weight"] = array_accumulator(weight * weights[np.searchsorted(dsids, dsid)])

        out = {
            self.sep.join((category,) + (key if isinstance(key, tuple) else (key,))): aa
            for category, arrays in output["arrays"].items()
            for key, aa in arrays.items()
        }

        output.clear()
        output.update(out)

    @classmethod
    def live_callback(cls, accumulator):
        return {cat: len(arrays["weight"]) for cat, arrays in accumulator["out"]["arrays"].items()}


class TreeExporter(ArrayExporter):
    output = "sync.root"
    dtype = None

    groups = {}

    @classmethod
    def group_tree(cls, outtree):
        # add regrouped categories
        cats = [c for c in outtree.keys() if c.startswith("is_")]

        regroups = []
        for dst, pat in cls.groups.items():
            if dst is None:
                continue
            cats_new = set()
            for cat in cats:
                rem = re.sub(pat, dst, cat)
                if rem != cat:
                    assert rem not in cats, f"new category {rem} already existing"
                    cats_new.add(rem)
            regroups.append((dst, pat, cats.copy()))
            cats.extend(sorted(cats_new))

        for dst, pat, cats in tqdm(regroups, desc="regroup", leave=False):
            for cat in tqdm(cats, unit="category", leave=False):
                rem = re.sub(pat, dst, cat)
                if rem != cat:
                    outtree[rem] = np.logical_or(outtree.get(rem, 0), outtree[cat])
        return outtree

    @classmethod
    def arrays_to_tree(cls, arrays, target, vars, **kwargs):
        import uproot3

        with uproot3.recreate(target.path) as file:
            tree = defaultdict(list)
            for cat, cat_array in arrays.items():
                # remove broken categories
                if re.search(cls.groups.get(None, "x^"), cat):
                    continue
                for var, _var_array in cat_array.items():
                    # remove special key "weight"
                    if var == "weight":
                        continue
                    var_array = _var_array.value
                    # if tensor in tensors, flatten and name as given
                    if var in cls.tensors.fget(cls).keys():
                        if vars[var][1] == 0:
                            name = f"{var}_" if "part" in vars[var][-1].get("groups", {}) else ""
                            for i, feat in enumerate(vars[var][2]):
                                tree[f"{name}{feat}"].append(var_array[:, i])
                        else:
                            for i in range(vars[var][1]):
                                for j, feat in enumerate(vars[var][2]):
                                    tree[f"{var}{i}_{feat}"].append(var_array[:, i, j])
                    # if not, just save raw values
                    else:
                        tree[f"{var}"].append(var_array)
                for cat_flag in arrays.keys():
                    tree[f"is_{cat_flag}"].append(np.ones(var_array.shape[0]) * (cat == cat_flag))
            outtree = {}
            for v, k in tree.items():
                outtree[v] = np.concatenate(k, axis=-1)
            outtree = cls.group_tree(outtree)
            file["tree"] = uproot3.newtree({n: v.dtype for n, v in outtree.items()})
            file["tree"].extend(outtree)

    def postprocess(self, output):
        pass

    @classmethod
    def save(cls, task, output, **kwargs):
        assert not callable(cls.output)
        target = task.output()
        target.parent.touch()
        cls.arrays_to_tree(
            output["arrays"],
            target=target,
            vars=task.Processor.tensors.fget(task.Processor),
            **kwargs,
        )


class _Preheater(_pABC):
    prefix = "preheat_"

    def __init__(self, proc):
        assert isinstance(proc, BaseProcessor)
        self.proc = proc

    @property
    def accumulator(self):
        return dict_accumulator({})

    def process(self, events):
        from distributed import Lock, Variable, worker_client

        assert isinstance(self.proc, BaseProcessor)
        assert not isinstance(self.proc, _Preheater)

        s = self.proc.get_dataset(events).data_source
        d = self.prefix + s

        with worker_client(separate_thread=False) as c:
            v = Variable(d, c)
            l = Lock(d, c)

            if l.acquire(blocking=False):
                self.proc.process(events)

                cols = set()
                for col in events.materialized:
                    col = col.replace("_", ".", 1)
                    try:
                        attrgetter(col)(events)
                    except AttributeError:
                        pass
                    else:
                        cols.add(col)
                cols = sorted(cols)
                v.set(cols)
                return dict_accumulator({s: set_accumulator(cols)})
            else:
                cols = v.get()

        for ag in map(attrgetter, cols):
            data = ag(events)
            data = getattr(data, "content", data)
            if callable(getattr(data, "materialize")):
                data.materialize()
        return dict_accumulator({})

    def postprocess(self, accumulator):
        return accumulator


class array_accumulator(column_accumulator):
    """column_accumulator with delayed concatenate"""

    def __init__(self, value):
        self._empty = value[:0]
        self._value = [value]

    def __repr__(self):
        return "%s(%r)" % (self.__class__.__name__, self.value)

    def identity(self):
        return self.__class__(self._empty)

    def add(self, other):
        assert (
            self._empty.shape == other._empty.shape
        ), f"shape mismatch: {self._empty.shape} != {other._empty.shape}"
        assert (
            self._empty.dtype == other._empty.dtype
        ), f"dtype mismatch: {self._empty.dtype} != {other._empty.dtype}"
        self._value.extend(v for v in other._value if len(v))

    @property
    def value(self):
        if len(self._value) > 1:
            self._value = [np.concatenate(self._value)]
        return self._value[0]

    def __len__(self):
        return sum(map(len, self._value))


class hist_accumulator(AccumulatorABC):
    """accumulator for hist"""

    def __init__(self, hist):
        assert isinstance(hist, (Hist, Hist5))
        self.hist = hist

    def identity(self):
        return self.__class__(self.hist.copy().reset())

    def add(self, other):
        if isinstance(other, self.__class__):
            other = other.hist
        # neither are Hist5 and would produce more than 1 dataset
        if (
            not any(isinstance(h, Hist5) for h in (self.hist, other))
            and all(isinstance(h.axes[0], BaseCategory) for h in (self.hist, other))
            and len(set(self.hist.axes[0]) | set(other.axes[0])) > 1
        ):
            # convert the bigger one to Hist5
            if other.size <= self.hist.size:
                self.hist = Hist5.from_hist(self.hist)
            else:
                other = Hist5.from_hist(other)
        # flip merge order, if:
        if isinstance(other, Hist5) and (
            not isinstance(self.hist, Hist5)  # merging Hist5 onto normal Hist
            or other.size > self.hist.size  # merging onto smaller Hist5
        ):
            self.hist, other = other.copy(), self.hist
        # always convert other to in-memory
        if isinstance(other, Hist5):
            other = Hist5.regrow(other, cls=Hist)
        # pre-inject new axes to avoid repeadet recompression
        if isinstance(self.hist, Hist5):
            other = Hist5.regrow(
                other,
                cls=Hist,
                categories={
                    ax0.name: list(ax0) + [c for c in ax1 if c not in ax0]
                    for ax0, ax1 in zip(self.hist.axes[1:-1], other.axes[1:-1])
                    if ax0.traits.growth
                    and ax1.traits.growth
                    and isinstance(ax0, BaseCategory)
                    and isinstance(ax1, BaseCategory)
                },
            )
        self.hist += other

    def _empty(self, categories=None):
        return Hist5.regrow(
            self.hist,
            cls=hist.Hist,
            categories=categories or min,
            copy=False,
        )

    @contextmanager
    def temp(self, **categories):
        tmp = self._empty(categories)
        yield tmp
        self.add(tmp)

    def alloc(self, **categories):
        if not isinstance(self.hist, Hist5):
            upd = {}
            for dim, catWant in categories.items():
                catHave = self.hist.axes[dim]
                catNew = [cat for cat in catWant if cat not in catHave]
                if catNew:
                    upd[dim] = list(catHave) + catNew
            if upd:
                self.hist = Hist5.regrow(self.hist, categories=upd)

    def hint(self, meta):
        pass

    def to_remote(self, client, workers):
        rhist = self.hist
        if isinstance(rhist, Hist5):
            rhist = rhist.to_hist()
        return hist_client(
            self.hist,
            [
                future.result()
                for future in [
                    client.submit(hist_server, rhist, pure=False, actor=True, workers=[worker])
                    for worker in workers
                ]
            ],
        )

    def to_local(self):
        return self

    def compress(self):
        if not isinstance(self.hist, Hist5):
            self.hist = Hist5.from_hist(self.hist)

    @property
    def as_hist(self):
        if isinstance(self.hist, Hist5):
            return self.hist.to_hist()
        return self.hist


hist5_opts = dict(f_opts=dict(rdcc_nbytes=500 << 20, rdcc_nslots=1_000_003))


class hist_client(hist_accumulator):
    def __init__(self, hist, actors):
        super().__init__(hist)
        assert 0 in self.hist.view(True).shape
        self._actors = actors

    def identity(self):
        return self.__class__(self.hist.copy().reset(), self._actors)

    def add(self, other, /, key=None):
        if isinstance(other, hist_accumulator):
            other = other.hist
        assert 0 in other.view(True).shape

    def hint(self, meta):
        self._hintkey = tokenize(meta["filename"], meta["entrystart"])
        self.actor = self._actors[int(self._hintkey[:10], 16) % len(self._actors)]

    def alloc(self, **categories):
        return self.actor.alloc(**categories)

    def to_local(self):
        ret = hist_accumulator(self.hist.copy())
        acc = Hist5.from_hist(self._empty(), **hist5_opts)
        if isinstance(self.hist.axes[0], BaseCategory) and len(self._actors) > 1:
            # fetch keys
            keys = set()
            for actor in self._actors:
                keys.update(actor._ax0list)
            keys = sorted(keys)

            # prepare
            acc.grow({acc.axes[0].name: keys})

            # accumulate
            for key in tqdm(keys, desc="collect", unit="key"):
                for future in [actor._ax0part(key) for actor in self._actors]:
                    res = future.result()
                    if res is not None:
                        acc += res

        else:
            # plain accumulation
            for actor in tqdm(self._actors, unit="actor", disable=len(self._actors) < 2):
                acc += actor.hist
        ret.add(acc)
        del acc
        return ret

    @contextmanager
    def temp(self, **categories):
        tmp = self._empty(categories)
        yield tmp
        # tmp = Hist5.from_hist(tmp)  # compress
        self.actor.add(tmp, tokenize(self._hintkey, tuple(map(tuple, tmp.axes))))


class hist_server:
    hist = None  # make hist remote accessible

    def __init__(self, hist):
        self.seen = set()
        self.hist = Hist5.from_hist(hist, **hist5_opts)

    def add(self, other, key):
        if key in self.seen:
            return False
        self.seen.add(key)
        if isinstance(other, hist_accumulator):
            other = other.hist
        self.hist += other

    def alloc(self, **categories):
        self.hist.grow(categories)

    @property
    def _ax0(self):
        ax0 = self.hist.axes[0]
        assert isinstance(ax0, BaseCategory)
        return ax0

    @property
    def _ax0list(self):
        return list(self._ax0)

    def _ax0part(self, key0):
        gc.collect()
        if key0 in self._ax0:
            return self.hist.regrow(cls=hist.Hist, categories={self._ax0.name: [key0]})


# https://github.com/CoffeaTeam/coffea/blob/c940f4118bb0fbbd7e2ae4fe27a7ddcd80634c2b/coffea/hist/hist_tools.py#L1244-L1280
def scale(self, factor, axis=None):
    assert isinstance(self, hist.Hist)
    """Scale histogram in-place by factor

    Parameters
    ----------
        factor : float or dict
            A number or mapping of identifier to number
        axis : optional
            Which (sparse) axis the dict applies to

    Examples
    --------
    This function is useful to quickly reweight according to some
    weight mapping along a sparse axis, such as the ``species`` axis
    in the `Hist` example:

    >>> h.scale({'ducks': 0.3, 'geese': 1.2}, axis='species')
    """
    if self._sumw2 is None:
        self._init_sumw2()
    if isinstance(factor, Number) and axis is None:
        for key in self._sumw.keys():
            self._sumw[key] *= factor
            self._sumw2[key] *= factor**2
    elif isinstance(factor, dict):
        if not isinstance(axis, tuple):
            axis = (axis,)
        axis = tuple(map(self.axis, axis))
        kget = itemgetter(*map(self._isparse, axis))
        factor = {tuple(a.index(e) for a, e in zip(axis, k)): v for k, v in factor.items()}
        for key in self._sumw.keys():
            fkey = kget(key)
            if fkey in factor:
                self._sumw[key] *= factor[fkey]
                self._sumw2[key] *= factor[fkey] ** 2
    elif isinstance(factor, np.ndarray):
        axis = self.axis(axis)
        raise NotImplementedError("Scale dense dimension by a factor")
    else:
        raise TypeError("Could not interpret scale factor")


def mk_dense_evaluator(values, edges=None):
    if edges is not None:
        if not isinstance(edges, dict):
            edges = {k: edges for k in values.keys()}
        values = {k: (v, edges[k]) for k, v in values.items()}

    return evaluator(
        names={k: k for k in values.keys()},
        types={k: "dense_lookup" for k in values.keys()},
        primitives=values,
    )


def hists2dense_evaluator(hists, err_suffix=""):
    values = {}
    for k, h in hists.items():
        edges = [
            np.r_[
                -np.inf if ax.traits.underflow else slice(None),
                ax.edges,
                np.inf if ax.traits.overflow else slice(None),
            ]
            for ax in h.axes
        ]
        if len(edges) == 1:
            edges = edges[0]  # circumvent supid coffea behaviour
        values[k] = (h.view(True)["value"].copy(), edges)
        if err_suffix:
            assert k + err_suffix not in values
            values[k + err_suffix] = (np.sqrt(h.view(True)["variance"]), edges)
    return mk_dense_evaluator(values=values)


class Efficency(AccumulatorABC):
    @classmethod
    def from_axes(cls, *axes) -> "Efficency":
        return cls(
            hist.Hist(
                *axes,
                hist.axis.Boolean(name="passing"),
                storage=hist.storage.Weight(),
            ),
            set(),
        )

    @classmethod
    def from_TGraphAsymmErrors(
        cls, obj, normal=False, cl=norm.cdf(1) - norm.cdf(-1)
    ) -> "Efficency":
        assert obj.classname == "TGraphAsymmErrors"
        assert 0 < cl < 1
        cl_lo = (1 - cl) / 2
        cl_hi = (1 + cl) / 2

        # x-Axis
        name, label, _ = obj.all_members.get("fTitle", ";;").split(";")
        name = obj.all_members.get("fName", name)

        x = obj.values("x")
        xerr_lo = obj.errors("low", "x")
        xerr_hi = obj.errors("high", "x")

        assert np.allclose(xerr_lo, xerr_hi), "unexpected asymmetric bin widths (x-errors)"

        if np.allclose(w := xerr_hi + xerr_lo, w_mean := w.mean()):  # regular axis
            x_lo = x.min() - w_mean / 2
            x_hi = x.max() + w_mean / 2
            x_tot = x_hi - x_lo
            n_bin = int(np.round(x_tot / w_mean))

            axis = hist.axis.Regular(n_bin, x_lo, x_hi, label=label)
        else:
            edges = np.unique([x - xerr_lo, x + xerr_hi])
            assert not np.any(np.isclose(np.diff(edges), 0)), "almost identical edges (precision?)"

            axis = hist.axis.Variable(edges, label=label)

        # y-Axis
        eff = obj.values("y")
        err_lo = obj.errors("low", "y")
        err_hi = obj.errors("high", "y")

        assert np.all(0 <= eff), "illegal efficency (<0)"
        assert np.all(eff <= 1), "illegal efficency (>1)"
        assert np.all(0 <= eff - err_lo), "illegal lower error reach (<0)"
        assert np.all(eff + err_hi <= 1), "illegal lower error reach (>1)"

        empty = (eff == 0) & (err_lo == 0) & (err_hi == 0)
        effF = eff[~empty]

        if normal:
            err_max = np.maximum(err_lo, err_hi)
            err_maxF = err_max[~empty]
            bounded = err_max >= np.minimum(eff, 1 - eff)
            assert np.all(np.isclose(err_lo, err_hi) | bounded), "unexpected asymmetric errors"

            def fun(ltot):
                tot = np.exp(ltot)

                a = effF * tot
                b = tot - a

                # va = beta.var(a, b)
                va = a * b / (tot * tot * (tot + 1))  # same as beta.var, but 10x faster

                return np.sqrt(va) - 2 * err_maxF

        else:  # Clopper-Pearson
            err_tot = err_hi + err_lo
            err_totF = err_tot[~empty]

            def fun(ltot):
                tot = np.exp(ltot)

                a = effF * tot
                b = tot - a

                cp_hi = np.where(b, beta.ppf(cl_hi, a + 1, b), 1)
                cp_lo = np.where(a, beta.ppf(cl_lo, a, b + 1), 0)
                cp_tot = cp_hi - cp_lo

                return cp_tot - err_totF

        # find total count
        res = newton(fun, x0=np.full_like(effF, 2), x1=np.full_like(effF, 5))
        tot = np.zeros_like(eff)
        tot[~empty] = np.exp(res)

        # build return value
        ret = cls.from_axes(axis)
        ret.weights.add(1)

        view = ret.hist.view(False)
        idx = axis.index(x)
        a = eff * tot
        b = tot - a
        view["value"][idx] = view["variance"][idx] = np.stack([b, a], axis=-1)

        return ret

    # unbound call makes sense too
    def unify(*effs: "Efficency", axis: int) -> Tuple["Efficency", ...]:
        rets = tuple(eff.identity() for eff in effs)
        for r, e, h in zip(rets, effs, unify(*(eff.hist for eff in effs), axis=axis)):
            r.weights = e.weights.copy()
            r.hist = h
        return rets

    def __init__(self, hist: hist.Hist, weights: Set[int]) -> None:
        super().__init__()
        self.hist = hist
        self.weights = weights

    def identity(self):
        return self.__class__(
            self.hist.copy().reset(),
            set(),
        )

    def __add__(self, other):
        return self.__class__(
            self.hist + other.hist,
            self.weights | other.weights,
        )

    def __mul__(self, other):
        assert isinstance(other, float)
        return self.__class__(
            self.hist * other,
            set(w * other for w in self.weights),
        )

    def add(self, other):
        if other is not None:
            self.hist += other.hist
            self.weights |= other.weights

    def mode_shift(self, sigma: float, flow: bool = False) -> "Efficency":
        """
        returns a mode-shifted distribution such that:
          x.shifted(sigma).mode() == x.val(sigma)
        WARNING: this
        """

        tot = self.hist[..., ::sum].view(flow)
        eff = self.val(sigma, flow)

        ret = self.identity()
        ret.weights = self.weights.copy()
        view = ret.hist.view(flow)
        for i, v in enumerate([1 - eff, eff]):
            for k in ["value", "variance"]:
                view[..., i][k] = tot[k] * v

        return ret

    def fill(self, passing: np.ndarray, weight: np.ndarray = None, **kwargs):
        kwargs["passing"] = passing
        if weight is not None:
            kwargs["weight"] = weight
            uw = np.unique(weight).tolist()
        else:
            uw = ()
        self.hist.fill(**kwargs)
        self.weights.update(uw)

    def ab(self, flow=False) -> Tuple[np.ndarray, np.ndarray]:
        w = np.array(list(self.weights))
        n = w.sum() / (w**2).sum()
        p = 1 + n * self.hist.view(flow)["value"]
        assert self.hist.axes.name[-1] == "passing"
        return p[..., 1], p[..., 0]

    def good(self, min_fill=0, flow=False) -> np.ndarray:
        a, b = self.ab(flow)
        return (a + b) > (2 + min_fill)

    def val(self, sigma, flow=False):
        ab = self.ab(flow)
        if sigma is None:
            return beta.mean(*ab)
        else:
            return beta.ppf(norm.cdf(sigma), *ab)

    def mode(self, flow=False):
        a, b = self.ab(flow)
        return (a - 1) / (a + b - 2)

    def cp(self, cl: float = norm.cdf(1) - norm.cdf(-1), flow: bool = False):
        assert 0 < cl < 1
        cl_lo = (1 - cl) / 2
        cl_hi = (1 + cl) / 2

        a, b = self.ab(flow) - 1  # use unbiased one
        tot = a + b

        eff = np.divide(a, tot, where=tot, out=np.zeros_like(tot))
        cp_hi = np.where(b, beta.ppf(cl_hi, a + 1, b), np.where(tot, 1, 0))
        cp_lo = np.where(a, beta.ppf(cl_lo, a, b + 1), 0)

        return eff, cp_hi, cp_lo

    @property
    def axis1D(self):
        assert len(self.hist.axes) == 2
        return self.hist.axes[0]

    def x_linspace(self, num=250):
        return np.linspace(*self.axis1D.edges[[0, -1]], num=num, endpoint=True)

    class OFunc:
        def __class_getitem__(cls, slices):
            if not isinstance(slices, tuple):
                slices = (slices,)
            return partial(cls, init_bounds=slices)

        def __init__(
            self,
            func: Callable[[Sequence[Number]], Callable[[np.ndarray], np.ndarray]],
            init: Optional[Sequence[Number]] = None,
            bounds: Optional[Sequence[Tuple[Optional[Number], Optional[Number]]]] = None,
            *,
            init_bounds: Optional[Sequence[slice]] = None,
        ) -> None:
            assert callable(func)
            assert init_bounds is None or (init is None and bounds is None)

            if init_bounds is not None:
                init = [s.step for s in init_bounds]
                bounds = [(s.start, s.stop) for s in init_bounds]

            self.func = func
            self.init = init
            self.bounds = bounds

        def __getitem__(self, slices):
            return self.__class__[slices](func=self.func)

        def bound_func(self, *args, _factor=1):
            assert self.bounds is not None
            assert len(self.bounds) == len(args)
            oob = 0
            call_args = []
            for (lo, hi), arg in zip(self.bounds, args):
                if lo is not None and arg < lo:
                    oob += abs(arg - lo)
                    arg = lo
                if hi is not None and hi < arg:
                    oob += abs(arg - hi)
                    arg = hi
                call_args.append(arg)
            return self.func(*call_args) + _factor * oob

    @dataclass
    class Func:
        func: Callable
        p: np.ndarray
        err: np.ndarray = None
        cov: np.ndarray = None
        up: "Efficency.Func" = None
        down: "Efficency.Func" = None

        def __call__(self, x: np.ndarray, err: float = None) -> np.ndarray:
            p = self.p
            if err is not None:
                assert self.err, "required parameter errors not available"
                p = p + err * self.err
            return np.clip(self.func(x, *p), 0, 1)

        def dice(
            self,
            x: np.ndarray,
            sigma: np.ndarray = (1, 0, -1),
            toys: int = 1000,
            rng: np.random.Generator = np.random,
        ) -> np.ndarray:
            """
            Generates *toys* toys using *rng* and evaluates each at all given *x*.
            Calulates the percentiles accoring to *sigma* (1D) points ot the normal distribution.
            The returned result has the shape *(len(sigma),) + x.shape*.
            """
            assert self.cov is not None, "requried covariance matrix not available"
            x, sigma = np.atleast_1d(x), np.atleast_1d(sigma)
            assert x.dims == 1, "x has too many dimensions"
            assert sigma.dims == 1, "sigma has too many dimensions"

            p = rng.multivariate_normal(self.p, self.cov, size=toys)
            toys = np.clip(self.func(x[..., None], *np.moveaxis(p, -1, 0)), 0, 1)
            res = np.nanpercentile(toys, 100 * norm.cdf(sigma), axis=-1, keepdims=True)
            return np.moveaxis(res, -1, 0)

        def ucd(
            self,
            x: np.ndarray,
            sigma: Optional[float] = None,
            toys: Optional[int] = None,
            rng: np.random.Generator = np.random,
        ) -> np.ndarray:
            """
            Returns the up, central, and down shfits, output shape is *(3, ) + x.shape*.
            If *sigma* is *None* use shapes given by *up* and *down* (or central if unavailable).
            Otherwise calculate them according to *sigma* using *err*, or if *toys* is given via *dice*.
            """
            if sigma is None:
                c = self(x)
                return np.stack([self.up(x) if self.up else c, c, self.down(x) if self.down else c])
            assert 0 < sigma
            ucd = [sigma, 0, -sigma]
            if toys is None:
                return np.stack([self(x, err=s) for s in ucd])
            else:
                return self.dice(x, sigma=ucd, toys=toys, rng=rng)

        def plot(
            self,
            axs,
            x: np.ndarray,
            fill: Union[Dict, None, Literal[False]] = None,
            ucd_kwargs: Dict = {},
            **kwargs,
        ):
            if fill is not False:
                up, nom, down = self.ucd(x, **ucd_kwargs)
            else:
                nom = self(x)

            def nf(n):
                return f"{{{n:.4g}}}".replace("e", r"}\cdot10^{")

            if "%s" in kwargs.get("label", ""):
                pnames = getattr(self.func, "pnames", [f"p{i}" for i in range(len(self.p))])
                kwargs["label"] = kwargs["label"] % "\n".join(
                    rf"{n}: ${nf(p)}\pm{{}}{nf(e)}$" for p, e, n in zip(self.p, self.err, pnames)
                )

            axs.plot(x, nom, **kwargs)
            if fill is not False:
                if fill is None:
                    fill = dict(alpha=0.3, label=None, linewidth=0)
                up, down = np.minimum(up, down), np.maximum(up, down)
                axs.fill_between(x, down, up, **dict(kwargs, **fill))

    # def fit(self, func: Callable, p0: Sequence[float], **kwargs) -> Func:
    def fit(self, fun: Callable, x0: Sequence[Number], bounds=None, **kwargs) -> Func:
        x = self.axis1D.centers
        ab = np.array(self.ab(False))

        good = self.good(min_fill=-1)
        x = x[good]
        ab = ab[:, good]

        def nll(p):
            y = fun(x, *p)
            y = np.clip(y, 0, 1)  # ensure withing valid range
            y = np.nextafter(y, 0.5)  # bump away from (invlaid) extrema
            return -2 * beta.logpdf(y, *ab).sum()

        kwargs.update(fun=nll, x0=x0, bounds=bounds)
        opts = kwargs.setdefault("options", {})
        opts.setdefault("maxfun", 10_000)
        opts.setdefault("stra", 2)

        res = iminuit.minimize(**kwargs)

        for i in range(20):  # repeatedly try to improve the hesse matrix
            res.minuit.hesse()

        err = np.array(res.minuit.errors)
        cov = np.array(res.minuit.covariance)

        return self.Func(fun, res.x, err, cov)

    def plot(self, axs, min_fill=-1, offset=0, cp=False, **kwargs):
        good = self.good(min_fill=min_fill)
        if cp:  # Clopper-Pearson interval
            val, *err = self.cp()
            err = abs(np.array(err) - val)
        else:
            val = self.val(0)
            err = abs(self.val([[-1], [1]]) - val)
        return axs.errorbar(
            **dict(
                x=self.axis1D.centers[good],
                y=val[good],
                yerr=err[:, good],
                xerr=self.axis1D.widths[good] / 2,
                **kwargs,
            )
        )


def akm(value, *ops):
    """akward Magic!"""
    for op in ops:
        if isinstance(op, (type, np.dtype)):
            if not isinstance(op, (np.generic, np.dtype)):
                op = np.dtype(op)  # fail if op is type but not dtype-able
            value = ak.values_astype(value, op)
        elif op in [any, all, sum]:
            value = getattr(ak, op.__name__)(value, axis=-1)
        elif op is np:
            value = ak.to_numpy(value)
        else:
            raise ValueError(f"unkown op: {op!r}")
    return value


def reduce_or(*items):
    return reduce(or_, items[0] if len(items) == 1 else items)


def reduce_and(*items):
    return reduce(and_, items[0] if len(items) == 1 else items)
