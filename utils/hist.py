# coding: utf-8

from functools import partial
import itertools
from typing import Optional

import numpy as np
import scipy
from scinum import Number, UP, DOWN
from hist import Hist
from hist.axis import Regular, Variable, AxisProtocol

from utils.util import view_sdtype_flat


def reduce_along_axis(func, axis, h):
    axes = np.array(h.axes, dtype=object)
    axes = np.delete(axes, axis)
    hnew = Hist(*axes)
    hnew.view()[:] = np.apply_along_axis(func, axis, h.view())
    return hnew


def _kstest(arr):
    with np.errstate(invalid="ignore"):
        return scipy.stats.kstest(arr[0, :], arr[1, :]).statistic
        return scipy.stats.chisquare(arr[0, :], arr[1, :]).statistic


def kstest(h1, h2, compare_axis, feature_axis):
    assert feature_axis == -1, "Other axes not implemented yet"
    axes1 = np.array(h1.axes, dtype=object)
    axes2 = np.array(h2.axes, dtype=object)
    # currently only works for StrCategory
    compared_axis = [
        f"{a}_{b}" for (a, b) in itertools.product(axes1[compare_axis], axes2[compare_axis])
    ]
    new_axes = np.copy(axes1)
    new_axes[compare_axis] = type(axes1[compare_axis])(compared_axis)
    new_axes = np.delete(new_axes, feature_axis)
    hnew = Hist(*new_axes)

    a1, a2 = h1.view(), h2.view()

    c, d = np.broadcast_arrays(
        np.expand_dims(a1, compare_axis), np.expand_dims(a2, compare_axis + 1)
    )

    s = np.stack([c, d], axis=-2)
    # reshape
    o = np.array(s.shape)
    o[compare_axis] = o[compare_axis] * o[compare_axis + 1]
    o = np.delete(o, compare_axis + 1)
    s = np.reshape(s, o)

    # perform test
    kstest = np.vectorize(_kstest, signature="(2,n)->()")
    hnew.view()[:] = kstest(s["value"])
    return hnew


def hv2sn(hview: np.ndarray) -> Number:
    return Number(hview["value"], np.sqrt(hview["variance"]))


def sn2hv(snum: Number) -> np.ndarray:
    n, u, d = np.broadcast_arrays(snum.nominal, *snum.get_uncertainty())
    assert np.array_equal(u, d, equal_nan=True), "can't convert asymmetric uncertainties"
    ret = np.empty(shape=n.shape, dtype=[("value", n.dtype), ("variance", u.dtype)])
    ret["value"] = n
    ret["variance"] = np.square(u)
    return ret


def edges_with_flow(axis: AxisProtocol) -> np.ndarray:
    ret = axis.edges
    if axis.traits.underflow:
        ret = np.r_[-np.inf, ret]
    if axis.traits.overflow:
        ret = np.r_[ret, np.inf]
    return ret


def reax(
    hist: Hist,
    idx: int,
    axis: AxisProtocol,
    retain_metadata: bool = True,
    eps: float = 1e-5,
    split_fill: Optional[float] = None,
) -> Hist:
    """
    In *hist* replace the axis at *idx* with a new one *axis* and optionally *retain_metadata*.
    If filled bin would need to be split, fill it instead with *split_fill* or raise an *ValueError*
    if it is *None* (the default).
    """
    axO = hist.axes[idx]
    axN = axis
    if retain_metadata:
        axN._ax.metadata.clear()
        axN._ax.metadata.update(axO._ax.metadata)

    axs = list(hist.axes)
    axs[idx] = axN
    ret = Hist(*axs, storage=hist._storage_type(), metadata=hist.metadata)

    vO = hist.view(True)
    vN = ret.view(True)

    def idx(i: int):
        return (slice(None),) * axs.index(axN) + (i,)

    edO = edges_with_flow(axO)
    edN = edges_with_flow(axN)

    iLO = np.searchsorted(edN, edO[:-1] + eps, side="right") - 1
    iRO = np.searchsorted(edN, edO[1:] - eps, side="left") - 1

    for o, (l, r) in enumerate(zip(iLO, iRO)):
        if l == r:  #  old bin ends up in same new bin
            vN[idx(l)] += vO[idx(o)]
        else:
            assert not np.any(view_sdtype_flat(vO[idx(o)])), "can't split non-empty bins"

    return ret


def any_fill_on_axis(hist: Hist, axis: int) -> np.ndarray:
    view = hist.view(True)
    flat = view_sdtype_flat(view)
    axes = list(range(view.ndim))
    del axes[axis]  # dont reduce the axis of interes
    if flat.ndim > view.ndim:  # include extra axis from flattening
        axes.append(view.ndim)
    return np.any(flat, axis=tuple(axes))


def unify(*hists, axis: int, eps: float = 1e-5, allow_split_empty: bool = True):
    if len(hists) < 2:
        return hists
    axs_old = [h.axes[axis] for h in hists]
    if all(a == axs_old[0] for a in axs_old):
        return hists

    eq = partial(np.isclose, rtol=0, atol=eps)

    edges = np.unique(np.concatenate(list(map(edges_with_flow, axs_old))))

    if np.any(bad := eq(edges[:-1], edges[1:])):  # remove (almost) identical edges
        edges = edges[np.r_[True, ~bad]]

    # eliminate edges that lie in filled bins
    for axO, h in zip(axs_old, hists):
        good = np.any(eq(edges[:, None], edges_with_flow(axO)), axis=1)  # has matching edge
        if not np.all(good) and allow_split_empty:
            idx = axO.index(edges) + int(axO.traits.underflow)
            good |= ~any_fill_on_axis(h, axis)[idx]  # edge lies in an emtpy bin
        if not np.all(good):
            edges = edges[good]

    if underflow := eq(edges[0], -np.inf):
        edges = edges[1:]
    if overflow := eq(edges[-1], np.inf):
        edges = edges[:-1]

    assert np.all(np.isfinite(edges))

    if np.allclose(w := np.diff(edges), w.mean()):
        axg = Regular(
            len(edges) - 1, edges.min(), edges.max(), underflow=underflow, overflow=overflow
        )
    else:
        axg = Variable(edges, underflow=underflow, overflow=overflow)

    return tuple(reax(h, axis, axg, eps=eps) for h in hists)
