"""
analysis configuration interface

must provide:
  - pickleable
  - no hidden states
  - copy
"""

from __future__ import annotations

import logging
import re
from collections.abc import MutableMapping
from copy import copy, deepcopy
from dataclasses import InitVar, dataclass, field
from typing import (
    Any,
    Dict,
    Generic,
    Iterator,
    List,
    Optional,
    Set,
    Tuple,
    Type,
    TypeVar,
    Union,
)

from rich.console import Console
from rich.table import Table
from scinum import Number

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


console = Console()


def rgb(r, g, b):
    """
    This function norms the rgb color inputs for
    matplotlib in case they are not normalized to 1.
    Otherwise just return inputs as tuple.
    Additionally this method displays the color inline,
    when using the atom package "pigments"!
    """
    return tuple(v if v <= 1.0 else float(v) / 255.0 for v in (r, g, b))


class DuplicateKeyError(KeyError):
    def __init__(self, msg: str) -> None:
        super().__init__(msg)


class UniqueKeyDict(MutableMapping):
    def __init__(self, acitype, *args, **kwargs):
        self.acitype = acitype
        self._dict = dict(*args, **kwargs)

    def __getitem__(self, key):
        return self._dict[key]

    def __setitem__(self, key, value):
        # only complain when the dataclass instance is really different
        if key in self and self[key] is not value:
            raise DuplicateKeyError(
                f"{value} already exists for ACI-Type '{self.acitype.__name__}'."
            )
        self._dict[key] = value

    def __delitem__(self, key):
        del self._dict[key]

    def __iter__(self):
        return iter(self._dict)

    def __len__(self):
        return len(self._dict)


@dataclass(eq=False)
class ACIBase:
    name: str
    id: Optional[int] = None
    label: Optional[str] = None
    label_short: Optional[str] = None
    tags: Set = field(default_factory=set)
    aux: Dict = field(default_factory=dict, repr=False)

    def __post_init__(self) -> None:
        # label defaults
        if self.label is None:
            self.label = self.name

        if self.label_short is None:
            self.label_short = self.label

    def copy(self, **kwargs) -> ACIBase:
        new = deepcopy(self)
        for k, v in kwargs.items():
            setattr(new, k, v)
        return new


ACIType = TypeVar("ACIType", bound=ACIBase)


class ACICachedProperty:
    _touched: int = 0
    _last: int = -1

    def __init__(self, func) -> None:
        self.func = func

    def __set__(self, inst, key, value):
        raise AttributeError("can't set attribute")

    def __get__(self, inst, cls) -> ACICachedProperty:
        if inst is None:
            return self

        key = self.func.__name__
        if ACICachedProperty._touched == ACICachedProperty._last and key in inst.__dict__:
            return inst.__dict__[key]
        else:
            result = inst.__dict__[key] = self.func(inst)
            ACICachedProperty._last = ACICachedProperty._touched
            return result


def invalidate_cache():
    ACICachedProperty._touched += 1
    if ACICachedProperty._touched == ACICachedProperty._last:
        logger.warning("Cache was not invalidated. Please call `invalidate_cache()` once more!")
    else:
        logger.warning("Cache is invalidated!")


class ACIContainer(Generic[ACIType]):
    def __init__(
        self,
        inst: ACIType,
        backref: Optional[str] = None,
        acitype: Type[ACIType] = ACIBase,
    ) -> None:
        self.inst = inst
        self._acitype = acitype
        self._backref = backref
        self._data_byname: UniqueKeyDict = UniqueKeyDict(acitype=acitype)
        self._data_byid: UniqueKeyDict = UniqueKeyDict(acitype=acitype)

    @property
    def backref(self) -> Optional[str]:
        return self._backref

    @property
    def keys(self) -> List[str]:
        return list(self._data_byname.keys())

    @property
    def values(self) -> List[ACIType]:
        return list(self._data_byname.values())

    @property
    def acitype(self) -> Type[ACIType]:
        return self._acitype

    def table(self, n=None):
        """
        Pretty print a table of the container content.
        """
        table = Table()
        table.add_column("No.", justify="left")
        table.add_column("Name", justify="left", style="magenta")
        table.add_column("Object", justify="left", style="green")
        assert n is None or isinstance(n, int)
        if n is None or n >= len(self):
            for i, name in enumerate(self.keys):
                table.add_row(str(i + 1), name, repr(self._data_byname[name]))
        else:
            firsts, lasts = self.keys[:n], self.keys[-n:]
            for i, first in enumerate(firsts):
                table.add_row(str(i + 1), first, repr(self._data_byname[first]))
            table.add_row("...", "...", "...")
            for i, last in enumerate(lasts):
                table.add_row(str(len(self) - n + i + 1), last, repr(self._data_byname[last]))
        console.print(table)

    def __repr__(self) -> str:
        return f"{type(self).__name__}(acitype={self.acitype}, len={len(self)})"

    def __len__(self) -> int:
        return len(self._data_byname)

    def extend(self, objs: List[ACIType]) -> ACIContainer:
        """
        Extend a `ACIContainer` by a iterable of objects of type `acitype` and `ACIContainer`.
        """
        assert all(isinstance(obj, ACIBase) for obj in objs)
        for obj in objs:
            self.add(obj)
        return self

    def add(self, obj: ACIType) -> ACIContainer:
        """
        Add an object of type `acitype` to the `ACIContainer`.
        """
        assert isinstance(obj, self._acitype)

        # add to self
        self._data_byname[obj.name] = obj
        if obj.id is not None:
            self._data_byid[obj.id] = obj
        logger.debug(f"Add {obj} to {self}")
        ACICachedProperty._touched += 1

        # add to backref
        if self.backref is not None:
            assert hasattr(obj, self.backref)
            backref = getattr(obj, self.backref)
            backref._data_byname[self.inst.name] = self.inst
            logger.debug(f"Add {obj} to {getattr(obj, self.backref)} (backref)")
            ACICachedProperty._touched += 1
        return self

    def new(self, *args, **kwargs) -> ACIType:
        """
        Create an object of type `acitype` by passing all parameters to it and `add`s it.
        Return the created object.
        """
        obj = self._acitype(*args, **kwargs)
        self.add(obj)
        return obj

    def remove(self, obj: ACIType) -> ACIContainer:
        """
        Remove an object of type `acitype` from the `ACIContainer`.
        """
        assert isinstance(obj, self._acitype)

        # remove from self
        del self._data_byname[obj.name]
        logger.debug(f"Remove {obj} from {self}")
        ACICachedProperty._touched += 1

        # remove from backref
        if self.backref is not None:
            assert hasattr(obj, self.backref)
            backref = getattr(obj, self.backref)
            del backref._data_byname[self.inst.name]
            logger.debug(f"Remove {obj} from {getattr(obj, self.backref)} (backref)")
        ACICachedProperty._touched += 1
        return self

    def get(self, name_or_id: Union[str, int], default: Any = None) -> ACIType:
        """
        Get an object by its name from the `ACIContainer`.
        """
        if isinstance(name_or_id, str):
            return self._data_byname.get(name_or_id, default)
        elif isinstance(name_or_id, int):
            return self._data_byid.get(name_or_id, default)
        else:
            raise TypeError(f"type(name_or_id) must be str or int, but is {type(name_or_id)}")

    def __getitem__(self, name_or_id: Union[str, int]) -> ACIType:
        """
        Get an object by its name from the `ACIContainer`.
        """
        obj = self.get(name_or_id, default=None)
        if obj is None:
            raise KeyError(name_or_id)
        return obj

    def __iter__(self) -> Iterator[ACIType]:
        """
        Iterate over the objects in the `ACIContainer`.
        """
        return iter(self._data_byname.values())

    def query(self, name: str, tags: Set[str] = set()) -> ACIContainer:
        """
        Get objects of type `acitype` from the `ACIContainer` by a regex of name and a set of tags.
        Returns a new `ACIContainer` containing only the objects with queried tags.
        """
        args = f"name='{name}', tags={tags}"
        assert isinstance(name, str)
        regex = re.compile(name)
        filtered = list(filter(regex.fullmatch, self.keys))
        objs = []
        for name in filtered:
            obj = self._data_byname[name]
            if tags:
                if set(tags) <= set(obj.tags):
                    objs.append(obj)
            else:
                objs.append(obj)
        logger.log(
            logging.DEBUG + 3,
            f"Query {len(objs)}/{len(self)} objects of type={self.acitype} with query: {args}",
        )
        new = type(self)(inst=self.inst, acitype=self.acitype, backref=self.backref)
        new.extend(objs=objs)
        return new

    @classmethod
    def merge(cls, inst: ACIType, instances: List, backref: Optional[str] = None) -> ACIContainer:
        assert len(instances) >= 1
        if len(instances) == 1:
            return instances[0]
        else:
            assert all(
                inst.acitype == instances[-1].acitype for inst in instances
            ), f"Can only merge {cls.__name__} with same acitype!"
            data_byname = UniqueKeyDict(instances[0].acitype)
            data_byid = UniqueKeyDict(instances[0].acitype)
            for inst in instances:
                if not isinstance(inst, ACIContainer):
                    raise ValueError(f"{inst} needs to be of type {cls}")
                data_byname.update(inst._data_byname)
                data_byid.update(inst._data_byid)
            new = cls(inst=inst, acitype=instances[0].acitype, backref=backref)
            new._data_byname = data_byname
            new._data_byid = data_byid
            return new


@dataclass(eq=False)
class IsData:
    is_data: bool = False

    @property
    def is_mc(self) -> bool:
        return not self.is_data

    @property
    def data_source(self) -> str:
        return "data" if self.is_data else "mc"


@dataclass(eq=False)
class AutoAddChildProcesses:
    def __post_init__(self) -> None:
        super().__post_init__()
        self._processes = ACIContainer(inst=self, acitype=Process)

    @ACICachedProperty
    def processes(self) -> ACIContainer:
        """
        Automatically adds all child processes of the given processes also to the analysis
        """
        for process in self._processes.values:
            for _, p in process.walk_processes(include_self=True):
                self._processes.add(p)
        return self._processes


@dataclass(eq=False)
class Analysis(AutoAddChildProcesses, ACIBase):
    """
    Analysis, which can hold linked relations to campaigns, processes, categories and variables.
    Additionally the Analysis functions as a _master_ configuration object,
    from which one can access everything directly.
    """

    def __post_init__(self) -> None:
        super().__post_init__()
        self._campaigns = ACIContainer(inst=self, acitype=Campaign)
        self._categories = ACIContainer(inst=self, acitype=Category)
        self._variables = ACIContainer(inst=self, acitype=Variable)

    @property
    def campaigns(self) -> ACIContainer:
        return self._campaigns

    @property
    def categories(self) -> ACIContainer:
        return self._categories

    @property
    def variables(self) -> ACIContainer:
        return self._variables


@dataclass(eq=False)
class Campaign(AutoAddChildProcesses, ACIBase):
    """
    Campaign, which can hold linked relations to datasets and processes.
    """

    ecm: float = 13.0

    def __post_init__(self) -> None:
        super().__post_init__()
        self._datasets = ACIContainer(inst=self, acitype=Dataset, backref="_campaigns")

    @property
    def datasets(self) -> ACIContainer:
        return self._datasets


@dataclass(eq=False)
class Dataset(IsData, ACIBase):
    """
    Dataset, which can hold linked relations to campaigns and processes.
    """

    info: Dict = field(default_factory=dict, repr=False)
    processes: InitVar[List["Process"]] = None

    def __post_init__(self, processes) -> None:
        super().__post_init__()
        self._processes = ACIContainer(inst=self, acitype=Process)
        self._campaigns = ACIContainer(inst=self, acitype=Campaign, backref="_datasets")
        if processes is not None and not isinstance(processes, property):
            self._processes.extend(processes)

    @property
    def processes(self) -> ACIContainer:
        return self._processes

    @property
    def campaigns(self) -> ACIContainer:
        return self._campaigns

    @property
    def process(self) -> Process:
        """
        By convention a dataset is only linked to one Process.
        """
        (process,) = self.processes.values
        return process

    @property
    def campaign(self) -> Optional[Campaign]:
        if len(self._campaigns) == 0:
            return None
        else:
            # Can a dataset be in multiple campaigns? I think not, but just in case...
            assert len(self._campaigns) == 1, f"{self} is in multiple campaigns!"
            return self._campaigns.values[0]

    @property
    def xs(self) -> Number:
        return self.process.xsecs[self.campaign.ecm]


@dataclass(eq=False)
class Process(IsData, ACIBase):
    """
    Process, which can hold linked relations to datasets and other processes.
    """

    xsecs: Dict[float, Number] = field(default_factory=dict)
    color: Tuple[int] = (0, 0, 0)
    processes: InitVar[List[Process]] = None

    def __post_init__(self, processes) -> None:
        super().__post_init__()
        self._color_int = copy(self.color)
        self.color = rgb(*self.color)
        # [sub]processes & parentprocesses
        self._processes = ACIContainer(inst=self, acitype=Process, backref="_parent_processes")
        self._parent_processes = ACIContainer(inst=self, acitype=Process)
        if processes is not None and not isinstance(processes, property):
            self._processes.extend(processes)

    @property
    def color_int(self):
        return self._color_int

    @property
    def processes(self) -> ACIContainer:
        return self._processes

    @property
    def parent_processes(self) -> ACIContainer:
        return self._parent_processes

    @property
    def is_leaf_process(self) -> bool:
        return len(self.processes) == 0

    @property
    def is_root_process(self) -> bool:
        return len(self.parent_processes) == 0

    def walk_processes(self, include_self: bool = False):
        def _walk(process: Process, depth: int):
            depth += 1
            for process in process.processes.values:
                yield depth, process
                yield from _walk(process=process, depth=depth)

        if include_self:
            yield 0, self
        yield from _walk(process=self, depth=0)

    def walk_parent_processes(self, include_self: bool = False):
        def _walk(process: Process, depth: int):
            depth += 1
            for process in process.parent_processes.values:
                yield depth, process
                yield from _walk(process=process, depth=depth)

        if include_self:
            yield 0, self
        yield from _walk(process=self, depth=0)

    def has_parent_process(
        self, process: Union[Process, Set[Process]], include_self: bool = False
    ) -> bool:
        """Check if (any of) the process is/are among the parent processes, may `include_self`."""
        seen = set()
        if not isinstance(process, set):
            process = {process}
        for _, p in self.walk_parent_processes(include_self=include_self):
            if p not in seen and p in process:
                return True
            seen.add(p)
        return False

    def has_process(
        self, process: Union[Process, Set[Process]], include_self: bool = False
    ) -> bool:
        """Check if (any of) the process is/are among the sub processes, may `include_self`."""
        seen = set()
        if not isinstance(process, set):
            process = {process}
        for _, p in self.walk_processes(include_self=include_self):
            if p not in seen and p in process:
                return True
            seen.add(p)
        return False

    def print_tree(self, repr: bool = True) -> None:
        for depth, process in self.walk_processes(include_self=True):
            prefix = ("" if depth else "\n") + "| " * depth + "> "
            if repr:
                console.print(f"{prefix + process.__repr__()}")
            else:
                console.print(
                    "%-50s xsec=%-50s tags=%s"
                    % (prefix + process.name, process.xsecs.get(13, None), process.tags)
                )


@dataclass(eq=False)
class Category(ACIBase):
    pass


class EvalFailed(Exception):
    pass


@dataclass(eq=False)
class Variable(ACIBase):
    expression: Optional[str] = None
    binning: Optional[Tuple] = None
    unit: Optional[str] = None
    x_title: Optional[str] = None
    y_title: Optional[str] = "Events / bin"

    def eval(self, globals: Dict, locals: Optional[Dict] = None) -> Any:
        """
        Evaluates the `expression` via `eval` given the respective `globals` and `locals`.
        """
        try:
            return eval(self.expression, globals, locals)
        except Exception as e:
            raise EvalFailed(self) from e


if __name__ == "__main__":
    """
    testing / example code
    """

    sl = Analysis(name="bbww_sl")

    c2016 = Campaign(name="c2016", ecm=13)
    c2017 = Campaign(name="c2017", ecm=13)

    sl.campaigns.add(c2016)
    sl.campaigns.add(c2017)

    # from config/Run2_pp_13TeV_2016.py.json, line 9021
    tttt_info = {
        "keys": [
            "/TTTT_TuneCUETP8M1_13TeV-amcatnlo-pythia8/RunIISummer16NanoAODv7-PUMoriond17_Nano02Apr2020_102X_mcRun2_asymptotic_v8-v1/NANOAODSIM"
        ],
        "lfn_prefix": "/store/mc/RunIISummer16NanoAODv7/TTTT_TuneCUETP8M1_13TeV-amcatnlo-pythia8/NANOAODSIM/PUMoriond17_Nano02Apr2020_102X_mcRun2_asymptotic_v8-v1/",
        "lfns": [
            "110000/62291B97-89B9-AF42-AAFB-41C0DB4ACDB2.root",
            "110000/DE0D5A12-33B7-024B-90F7-8CDE98F96A50.root",
            "130000/1DCE4E31-1AA4-3D45-87D0-77E72BB41BD7.root",
        ],
        "n_bytes": 934218766,
        "n_events": 250000,
        "n_files": 3,
    }

    dstttt_2016 = Dataset(name="TTTT", info=tttt_info, is_data=False)

    dsttbar1_2016 = Dataset(name="ttbar1_2016", is_data=False)
    dsttbar2_2016 = Dataset(name="ttbar2_2016", is_data=False)

    dsttbar1_2017 = Dataset(name="ttbar1_2017", is_data=False)
    dsttbar2_2017 = Dataset(name="ttbar2_2017", is_data=False)

    c2016.datasets.add(dstttt_2016)
    c2016.datasets.add(dsttbar1_2016)
    c2016.datasets.add(dsttbar2_2016)

    for i in range(10000):
        c2016.datasets.add(Dataset(name=f"dy_{i}", is_data=False))

    c2017.datasets.add(dsttbar1_2017)
    c2017.datasets.add(dsttbar2_2017)

    ttbar_fh = Process(name="ttbar_fh", is_data=False, tags={"background"})
    ttbar_sl = Process(name="ttbar_sl", is_data=False, tags={"background"})
    ttbar_dl = Process(name="ttbar_dl", is_data=False, tags={"background"})

    ttbar = Process(name="ttbar", is_data=False)
    ttbar.processes.extend([ttbar_fh, ttbar_sl, ttbar_dl])

    root = Process(name="root", is_data=False)
    root.processes.add(ttbar)

    dsttbar1_2016.processes.add(ttbar)
    dsttbar2_2016.processes.add(ttbar)

    dsttbar1_2017.processes.add(ttbar)
    dsttbar2_2017.processes.add(ttbar)
    # sl.processes.add(ttbar)

    dl = sl.copy(name="bbww_dl")

    dl.processes.query(name=".+", tags={"background"})
