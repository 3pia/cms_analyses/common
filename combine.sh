#!/bin/bash
BASE="$DHA_SOFTWARE/../cc7/combine"
REPO="$BASE/HiggsAnalysis/CombinedLimit"
set -e
if [ -d /software ]
then
    export MEMORY=${MEMORY:-10000}
    export IMAGE=${IMAGE:-cmssw/cc7}
    export UNIVERSE="docker"
    if [[ ! -d "$REPO" ]]
    then
        mkdir -p $REPO
        git clone --depth 1 --branch v8.2.0 "https://github.com/cms-analysis/HiggsAnalysis-CombinedLimit.git" "$REPO"
        chmod ug+x "$REPO/test/diffNuisances.py"
    fi
    if [[ ! -f "$BASE/.done" ]]
    then
        submit -f bash -c "cd '$REPO' && source ./env_standalone.sh && { make -kj $(grep -c ^processor /proc/cpuinfo) ; make && touch '$BASE/.done' ; }"
    fi
    exec submit -f "$BASH_SOURCE" "$@"
else # we are in an image, just try to start it
    pushd "$REPO" >/dev/null
    source "./env_standalone.sh"
    popd >/dev/null
    exec "$REPO/exe/combine" "$@"
fi
