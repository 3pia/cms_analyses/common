# -*- coding: utf-8 -*-

import hashlib
import re
from collections import defaultdict
from functools import cached_property

import hist
import law
import luigi
import numpy as np
import uproot3 as uproot
from tqdm.auto import tqdm

from tasks.base import (
    AnalysisCampaignTask,
    AnalysisTask,
    CategoriesMixin,
    CombinationTask,
    PoolMap,
)
from tasks.combine import (
    AddFitDiagsMixin,
    BlindingMixin,
    FitDiagnosticsCombinedPostprocessed,
    FitDiagnosticsFrequentistToysMixin,
    FitDiagnosticsMixin,
    FitDiagnosticsPostprocessed,
    PlotUpperLimitsAtPoint,
    PostfitShapesFromWorkspace,
    PostfitShapesFromWorkspacePostprocessed,
    years,
)
from tasks.datacard import DatacardProducer, DatacardsVariablesWrapper, DatacardsWrapper
from tasks.mixins import CreateIssueMixin, ModelMixin, RecipeMixin, CMSLabelMixin
from tasks.plotting import PlotHistsBase
from tools.numpy import set_thresh
from utils.uproot import decycle
from utils.util import GitlabIssue, pscan_vispa


class PrepareFitDiagnostics(FitDiagnosticsMixin, BlindingMixin, AnalysisCampaignTask):
    fit_style = luigi.ChoiceParameter(
        default="prefit",
        choices=("prefit", "preandpostfit"),
    )

    @cached_property
    def legacy_processes_hash(self):
        if self.year == "run2":
            return "-".join(
                self.req(self, year=str(year)).legacy_processes_hash[:20] for year in years
            )
        else:
            return super().legacy_processes_hash

    @property
    def filtered_input(self):
        if self.fit_style == "prefit":
            shapes = "shapes_prefit"
        elif self.fit_style == "preandpostfit":
            shapes = "shapes_"
        _input = self.input()["fitdiags"].load()
        _filtered_input = {k: v for k, v in _input.items() if k.startswith(shapes)}
        return _filtered_input

    def store_parts(self):
        parts = super().store_parts()
        parts += (
            self.requires()["fitdiags"].datacards_hash,
            self.fit_style,
        )
        return parts

    @cached_property
    def plotting_process_groups(self):
        ret = super().plotting_process_groups
        ret["data"].append("data_err")
        return ret

    def parse_cat(self, cat):
        _, cat = cat.split("__", 1)
        for shape in ("_prefit", "_postfit"):
            if cat.endswith(shape):
                cat = cat.replace(shape, "")
        year, cat = cat.split("_", 1)
        year = year.replace("y", "")
        label = self.analysis_inst.categories.get(cat).label_short
        return label, year

    def unpack_data(self, shape, cat, _):
        cat_label, year = self.parse_cat(cat)
        return year, cat_label, self.input()["fitdiags"].load()[shape][cat]


class PlotFitDiagnosticsBase(CMSLabelMixin, PrepareFitDiagnostics, PlotHistsBase, PoolMap):
    eopt = dict(PlotHistsBase.eopt, label="Total Unc.", facecolor=(0, 0, 0, 0.3), linewidth=0)
    ratio_max_delta = 0.5
    group_ignore = ("total", "total_background", "total_signal")

    scale_signal_to_upper_limit = luigi.BoolParameter(default=False)

    @cached_property
    def basename(self):
        return f"{super().basename}_{self.cms_label}"

    @property
    def ratio_extra_offset_lines(self):
        return not self.scale_signal_to_upper_limit

    def store_parts(self):
        parts = super().store_parts()
        if self.scale_signal_to_upper_limit:
            parts += ("ScaledToUpperLimit",)
        return parts

    @property
    def pscan(self):
        return pscan_vispa(
            directory=self.local_path(),
            regex=r"(?P<stage>.+)/(?P<channel>[^_]+)_(?P<category>.+)/(?P<format>[^\.]+)",
        )

    @law.decorator.safe_output
    def run(self):
        work = [
            (shape, cat, None)
            for shape, shape_hists in self.filtered_input.items()
            if shape != "fit_results"
            for cat in shape_hists.keys()
            if cat.startswith(self.analysis_choice)
        ]
        work += self.collate(work, self.analysis_inst.aux["collate_cats"])
        self.basename
        for _ in self.pmap(self.worker_prefit_postfit, work, unit="plot", unordered=True):
            pass

        self.output().touch()
        self.logger.info(self.pscan)
        if getattr(self, "create_issue", None):
            GI = GitlabIssue(
                self,
                opts={
                    "important_params": [
                        "analysis_choice",
                        "year",
                        "blind_thresh",
                        "log_scale",
                        "format",
                        "fit_style",
                        "scale_signal_to_upper_limit",
                    ],
                },
            )
            GI.post_to_issue(message=self.pscan)

    def get_title(self, shape, cat):
        cat_label, _ = self.parse_cat(cat)
        s = shape.split("_", 1)[-1]
        s = dict(
            prefit=r"Prefit",
            fit_s=r"Postfit (S+B)",
            fit_b=r"Postfit (B)",
            fit_sb=r"Postfit (S+B)",  # this should not exist according to https://github.com/cms-analysis/HiggsAnalysis-CombinedLimit/wiki/nonstandard#fitting-diagnostics
        ).get(s, s)
        # inser info about covQual
        q = self.fit_results.get(shape[7:], {}).get("_covQual", None)
        q = {
            -1: None,  # "ext. provided",
            0: "not pos. def.",
            1: "approximate",
            2: "forced pos. def.",
            3: None,
        }.get(q, q)
        if q is not None:
            s = f"{s}, {q} cov. matrix"
        cat_label = re.sub(r"^all incl\., ", "", cat_label)
        if "S+B" in s:
            return cat_label
        return f"{cat_label} ({s})"

    @property
    def fit_results(self):
        return self.input()["fitdiags"].load()["fit_results"]

    def worker_prefit_postfit(self, cch):
        from collections import defaultdict

        from law.util import patch_object

        from config.Run2_pp_13TeV_2016 import campaign as run_2016
        from config.Run2_pp_13TeV_2017 import campaign as run_2017
        from config.Run2_pp_13TeV_2018 import campaign as run_2018
        from config.Run2_pp_13TeV_run2 import campaign as run2

        campaigns = {
            run_2016.name: run_2016,
            run_2017.name: run_2017,
            run_2018.name: run_2018,
            run2.name: run2,
        }

        ctx, cat, data = cch

        if isinstance(data, list):
            cat, name = cat
        else:
            name = cat

        if isinstance(data, list):
            years, titles, hists = zip(*(self.unpack_data(ctx, *d) for d in data))
            assert len(set(years)) == 1
            year = years[0]
        else:
            year, _, hist = self.unpack_data(ctx, cat, data)
            titles = None
            hists = [hist]

        # patch campaign_inst for proper lumi label
        with patch_object(
            self,
            "campaign_inst",
            campaigns.get(f"Run2_pp_13TeV_{year}", None),
        ):
            hists = [self.regularize(hist) for hist in hists]
            x_label = hists[0].axes[-1].label

            ghist = defaultdict(list)
            for h in hists:
                gh = self.group_hist(h, self.plotting_process_groups, ignore=self.group_ignore)

                # scale variances: sum background to match total_background
                scale_by_hand = False
                if scale_by_hand:
                    if "total_background" in h.axes[0]:
                        total_background_identifier = "total_background"
                    else:
                        total_background_identifier = "TotalBkg"
                    if "prefit" not in ctx and np.allclose(
                        (tbg := h[total_background_identifier, :].view(True)).value,
                        (sbg := gh["background"][::sum, :].view(True)).value,
                    ):
                        target = np.array(tbg.variance, copy=True)
                        target *= h["data", ::sum].value / target.sum()
                        gh["background"].view(True).variance *= np.divide(
                            target,
                            sbg.variance,
                            out=np.ones_like(sbg.variance),
                            where=sbg.variance > 0,
                        )
                if self.postfit_unc_frequentist_toys:
                    if "total_background" in h.axes[0]:
                        total_background_identifier = "total_background"
                    else:
                        total_background_identifier = "TotalBkg"
                    if "prefit" not in ctx:
                        # this is a bit hacky though...
                        gh["background"].view(True).variance[...] = 0
                        gh["background"].view(True).variance[0] = (
                            h["TotalBkg", :].view(True).variance
                        )

                gh["ratio"] = d = self.blind(**gh)

                if d and "data_blind" in d.axes[0]:
                    d = d.copy()
                    d.view(True)[d.axes[0].index("data_blind"), ...]["value"] = np.nan

                gh["data"] = d

                for k, v in gh.items():
                    ghist[k].append(v)

            if self.scale_signal_to_upper_limit:
                update_legend_label = True
                for signal in ghist["signal"]:
                    procs = signal.axes[0]
                    view = signal.view()
                    # first normalize signal hist to r=1 if postfit
                    if ctx in ("shapes_fit_s", "shapes_fit_sb") and self.unblind:
                        r = np.abs(np.array(self.fit_results["r"]))
                        # fix negative r in fit
                        if np.any(view["value"] < 0):
                            r = -r
                        view["value"] /= r
                        view["variance"] /= r**2

                    # now scale by upper limit for the respective process
                    for proc in procs:
                        # this is specific to HH analysis, but this task is not used for anything else right now anyway...
                        if proc.startswith("ggHH"):
                            mode = "ggF"
                        elif proc.startswith("qqHH"):
                            mode = "VBF"
                        else:
                            mode = "inclusive"

                        year = self.parse_cat(cat)[1]
                        if hasattr(self, "limit_analyses") and len(self.limit_analyses) > 1:
                            identifier = self.analysis_inst.label_short
                        else:
                            if year == "run2":
                                identifier = "Combined"
                            else:
                                identifier = year
                        hepdata_path = self.input()["limits"][mode].path
                        limit, _, _ = PlotUpperLimitsAtPointSaveHEPData.get_hepdata(
                            path=hepdata_path,
                            identifier=identifier,
                            kind="Observed limit" if self.unblind else "Expected limit, 68%",
                        )
                        view[procs.index(proc)]["value"] *= limit
                        view[procs.index(proc)]["variance"] *= limit**2

                        tqdm.write(
                            f"Scaling signal {proc} in {' - '.join(self.parse_cat(cat))}: x {int(limit)}"
                        )

                        # for legend we need to path the labels for the scaled processes
                        if update_legend_label:
                            # fmt: off
                            self.analysis_inst.processes.get(proc).label += rf" $\times {int(limit)}$"
                            self.analysis_inst.processes.get(proc).label_short += rf" $\times {int(limit)}$"
                            # fmt: on
                    # disable now, otherwise we would update the label mulitple times
                    update_legend_label = False

                    signal[...] = view


            var_name = hists[0].axes[-1].name
            var = self.analysis_inst.variables.get(var_name)
            if var.unit:
                bins, low, high = var.binning
                size = (high-low) / bins
                md = ghist["ratio"][-1].metadata
                if md is None:
                    md = {}
                ghist["ratio"][-1].metadata = {"y_title": f"Events / {size:g} {var.unit}", **md}

            with self.plot_silencer():
                self.plot(
                    targets=[
                        self.local_target(ctx, name, f"{self.basename}.{f}") for f in self.format
                    ],
                    stack=ghist["background"],
                    lines=ghist["signal"],
                    points=ghist["data"],
                    ratio=ghist["ratio"],
                    x_label=x_label,
                    legend_title=self.get_title(ctx, cat),
                    legend_fraction=0.25,
                    hstack_labels=titles,
                )


class PlotFitDiagnostics(PlotFitDiagnosticsBase):
    def requires(self):
        return FitDiagnosticsPostprocessed.req(self)


class PlotFitDiagnosticsWrapper(DatacardsWrapper):
    _task = PlotFitDiagnostics


class PlotFitDiagnosticsVariablesWrapper(DatacardsVariablesWrapper):
    _producer = PlotFitDiagnostics


class LimitMixin:
    limit_version = luigi.Parameter(description="task version")
    limit_analyses = law.CSVParameter(
        default=("bbww_dl", "bbww_sl"),
        description="Analyses for combination",
    )
    limit_years = law.CSVParameter(
        default=("2016", "2017", "2018"),
        description="Years for combination",
    )
    limit_categories = luigi.DictParameter(default={}, description="Categories")
    limit_categories_tags = law.CSVParameter(default=("fit",), description="Categories tags")
    limit_categories_regex = luigi.Parameter(default=".+", description="Categories name regex")
    limit_model_version = luigi.Parameter(default="")
    limit_statmodel = luigi.Parameter(default="StatModel")


class PlotFitDiagnosticsCombined(LimitMixin, PlotFitDiagnosticsBase, CreateIssueMixin):
    priority = 200

    @property
    def pscan(self):
        return pscan_vispa(
            directory=self.local_path(),
            regex=r"(?P<stage>.+)/(?P<channel>.+)__(?P<year>[^_]+)_(?P<category>.+)/(?P<format>[^\.]+)",
        )

    @property
    def legacy_processes_hash(self):
        if self.year == "run2":
            """
            For the combined year (run2), the "config" is the set of all individual per-year configs.
            We merge the legacy_process_hashes into one new hash.
            """
            hashes = []

            def condition(outp):
                return isinstance(outp, dict) and all(
                    isinstance(o, DatacardsWrapper) for o in outp.values()
                )

            for year, task in self.deep_requires(
                req=self.requires()["fitdiags"], condition=condition
            ).items():
                processes = task.analysis_inst.aux["process_groups"].get(
                    self.process_group[0], self.process_group
                )
                hash = hashlib.sha256("".join(sorted(processes)).encode("utf-8")).hexdigest()
                hashes.append(hash)
            if len(set(hashes)) == 1:
                return hashes[0]
            else:
                return hashlib.sha256("".join(hashes).encode("utf-8")).hexdigest()
        else:
            return super().legacy_processes_hash

    @property
    def _years(self):
        if self.year == "run2":
            return ("2016", "2017", "2018")
        else:
            return (self.year,)

    def requires(self):
        reqs = {
            "fitdiags": FitDiagnosticsCombinedPostprocessed.req(
                self, analyses=(self.analysis_choice,), years=self._years
            )
        }
        if self.scale_signal_to_upper_limit:
            reqs.update(
                {
                    "limits": {
                        mode: PlotUpperLimitsAtPointSaveHEPData.req(
                            self,
                            poi=poi,
                            version=self.limit_version,
                            analyses=self.limit_analyses,
                            years=self.limit_years,
                            model_version=self.limit_model_version,
                            statmodel=self.limit_statmodel,
                            categories=self.limit_categories,
                            categories_tags=self.limit_categories_tags,
                            categories_regex=self.limit_categories_regex,
                        )
                        for mode, poi in [("inclusive", "r"), ("ggF", "r_gghh"), ("VBF", "r_qqhh")]
                    }
                }
            )
        return reqs


class PlotFitDiagnosticsCombinedAnalyses(PlotFitDiagnosticsCombined):
    analyses = law.CSVParameter(
        default=("bbww_dl", "bbww_sl"),
        description="Analyses for combination",
    )

    def requires(self):
        reqs = super().requires()
        reqs["fitdiags"] = FitDiagnosticsCombinedPostprocessed.req(
            self, analyses=self.analyses, years=self._years
        )
        if self.scale_signal_to_upper_limit:
            reqs["limits"] = {
                mode: PlotUpperLimitsAtPointSaveHEPData.req(
                    self,
                    poi=poi,
                    analyses=self.limit_analyses,
                    version=self.limit_version,
                    model_version=self.limit_model_version,
                    statmodel=self.limit_statmodel,
                    categories=self.limit_categories,
                    categories_tags=self.limit_categories_tags,
                    categories_regex=self.limit_categories_regex,
                )
                for mode, poi in [("inclusive", "r"), ("ggF", "r_gghh"), ("VBF", "r_qqhh")]
            }
        return reqs


class PlotFitDiagnosticsCombinedCategories(CategoriesMixin, PlotFitDiagnosticsCombinedAnalyses):
    _task = FitDiagnosticsCombinedPostprocessed

    def requires(self):
        reqs = super().requires()
        reqs["fitdiags"] = self._task.req(
            self,
            analyses=[self.analysis_choice],
            years=self._years,
        )
        return reqs


class PlotPostfitShapesFromWorkspace(
    FitDiagnosticsFrequentistToysMixin,
    AddFitDiagsMixin,
    CombinationTask,
    PlotFitDiagnosticsCombinedAnalyses,
):
    # Example command: law run PlotPostfitShapesFromWorkspace --version prod42 --recipe tth --analyses bbww_sl --analysis-choice bbww_sl --year run2 --categories '{"bbww_sl": ["all_incl_sr_prompt_smin"]}' --categories-tags hl --process-group compact --fit-style preandpostfit --n-parallel 1 --blind-thresh 0 --log-scale --model-version approval --fit-version prod42 --fit-categories '{"bbww_sl": ["all_incl_sr_prompt_smin"]}' --fit-analyses bbww_sl --fit-categories-tags hl --fit-model-version approval --fit-years 2016,2017,2018

    def deep_requires(self, req: law.Task = None, condition=lambda x: True):
        if req is None:
            outp = self.requires()
        else:
            outp = req.requires()
        while not condition(outp):
            if isinstance(outp, PostfitShapesFromWorkspace):
                # PostfitShapesFromWorkspace has two datacard like requirements (workspace and fitdiagnostics), we will only use the datacards from the workspace to distinguish the targets and keep the fitdiagnostics constant (bbWW FR2 SL+DL fit)
                outp = outp.requires()["workspace"]
            else:
                outp = outp.requires()
        return outp

    def requires(self):
        reqs = super().requires()
        reqs["fitdiags"] = {
            category: PostfitShapesFromWorkspacePostprocessed.req(
                self,
                years=self._years,
                analyses=[self.analysis_choice],
                categories={self.analysis_choice: [category]},
            )
            for category in self.categories[self.analysis_choice]
        }
        return reqs

    def store_parts(self):
        return super().store_parts() + (
            # HACK: just stuff everything relevant into a hash
            hashlib.sha256(
                (
                    "".join(sorted(task.task_id for task in law.util.flatten(self.requires())))
                ).encode()
            ).hexdigest()[:20],
            self.analysis_choice,
        )

    @property
    def filtered_input(self):
        if self.fit_style == "prefit":
            shapes = "shapes_prefit"
        elif self.fit_style == "preandpostfit":
            shapes = "shapes_"

        ret = defaultdict(dict)
        for cat, tar in self.input()["fitdiags"].items():
            for shape, hists in tar.load().items():
                if shape.startswith(shapes):
                    ret[shape].update(hists)

        return dict(ret)

    def unpack_data(self, shape, cat, _):
        cat_label, year = self.parse_cat(cat)
        return year, cat_label, self.filtered_input[shape][cat]

    @property
    def fit_results(self):
        frs = {
            (self.analysis_choice, cat): tar.load()["fit_results"]
            for cat, tar in self.input()["fitdiags"].items()
        }
        assert len(set(map(repr, frs.values()))) == 1
        return list(frs.values())[0]


class PlotPostfitShapesFromWorkspaceHL(ModelMixin, RecipeMixin, AnalysisTask, law.WrapperTask):
    cms_label = CMSLabelMixin.cms_label

    def requires(self):
        reqs = super().requires()
        categories = self.analysis_inst.categories.query(name=".+", tags={"hl"})
        reqs = {
            category.name: PlotPostfitShapesFromWorkspace.req(
                self,
                process_group=["compact"],
                fit_style="preandpostfit",
                blind_thresh=0,
                log_scale=True,
                limit_analyses=("bbww_dl", "bbww_sl"),
                limit_version="prod41",
                limit_model_version="approval",
                unblind=True,
                scale_signal_to_upper_limit=True,
                categories={self.analysis_choice: [category.name]},
                categories_tags={"hl"},
                analyses=[self.analysis_choice],
                year="run2",
                fit_version=self.version,
                fit_model_version=self.model_version,
                fit_years=("2016", "2017", "2018"),
                fit_analyses=[self.analysis_choice],
                fit_categories={self.analysis_choice: [category.name]},
                fit_categories_tags={"hl"},
            )
            for category in categories
        }
        return reqs


class PlotUpperLimitsAtPointSaveHEPData(PlotUpperLimitsAtPoint):
    outmap = {"hepdata.yaml": "*/hepdata__*.yaml"}

    def command(self):
        cmd = super().command()
        cmd[0] = "PlotUpperLimitsAtPoint"
        cmd += ["--save-hep-data", "True"]
        return cmd

    @staticmethod
    def get_hepdata(path: str, identifier: str, kind: str = "Expected limit, 68%"):
        """
        Examplary HEP Data entry (blinded):

        ```yaml
        independent_variables:
            - header:
                name: Measurement
                values:
                - value: bbWW (SL)
                - value: bbWW (DL)
                - value: Combined
            dependent_variables:
            - header:
                name: Expected limit, 68%
                qualifiers:
                - name: POI
                    value: $\sigma$(pp $\rightarrow$ HH) / $\sigma_{Theory}$
                values:
                - value: --
                    errors:
                    - label: 68%
                        asymerror:
                        plus: --
                        minus: --
                - value: --
                    errors:
                    - label: 68%
                        asymerror:
                        plus: --
                        minus: --
                - value: --
                    errors:
                    - label: 68%
                        asymerror:
                        plus: --
                        minus: --
            - header:
                name: Expected limit, 95%
                qualifiers:
                - name: POI
                    value: $\sigma$(pp $\rightarrow$ HH) / $\sigma_{Theory}$
                values:
                - value: --
                    errors:
                    - label: 95%
                        asymerror:
                        plus: --
                        minus: --
                - value: --
                    errors:
                    - label: 95%
                        asymerror:
                        plus: --
                        minus: --
                - value: --
                    errors:
                    - label: 95%
                        asymerror:
                        plus: --
                        minus: --
            - header:
                name: Observed limit
                qualifiers:
                - name: POI
                    value: $\sigma$(pp $\rightarrow$ HH) / $\sigma_{Theory}$
                values:
                - value: --
                - value: --
                - value: --
        ```
        """
        import yaml

        assert kind in ("Expected limit, 68%", "Expected limit, 95%", "Observed limit")

        with open(path) as f:
            data = yaml.safe_load(f)
            idx = law.util.flatten(data["independent_variables"][0]["values"]).index(identifier)
            dep_data = data["dependent_variables"]
            for dep in dep_data:
                if dep["header"]["name"] == kind:
                    break
            limit = dep["values"][idx]
            # return limits as: (nominal, up, down)
            if "Expected" in kind:
                limit = (
                    limit["value"],
                    limit["errors"][0]["asymerror"]["plus"],
                    limit["errors"][0]["asymerror"]["minus"],
                )
            else:
                limit = (limit["value"], 0, 0)
            return limit
