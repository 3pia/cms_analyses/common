# coding: utf-8

import os
from collections import Counter
from concurrent.futures import process
from hashlib import sha512
from pathlib import Path
from subprocess import DEVNULL, Popen
from weakref import finalize

import law
import law.contrib.coffea
import numpy as np
import uproot
from luigi import BoolParameter, Parameter

from tasks.base import AnalysisCampaignTask, PoolMap
from tasks.files import DownloadNanoAODs
from tools.data import DSS
from utils.coffea import ProcessorABC, _Preheater
from utils.dask import (
    AffineRescheduling,
    HTCondorCluster,
    dask_executor,
    register_plugins,
)
from utils.sandbox import OpenportsTask
from utils.util import MaxRSSWatch, ulimit
from tasks.mixins import RecipeMixin


class DSSFormatter(law.target.formatter.Formatter):

    name = "dss"

    @classmethod
    def _get_dir(cls, path):
        path = law.target.file.get_path(path).rsplit("/.dss", 1)
        return None if len(path) == 1 else path[0]

    @classmethod
    def accepts(cls, path, mode):
        return cls._get_dir(path) is not None

    @classmethod
    def load(cls, path, *args, **kwargs):
        kwargs.setdefault("mmap_mode", "r")
        return DSS.from_npy(cls._get_dir(path), **kwargs)

    @classmethod
    def dump(cls, path, out, *args, **kwargs):
        DSS(out).to_npy(cls._get_dir(path), **kwargs)
        Path(path).touch()


class CoffeaProcessor(RecipeMixin, OpenportsTask, AnalysisCampaignTask):
    threshold = DownloadNanoAODs.threshold
    processor = Parameter(default="Processor")
    debug = BoolParameter()
    preheat = BoolParameter()
    explorative = BoolParameter()

    @property
    def resources(self):
        return {} if self.debug or self.explorative else {"cluster": 1}

    @property
    def priority(self):
        return 5000 + max(2020 - int(self.year), 0)

    @property
    def skip_output_removal(self):
        return not self.debug

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.Processor = getattr(self._import("recipes", self.recipe), self.processor)
        assert issubclass(self.Processor, ProcessorABC)
        assert not (
            self.debug and self.explorative
        ), "You can not run in debug and explorative mode simultaneously!"

    def requires(self):
        return self.Processor.requires(self)

    def base_requires(
        self, data_source=("mc", "data"), data_filter=lambda ds: True, corrections=True, **kwargs
    ):
        import tasks.corrections as corr

        # there is no prefiring issue in 2018
        if self.year == "2018":
            kwargs["prefiring"] = False

        if isinstance(kwargs.get("btag", None), str):
            kwargs["btag"] = dict(wp=kwargs["btag"])
        corr_insts = {
            k: v.req(self, **kwargs.get(k, {}))
            for k, v in (
                (n.replace("Corrections", "").lower(), c)
                for n, c in vars(corr).items()
                if isinstance(c, type) and issubclass(c, corr.base.Correction)
            )
            if kwargs.get(k, corrections) is not False
        }
        ret = dict(
            data=DownloadNanoAODs.req(
                self,
                datasets=[
                    ds.name
                    for ds in self.campaign_inst.datasets.values
                    if ds.data_source in data_source
                    and data_filter(ds)
                    and (
                        not self.debug
                        or not self.Processor.debug_dataset
                        or ds.name == self.Processor.debug_dataset
                    )
                ],
                only_nominal=self.Processor.dataset_shifts is False,  # could also be a property
            ),
            # TODO SF: require corrections
            corrections={
                k: v
                for k, v in corr_insts.items()
                if v.available or self.logger.info("skipping unavailable correction: %s", k)
            },
        )
        return ret

    def load_corrections(self):
        return {corr: target.load() for corr, target in self.input()["corrections"].items()}

    def output(self):
        out = self.Processor.output
        if callable(out):
            return out(self)
        if out == "*.npy":
            out = ".dss"
        elif "." not in out:
            out = "data.%s" % out
        return self.local_target(out)

    def store_parts(self):
        parts = (self.analysis_choice, self.recipe, self.processor)
        if self.debug:
            parts += ("debug", self.Processor.debug_dataset)
            objs = self.Processor.debug_paths or self.Processor.debug_uuids or ()
            if objs:
                parts += (sha512(".".join(sorted(objs)).encode("utf8")).hexdigest()[:16],)
        elif self.explorative:
            parts += ("explorative",)
        return super().store_parts() + parts

    @law.decorator.safe_output
    def run(self):
        import time

        import coffea.processor as processor
        from dask.distributed import Client
        from distributed.security import Security
        from rich.console import Console

        console = Console()

        if not self.debug:
            ulimit(AS="hard", NOFILE="hard")
            if os.environ.get("CUDA_VISIBLE_DEVICES", None) == "-1":
                console.print(f"Set: CUDA_VISIBLE_DEVICES=")
                del os.environ["CUDA_VISIBLE_DEVICES"]

        processor_inst = self.Processor(self)

        fileset = self.input()["data"]
        fileset = {
            (dsname, shift): sorted(set(target.path for target in files.targets if target.exists()))
            for (dsname, shift), files in fileset.items()
            if dsname in self.campaign_inst.datasets.keys
            and (processor_inst.dataset_shifts or shift == "nominal")
        }

        # remove empty datasets
        empty = [key for key, paths in fileset.items() if not paths]
        if empty:
            for e in empty:
                del fileset[e]
            self.logger.warning("skipping empty datasets: %s", ", ".join(map(str, sorted(empty))))

        if self.debug:
            if self.Processor.debug_paths:
                fileset = list(self.Processor.debug_paths)
                for i, fn in enumerate(fileset):
                    if fn.startswith("lfn:"):
                        fileset[i] = self.wlcg_target(fn[4:], fs="cms_fs").copy_to_local(cache=True)
                        print("localized debug LFN:", fn, "->", fileset[i])
                fileset = {(self.Processor.debug_dataset, "nominal"): fileset}
            else:
                uuids = self.Processor.debug_uuids
                fileset = {
                    dskey: (
                        [fn for fn in files if any(uuid in fn for uuid in uuids)]
                        if uuids
                        else files[:1]
                    )
                    for dskey, files in fileset.items()
                    if (self.Processor.debug_dataset or dskey[0]) == dskey[0]
                }
                if not any(fileset.values()):
                    raise RuntimeError(
                        "\n\t".join(
                            [
                                "no datasets/files passed debug criterions:",
                                "debug_dataset: %r" % self.Processor.debug_dataset,
                                "debug_uuids: %r" % self.Processor.debug_uuids,
                            ]
                        )
                    )
            ea = {}
            plugins = {}

        elif self.explorative:
            fileset = {k: v[:1] for k, v in fileset.items()}
            ea = {"workers": 55}
            plugins = {}

        else:

            def n2ip(n):
                return "134.61.19.%d" % n

            cores = 6
            ratio = 7
            ratio2 = 5
            merger = 10
            cluster = HTCondorCluster(
                cores=1,
                processes=1,
                memory=getattr(processor_inst, "memory", "2000MiB"),
                disk="2GB",
                extra=["--lifetime", "90m", "--lifetime-stagger", "15m"],
                job_extra=dict(Request_CPUs=0, Request_GPUs=0, Getenv=True),
                security=Security(),
                protocol="tls://",
                scheduler_options=dict(dashboard_address=os.environ["DHA_DASHBOARD_ADDRESS"]),
                local_directory="/tmp",
                # log_directory=".logs/",
                maintain=True,
            )
            proc = Popen(
                [
                    "dask-worker",
                    cluster.scheduler_address,
                    "--memory-limit=0",
                    f"--nprocs={merger}",
                    "--nthreads=1",
                    "--name=merger",
                ],
                stderr=DEVNULL,
                stdout=DEVNULL,
            )
            finalize(cluster, proc.terminate)

            # **2.5 determinied empirically, for 8 vs 3 @ 8 worker hashes
            aff = {n2ip(n): ratio**2.5 for n in np.r_[61:64]}
            aff[n2ip(42)] = ratio2**2.5
            AffineRescheduling(
                cluster.scheduler,
                factor=aff,
                ignore_prefix={"reduce", "lambda", "partial", "hist_server"},
            )
            cluster.scale_machines(
                {
                    "*": ratio2 * cores,
                    "*worker*": ratio * cores,
                    "*gpu04*": 0,  # not a worker anymore
                    "*portal*": 0,  # not actually a worker
                },
                timeout=30,
            )
            if not self.preheat:
                cluster.scheduler.start_periodic_callbacks()  # enable rescheduling

            console.print(cluster)
            console.print(cluster.status_table())

            client = Client(cluster, security=Security())
            console.print(f"Dashboard address:\n{client.dashboard_link}")

            plugins = {"RestartIdleOncePaused": {}}
            register_plugins(client, plugins)
            console.print("plugins:", plugins)

            ea = dict(
                retries=0,
                compression=1,
                client=client,
                workers=set(map(n2ip, np.r_[41:50, 61:64])),
                merge_workers={n2ip(51)},
                treereduction=20,
                # live_callback=processor_inst.live_callback,
            )

        # touch this cached property do stat the tf_server.AdHoc instance
        getattr(processor_inst, "dnn", None)

        if self.preheat:
            processor_inst = _Preheater(processor_inst)

        tic = time.time()

        exe = dask_executor
        if self.debug:
            exe = processor.iterative_executor
        elif self.explorative:
            exe = processor.futures_executor
        with MaxRSSWatch("preExe"):
            processor_inst._preExe(exe, ea)

        output, metrics = processor.run_uproot_job(
            fileset,
            treename="Events",
            processor_instance=processor_inst,
            pre_executor=processor.futures_executor,
            pre_args=dict(workers=55),
            executor=exe,
            executor_args=dict(
                schema=processor_inst.schemaclass,
                mmap=True,
                savemetrics=1,
                # xrootdtimeout=30,
                # align_clusters=True,
                # processor_compression=None,
                **ea,
            ),
            chunksize=100_000,
        )

        with MaxRSSWatch("posExe"):
            output = processor_inst._postExe(exe, ea, output)
        toc = time.time()
        if isinstance(output, dict):
            output.setdefault("metrics", dict(metrics, total_time=toc - tic))

        # save outputs
        if self.preheat:
            console.print("heated columns:", output)
        else:
            self.output().parent.touch()
            self.Processor.save(self, output)

        events = metrics["entries"]
        processtime = metrics["processtime"]
        total_time = toc - tic
        console.print("\n[u][bold magenta]Summary metrics:[/bold magenta][/u]")
        console.print(f"* Total time: {total_time:.2f}s")
        console.print(f"* Total events: {events:e}")
        console.print(f"* Events / s: {events/total_time:.0f}")
        console.print(f"* Events / s / thread: {events/processtime:.0f}")


def _fn2availcols(fn):
    with uproot.open(fn) as f:
        return frozenset(f["Events"].keys()), fn.rsplit("/", 3)[-3:]


class AvailableColumns(AnalysisCampaignTask, PoolMap):
    n_parallel_max = 32
    version = None

    def requires(self):
        return DownloadNanoAODs.req(self, datasets=self.campaign_inst.datasets.keys)

    def output(self):
        return self.local_target("data.coffea")

    def run(self):
        fns = set(
            target.path
            for files in self.input().values()
            for target in files.targets
            if target.exists()
        )
        seen = {}
        count = Counter()
        for availcols, (*key, fn) in self.pmap(_fn2availcols, fns, unit="file"):
            key = tuple(key)
            seen.setdefault(availcols, {}).setdefault(key, set()).add(fn)
            count[key] += 1

        totalcols = set()
        for availcols in seen.keys():
            totalcols |= availcols

        missing = {frozenset(totalcols - availcols): content for availcols, content in seen.items()}

        def merge(src, dst):
            for key, fns in src.items():
                dst.setdefault(key, set()).update(fns)

        fine = {}
        for mc, content in missing.items():
            for col in mc:
                merge(src=content, dst=fine.setdefault(col, {}))

        factored = {}
        todo = missing.copy()
        while todo:
            cur = frozenset(min(todo.keys(), key=len))
            if not cur:
                break
            # print("factoring", len(cur), sorted(cur)[:5])
            todo2 = {}
            for mc, content in todo.items():
                if cur <= mc:
                    merge(src=content, dst=factored.setdefault(cur, {}))
                if mc - cur:
                    merge(src=content, dst=todo2.setdefault(frozenset(mc - cur), {}))
            todo = todo2

        for coll in missing, fine, factored:
            for content in coll.values():
                for key, fns in content.items():
                    if len(fns) == count[key]:
                        content[key] = all

        self.output().dump(
            dict(
                totalcols=totalcols,
                missing=missing,
                fine=fine,
                factored=factored,
            )
        )
