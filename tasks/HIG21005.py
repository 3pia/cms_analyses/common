# -*- coding: utf-8 -*-

import os
import re
import shutil
from collections import defaultdict
from pathlib import Path
from subprocess import check_call
from tempfile import TemporaryDirectory

import law
import luigi
import numpy as np
from rich.console import Console
from rich.tree import Tree
from tqdm.auto import tqdm

from tasks.base import AnalysisCampaignTask, AnalysisTask, CombinationTask
from tasks.combine import PlotPullsAndImpacts  # patched
from tasks.combine import (
    DHI,
    DHITask,
    FitDiagnosticsCombinedPostprocessed,
    PlotExclusionAndBestFit,
    PlotExclusionAndBestFit2D,
    PlotLikelihoodScan,
    PlotMultipleGoodnessOfFits,
    PlotMultipleLikelihoodScans,
    PlotMultipleUpperLimits,
    PlotUpperLimits,
    PlotUpperLimits2D,
    PlotUpperLimitsAtPoint,
)
from tasks.corrections.fake import PlotFakeCorrections
from tasks.mixins import CreateIssueMixin, ModelMixin, RecipeMixin
from tasks.multiclass import MulticlassEvalPlot, MulticlassStitchedEvalPlot
from tasks.plotting import PlotSysts
from tasks.prefit import (
    PlotFitDiagnosticsCombined,
    PlotFitDiagnosticsCombinedAnalyses,
    PlotFitDiagnosticsCombinedCategories,
    PlotPostfitShapesFromWorkspace,
    PlotPostfitShapesFromWorkspaceHL,
)
from tasks.yields import YieldTables, YieldTablesPSFW
from utils.util import walk_directory


class PlotPullsAndImpactsAN20119(PlotPullsAndImpacts):
    # outmap = {"plot.pdf": "*.pdf"}

    @property
    def outmap(self):
        # only pdf
        outmap = dict(super().outmap)
        new_outmap = {k: v for k, v in outmap.items() if k.endswith(".pdf")}
        return new_outmap

    def command(self):
        # multiple pdf pages
        cmd = super().command()
        cmd[0] = "PlotPullsAndImpacts"
        # only pdf for multiple pages
        idx = cmd.index("--file-types")
        cmd[idx + 1] = "pdf"
        cmd += ["--parameters-per-page", "50"]
        return cmd


class YieldTablesAN2019(YieldTables):
    def requires(self):
        return {
            "fitdiags": FitDiagnosticsCombinedPostprocessed.req(
                self,
                analyses=AN20119.analyses,
                years=AN20119.years,
            )
        }


class AN20119Base(
    ModelMixin,
    RecipeMixin,
    CreateIssueMixin,
    CombinationTask,
):
    dhi_command = DHITask.dhi_command
    no_poll = DHI.no_poll

    cms_labels = ["default"]

    def requires(self):
        reqs = {}

        # Yield Tables
        reqs["yields"] = {
            (analysis, year): YieldTablesAN2019.req(
                self,
                year=year,
                analysis_choice=analysis,
                fit_style="preandpostfit",
                unblind=True,
            )
            for analysis in self.analyses
            for year in self.years
        }

        reqs["limits"] = []
        reqs["scans"] = []
        reqs["pulls"] = {"unblind": {}, "asimov": {}}
        reqs["gof"] = []
        reqs["postfit"] = []
        reqs["prefit+postfit"] = []
        reqs["hl_vars"] = []

        for cms_label in self.cms_labels:
            for analysis in self.analyses:
                ana = self.analysis_insts[analysis]
                categories = ana.categories.query(name=".+", tags={"hl"})
                model_version = {
                    "bbww_sl": "approval_dnn_inps_5",
                    "bbww_dl": "approval_dnn_inps_7",
                }[analysis]
                reqs["hl_vars"].extend(
                    PlotPostfitShapesFromWorkspace.req(
                        self,
                        version="prod42",
                        model_version=model_version,
                        analysis_choice=analysis,
                        process_group=["compact"],
                        fit_style="preandpostfit",
                        postfit_unc_frequentist_toys=True,
                        blind_thresh=0,
                        log_scale=True,
                        limit_analyses=("bbww_dl", "bbww_sl"),
                        limit_version="prod41",
                        limit_model_version="approval",
                        unblind=True,
                        scale_signal_to_upper_limit=True,
                        categories={analysis: [category.name]},
                        categories_tags={"hl"},
                        analyses=[analysis],
                        year="run2",
                        fit_version="prod42",
                        fit_model_version=model_version,
                        fit_years=("2016", "2017", "2018"),
                        fit_analyses=[analysis],
                        fit_categories={analysis: [category.name]},
                        fit_categories_tags={"hl"},
                        cms_label=cms_label,
                    )
                    for category in categories
                )
                # reqs["hl_vars"].extend(
                #     PlotPostfitShapesFromWorkspace.req(
                #         self,
                #         version="prod42",
                #         model_version="approval_dnn_inps_3",
                #         analysis_choice=analysis,
                #         process_group=["compact"],
                #         fit_style="preandpostfit",
                #         blind_thresh=0,
                #         log_scale=True,
                #         limit_analyses=("bbww_dl", "bbww_sl"),
                #         limit_version="prod41",
                #         limit_model_version="approval",
                #         unblind=True,
                #         scale_signal_to_upper_limit=True,
                #         categories={analysis: [category.name]},
                #         categories_tags={"hl"},
                #         analyses=[analysis],
                #         year="run2",
                #         fit_version="prod41",
                #         fit_model_version="approval",
                #         fit_years=("2016", "2017", "2018"),
                #         fit_analyses=("bbww_dl", "bbww_sl"),
                #         cms_label=cms_label,
                #     )
                #     for category in categories
                # )

            # reqs["prefit+postfit"].extend(
            #     PlotFitDiagnosticsCombinedAnalyses.req(
            #         self,
            #         year="run2",
            #         analysis_choice=analysis,
            #         analyses=self.analyses[::-1],
            #         log_scale=True,
            #         blind_thresh=0,
            #         process_group=("compact",),
            #         fit_style="preandpostfit",
            #         scale_signal_to_upper_limit=True,
            #         limit_version=self.version,
            #         limit_model_version=self.model_version,
            #         unblind=True,
            #         cms_label=cms_label,
            #     )
            #     for analysis in self.analyses
            # )

            reqs["postfit"].extend(
                PlotPostfitShapesFromWorkspace.req(
                    self,
                    year="run2",
                    analysis_choice=analysis,
                    analyses=self.analyses[::-1],
                    log_scale=True,
                    blind_thresh=0,
                    process_group=("compact",),
                    fit_style="preandpostfit",
                    scale_signal_to_upper_limit=True,
                    postfit_unc_frequentist_toys=True,
                    limit_version=self.version,
                    limit_model_version=self.model_version,
                    fit_version=self.version,
                    fit_model_version=self.model_version,
                    unblind=True,
                    cms_label=cms_label,
                )
                for analysis in self.analyses
            )

            # fmt: off
            # DL, SL, Comb: ["bbww_dl", "bbww_sl", ["bbww_dl", "bbww_sl"]]
            for analyses in list(self.analyses) + [self.analyses]:
            # for analyses in [self.analyses]:
                analyses = law.util.make_list(analyses)

                # limits @SM
                reqs["limits"].append(PlotUpperLimitsAtPoint.req(self, poi="r", analyses=analyses, unblind=True, cms_label=cms_label))
                reqs["limits"].append(PlotUpperLimitsAtPoint.req(self, poi="r_qqhh", analyses=analyses, unblind=True, cms_label=cms_label))

                # Stat. only
                # only with autoMCStats
                reqs["limits"].append(PlotUpperLimitsAtPoint.req(self, poi="r", analyses=analyses, statmodel="StatModelNoSysts", unblind=False, cms_label=cms_label))
                reqs["limits"].append(PlotUpperLimitsAtPoint.req(self, poi="r_qqhh", analyses=analyses, statmodel="StatModelNoSysts", unblind=False, cms_label=cms_label))

                # # only data stat uncertainties
                reqs["limits"].append(PlotUpperLimitsAtPoint.req(self, poi="r", analyses=analyses, statmodel="StatModelStatOnly", unblind=False, cms_label=cms_label))
                reqs["limits"].append(PlotUpperLimitsAtPoint.req(self, poi="r_qqhh", analyses=analyses, statmodel="StatModelStatOnly", unblind=False, cms_label=cms_label))

                # best fit and exclusion
                reqs["limits"].append(PlotExclusionAndBestFit.req(self, pois="r", scan_parameter="kl,-20,20,81", analyses=analyses, unblind=True, cms_label=cms_label))
                reqs["limits"].append(PlotExclusionAndBestFit.req(self, pois="r_qqhh", scan_parameter="C2V,-4,6,81", analyses=analyses, unblind=True, cms_label=cms_label))

                # brazil band
                reqs["scans"].append(PlotUpperLimits.req(self, scan_parameter="kl,-20,25,91", analyses=analyses, unblind=True, cms_label=cms_label))
                reqs["scans"].append(PlotUpperLimits.req(self, scan_parameter="C2V,-4,6,81", analyses=analyses, unblind=True, cms_label=cms_label))

                # multiple brazil bands
                reqs["scans"].append(PlotMultipleUpperLimits.req(self, scan_parameter="kl,-20,20,81", analyses=analyses, unblind=True, cms_label=cms_label))
                reqs["scans"].append(PlotMultipleUpperLimits.req(self, scan_parameter="C2V,-4,6,81", analyses=analyses, unblind=True, cms_label=cms_label))

                # likelihood
                likelihood_params = dict(analyses=analyses, y_log=False, y_min="0", y_max="10")
                reqs["scans"].append(PlotLikelihoodScan.req(self, scan_parameter="kl,-20,20,81", unblind=True, cms_label=cms_label, **likelihood_params))
                reqs["scans"].append(PlotLikelihoodScan.req(self, scan_parameter="C2V,-4,6,81", unblind=True, cms_label=cms_label, **likelihood_params))
                reqs["scans"].append(PlotLikelihoodScan.req(self, scan_parameter="CV,-5,5,21", unblind=True, cms_label=cms_label, **likelihood_params))
                reqs["scans"].append(PlotLikelihoodScan.req(self, scan_parameter="kt,-5,5,21", unblind=True, cms_label=cms_label, **likelihood_params))

                # multiple likelihood
                reqs["scans"].append(PlotMultipleLikelihoodScans.req(self, scan_parameter="kl,-20,20,81", unblind=True, cms_label=cms_label, **likelihood_params))
                reqs["scans"].append(PlotMultipleLikelihoodScans.req(self, scan_parameter="kl,-20,20,81", unblind=False, cms_label=cms_label, **likelihood_params))
                reqs["scans"].append(PlotMultipleLikelihoodScans.req(self, scan_parameter="C2V,-4,6,81", unblind=True, cms_label=cms_label, **likelihood_params))
                reqs["scans"].append(PlotMultipleLikelihoodScans.req(self, scan_parameter="C2V,-4,6,81", unblind=False, cms_label=cms_label, **likelihood_params))
                reqs["scans"].append(PlotMultipleLikelihoodScans.req(self, scan_parameter="CV,-5,5,21", unblind=True, cms_label=cms_label, **likelihood_params))
                reqs["scans"].append(PlotMultipleLikelihoodScans.req(self, scan_parameter="kt,-5,5,21", unblind=True, cms_label=cms_label, **likelihood_params))

                # combined pulls
                reqs["pulls"]["unblind"].update({tuple(analyses): PlotPullsAndImpactsAN20119.req(self, unblind=True, analyses=analyses[::-1], mc_stats=True, cms_label=cms_label)})  # [::-1] due to issue #405
                # reqs["pulls"]["asimov"].update({tuple(analyses): PlotPullsAndImpactsAN20119.req(self, unblind=False, analyses=analyses[::-1], mc_stats=True, cms_label=cms_label)})  # [::-1] due to issue #405

                # gof
                reqs["gof"].append(PlotMultipleGoodnessOfFits.req(self, algo="saturated", toys=300, analyses=analyses, cms_label=cms_label))

            for analyses in [self.analyses]:  # only for total combinations
                # 2D
                # reqs["scans"].append(PlotLikelihoodScan.req(self, scan_parameter="kl,-20,20,41:kt,-5,5,41", analyses=analyses, unblind=True, cms_label=cms_label))  # TODO: may not be needed
                # reqs["scans"].append(PlotLikelihoodScan.req(self, scan_parameter="CV,-5,5,41:C2V,-4,6,41", analyses=analyses, unblind=True, cms_label=cms_label))  # TODO: may not be needed
                # reqs["scans"].append(PlotLikelihoodScan.req(self, scan_parameter="kl,-20,20,41:C2V,-4,6,41", analyses=analyses, unblind=True, cms_label=cms_label))  # TODO: may not be needed
                # reqs["scans"].append(PlotUpperLimits2D.req(self, scan_parameter="kl,-40,40,41:kt,-10,10,41", analyses=analyses, unblind=True, cms_label=cms_label))
                # reqs["scans"].append(PlotUpperLimits2D.req(self, scan_parameter="CV,-5,5,41:C2V,-4,6,41", analyses=analyses, unblind=True, cms_label=cms_label))
                # reqs["scans"].append(PlotUpperLimits2D.req(self, scan_parameter="kl,-20,20,41:C2V,-4,6,41", analyses=analyses, unblind=True, cms_label=cms_label))
                reqs["limits"].append(PlotExclusionAndBestFit2D.req(self, pois="r", scan_parameter="kl,-30,30,31:kt,-10,10,41:kl,-30,30,61:kt,-5,5,41", analyses=analyses, unblind=True, cms_label=cms_label))
                reqs["limits"].append(PlotExclusionAndBestFit2D.req(self, pois="r", scan_parameter="C2V,-5,10,61:CV,-5,5,41", analyses=analyses, unblind=True, cms_label=cms_label))
                reqs["limits"].append(PlotExclusionAndBestFit2D.req(self, pois="r", scan_parameter="kl,-20,25,46:C2V,-4,6,41", analyses=analyses, unblind=True, cms_label=cms_label))
            # fmt: on
        return reqs


class ReportBase:
    def output(self):
        return self.local_target("doc.pdf")

    def make_pdf(self, chapters):
        with TemporaryDirectory() as tmp:
            with open(f"{tmp}/doc.tex", "w") as doc:
                doc.write(
                    r"""\documentclass{article}
                    \usepackage[utf8]{inputenc}
                    \usepackage{siunitx,booktabs}
                    \usepackage{graphicx,graphics}
                    \usepackage{rotating}
                    \usepackage{topcapt}
                    \usepackage[version=4]{mhchem}
                    \usepackage{placeins}
                    \usepackage{newunicodechar}
                    \newunicodechar{≥}{\ensuremath{\geq}}
                    \pdfsuppresswarningpagegroup=1
                    \begin{document}
                    """
                )
                for head, cont in chapters.items():
                    doc.write(f"\\FloatBarrier\n\\section{{{head}}}\n")
                    doc.write(cont)
                    doc.write("\n")
                doc.write("\\end{document}")

            check_call(
                ["latexmk", "-interaction=nonstopmode", "-pdf", "doc.tex"],
                cwd=tmp,
                env=dict(
                    os.environ, max_print_line="1000", error_line="254", half_error_line="238"
                ),
            )

            self.output().copy_from_local(f"{tmp}/doc.pdf")

    def includegraphics(self, path, caption=None):
        tex = r"\begin{figure}[h!]"
        tex += rf"\includegraphics[width=0.9\textwidth]{{{path}}}"
        if caption:
            tex += rf"\caption{{{caption}}}"
        tex += r"\end{figure}"
        return tex


class FakeNCCReport(ReportBase, ModelMixin, RecipeMixin, CombinationTask):
    def requires(self):
        return {
            year: PlotFakeCorrections.req(
                self, equal_bin_widths=True, show_values=True, with_errors=True, year=int(year)
            )
            for year in self.years
        }

    def run(self):
        chapters = defaultdict(str)

        for year in self.years:
            base = self.input()[year].path
            for analysis in ("sl", "dl"):
                for lepton in ("mva030_el", "mva050_mu"):
                    for proc in ("data_comb",):
                        for factor in ("", "pt1", "pt2", "be1", "be2", "up", "down"):
                            if factor:
                                factor = f"_{factor}"
                            path = (
                                base.replace(
                                    ".done",
                                    f"Tallinn_fakerates_{analysis}_FR_{lepton}_{proc}{factor}",
                                )
                                + ".pdf"
                            )
                            chapters[f"Histogram {year} {analysis}"] += self.includegraphics(
                                path, caption=f"{lepton}_{proc}{factor}".replace("_", "-")
                            )

        self.make_pdf(chapters)


class AN20119Checks(ReportBase, AN20119Base):
    def requires(self):
        ret = super().requires()
        ret.pop("limits")
        ret.pop("scans")
        # ret.pop("yields")
        for pulls in ret["pulls"].values():
            for key, val in list(pulls.items()):
                if val.mc_stats and len(val.years) == 1:
                    del pulls[key]

        ret["limits"] = []
        for analyses in list(self.analyses) + [self.analyses]:
            analyses = law.util.make_list(analyses)
            ret["limits"].append(PlotUpperLimitsAtPoint.req(self, poi="r", analyses=analyses))
            ret["limits"].append(PlotUpperLimitsAtPoint.req(self, poi="r_qqhh", analyses=analyses))

        # ret = {k: ret[k] for k in ["yields", "prefit+postfit"]}
        # ret = {
        #     k: {(a, b, c): v for (a, b, c), v in ret[k].items() if c is True}
        #     for k in ["yields", "prefit+postfit"]
        # }

        return ret

    def run(self):
        kinds = [
            ("prefit", "Prefit"),
            ("fit_b", "Postfit (BG only)"),
            # ("fit_s", "Postfit"),
        ]

        chapters = defaultdict(str)
        for (_, _, unblind), base in self.input().get("yields", {}).items():
            if not unblind:  # ignore blinded fitdiagnostics, since unblinded also contain prefit
                continue
            for key, label in kinds:
                tar = base.child(f"{key}_norm/an.tex", "f")
                if tar.exists():
                    tex = tar.load(formatter="text")
                    tex = tex.replace("{bbww_", r"{bbww\_")
                    tex = tex.split("\n", 2)[-1]
                    chapters[f"Yields - {label}"] += f"{tex}\n"

            for task in self.requires().get("prefit+postfit", {}).values():
                if not task.unblind:
                    continue
                for key, label in kinds:
                    shapes = task.output().parent.child(f"shapes_{key}", "d")
                    if not shapes.exists():
                        continue
                    for cat in sorted(shapes.listdir()):
                        # skip signal categories for now, since they dont get blinded correctly
                        if "HH" in cat and key == "fit_b":
                            continue
                        tar = shapes.child(cat, "d").child(f"{task.basename}.pdf")
                        if not tar.exists():
                            continue
                        chapters[f"Histograms - {label}"] += self.includegraphics(tar.path)
            self.make_pdf(chapters)


class AN20119(AN20119Base):
    # fix for AN:
    analyses = ["bbww_dl", "bbww_sl"][::-1]
    years = ["2016", "2017", "2018"]
    # cms_labels = ["default", "pas", "paper"]
    cms_labels = ["pas", "paper"]
    fitdiags_only_run2 = False

    def output(self):
        return self.local_directory_target()

    @property
    def base(self):
        return Path(self.output().path)

    def bundle_output(self, task):
        # TODO: fix PlotFitDiagnosticsCombined

        # general
        if isinstance(task, AnalysisCampaignTask):
            analysis = task.analysis_choice
            year = task.year
        elif isinstance(task, AnalysisTask):
            analysis = task.analysis_choice
            year = "Run2"
        else:
            assert isinstance(task, CombinationTask)
            if len(task.analyses) == 1:
                (analysis,) = task.analyses
            else:
                analysis = "combined"
            if len(task.years) == 1:
                (year,) = task.years
            else:
                year = "Run2"
        # task dependent
        base = self.base / Path(analysis) / year
        # create directory
        base.mkdir(parents=True, exist_ok=True)
        if isinstance(task, YieldTables):
            root = task.output()
            for shape in root.listdir():
                if shape.endswith("_norm"):
                    continue
                shutil.copy(root.child(f"{shape}/an.tex", "f").path, base / f"yield_{shape}.tex")
            return
        if isinstance(task, PlotFitDiagnosticsCombined):
            for prefix, dirs, files, depth in task.output().parent.walk():
                if depth != 2:
                    continue
                prefix = Path(prefix)
                for fn in files:
                    if not fn.endswith(".pdf"):
                        continue
                    fn = prefix / fn
                    shape, cat, name = fn.parts[-3:]
                    # remove prefixes
                    assert shape.startswith(shape_prefix := "shapes_")
                    assert cat.startswith(cat_prefix := f"{analysis}__")
                    shape = shape[len(shape_prefix) :]
                    cat = cat[len(cat_prefix) :]
                    if not "fit" in task.fit_categories_tags:
                        cat += "_on_var"
                    if self.fitdiags_only_run2 and not "run2" in cat:
                        continue
                    # target directory
                    dest = base / shape
                    dest.mkdir(exist_ok=True)
                    # copy
                    shutil.copy(fn, dest / f"{cat}_{name}")
            return
        if isinstance(task, PlotSysts):
            if not isinstance(task.output(), dict):
                raise NotImplementedError(
                    "Fully inclusive PlotSysts plot (.done) currently not supported"
                )
            category = task.category
            for proc_name, proc in task.output().items():
                for syst_name, syst in proc.items():
                    for file_type, file_target in syst.items():
                        if not file_type == "pdf":
                            continue
                        file_path = Path(file_target.path)
                        file_name = file_path.parts[-1]
                        outpath = base / f"{category}_{proc_name}_{syst_name}_{file_name}"
                        shutil.copy(file_path, outpath)
            return
        if isinstance(task, MulticlassEvalPlot):
            for name, directory in task.output().items():
                outpath = base / f"{name}"
                shutil.copytree(directory.path, outpath)
            return
        parts = [type(task).__name__]
        if isinstance(task, PlotUpperLimitsAtPoint):
            parts.append(task.poi)
        if isinstance(task, (PlotLikelihoodScan, PlotMultipleLikelihoodScans)):
            # also handles 2D case
            parts.append(task.pois.replace(",", "_"))
        if isinstance(task, (PlotUpperLimits, PlotMultipleUpperLimits)):
            parts.append(task.scan_parameter.split(",")[0])
        if isinstance(task, PlotMultipleGoodnessOfFits):
            parts.append(task.algo)
        if isinstance(task, PlotExclusionAndBestFit):
            parts.append(f"{task.pois}_{task.scan_parameter.split(',')[0]}")
        if isinstance(task, PlotExclusionAndBestFit2D):
            parts.append(
                f"{task.pois}_{'_'.join(map(lambda x: x.split(',')[0], task.scan_parameter.split(':')[:2]))}"
            )
        if hasattr(task, "unblind"):
            parts.append("unblinded" if task.unblind else "asimov")
        if isinstance(task.output(), (list, tuple)):
            pdfs = list(filter(lambda x: x.path.endswith(".pdf"), task.output()))
            assert len(pdfs) == 1, f"Found multiple pdfs: {pdfs}"
            pdf = pdfs[0]
        elif isinstance(task.output(), law.LocalFileTarget):
            pdf = task.output()
        else:
            raise ValueError(f"Can not interpret task.output type: {type(task.output())}")
        if task.statmodel == "StatModelNoSysts":
            parts.append("autoMCStats_only")
        if task.statmodel == "StatModelStatOnly":
            parts.append("stat_only")
        if hasattr(task, "cms_label"):
            # in case cms_label == "default", the label is not added (consistency with previous behavior)
            if task.cms_label != "default":
                parts.append(task.cms_label)
            # parts.append(task.cms_label)
        # full path
        newpath = base / ("__".join(parts) + ".pdf")
        oldpath = pdf.path
        shutil.copy(oldpath, newpath)

    @law.decorator.safe_output
    def run(self):
        print(f"Bundling outputs into {self.base} ...")

        for task in tqdm(law.util.flatten(self.requires()), desc="Bundle", unit="task"):
            print(task)
            self.bundle_output(task)

        total = law.util.human_bytes(
            sum(f.stat().st_size for f in self.base.rglob("*")), "MB", fmt=True
        )
        print(f"Successfully bundled {total} into {self.base}:")

        tree = Tree(
            f":open_file_folder: [link file://{self.base}]{self.base}",
            guide_style="bold bright_blue",
        )
        walk_directory(self.base, tree)
        Console().print(tree)
        print(
            "Copy output to AN repo via:",
            f"rsync -r vispa:{self.output().path}/ </path/to/AN-20-119>/figures/nonresonant/results",
            sep="\n\t",
        )


class UnblindingSteps(AN20119):
    step = luigi.IntParameter(default=1)
    individual_analyses = luigi.BoolParameter()

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        assert 1 <= self.step <= 5, f"step must be between 1 and 3, got {self.step}"

        if self.step == 1 or self.step == 2 or self.step == 5:
            # all signal + background
            self.categories = {
                analysis: analysis_inst.categories.query(name=".+", tags={"fit"}).keys
                for analysis, analysis_inst in self.analysis_insts.items()
            }
        elif self.step == 3:
            # resolved 1b signal + background
            self.categories = {
                analysis: analysis_inst.categories.query(name=".+", tags={"background"}).keys
                + analysis_inst.categories.query(name=".+", tags={"signal", "resolved_1b"}).keys
                for analysis, analysis_inst in self.analysis_insts.items()
            }
        elif self.step == 4:
            # resolved 1b signal + resolved 2b signal + background
            self.categories = {
                analysis: analysis_inst.categories.query(name=".+", tags={"background"}).keys
                + analysis_inst.categories.query(name=".+", tags={"signal", "resolved_1b"}).keys
                + analysis_inst.categories.query(name=".+", tags={"signal", "resolved_2b"}).keys
                for analysis, analysis_inst in self.analysis_insts.items()
            }

    def store_parts(self):
        return super().store_parts() + (f"step_{self.step}",)

    @property
    def issue_title(self):
        if self.create_issue:
            return (
                f"{self.__class__.__name__} - {self.step} | {self.version} - {self.model_version}"
            )
        else:
            return ""

    def requires(self):
        reqs = {}

        _analyses = [self.analyses]
        if self.individual_analyses:
            _analyses += self.analyses

        if self.step == 1:
            reqs["pulls"] = []
            for analyses in _analyses:
                analyses = law.util.make_list(analyses)
                reqs["pulls"].append(
                    PlotPullsAndImpactsAN20119.req(
                        self,
                        unblind=True,
                        analyses=analyses,
                        categories=self.categories,
                        mc_stats=True,
                        show_best_fit=False,
                        issue_title=self.issue_title,
                    )
                )
            reqs["gof"] = []
            for analyses in _analyses:
                analyses = law.util.make_list(analyses)
                reqs["gof"].append(
                    PlotMultipleGoodnessOfFits.req(
                        self,
                        algo="saturated",
                        toys=1000,
                        analyses=analyses,
                        categories=self.categories,
                        issue_title=self.issue_title,
                    )
                )

        elif self.step == 2:
            # Prefit/Postfit Plots Background Regions
            raise NotImplementedError(
                "Background region only Pre-Postfit at the moment not implemented"
            )
            reqs["prefit+postfit"] = {
                analysis: PlotFitDiagnosticsCombinedAnalyses.req(
                    self,
                    year="run2",
                    analysis_choice=analysis,
                    analyses=self.analyses,
                    log_scale=True,
                    blind_thresh=-np.inf,
                    process_group=("compact",),
                    issue_title=self.issue_title,
                    fit_style="preandpostfit",
                    scale_signal_to_upper_limit=True,
                )
                for analysis in self.analyses
            }
        elif self.step >= 3:
            reqs["limits"] = []
            reqs["scans"] = []

            # Prefit/Postfit Plots
            reqs["prefit+postfit"] = {
                analysis: PlotFitDiagnosticsCombinedAnalyses.req(
                    self,
                    year="run2",
                    analysis_choice=analysis,
                    analyses=self.analyses,
                    log_scale=True,
                    blind_thresh=-np.inf,
                    process_group=("compact",),
                    issue_title=self.issue_title,
                    fit_style="preandpostfit",
                    scale_signal_to_upper_limit=True,
                )
                for analysis in self.analyses
            }

            for analyses in _analyses:
                analyses = law.util.make_list(analyses)
                # fmt: off
                # limits at point
                reqs["limits"].append(PlotUpperLimitsAtPoint.req(self, poi="r", analyses=analyses, categories=self.categories, issue_title=self.issue_title, unblind=True))
                reqs["limits"].append(PlotUpperLimitsAtPoint.req(self, poi="r_qqhh", analyses=analyses, categories=self.categories, issue_title=self.issue_title, unblind=True))
                # limits in scan
                reqs["scans"].append(PlotUpperLimits.req(self, scan_parameter="kl,-20,20,81", analyses=analyses, categories=self.categories, issue_title=self.issue_title, unblind=True))
                reqs["scans"].append(PlotUpperLimits.req(self, scan_parameter="C2V,-4,6,81", analyses=analyses, categories=self.categories, issue_title=self.issue_title, unblind=True))
                reqs["scans"].append(PlotUpperLimits.req(self, scan_parameter="CV,-5,5,161", analyses=analyses, categories=self.categories, issue_title=self.issue_title, unblind=True))
                reqs["scans"].append(PlotUpperLimits.req(self, scan_parameter="kt,-5,5,21", analyses=analyses, categories=self.categories, issue_title=self.issue_title, unblind=True))
                # multiple limits in scan
                reqs["scans"].append(PlotMultipleUpperLimits.req(self, scan_parameter="kl,-20,20,81", analyses=analyses, categories=self.categories, issue_title=self.issue_title, unblind=True))
                reqs["scans"].append(PlotMultipleUpperLimits.req(self, scan_parameter="C2V,-4,6,81", analyses=analyses, categories=self.categories, issue_title=self.issue_title, unblind=True))
                reqs["scans"].append(PlotMultipleUpperLimits.req(self, scan_parameter="CV,-5,5,81", analyses=analyses, categories=self.categories, issue_title=self.issue_title, unblind=True))
                reqs["scans"].append(PlotMultipleUpperLimits.req(self, scan_parameter="kt,-5,5,21", analyses=analyses, categories=self.categories, issue_title=self.issue_title, unblind=True))
                # likelihood in scan
                reqs["scans"].append(PlotLikelihoodScan.req(self, scan_parameter="kl,-20,20,81", analyses=analyses, categories=self.categories, issue_title=self.issue_title, unblind=True))
                reqs["scans"].append(PlotLikelihoodScan.req(self, scan_parameter="C2V,-4,6,81", analyses=analyses, categories=self.categories, issue_title=self.issue_title, unblind=True))
                reqs["scans"].append(PlotLikelihoodScan.req(self, scan_parameter="CV,-5,5,21", analyses=analyses, categories=self.categories, issue_title=self.issue_title, unblind=True))
                reqs["scans"].append(PlotLikelihoodScan.req(self, scan_parameter="kt,-5,5,21", analyses=analyses, categories=self.categories, issue_title=self.issue_title, unblind=True))
                # multiple likelihood in scan
                reqs["scans"].append(PlotMultipleLikelihoodScans.req(self, scan_parameter="kl,-20,20,81", analyses=analyses, categories=self.categories, issue_title=self.issue_title, unblind=True))
                reqs["scans"].append(PlotMultipleLikelihoodScans.req(self, scan_parameter="C2V,-4,6,81", analyses=analyses, categories=self.categories, issue_title=self.issue_title, unblind=True))
                reqs["scans"].append(PlotMultipleLikelihoodScans.req(self, scan_parameter="CV,-5,5,21", analyses=analyses, categories=self.categories, issue_title=self.issue_title, unblind=True))
                reqs["scans"].append(PlotMultipleLikelihoodScans.req(self, scan_parameter="kt,-5,5,21", analyses=analyses, categories=self.categories, issue_title=self.issue_title, unblind=True))
                # fmt: on

        reqs["limits"] = []
        reqs["scans"] = []

        return reqs

    @property
    def copy_message(self):
        return f"[b]Now copy the unblinding step {self.step} from your local laptop[/b]: [cyan]'rsync -r vispa:{self.output().path}/ .'[/cyan]"


class BinningCheck(UnblindingSteps):
    step = 1

    def requires(self):
        reqs = super().requires()
        reqs["expected_limit"] = PlotUpperLimitsAtPoint.req(
            self,
            poi="r",
            analyses=self.analyses,
            categories=self.categories,
            issue_title=self.issue_title,
            unblind=False,
        )
        return reqs


class FreeFloating:
    kappas = {"kl", "kt", "CV", "C2V"}

    def create_model(self):
        model = "hh_model.model_default"
        for kappa in self.kappas:
            if kappa not in self.scan_parameter:
                model += f"@doProfile{kappa}=flat"
        return model


class FreeFloatingPlotUpperLimits(FreeFloating, PlotUpperLimits):
    def command(self):
        cmd = super().command()
        cmd[0] = "PlotUpperLimits"
        # change physics model
        idx = cmd.index("--hh-model")
        cmd[idx + 1] = self.create_model()
        cmd += ["--show-parameters", "True"]
        return cmd


class FreeFloatingPlotLikelihoodScan(FreeFloating, PlotLikelihoodScan):
    def command(self):
        cmd = super().command()
        cmd[0] = "PlotLikelihoodScan"
        # change physics model
        idx = cmd.index("--hh-model")
        cmd[idx + 1] = self.create_model()
        cmd += ["--show-parameters", "True"]
        cmd += ["--shift-negative-values", "True"]
        return cmd


class FreeFloatingScanCheck(UnblindingSteps):
    step = 3

    def requires(self):
        reqs = {"scans": []}
        # fmt: off
        # CV
        reqs["scans"].append(FreeFloatingPlotUpperLimits.req(self, scan_parameter="CV,-5,5,161", analyses=["bbww_dl"], categories=self.categories, issue_title=self.issue_title, unblind=True))
        reqs["scans"].append(FreeFloatingPlotLikelihoodScan.req(self, scan_parameter="CV,-5,5,21", analyses=["bbww_dl"], categories=self.categories, issue_title=self.issue_title, unblind=True))
        # kt
        reqs["scans"].append(FreeFloatingPlotLikelihoodScan.req(self, scan_parameter="kt,-5,5,21", analyses=["bbww_sl"], categories=self.categories, issue_title=self.issue_title, unblind=True))
        # fmt: on
        return reqs


class Issue460(RecipeMixin, AnalysisCampaignTask, law.WrapperTask):
    def requires(self):
        kwargs = dict(
            process_group=["compact"],
            fit_style="preandpostfit",
            blind_thresh=0,
            log_scale=True,
            skip_b_only=True,
            model_version="issue_460_03",
        )
        reqs = []
        for unblind, statmodel in [
            (True, "StatModel"),
            (False, "StatModel"),
            (False, "StatModelNoMCStats"),
            (False, "Issue460GlobalLnN1p001"),
            (False, "Issue460GlobalLnN1p01"),
            (False, "Issue460GlobalLnN1p1"),
            (False, "Issue460GlobalLnN1p5"),
            (False, "Issue460ChristianSuggestion01Scaling2p0"),
            (False, "Issue460ChristianSuggestion01Scaling1p0"),
            (False, "Issue460ChristianSuggestion01Scaling0p1"),
            (False, "Issue460ChristianSuggestion01Scaling0p01"),
        ]:
            reqs.append(
                PlotFitDiagnosticsCombinedCategories.req(
                    self,
                    categories={"bbww_sl": ["all_resolved_sr_prompt_tophiggs"]},
                    unblind=unblind,
                    statmodel=statmodel,
                    **kwargs,
                )
            )
            # the global lnN and christians suggestion (01) are sufficient to check with the top higgs category only
            if statmodel.startswith(("Issue460GlobalLnN", "Issue460ChristianSuggestion01")):
                continue
            reqs.append(
                PlotFitDiagnosticsCombinedCategories.req(
                    self,
                    categories={
                        "bbww_sl": [
                            "all_resolved_sr_prompt_tophiggs",
                            "all_boosted_sr_prompt_dnn_node_class_HHGluGlu_NLO",
                            "all_resolved_1b_sr_prompt_dnn_node_class_HHGluGlu_NLO",
                            "all_resolved_2b_sr_prompt_dnn_node_class_HHGluGlu_NLO",
                        ]
                    },
                    unblind=unblind,
                    statmodel=statmodel,
                    **kwargs,
                )
            )
        return reqs


class ThesisbbWWSL(AN20119):
    # PlotFakeCorrections # Fakerates from ttH Multilep
    # PlotElectronCorrections # Electron SF from ttH Multilep
    # PlotMuonCorrections # Muon SF from ttH Multilep
    # PlotTriggerCorrections # plot trigger corrections
    analyses = ("bbww_sl", "bbww_dl")
    fitdiags_only_run2 = True

    def requires(self):
        analysis = self.analysis_insts["bbww_sl"]
        reqs = {}
        reqs["yields_kin_cat"] = []
        reqs["yields_fit_cat"] = []
        reqs["low_level"] = []
        reqs["high_level"] = []
        reqs["dnn_outputs_prefit"] = []
        reqs["dnn_outputs_postfit"] = []
        reqs["fits"] = []
        reqs["systematic_uncertainties"] = []
        reqs["combination"] = []
        reqs["multiclass_kin"] = []
        reqs["multiclass_nonkin"] = []

        postfit_params = dict(
            year="run2",
            analysis_choice="bbww_sl",
            analyses=["bbww_sl"],
            process_group=["compact"],
            fit_style="preandpostfit",
            unblind=True,
        )
        fit_params = dict(
            fit_version="prod41",
            fit_analyses=["bbww_sl"],
            fit_model_version="thesis_noll_01",
            fit_years=("2016", "2017", "2018"),
            fit_categories_tags={"fit"},
        )
        limit_params = dict(
            limit_version="prod41",
            limit_analyses=["bbww_sl"],
            limit_model_version="thesis_noll_01",
            limit_statmodel="StatModel",
        )
        plot_params = dict(
            blind_thresh=0,
            log_scale=True,
            scale_signal_to_upper_limit=True,
            cms_label="private",
        )

        # Yield Tables
        reqs["yields_kin_cat"].append(
            YieldTablesPSFW.req(
                self,
                model_version="thesis_noll_01",
                years=("2016", "2017", "2018"),
                categories={
                    "bbww_sl": [
                        "all_resolved_1b_sr_prompt",
                        "all_resolved_2b_sr_prompt",
                        "all_boosted_sr_prompt",
                    ]
                },
                categories_tags={},
                n_digits=None,
                **postfit_params,
                **fit_params,
                **limit_params,
            )
        )

        reqs["yields_fit_cat"].append(
            YieldTablesPSFW.req(
                self,
                model_version="approval",
                years=("2016", "2017", "2018"),
                n_digits=None,
                postfit_unc_frequentist_toys=True,
                **postfit_params,
                **fit_params,
                **limit_params,
            )
        )

        # Low-level features
        for category in list(analysis.categories.query(name=".+", tags={"check"})):
            # we can not use the full statmodel for the low-level variables because we do not have an extra processing for these variables
            reqs["low_level"].append(
                PlotPostfitShapesFromWorkspace.req(
                    self,
                    statmodel="StatModelOnlyExtraLnN",
                    categories={"bbww_sl": [category.name]},
                    categories_tags={},
                    fit_statmodel="StatModelOnlyExtraLnN",
                    **postfit_params,
                    **fit_params,
                    **limit_params,
                    **plot_params,
                )
            )

        # High-level features
        for category in list(analysis.categories.query(name=".+", tags={"hl"})):
            # use only prefit plots
            # uses fit categories which are plotted, however because we anyway only use prefit, the exact fit config does not matter as long as the statmodel is the same. switch back to full fit fit model once it has been fitted
            reqs["high_level"].append(
                PlotPostfitShapesFromWorkspace.req(
                    self,
                    version="prod42",  # has full uncertainty model
                    statmodel="StatModelExtraLnN",
                    categories={"bbww_sl": [category.name]},
                    categories_tags={},
                    fit_version="prod42",
                    fit_analyses=["bbww_sl"],
                    fit_model_version="thesis_noll_01_1",
                    fit_years=("2016", "2017", "2018"),
                    fit_statmodel="StatModelExtraLnN",
                    fit_categories={"bbww_sl": [category.name]},
                    fit_categories_tags={},
                    **postfit_params,
                    **limit_params,
                    **plot_params,
                )
            )

        # DNN Outputs Prefit (Uses extra unc. model to "see" rateParams as lnNs)
        reqs["dnn_outputs_prefit"].append(
            PlotPostfitShapesFromWorkspace.req(
                self,
                statmodel="StatModelExtraLnN",
                model_version="thesis_noll_01_1_2",
                fit_version="prod41",
                fit_statmodel="StatModelExtraLnN",
                fit_analyses=["bbww_sl"],
                fit_model_version="thesis_noll_01_1_2",
                fit_years=("2016", "2017", "2018"),
                fit_categories_tags={"fit"},
                **postfit_params,
                **plot_params,
                **limit_params,
            )
        )

        plot_params["cms_label"] = "pas"
        # DNN Outputs Postift
        reqs["dnn_outputs_postfit"].append(
            PlotPostfitShapesFromWorkspace.req(
                self,
                postfit_unc_frequentist_toys=True,
                **postfit_params,
                **plot_params,
                **fit_params,
                **limit_params,
            )
        )

        # Fit results
        combine_params = dict(
            analyses=["bbww_sl"],
            cms_label="private",
            version="prod41",
            model_version="approval",
        )
        likelihood_params = dict(
            y_log=False,
            y_min="0",
            y_max="10",
        )

        # fmt: off
        # limits @SM
        reqs["fits"].append(PlotUpperLimitsAtPoint.req(self, poi="r", unblind=True, **combine_params))
        reqs["fits"].append(PlotUpperLimitsAtPoint.req(self, poi="r_qqhh", unblind=True, **combine_params))

        # Stat. only
        # only with autoMCStats
        reqs["fits"].append(PlotUpperLimitsAtPoint.req(self, poi="r", statmodel="StatModelNoSysts", unblind=False, **combine_params))
        reqs["fits"].append(PlotUpperLimitsAtPoint.req(self, poi="r_qqhh", statmodel="StatModelNoSysts", unblind=False, **combine_params))

        # # only data stat uncertainties
        reqs["fits"].append(PlotUpperLimitsAtPoint.req(self, poi="r", statmodel="StatModelStatOnly", unblind=False, **combine_params))
        reqs["fits"].append(PlotUpperLimitsAtPoint.req(self, poi="r_qqhh", statmodel="StatModelStatOnly", unblind=False, **combine_params))

        # best fit and exclusion
        reqs["fits"].append(PlotExclusionAndBestFit.req(self, pois="r", scan_parameter="kl,-20,20,81", unblind=True, **combine_params))
        reqs["fits"].append(PlotExclusionAndBestFit.req(self, pois="r_qqhh", scan_parameter="C2V,-4,6,81", unblind=True, **combine_params))

        # brazil band
        reqs["fits"].append(PlotUpperLimits.req(self, scan_parameter="kl,-20,25,91", unblind=True, **combine_params))
        reqs["fits"].append(PlotUpperLimits.req(self, scan_parameter="C2V,-4,6,81", unblind=True, **combine_params))

        # multiple brazil bands
        reqs["fits"].append(PlotMultipleUpperLimits.req(self, scan_parameter="kl,-20,20,81", unblind=True, **combine_params))
        reqs["fits"].append(PlotMultipleUpperLimits.req(self, scan_parameter="C2V,-4,6,81", unblind=True, **combine_params))

        # likelihood
        reqs["fits"].append(PlotLikelihoodScan.req(self, scan_parameter="kl,-20,20,81", unblind=True, **combine_params, **likelihood_params))
        reqs["fits"].append(PlotLikelihoodScan.req(self, scan_parameter="C2V,-4,6,81", unblind=True, **combine_params, **likelihood_params))
        reqs["fits"].append(PlotLikelihoodScan.req(self, scan_parameter="CV,-5,5,21", unblind=True, **combine_params, **likelihood_params))
        reqs["fits"].append(PlotLikelihoodScan.req(self, scan_parameter="kt,-5,5,21", unblind=True, **combine_params, **likelihood_params))

        # multiple likelihood
        reqs["fits"].append(PlotMultipleLikelihoodScans.req(self, scan_parameter="kl,-20,20,81", unblind=True, **combine_params, **likelihood_params))
        reqs["fits"].append(PlotMultipleLikelihoodScans.req(self, scan_parameter="kl,-20,20,81", unblind=False, **combine_params, **likelihood_params))
        reqs["fits"].append(PlotMultipleLikelihoodScans.req(self, scan_parameter="C2V,-4,6,81", unblind=True, **combine_params, **likelihood_params))
        reqs["fits"].append(PlotMultipleLikelihoodScans.req(self, scan_parameter="C2V,-4,6,81", unblind=False, **combine_params, **likelihood_params))
        reqs["fits"].append(PlotMultipleLikelihoodScans.req(self, scan_parameter="CV,-5,5,21", unblind=True, **combine_params, **likelihood_params))
        reqs["fits"].append(PlotMultipleLikelihoodScans.req(self, scan_parameter="kt,-5,5,21", unblind=True, **combine_params, **likelihood_params))

        # combined pulls
        reqs["fits"].append(PlotPullsAndImpactsAN20119.req(self, unblind=True, **combine_params))

        # gof
        reqs["fits"].append(PlotMultipleGoodnessOfFits.req(self, algo="saturated", toys=300, **combine_params))
        # fmt: on
        categories = [
            "all_resolved_sr_prompt_tophiggs",
            # "all_resolved_1b_sr_prompt_dnn_node_class_HHGluGlu_NLO",
        ]
        for year, systematics in [
            ("2016", ["UnclustEn", "jer", "RelativeSample_2016", "erdON"]),
            ("2017", ["RelativeSample_2017", "erdON"]),
            ("2018", ["btagWeight_lf", "ScaleWeight_Envelope", "erdON", "jer", "BBEC1", "RelativeBal", "FlavorQCD", "UnclustEn"]),  # fmt: skip
        ]:
            reqs["systematic_uncertainties"].extend(
                PlotSysts.req(
                    self,
                    analysis_choice="bbww_sl",
                    version="prod41",
                    category=category,
                    year=year,
                    cms_label="private",
                    processes=["sum_background"],
                    systematics=systematics,
                    variable="dnn_score_max",
                )
                for category in categories
            )
        for year, systematics in [
            ("2018", ["btagWeight_lf", "PSWeight_FSR", "PSWeight_ISR", "erdON"]),
        ]:
            reqs["systematic_uncertainties"].extend(
                PlotSysts.req(
                    self,
                    analysis_choice="bbww_sl",
                    version="prod41",
                    category=category,
                    year=year,
                    cms_label="private",
                    processes=["tt"],
                    systematics=systematics,
                    variable="dnn_score_max",
                )
                for category in categories
            )
        # combination SL & DL
        combine_params = dict(
            analyses=["bbww_sl", "bbww_dl"],
            cms_label="pas",
            version="prod41",
            model_version="approval",
        )

        # fmt: off
        # at point
        reqs["combination"].append(PlotUpperLimitsAtPoint.req(self, poi="r", unblind=True, **combine_params))
        reqs["combination"].append(PlotUpperLimitsAtPoint.req(self, poi="r_qqhh", unblind=True, **combine_params))

        # brazil band
        reqs["combination"].append(PlotUpperLimits.req(self, scan_parameter="kl,-20,25,91", unblind=True, **combine_params))
        reqs["combination"].append(PlotUpperLimits.req(self, scan_parameter="C2V,-4,6,81", unblind=True, **combine_params))

        # 2D
        reqs["combination"].append(PlotExclusionAndBestFit2D.req(self, pois="r", scan_parameter="kl,-20,25,46:C2V,-4,6,41", unblind=True, **combine_params))
        reqs["combination"].append(PlotExclusionAndBestFit2D.req(self, pois="r", scan_parameter="kl,-30,30,31:kt,-10,10,41:kl,-30,30,61:kt,-5,5,41", unblind=True, **combine_params))
        reqs["combination"].append(PlotExclusionAndBestFit2D.req(self, pois="r", scan_parameter="C2V,-5,10,61:CV,-5,5,41", unblind=True, **combine_params))
        # fmt: on

        dnn = dict(
            recipe="tth",
            version="prod35",
            analysis_choice="bbww_sl",
            training_version=1,
            lbn_particles=12,
            network_architecture="ResNet",
            jump=2,
            layers=3,
            nodes=256,
            activation="relu",
            batch_norm=1,
            optimizer_name="adam",
            class_weight=[0.125, 0.125, 1, 1, 1, 1, 1],
        )
        features_kin = [
            f"{part}_{feat}"
            for part in [
                "lep_cone",
                "analysis_jets0",
                "analysis_jets1",
                "analysis_jets2",
                "analysis_jets3",
                "good_fat_bjets",
            ]
            for feat in ("energy", "x", "y", "z")
        ] + ["metp4_x", "metp4_y"]
        features_nonkin = [
            "m_hh_bregcorr",
            "pt_hh",
            "m_hbb_bregcorr",
            "pt_hbb",
            "m_hww",
            "pt_hww",
            "m_wjj",
            "pt_wjj",
            "m_wlep",
            "pt_wlep",
            "min_dr_lepbjets",
            "dphi_hbb_hww",
            "dphi_hbb_hwwvis",
            "dphi_met_lep",
            "dphi_met_hbb",
            "dphi_met_wjj",
            "dr_lep_hbb",
            "dr_lep_wjj",
            "min_dr_wjets",
            "min_dhi_wjets",
            "min_dr_bjets",
            "min_dphi_bjets",
            "ht",
            "smin",
            "vbf_pair_mass",
            "vbf_pairs_absdeltaeta",
            "lep_conept",
            "vbf_tag",
            "boosted_tag",
            "n_btag",
            "year",
        ]

        reqs["multiclass_kin"].append(
            MulticlassStitchedEvalPlot.req(self, **dnn, plot_features=features_kin)
        )
        reqs["multiclass_nonkin"].append(
            MulticlassStitchedEvalPlot.req(self, **dnn, plot_features=features_nonkin)
        )

        # PlotFakeCorrections --analysis-choice bbww_sl --year 2018 --with-errors --equal-bin-widths --show-values

        # reqs["yields_kin_cat"] = []
        # reqs["yields_fit_cat"] = []
        # reqs["low_level"] = []
        # reqs["high_level"] = []
        # reqs["dnn_outputs_prefit"] = []
        # reqs["dnn_outputs_postfit"] = []
        # reqs["fits"] = []
        # reqs["systematic_uncertainties"] = []
        # reqs["combination"] = []
        # reqs["multiclass_kin"] = []
        # reqs["multiclass_nonkin"] = []
        return reqs

    @property
    def base(self):
        return Path(self.output().path) / self.suffix

    @law.decorator.safe_output
    def run(self):
        self.suffix = ""
        print(f"Bundling outputs into {self.base} ...")
        base = self.base
        for output_class, tasks in self.requires().items():
            if len(tasks) == 0:
                continue
            print(f"Bundling {output_class}")
            self.suffix = output_class
            for task in tasks:
                self.bundle_output(task)

        total = law.util.human_bytes(sum(f.stat().st_size for f in base.rglob("*")), "MB", fmt=True)
        print(f"Successfully bundled {total} into {base}:")

        tree = Tree(
            f":open_file_folder: [link file://{base}]{base}",
            guide_style="bold bright_blue",
        )
        walk_directory(base, tree)
        Console().print(tree)
        print(
            "Copy output to AN repo via:",
            f"rsync -r vispa:{self.output().path}/ </path/to/AN-20-119>/figures/nonresonant/results",
            sep="\n\t",
        )


class CheckSR(ThesisbbWWSL):
    # Check needed during CWR
    # run with: law run CheckSR --version prod41 --recipe tth --model-version sr_check_01 --DatacardProducer-model-version approval --workers 12
    analyses = ("bbww_sl", "bbww_dl")

    def requires(self):
        reqs = {}
        reqs["dnn_outputs_postfit"] = []
        reqs["fits"] = []
        reqs["systematic_uncertainties"] = []

        postfit_params = dict(
            year="run2",
            analyses=["bbww_sl", "bbww_dl"],
            process_group=["compact"],
            fit_style="preandpostfit",
            unblind=True,
            categories_tags={"fit", "signal"},
        )
        fit_params = dict(
            fit_version="prod41",
            fit_analyses=["bbww_sl", "bbww_dl"],
            fit_model_version=self.model_version,
            fit_years=("2016", "2017", "2018"),
            fit_categories_tags={"fit", "signal"},
        )
        limit_params = dict(
            limit_version="prod41",
            limit_analyses=["bbww_sl", "bbww_dl"],
            limit_model_version=self.model_version,
            limit_statmodel="StatModel",
            limit_categories_tags={"fit", "signal"},
        )
        plot_params = dict(
            blind_thresh=0,
            log_scale=True,
            scale_signal_to_upper_limit=True,
            cms_label="private",
        )

        # DNN Outputs Postift
        reqs["dnn_outputs_postfit"].append(
            PlotPostfitShapesFromWorkspace.req(
                self,
                analysis_choice="bbww_sl",
                postfit_unc_frequentist_toys=True,
                **postfit_params,
                **plot_params,
                **fit_params,
                **limit_params,
            )
        )
        reqs["dnn_outputs_postfit"].append(
            PlotPostfitShapesFromWorkspace.req(
                self,
                analysis_choice="bbww_dl",
                postfit_unc_frequentist_toys=True,
                **postfit_params,
                **plot_params,
                **fit_params,
                **limit_params,
            )
        )

        # Fit results
        combine_params = dict(
            analyses=["bbww_sl", "bbww_dl"],
            cms_label="private",
            version="prod41",
            model_version=self.model_version,
            categories_tags={"fit", "signal"},
        )

        # fmt: off
        # limits @SM
        reqs["fits"].append(PlotUpperLimitsAtPoint.req(self, poi="r", unblind=True, **combine_params))
        reqs["fits"].append(PlotUpperLimitsAtPoint.req(self, poi="r_qqhh", unblind=True, **combine_params))

        # combined pulls
        reqs["fits"].append(PlotPullsAndImpactsAN20119.req(self, unblind=True, **combine_params))

        return reqs
