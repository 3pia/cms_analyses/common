# coding: utf-8

from collections import defaultdict
from functools import cached_property
import logging

import hist
import law
import luigi
import luigi.util
import numpy as np
import scipy
from tasks.files import DownloadFiles
import uproot3 as uproot
from rich.console import Console
from rich.table import Table
from tqdm import tqdm

import utils.hist as histutils
from tasks.base import AnalysisCampaignTask, FileProvider, PoolMap
from tasks.coffea import CoffeaProcessor
from tasks.mixins import RecipeMixin
from tasks.plotting import PlotHistsBase
from tools.numpy import intersect2d
from utils.util import NumpyEncoder, pscan_vispa

logger = logging.getLogger(__name__)


class Sync(AnalysisCampaignTask):
    possible_tree_names = {b"Events;1", b"tree;1"}

    @cached_property
    def sync_config(self):
        return self.analysis_inst.aux.get("sync", {})

    @cached_property
    def lookup_set(self):
        raise NotImplementedError

    def lookup(self, key):
        for (old, new) in self.lookup_set:
            key = key.replace(old, new)
        return key

    def fix_dtype(self, arr):
        dtype = arr.dtype
        if dtype == np.uint8:
            return arr.astype(np.int8)
        if dtype == np.uint32:
            return arr.astype(np.int32)
        if dtype == np.uint64:
            return arr.astype(np.int64)
        else:
            return arr

    def write_refactored_tree(self, intree, target):
        target.touch()
        with uproot.recreate(target.path) as file:
            outtree = {}
            for k_in, v_in in intree.items():
                k_in = k_in.decode("utf-8")
                v_in_arr = v_in.array()

                v_out = self.fix_dtype(v_in_arr)
                k_out = self.lookup(k_in)
                outtree[k_out] = v_out
            file["tree"] = uproot.newtree({n: v.dtype for n, v in outtree.items()})
            file["tree"].extend(outtree)

    @cached_property
    def categories(self):
        return self.sync_config.get("categories", None)

    @cached_property
    def int_vars(self):
        return self.sync_config.get("int_vars", [])

    @cached_property
    def eventnr(self):
        return self.sync_config.get("eventnr", "eventnr")

    @cached_property
    def dataset_id(self):
        return self.sync_config.get("dataset_id", "dataset_id")


class SyncUpload(Sync, RecipeMixin):
    processor = luigi.Parameter(default="SyncExporter")
    upload_identifier = luigi.Parameter(default="")

    def requires(self):
        return CoffeaProcessor.req(self, processor=self.processor, debug=True)

    def output(self):
        return self.wlcg_target(f"{self.upload_identifier}sync.root", fs="wlcg_fs_public")

    @cached_property
    def lookup_set(self):
        return self.sync_config.get("lookup_export", {})

    def run(self):
        intree = uproot.open(self.input().path)
        with self.output().localize("w") as target:
            with uproot.recreate(target.path) as file:
                outtree = {k: intree["tree"][k].array() for k in intree["tree"].keys()}
                for k_old in list(outtree.keys()):
                    k_new = self.lookup(k_old)
                    outtree[k_new] = outtree.pop(k_old)
                file["tree"] = uproot.newtree({n: v.dtype for n, v in outtree.items()})
                file["tree"].extend(outtree)
        logger.info(
            f"Succesfully wrote tree at: {'/'.join(self.output().fs.base+[self.output().path])}"
        )


@luigi.util.inherits(SyncUpload)
class SyncUploadWrapper(law.WrapperTask):
    def requires(self):
        return [
            SyncUpload.req(
                self,
                year=y,
                processor="SyncExporter",
            )
            for y in ("2016", "2017", "2018")
        ]


class RefactorSyncFiles(Sync):
    def requires(self):
        return DownloadFiles.req(self, type="sync")

    @cached_property
    def lookup_set(self):
        return self.sync_config.get("lookup_import", {})

    def output(self):
        inp = self.requires().output()
        return {k: self.local_target(f"{k}.root") for k in inp.keys()}

    def run(self):
        for k, v in self.input().items():
            file = uproot.open(v.path)
            tree = [
                file[tree_name]
                for tree_name in self.possible_tree_names
                if tree_name in file.keys()
            ][0]
            self.write_refactored_tree(tree, self.output()[k])


class SyncSelection(PlotHistsBase, Sync, PoolMap):
    files = law.CSVParameter(default=[])
    debug = luigi.BoolParameter()

    n_parallel_max = 32

    @property
    def _files(self):
        out = {}
        for i, f in enumerate(self.files):
            if ":" in f:
                name, file = f.split(":")
            else:
                name, file = str(i), f
            out[name] = file
        return out

    def requires(self):
        return {
            "own": {
                "aachen": CoffeaProcessor.req(self, processor="SyncSelectionExporter", debug=True)
            },
            "external": RefactorSyncFiles.req(self),
        }

    @cached_property
    def keys(self):
        return list(np.intersect1d(*[np.array(dat.dtype.names) for dat in self.data.values()]))

    def is_mask_key(self, key):
        return key.startswith("is_")

    @cached_property
    def mask_keys(self):
        return ["incl"]  # [k for k in self.keys if self.is_mask_key(k)]

    @cached_property
    def feature_keys(self):
        return [k for k in self.keys if k not in self.mask_keys]

    def get_type(self, key):
        if self.is_mask_key(key):
            return "i4"
        elif key in self.int_vars:
            return "i8"
        else:
            return "f4"

    @cached_property
    def data(self):
        inps = self.input()
        inps = {**inps["external"], **inps["own"]}

        out = {}
        for key, target in tqdm(inps.items(), desc="load data"):
            tree = uproot.open(target.path)["tree"]
            keys, size = tree.keys(), len(tree[self.eventnr].array())
            dtype = []
            for k in keys:
                _k = k.decode()
                typ = self.get_type(_k)
                dtype.append((_k, typ))
            arr = np.zeros(shape=(size,), dtype=dtype)
            for name in arr.dtype.names:
                arr[name] = tree[name].array()
            out[key] = arr
        return out

    def output(self):
        return {
            "plots": self.local_target("plots"),
            "metrics": self.local_target("metrics.json"),
        }

    def sync(self, keys):
        metrics = {}
        mask_key, feature_key = keys

        # extract name + binning from variables if possible
        var = self.analysis_inst.variables.get(feature_key)
        if var is None:
            # default binning
            binning = (20, 0, 2)
            x_label = feature_key
        else:
            binning = var.binning
            x_label = rf"{var.x_title} [{var.unit}]"

        compare0 = hist.Hist(
            hist.axis.StrCategory(list(self.data.keys())[:1], name="group"),
            hist.axis.Regular(*binning, name="variable"),
            storage=hist.storage.Weight(),
        )
        compare1 = hist.Hist(
            hist.axis.StrCategory(list(self.data.keys())[1:], name="group"),
            hist.axis.Regular(*binning, name="variable"),
            storage=hist.storage.Weight(),
        )

        comparison, comparison_eventid = None, None
        setdiffs, cleareddiffs, eventdiffs = [], [], []
        for key, dat in self.data.items():
            # mask = dat[mask_key].astype(bool)
            # values = dat[feature_key][mask]
            values = dat[feature_key]
            mask = np.ones_like(values, dtype=np.bool)
            eventid = np.concatenate(
                [
                    np.ones(
                        (sum(mask), 1)
                    ),  # dat[self.dataset_id][mask][:, None],  # can only make this unique if dataset id is known
                    dat[self.eventnr][mask][:, None],
                ],
                axis=-1,
            )

            if comparison is None:
                comparison = values
                comparison_eventid = eventid
                compare0.fill(group=key, variable=values)
            else:
                compare1.fill(group=key, variable=values)

            diff = len(np.setdiff1d(values, comparison))
            rel_setdiff = diff / len(comparison) if len(comparison) != 0 else 1e5
            setdiffs.append(rel_setdiff)
            ind, m, comp_m = intersect2d(eventid, comparison_eventid, return_indices=True)
            cleareddiff = values[m] - comparison[comp_m]
            rel_cleareddiff = (
                np.sum(cleareddiff != 0) / len(cleareddiff) if len(cleareddiff) != 0 else 1e5
            )
            cleareddiffs.append(rel_cleareddiff)
            eventdiffs.append(len(eventid) - len(m) + len(comparison_eventid) - len(comp_m))

        metrics["setdiff"] = setdiffs
        metrics["cleareddiff"] = cleareddiffs
        metrics["eventdiff"] = eventdiffs
        compare0.axes["group"].styles = [dict(color="lightblue")]

        # plotting of histograms
        with self.plot_silencer():
            self.plot(
                targets=[self.local_target(f"plots/{mask_key}/{feature_key}.pdf")],
                stack=compare0,
                lines=compare1,
                ratio="lines",
                x_label=x_label,
            )
        # statistical tests on histograms
        hnew = histutils.kstest(compare0, compare1, 0, -1)
        metrics["kstest"] = hnew.view()
        return keys, metrics

    def run(self):
        metrics = defaultdict(dict)

        # touch cached properties
        self.data
        self.keys
        self.mask_keys
        self.feature_keys

        work = [(m, v) for m in self.mask_keys for v in self.feature_keys]
        for (mask_key, feature_key), metric in self.pmap(
            self.sync,
            work,
            unit="sync",
            unordered=True,
        ):
            metrics[mask_key][feature_key] = metric

        # print summary metrics to console
        table = Table(title="Maximum discrepancies")
        table.add_column("Category", justify="right")
        table.add_column("max setdiff")
        table.add_column("max cleareddiff")
        table.add_column("max kstest")
        table.add_column("eventdiff")
        for k, v in metrics.items():
            max_var_setdiff = max(v, key=lambda k: np.max(v[k]["setdiff"]))
            max_var_cleareddiff = max(v, key=lambda k: np.max(v[k]["cleareddiff"]))
            max_var_kstest = max(v, key=lambda k: np.max(v[k]["kstest"]))
            table.add_row(
                k,
                f"{max_var_setdiff}: {np.max(v[max_var_setdiff]['setdiff'])}",
                f"{max_var_cleareddiff}: {np.max(v[max_var_cleareddiff]['cleareddiff'])}",
                f"{max_var_kstest}: {np.max(v[max_var_kstest]['kstest'])}",
                f"{np.max(v[max_var_kstest]['eventdiff'])}",
            )

        console = Console()
        console.print(table)

        self.output()["metrics"].dump(metrics, cls=NumpyEncoder)
        self.logger.info(
            pscan_vispa(
                directory={self.output()["plots"].path},
                regex=r"(?P<category>.+)/(?P<variable>.+).pdf",
            )
        )
