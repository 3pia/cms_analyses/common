# coding: utf-8

import os
from operator import itemgetter

import law
import luigi
import numpy as np

from tasks.base import AnalysisCampaignTask
from tasks.binning import Rebin
from tasks.mixins import ModelMixin, RecipeMixin, VariableMixin


class DatacardNuisances(ModelMixin, RecipeMixin, AnalysisCampaignTask):

    category = luigi.Parameter(default="mumu_2b")
    variable = luigi.Parameter(default="jet1_pt", description="variable to fit")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # overwrite normal write method
        self.StatModel.dump = dump_yields

    def requires(self):
        return self.StatModel.requires(self)

    def base_requires(self):
        return Rebin.req(self, process_group=("default"))

    def output(self):
        return {"datacard": self.local_target("datacard.txt")}

    def store_parts(self):
        return super().store_parts() + (self.variable,) + (self.category,)

    @law.decorator.timeit(publish_message=True)
    @law.decorator.safe_output
    def run(self):
        output = self.output()
        category_inst = self.analysis_inst.categories.get(self.category)
        if (var := category_inst.get("fit_variable", None)) is not None:
            self.variable = var
            self.logger.info(
                "new variable for {cat} is {var}".format(cat=self.category, var=self.variable)
            )

        hists = self.input().load()[self.variable][self.category]

        model = self.StatModel(
            analysis=self.analysis_choice,
            variable=self.variable,
            category=self.category,
            hists=hists,
            analysis_inst=self.analysis_inst,
            campaign_inst=self.campaign_inst,
        )

        # Write datacard to vispa
        output["datacard"].parent.touch()
        model.dump(
            output["datacard"].dirname,
            output["datacard"].basename,
        )


def dump_yields(
    self,
    directory,
    txt="datacard.txt",
):
    txt_path = os.path.join(directory, txt)
    card = open(txt_path, "w")
    # first write header
    card.write(f"# {repr(self)}{self.sep}")
    # build:
    #  - check histograms
    #  - fix histograms
    #  - build systematics
    self.build()
    card.write(f"imax 1 number of bins{self.sep}")
    card.write(f"jmax * number of processes minus 1{self.sep}")
    card.write(f"kmax * number of nuisance parameters{self.sep}")
    card.write(f"{self.dashes}{self.sep}")
    card.write(f"shapes * {self.bin} IAMADUMMY {self.nominal_pattern} {self.sys_pattern}{self.sep}")
    card.write(f"{self.dashes}{self.sep}")
    card.write(f"bin {self.bin}{self.sep}")
    card.write(f"observation {self.observation}{self.sep}")
    card.write(f"{self.dashes}{self.sep}")
    card.write("bin".ljust(self.spaces))  # category
    card.write("".join([f"{self.bin.ljust(self.spaces)}"] * len(self.vps)) + f"{self.sep}")
    card.write("process".ljust(self.spaces))  # process names
    card.write(
        "".join([f"{self.rprocesses[p]}".ljust(self.spaces) for p in self.vps]) + f"{self.sep}"
    )
    card.write("process".ljust(self.spaces))  # process ids
    card.write("".join([f"{i}".ljust(self.spaces) for i in self.vids]) + f"{self.sep}")
    card.write("rate".ljust(self.spaces))
    card.write("".join([f"{self.rate(p)}".ljust(self.spaces) for p in self.vps]) + f"{self.sep}")
    card.write(f"{self.dashes}{self.sep}")
    if self._nuisances_names:
        for name, type in self._nuisances_names:
            if not type == "shape":
                continue
            nidx = self.nuisance_idx(name)
            card.write(f"{self.rnuisances[name]} {type}".ljust(self.spaces))
            for pidx, pname in enumerate(self.vps):
                s = self._nuisances_arr[nidx, pidx, ...]
                assert s.size == 2
                if not np.any(s):
                    card.write("-".ljust(self.spaces))
                    continue
                hname = self.sys_pattern.replace("$PROCESS", self.rprocesses[pname]).replace(
                    "$SYSTEMATIC", self.rnuisances[name]
                )
                hup = np.sum(self._newhists[hname + "Up"].allvalues) / self.rate(pname)
                hdown = np.sum(self._newhists[hname + "Down"].allvalues) / self.rate(pname)
                card.write(f"{round(hup, 4)}/{round(hdown, 4)}".ljust(self.spaces))
            card.write(f"{self.sep}")


class DipoleRecoilOn(ModelMixin, RecipeMixin, VariableMixin, AnalysisCampaignTask):
    template = 'self.add_systematics(names="{name}", type="lnN", strengths={strength}, processes="{process}",)'
    default_lnN = "1.1"

    def requires(self):
        return self.StatModel.requires(self)

    def base_requires(self):
        return Rebin.req(self, process_group=("default"))

    def output(self):
        return self.local_target(f"dipolerecoilon_{self.analysis_choice}_{self.year}.md")

    def complete(self):
        return False

    @law.decorator.timeit(publish_message=True)
    @law.decorator.safe_output
    def run(self):
        from rich import print as pprint
        from tabulate import tabulate

        hists = self.input().load()[self.variable]

        from rich import box
        from rich.table import Table

        def convert_lnN(x, digits=2):
            assert x > 0.0
            u = x - 1.0
            # if x > 1.0:
            #     u = x - 1.0
            # else:
            #     u = 1.0 - x

            return (
                np.round(np.clip(1.0 - u, a_min=0.01, a_max=None), digits),
                np.round(1.0 + u, digits),
            )

        def colorize(v, lnN=True):
            good = "[green]{:.2f}[/green]"
            okay = "[yellow]{:.2f}[/yellow]"
            bad = "[red]{:.2f}[/red]"

            if lnN:
                if v < 1.0:
                    v_ = 1.0 + (1.0 - v)
                else:
                    v_ = v
            else:
                v_ = v / 100.0
                if v_ < 1.0:
                    v_ += 1.0
            if v_ - 1 <= 0.05:
                return good.format(v)
            elif v_ - 1 <= 0.15:
                return okay.format(v)
            else:
                return bad.format(v)

        table = Table(
            title=f"{type(self).__qualname__} - {self.analysis_choice}, {self.year}",
            box=box.SIMPLE,
            show_footer=True,
        )

        table.add_column(
            "Category / Process (CV, C2V, kl)", justify="left", style="cyan", no_wrap=True
        )
        processes = [
            ("qqHH_CV_1_C2V_1_kl_1_2B2Tau", "2B2Tau (1, 1, 1)"),
            ("qqHH_CV_1_C2V_1_kl_1_2B2V", "DL+SL - 2B2V (1, 1, 1)"),
            ("qqHH_CV_1_C2V_2_kl_1_2B2Tau", "2B2Tau (1, 2, 1)"),
            ("qqHH_CV_1_C2V_2_kl_1_2B2V", "DL+SL - 2B2V (1, 2, 1)"),
        ]
        for p in processes:
            table.add_column(p[1], justify="right")

        md = {k: [] for k in ["Category"] + [*map(itemgetter(1), processes)]}

        for cat, hist in hists.items():
            if (
                (cat not in self.analysis_inst.categories.keys)
                or (not "fit" in self.analysis_inst.categories.get(cat).tags)
                or ("VBF" not in cat)
            ):
                continue
            tmp = []
            md["Category"].append(self.analysis_inst.categories.get(cat).label_short)
            for p in processes:
                # nom
                nom = np.sum(hist[p[0], "nominal", :].view()["value"])
                on = np.sum(hist[p[0], "dipoleRecoilOn", :].view()["value"])
                if nom == 0 or on == 0:
                    continue

                lnN = convert_lnN(nom / on)

                # stat unc
                nom_unc = np.sqrt(np.sum(hist[p[0], "nominal", :].view()["variance"]))
                on_unc = np.sqrt(np.sum(hist[p[0], "dipoleRecoilOn", :].view()["variance"]))
                nom_unc = 100 * nom_unc / nom
                on_unc = 100 * on_unc / on

                entry = f"{str(colorize(lnN[0]))}/{str(colorize(lnN[1]))} (Off={colorize(nom_unc, lnN=False)}%, On={colorize(on_unc, lnN=False)}%)"
                # is stat unc larger than this lnN?
                if nom_unc / 100 >= abs(1 - lnN[0]):
                    entry = "[bold]X[/bold] " + entry
                    md[p[1]].append("1.00")
                else:
                    md[p[1]].append(f"{lnN[1]}")
                tmp.append(entry)

            table.add_row(self.analysis_inst.categories.get(cat).label_short, *tmp)

        # pretty table
        pprint(table)
        pprint(
            "[bold]Value format[/bold]: [[bold]X[/bold] if nominal stat_unc. >= lnN] lnN (Off = stat_unc., On = stat_unc.)"
        )
        pprint(
            "[bold]Color Level[/bold] : [green]0-5%[/green], [yellow]5-15%[/yellow], [red]>=15%[/red]\n"
        )

        # markdown results
        md["Category"].append("Rest")
        for p in processes:
            md[p[1]].append(self.default_lnN)

        self.output().parent.touch()
        self.output().dump(
            tabulate(md, headers="keys", floatfmt=".2f", tablefmt="github"), formatter="text"
        )
