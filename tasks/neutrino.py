# coding: utf-8

import luigi
import numpy as np
import tensorflow as tf

from config import constants
from tasks.base import BaseTask


# neutrino reconstruction
@tf.function
def calc_v_y(v_x, part_x, part_y, mc2, sign=+1, guard_s=False):
    s = (mc2 / 2 * (part_x ** 2 + part_y ** 2) * (2 * part_x * v_x + mc2 / 2)) ** 0.5
    if guard_s:
        s = tf.where(s > 0, s, 0)
    return (part_x * part_y * v_x + part_y * mc2 / 2 + sign * s) / part_x ** 2


@tf.function
def shift(v_x, v_y, met_x, met_y):
    return (met_x - v_x) ** 2 + (met_y - v_y) ** 2


@tf.function
def get_k(part_x, part_y, v_x, v_y, mc2):
    return mc2 / 2 + part_x * v_x + part_y * v_y


@tf.function
def get_pt(px, py):
    return (px ** 2 + py ** 2) ** 0.5


@tf.function
def get_h(part_x, part_y, met_x, met_y, mc2):
    k = get_k(part_x, part_y, met_x, met_y, mc2)
    part_pt = get_pt(part_x, part_y)
    metpt = get_pt(met_x, met_y)
    return (part_pt * metpt / k) ** 2


@tf.function
def neutrino_reco_real(part_e, part_x, part_y, part_z, v_x, v_y, mc2):
    # standard reconstruction
    k = get_k(part_x, part_y, v_x, v_y, mc2)
    part_pt = get_pt(part_x, part_y)
    h = get_h(part_x, part_y, v_x, v_y, mc2)

    # positive solution
    v_zp = k / part_pt ** 2 * (part_z + part_e * (1 - h) ** 0.5)
    # negative solution
    v_zn = k / part_pt ** 2 * (part_z - part_e * (1 - h) ** 0.5)

    # # variant 1: take v_z which better reproduces mass constrain (doesn't this always fit exactly?)
    # mcrp = 2 * (part_x * v_x + part_y * v_y + part_z * v_zp)
    # mcrn = 2 * (part_x * v_x + part_y * v_y + part_z * v_zn)

    # delta2p = (mcrp - mc) ** 2
    # delta2n = (mcrn - mc) ** 2
    # v_z = tf.where(delta2p <= delta2n, v_zp, v_zn)

    # variant 2: take smaller momentum (correct in 70% of cases)
    v_z = tf.where(tf.abs(v_zp) <= tf.abs(v_zn), v_zp, v_zn)

    return v_x, v_y, v_z


@tf.function
def v_x_constrain(v_x, part_x, mc2):
    inf = tf.constant(np.inf)
    constr_val = -mc2 / (4 * part_x)
    return tf.where(
        part_x >= 0,
        tf.clip_by_value(v_x, constr_val, inf),
        tf.clip_by_value(v_x, -inf, constr_val),
    )


@tf.function
def opt(v_x0, part_x, part_y, met_x, met_y, mc2, sign=+1):
    v_y = calc_v_y(v_x0, part_x, part_y, mc2, sign=sign)
    return shift(v_x0, v_y, met_x, met_y)


@tf.function
def optimize1(optimizer, v_x, met_x, met_y, part_x, part_y, mc2, sign=+1):
    # two different optimise functions to use them simultaneaously in one computing graph
    batch_size = tf.shape(met_x)[0]
    for _ in range(100):
        with tf.control_dependencies(
            [
                optimizer.minimize(
                    lambda: opt(v_x[:batch_size], part_x, part_y, met_x, met_y, mc2, sign=sign),
                    var_list=[v_x],
                )
            ]
        ):
            v_x[:batch_size].assign(v_x_constrain(v_x[:batch_size], part_x, mc2))
    return v_x[:batch_size]


@tf.function
def optimize2(optimizer, v_x, met_x, met_y, part_x, part_y, mc2, sign=+1):
    # two different optimise functions to use them simultaneaously in one computing graph
    batch_size = tf.shape(met_x)[0]
    for _ in range(100):
        with tf.control_dependencies(
            [
                optimizer.minimize(
                    lambda: opt(v_x[:batch_size], part_x, part_y, met_x, met_y, mc2, sign=sign),
                    var_list=[v_x],
                )
            ]
        ):
            v_x[:batch_size].assign(v_x_constrain(v_x[:batch_size], part_x, mc2))
    return v_x[:batch_size]


@tf.function
def neutrino_reco_complex(
    part_e, part_x, part_y, part_z, met_x, met_y, v_x1, v_x2, opt1, opt2, mc2
):
    # complex solution, fit using tensorflow
    v_x1 = optimize1(opt1, v_x1, met_x, met_y, part_x, part_y, mc2, sign=+1)
    v_x2 = optimize2(opt2, v_x2, met_x, met_y, part_x, part_y, mc2, sign=-1)
    # tf.print

    # catch failed optimizations
    constr_val = -mc2 / (4 * part_x)
    v_x1 = tf.where(tf.math.is_nan(v_x1), constr_val, v_x1)
    v_x2 = tf.where(tf.math.is_nan(v_x2), constr_val, v_x2)

    # reconstruct y component
    v_y1 = calc_v_y(v_x1, part_x, part_y, mc2, sign=+1, guard_s=True)
    v_y2 = calc_v_y(v_x2, part_x, part_y, mc2, sign=-1, guard_s=True)

    # calculate shift between reco and met
    diff1 = shift(v_x1, v_y1, met_x, met_y)
    diff2 = shift(v_x2, v_y2, met_x, met_y)

    # take smaller shift
    v_x = tf.where(diff1 <= diff2, v_x1, v_x2)
    v_y = tf.where(diff1 <= diff2, v_y1, v_y2)

    # reconstruct pz component
    k = get_k(part_x, part_y, v_x, v_y, mc2)
    part_pt = get_pt(part_x, part_y)
    v_z = k * part_z / part_pt ** 2
    return v_x, v_y, v_z


@tf.function
def neutrino_reco(part_e, part_x, part_y, part_z, met_x, met_y, v_x1, v_x2, opt1, opt2, mc):
    # include part_ mass in mass constrain
    part_m = (part_e ** 2 - part_x ** 2 - part_y ** 2 - part_z ** 2) ** 0.5
    part_m = tf.where(tf.math.is_nan(part_m), tf.zeros_like(part_m), part_m)
    mc2 = mc ** 2 - part_m ** 2

    # interleave standard reconstruction (1) and complex solution (2)
    v_x_real, v_y_real, v_z_real = neutrino_reco_real(
        part_e, part_x, part_y, part_z, met_x, met_y, mc2
    )
    v_x_complex, v_y_complex, v_z_complex = neutrino_reco_complex(
        part_e, part_x, part_y, part_z, met_x, met_y, v_x1, v_x2, opt1, opt2, mc2
    )
    h = get_h(part_x, part_y, met_x, met_y, mc2)
    v_x = tf.where(h <= 1, v_x_real, v_x_complex)
    v_y = tf.where(h <= 1, v_y_real, v_y_complex)
    v_z = tf.where(h <= 1, v_z_real, v_z_complex)

    return v_x, v_y, v_z


class NeutrinoReco(tf.keras.layers.Layer):
    def __init__(self, mass_constraint=constants.W_MASS.nominal, optimizer="Adam", batch_size=1000):
        super(NeutrinoReco, self).__init__(name="")
        self.opt1 = tf.keras.optimizers.get(optimizer)
        self.opt2 = tf.keras.optimizers.get(optimizer)

        self.mass_constraint = mass_constraint
        self.batch_size = batch_size

    def build(self, input_shape):
        self.v_x1 = self.add_weight(shape=(self.batch_size,), name="v_x1")
        self.v_x2 = self.add_weight(shape=(self.batch_size,), name="v_x2")

    def reset_optimizer(self, *args, **kwargs):
        # reset optimizer to initial values (all zeros)
        for var in self.opt1.variables():
            var.assign(tf.zeros_like(var))
        for var in self.opt2.variables():
            var.assign(tf.zeros_like(var))

    @tf.function
    def call(self, inputs, training=False):
        self.reset_optimizer()
        part_e, part_x, part_y, part_z, met_x, met_y = (inputs[:, i] for i in range(6))

        with tf.control_dependencies(
            [
                self.v_x1[: tf.shape(met_x)[0]].assign(met_x),
                self.v_x2[: tf.shape(met_x)[0]].assign(met_x),
            ]
        ):
            v_x, v_y, v_z = neutrino_reco(
                part_e,
                part_x,
                part_y,
                part_z,
                met_x,
                met_y,
                self.v_x1,
                self.v_x2,
                self.opt1,
                self.opt2,
                self.mass_constraint,
            )
        return v_x, v_y, v_z


class NeutrinoReconstructionFit1(BaseTask):
    batch_size = luigi.IntParameter(default=150_000)
    optimizer = luigi.ChoiceParameter(
        choices=["SGD", "Adam", "Adadelta", "RMSprop"],
        default="SGD",
        description="Choice of the optimizer which is used in the neutrino reconstruction. Careful: RMSprop has been observed to sometimes (5/10^6) lead to failed minimisation and broken neutrino momenta (>100TeV).",
    )
    mc = luigi.ChoiceParameter(choices=["W_MASS", "H_MASS"], default="W_MASS")

    def store_parts(self):
        return super().store_parts() + (
            f"optimizer_{self.optimizer}",
            f"batch_size_{self.batch_size}",
            f"mc_{self.mc}",
        )

    def output(self):
        return self.local_target("model/.done")

    def run(self):
        inp = tf.keras.layers.Input(shape=(6,))
        mass_constrain = getattr(constants, self.mc).nominal
        reco = NeutrinoReco(
            mass_constraint=mass_constrain,
            optimizer=self.optimizer,
            batch_size=self.batch_size,
        )
        v_x, v_y, v_z = reco(inp)
        outp = [
            tf.keras.layers.Lambda(lambda x: x, name=n)(t)
            for (n, t) in [("x", v_x), ("y", v_y), ("z", v_z)]
        ]
        model = tf.keras.models.Model(inputs=inp, outputs=outp)
        model.save(self.output().parent.path)
        self.output().touch()


class NeutrinoReconstructionFit2(BaseTask):
    batch_size = NeutrinoReconstructionFit1.batch_size

    def output(self):
        return self.local_target("model_dilepton_test/.done")

    def run(self):
        inp = tf.keras.layers.Input(shape=(6,))
        reco = NeutrinoReco(mass_constraint=H_MASS.nominal, batch_size=self.batch_size)
        v_x, v_y, v_z = reco(inp)
        outp = [
            tf.keras.layers.Lambda(lambda x: x, name=n)(t)
            for (n, t) in [("x", v_x), ("y", v_y), ("z", v_z)]
        ]
        model = tf.keras.models.Model(inputs=inp, outputs=outp)
        model.save(self.output().parent.path)
        self.output().touch()
