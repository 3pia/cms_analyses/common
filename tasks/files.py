# -*- coding=utf-8 -*-
import hashlib
import logging
import os
import stat
from functools import cached_property
from multiprocessing import Pool
from multiprocessing.dummy import Pool as ThreadPool
from shutil import copytree
from subprocess import check_call

import law
import luigi
from tqdm.auto import tqdm

from tasks.base import AnalysisCampaignTask, BaseTask, CampaignTask
from utils.cern import CERNSession, Session

logger = logging.getLogger(__name__)

allowed_types = frozenset(
    {"default", "histo", "btag", "jec", "jersf", "jr", "junc", "sync", "triggersf"}
)


class DownloadFilesCampaign(CampaignTask):

    afs_host = "lxplus.cern.ch"
    afs_user = os.environ.get("DHA_GRID_USER", None)

    type = luigi.ChoiceParameter(
        choices=list(allowed_types),
        description="Type of correction format for further coffea processing",
    )
    version = None

    @property
    def files(self):
        return self.campaign_inst.aux["files"]

    @property
    def urls(self):
        return self.files[self.type]

    def url2target(self, src):
        # map e.g. https source path to local file target
        # src url -> local path dst
        assert self.type in allowed_types
        full_filename = os.path.split(src)[-1]
        filename, file_extension = os.path.splitext(full_filename)
        hashed = hashlib.sha256(src.encode("utf-8")).hexdigest()
        hashed_file_name = "{hashed}/{filename}.{type}{format}".format(
            hashed=hashed, filename=filename, type=self.type, format=file_extension
        )
        return self.local_target(hashed_file_name)

    def output(self):
        return law.util.map_struct(self.url2target, self.urls)

    def run(self):
        urls = set(law.util.flatten(self.urls))
        bad = []

        with CERNSession() as csess, Session() as sess:
            for url in tqdm(urls):
                dst = self.url2target(url)
                if dst.exists():
                    continue
                dst.parent.touch()

                if url.startswith("http"):
                    s = csess if "cern.ch/" in url else sess
                    with s.get(url, stream=True) as r:
                        if not r.ok:
                            bad.append(url)
                            continue
                        with dst.localize(mode="w") as t, t.open("wb") as f:
                            for chunk in r.iter_content():
                                f.write(chunk)
                elif url.startswith(("/afs", "/eos")):
                    url = f"{self.afs_host}:{url}"
                    if self.afs_user:
                        url = f"{self.afs_user}@{url}"
                    with dst.localize(mode="w") as tar:
                        check_call(["scp", url, tar.path])
                else:
                    dst.copy_from_local(url)

        if bad:
            raise RuntimeError("\n\t".join(["failed to download:"] + sorted(bad)))


class DownloadFiles(AnalysisCampaignTask, DownloadFilesCampaign):
    @property
    def files(self):
        return self.analysis_inst.aux.get("files", self.campaign_inst.aux["files"])


class DownloadFilesWrapper(CampaignTask, law.WrapperTask):

    version = None

    def requires(self):
        return [DownloadFiles.req(self, type=type) for type in allowed_types]


def download(job):
    src, dst = job
    if dst.exists():
        try:
            assert dst.stat().st_size == src.stat().st_size
            return dst.stat().st_size
        except:
            pass
    try:
        dst.parent.touch()  # explicitly create parent(s) w/o unwanted chmod (unlike via tmp_dir)
        with dst.localize(mode="w", tmp_dir=dst.parent) as tmp:
            src.copy_to_local(tmp, perm=stat.S_IRUSR | stat.S_IRGRP)
    except Exception as e:
        logger.warn("failed to downlaod %s due to %s" % (src, e))
        return 0
    return dst.stat().st_size


def size(job):
    try:
        return job[0].stat().st_size
    except Exception as e:
        logger.warn(f"failed to retrieve a size due to {e}")
        return 1


class DownloadNanoAODs(CampaignTask):
    skip_output_removal = True
    version = None
    threshold = luigi.FloatParameter(default=1.0, significant=False)
    only_missing = luigi.BoolParameter(significant=False)
    parallel_downloads = luigi.IntParameter(
        -1,
        significant=False,
        description=", ".join(
            [
                "number of parallel_downloads download:",
                "=0 no parallelization (for debugging)",
                ">0 number of threads to use",
                "<0 -number of threads to use on each dask worker",
            ]
        ),
    )
    datasets = law.CSVParameter(default=[], significant=False)
    only_nominal = luigi.BoolParameter(significant=False)

    @cached_property
    def mapping(self):
        datasets = set(self.datasets or self.campaign_inst.datasets.keys)
        fs = self.wlcg_target("", fs="cms_fs").fs
        return {
            (ds.name, shift): (
                info["aux"]["n_bytes"],
                [
                    (
                        self.wlcg_target(lfn, fs=fs),
                        self.local_target(ds.name, shift, lfn.rsplit("/", 1)[-1]),
                    )
                    for lfn in info["aux"]["lfns"]
                ],
            )
            for ds in self.campaign_inst.datasets.values
            if ds.name in datasets
            for shift, info in ds.info.items()
            if "lfns" in info["aux"] and shift == ("nominal" if self.only_nominal else shift)
        }

    def output(self):
        assert 0 <= self.threshold <= 1.0
        return {
            key: law.SiblingFileCollection([dst for src, dst in jobs], threshold=self.threshold)
            for key, (bytes, jobs) in self.mapping.items()
        }

    def run(self):
        work = self.mapping.values()

        if self.only_missing:
            with ThreadPool(16) as pool:

                def sum_size(jobs, label: str) -> int:
                    return sum(
                        tqdm(
                            pool.imap_unordered(
                                size,
                                jobs,
                                chunksize=1,
                            ),
                            desc=f"still needed download size ({label})",
                            unit="file",
                            total=len(jobs),
                            leave=False,
                        )
                    )

                fs = self.wlcg_target("", fs="cms_fs").fs
                work = [
                    (
                        (
                            # count missing
                            sum_size(missing, "missing")
                            # are missing less than total
                            if (
                                len(
                                    missing := list(
                                        tqdm(
                                            (job for job in jobs if not job[1].exists()),
                                            desc="count existing",
                                            unit="file",
                                            total=len(jobs),
                                            leave=False,
                                        )
                                    )
                                )
                                < 0.5 * len(jobs)
                            )
                            # count existing
                            else (bytes - sum_size(set(jobs) - set(missing), "done"))
                        ),
                        [(self.wlcg_target(src.path, fs=fs), dst) for src, dst in missing],
                    )
                    for bytes, jobs in tqdm(work, desc="calc total download size", unit="dataset")
                ]

        total = sum(bytes for bytes, jobs in work)
        jobs = [job for bytes, jobs in work for job in jobs]

        if self.parallel_downloads == 0:
            results = map(download, jobs)
        elif self.parallel_downloads > 0:
            results = Pool(self.parallel_downloads).imap_unordered(download, jobs, chunksize=1)
        else:
            from dask.distributed import Client
            from distributed import as_completed
            from distributed.security import Security

            from utils.dask import HTCondorCluster

            cluster = HTCondorCluster(
                cores=-self.parallel_downloads,
                memory="1000MiB",
                disk="2GB",
                job_extra=dict(Request_CPUs=0, Request_GPUs=0, Request_GpuMemory=1, Getenv=True),
                security=Security(),
                protocol="tls://",
                scheduler_options=dict(dashboard_address=os.environ["DHA_DASHBOARD_ADDRESS"]),
            )
            cluster.scale_machines({"*": 1, "*worker*": 1, "*gpu10*": 0, "*portal*": 0}, timeout=30)
            client = Client(cluster, security=Security())

            results = (
                sum(future.result() for future in batch)
                for batch in as_completed(client.map(download, jobs, pure=True)).batches()
            )

        with tqdm(total=total, unit="B", unit_scale=True, desc="download") as prog:
            for done in results:
                prog.update(done)


class DownloadNanoAODsWrapper(law.WrapperTask):
    threshold = DownloadNanoAODs.threshold
    only_missing = DownloadNanoAODs.only_missing
    parallel_downloads = DownloadNanoAODs.parallel_downloads

    def requires(self):
        return [DownloadNanoAODs.req(self, year=str(y)) for y in [2016, 2017, 2018]]


class LHAPDFData(law.ExternalTask):
    def output(self):
        return law.LocalDirectoryTarget(
            "/cvmfs/cms.cern.ch/slc7_amd64_gcc700/external/lhapdf/6.2.1-gnimlf4/share/LHAPDF"
        )


class LHAPDF(BaseTask):
    version = None

    def requires(self):
        return LHAPDFData()

    def output(self):
        return self.local_target("lhapdf.coffea")

    def run(self):
        from utils.lhapdfv import LHAPDF

        return self.output().dump(LHAPDF(self.input().path))


class LHAPDFCompact(CampaignTask):
    version = None
    names = law.CSVParameter()

    def requires(self):
        return LHAPDFData()

    def output(self):
        return self.local_target("lhapdf.coffea")

    def complete(self):
        out = self.output()
        return out.exists() and all(out.parent.child(n, "d").exists() for n in self.names)

    def run(self):
        from utils.lhapdfv import LHAPDF

        out = self.output().parent
        out.touch()

        for name in tqdm(self.names, desc="copy files", unit="pdf set"):
            copytree(
                src=self.input().child(name, "d").path,
                dst=out.child(name, "d").path,
                dirs_exist_ok=True,
            )

        for name in "pdfsets.index", "lhapdf.conf":
            self.input().child(name, "f").copy_to(out.child(name, "f"))

        return self.output().dump(LHAPDF(out.path))
