# coding: utf-8

from collections import defaultdict
from typing import Iterable

import law
import luigi
import luigi.util
import numpy as np
from rich.console import Console
from rich.table import Table

from tasks.base import AnalysisCampaignTask
from tasks.combine import (
    FitDiagnosticsCombinedPostprocessed,
    PostfitShapesFromWorkspacePostprocessed,
)
from tasks.mixins import ModelMixin, PGroupMixin, RecipeMixin
from tasks.plotting import PlotBase
from tasks.prefit import PlotPostfitShapesFromWorkspace, PrepareFitDiagnostics


class YieldTables(PrepareFitDiagnostics, ModelMixin, RecipeMixin, AnalysisCampaignTask):
    n_digits = luigi.IntParameter(
        default=2,
        description="Number of digits to round. None will print integers.",
    )

    def requires(self):
        return {
            "fitdiags": FitDiagnosticsCombinedPostprocessed.req(
                self,
                analyses=(self.analysis_choice,),
                years=(self.year,),
            )
        }

    def output(self):
        return self.local_target()

    def bkgs(self, h):
        return list(set(h.axes["process"]) & set(self.StatModel.background_processes))

    @law.decorator.timeit(publish_message=True)
    @law.decorator.safe_output
    def run(self):
        for shape, shape_hists in self.filtered_input.items():
            if not shape.startswith("shapes_"):
                continue
            table = self.mk_table(shape, shape_hists)
            for tbl, suffix in [(table, ""), (self.norm_table(table), "_norm")]:
                name = shape[7:] + suffix
                base = self.output().child(name, "d")
                base.child("table.json", "f").dump(tbl)
                base.child("page.tex", "f").dump(self.as_page(tbl), formatter="text")
                base.child("an.tex", "f").dump(self.format_AN(tbl, ref=name), formatter="text")

    def mk_table(self, shape: str, shape_hists: Iterable[str]) -> dict:
        get = self.analysis_inst.processes.get
        table = defaultdict(dict)

        def _prettify(label: str) -> str:
            if " - " in label:
                labels = label.split(" - ")
                assert len(labels) == 2, f"{label}"
                labels[1] = "SM"
                return " - ".join(labels)
            if label.startswith(("THQ", "THW")):
                labels = label.split("_")
                return {"THQ": "tHq (4f)", "THW": "tHW (5f)"}[labels[0]]
            else:
                return label

        # only SM signals
        processes_ = [
            (_prettify(get(proc).label), [proc])
            for proc in self.StatModel.signal_processes
            if proc.startswith(("qqHH_CV_1_C2V_1_kl_1", "ggHH_kl_1_kt_1"))
        ]
        # backgrounds
        processes_ += [
            (_prettify(get(proc).label), [proc]) for proc in self.StatModel.background_processes
        ]

        # use only the histograms we need
        shape_hists = {
            k: h
            for k, h in shape_hists.items()
            if k.startswith(f"{self.analysis_choice}__{self.year}")
        }
        for sh in shape_hists:
            _, c, h = self.unpack_data(shape, sh, None)

            processes = processes_ + [
                ("$\sum$ backgrounds", self.bkgs(h)),
                ("Data", ["data"]),
            ]

            hv = h.view(flow=True)
            ax = h.axes["process"]
            for (process_name, process) in processes:
                if process_name == "Data":
                    v = np.sum(hv[ax.index(process), ...], axis=0)["value"].sum()
                    u = np.sum(hv[ax.index(["data_err"]), ...], axis=0)["variance"].sum() + np.abs(
                        np.sum(hv[ax.index(["data_err"]), ...], axis=0)["value"].sum()
                        - np.sum(hv[ax.index(process), ...], axis=0)["value"].sum()
                    )
                elif all(p not in ax for p in process):
                    v = u = 0.0
                else:
                    spv = np.sum(hv[ax.index(process), ...], axis=0)
                    v = spv["value"].sum()
                    u = spv["variance"].sum()
                table[c][process_name] = (v, np.sqrt(u))

        return dict(table)

    def norm_table(self, table: dict) -> dict:
        div = np.round(self.campaign_inst.aux["lumi"] / 1000.0, 2)
        return {
            cat: {key: (val / div, unc / div) for key, (val, unc) in sub.items()}
            for cat, sub in table.items()
        }

    def all_procs(self, table: dict) -> dict:
        procs = defaultdict(list)
        for k, v in table.items():
            for p, pv in v.items():
                procs[p].append(pv)

        return dict(procs)

    def format_AN(self, table: dict, ref: str = "") -> str:
        cats = {c: "{}".format(c).replace("all ", "") for c in table.keys()}
        latex = "\\begin{sidewaystable}[h!]\n\\centering\n"
        latex += "\\topcaption{{\\scriptsize{{{analysis}, {year}: {cap}}}}}\n".format(
            analysis=self.analysis_inst.label, year=self.year, cap="; ".join([*cats.values()])
        )
        latex += "\\scalebox{0.55}{"
        latex += f"\\begin{{tabular}}{{l {' '.join(len(table) * ['r'])}}}\n"
        latex += "\\hline\n"
        latex += f"Process & {' & '.join([*cats.values()])}\\\\\n"
        latex += "\\hline\n"
        for p, vs in self.all_procs(table).items():
            nvs = []
            for v, u in vs:
                nvs.extend([f"$ {round(v, self.n_digits)} \pm {round(u, self.n_digits)} $"])
            latex += f"{p} & {' & '.join(nvs)}\\\\\n"
        latex += "\\end{{tabular}}}}\n\\label{{tab:yield_table_{year}_{analysis}{modified_ref}}}\n\\end{{sidewaystable}}\n".format(
            year=self.year, analysis=self.analysis_choice, modified_ref=f"_{ref}" if ref else ref
        )

        return latex

    def as_page(self, table: dict) -> str:
        cats = {c: r"\multicolumn{2}{c}{%s}" % c.replace("all ", "") for c in table.keys()}
        latex = "\\documentclass{article}\n"
        latex += "\\usepackage{siunitx,booktabs}\n"
        latex += "\\usepackage[version=4]{mhchem}\n"
        latex += "\\begin{document}\n"
        latex += "\\begin{table}\n\centering\n"
        coltex = " S@{${}\pm{}$}S"
        latex += f"\\begin{{tabular}}{{l {' '.join(len(table) * [coltex])}}}\n"
        latex += "\\toprule \n"
        latex += f"Process & {' & '.join([*cats.values()])}\\\\\n"
        latex += "\\midrule \n"
        for p, vs in self.all_procs(table).items():
            nvs = [f"{round(a, self.n_digits)}" for v in vs for a in v]
            latex += f"{' & '.join([p, *nvs])}\\\\\n"
        latex += "\\bottomrule \n"
        latex += "\\end{tabular}\n\\end{table}\n\\end{document}"

        return latex


class YieldTablesPSFW(YieldTables, PlotPostfitShapesFromWorkspace):
    only_inclusive = luigi.BoolParameter()

    def requires(self):
        reqs = {}
        reqs["fitdiags"] = {
            category: PostfitShapesFromWorkspacePostprocessed.req(
                self,
                years=self._years,
                analyses=[self.analysis_choice],
                categories={self.analysis_choice: [category]},
            )
            for category in self.categories[self.analysis_choice]
        }
        return reqs

    def mk_table(self, shape: str, shape_hists: Iterable[str]) -> dict:
        get = self.analysis_inst.processes.get
        table = defaultdict(dict)

        # use only the histograms we need
        shape_hists = {
            k: h
            for k, h in shape_hists.items()
            if k.startswith(f"{self.analysis_choice}__{self.year}")
        }
        for sh in shape_hists:
            _, c, h = self.unpack_data(shape, sh, None)

            grouped_hists = self.group_hist(
                h, self.plotting_process_groups, ignore=self.group_ignore
            )
            # Hack to scale the variance of the TotalBkg to toy band
            if self.postfit_unc_frequentist_toys and "prefit" not in shape:
                variance_before = grouped_hists["background"].view(True).variance.sum(axis=0)
                total_variance_before = variance_before.sum(axis=0)
                total_variance_after = h["TotalBkg", :].view(True).variance
                scaling_factor = np.divide(
                    total_variance_after,
                    total_variance_before,
                    out=np.zeros_like(total_variance_after),
                    where=total_variance_before != 0,
                )
                grouped_hists["background"].view(True).variance *= scaling_factor
            for group, group_hist in grouped_hists.items():
                gv, gu = 0, 0
                assert group_hist.axes[0].name == "process"
                for process in group_hist.axes["process"]:
                    if process == "data_err":
                        continue
                    v = group_hist[process, ...].sum().value
                    u = group_hist[process, ...].sum().variance
                    gv += v
                    gu += u
                    table[c][process] = (v, np.sqrt(u))
                table[c][f"total_{group}"] = (gv, np.sqrt(gu))

        return dict(table)

    @law.decorator.timeit(publish_message=True)
    @law.decorator.safe_output
    def run(self):
        for shape, shape_hists in self.filtered_input.items():
            if not shape.startswith("shapes_"):
                continue

            table = self.mk_table(shape, shape_hists)
            if self.only_inclusive:
                summed = list(table.values())[0]
                for key in summed.keys():
                    v, u = 0, 0
                    for cat, cat_dict in table.items():
                        v += cat_dict[key][0]
                        u += cat_dict[key][1] ** 2
                    summed[key] = (v, np.sqrt(u))
                table = {"all_incl": summed}

            for tbl, suffix in [
                (table, ""),
            ]:
                name = shape[7:] + suffix
                base = self.output().child(name, "d")
                base.child("table.json", "f").dump(tbl)
                base.child("page.tex", "f").dump(self.as_page(tbl), formatter="text")
                base.child("an.tex", "f").dump(self.format_AN(tbl, ref=name), formatter="text")


class OtherYieldTables(law.ExternalTask, AnalysisCampaignTask):
    def output(self):
        sync_yield_paths = self.analysis_inst.aux.get("sync_yield", {}).get("paths", {})
        return {name: law.LocalFileTarget(path) for name, path in sync_yield_paths.items()}


class CompareYieldTables(PGroupMixin, RecipeMixin, AnalysisCampaignTask):
    @property
    def lut(self):
        return self.analysis_inst.aux.get("sync_yield", {}).get("lut", {})

    def requires(self):
        sync_yield_category = self.analysis_inst.aux.get("sync_yield", {}).get("category", None)
        return {
            "we": YieldTables.req(
                self,
                category=sync_yield_category,
            ),
            "other": OtherYieldTables.req(self),
        }

    def run(self):
        console = Console()

        inp = self.input()
        if len(inp["other"]) == 0:
            self.logger.info("I do not have yield tables for comparison.")
        we = inp["we"].child("prefit/table.json", type="f").load()
        other = {name: target.load() for name, target in inp["other"].items()}

        for cat, cat_val in sorted(we.items(), key=lambda x: x[0][::-1]):
            table = Table(title=cat)
            table.add_column("Process", justify="right", style="cyan", no_wrap=True)
            table.add_column("RWTH", style="magenta")
            for name, _ in other.items():
                table.add_column(name, justify="right", style="magenta")
                table.add_column(f"{name}/RWTH", justify="right", style="green")

            for proc, proc_val in cat_val.items():
                w = proc_val[0]
                o = []
                for name, yields in other.items():
                    try:
                        _cat = self.lut.get(cat, cat)
                        _proc = self.lut.get(proc, proc)
                        y = yields[_cat][_proc][0]
                        o.append(f"{int(y)}")
                        o.append(f"{int(y / w * 100)}%")
                    except:
                        o += ["", ""]
                table.add_row(proc, f"{int(w)}", *o)
            console.print(table)
