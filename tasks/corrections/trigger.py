# coding: utf-8

from enum import IntEnum

import numpy as np
import uproot3
from tqdm.auto import tqdm

from tasks.files import DownloadFiles
from tools.numpy import array_to_hist
from utils.coffea import mk_dense_evaluator

from .base import CorrectionsBase, PlotCorrectionsBase


class Trigger1DTo2D(CorrectionsBase):

    sf_class = "trigger_sf_files_1d"

    def output(self):
        return self.local_target("correction.root")

    def requires(self):
        return DownloadFiles.req(self, type="histo")

    @property
    def particle(self):
        raise NotImplementedError

    def onify(self, arr, one_mask):
        arr[one_mask] = 1.0
        return arr

    def run(self):
        eta_bins = [0]
        nominal, up, down = [], [], []
        one_d = uproot3.open(self.input()[self.sf_class][self.sf_name].path)

        present_keys = [k[:-5].decode("ascii") for k in one_d.keys() if k.endswith(b"_MC;1")]
        assert all([k in self.eta_boundaries for k in present_keys])

        for eta_boundary_name in self.eta_boundaries.keys():
            if str.encode(f"{eta_boundary_name}_MC;1") not in one_d.keys():
                continue
            _mc = one_d[eta_boundary_name + "_MC"]
            _data = one_d[eta_boundary_name + "_Data"]

            assert all(_mc.xvalues == _data.xvalues)
            pt_bins = np.linspace(min(_mc.xvalues), max(_mc.xvalues), num=10000)
            pt_center = (pt_bins[:-1] + pt_bins[1:]) / 2.0
            mc = np.interp(pt_center, _mc.xvalues, _mc.yvalues)
            data = np.interp(pt_center, _data.xvalues, _data.yvalues)
            sf = np.minimum(data / np.maximum(1.0e-6, mc), 10.0)

            nominal.append(sf)
            up.append(sf * 1.02)
            down.append(sf * 0.98)
            eta_bins.append(self.eta_boundaries[eta_boundary_name])

        nominal, up, down = map(lambda x: np.array(x), [nominal, up, down])
        pt_bins[0], pt_bins[-1] = 0, np.inf
        eta_bins = np.array(eta_bins)

        self.output().parent.touch()
        file = uproot3.create(self.output().path)
        file["nominal"] = array_to_hist(nominal, (eta_bins, pt_bins))
        file["up"] = array_to_hist(up, (eta_bins, pt_bins))
        file["down"] = array_to_hist(down, (eta_bins, pt_bins))


class TallinnTriggerSingleElectron(Trigger1DTo2D):

    sf_name = "Tallinn_sl_electron_1d"
    eta_boundaries = {
        "ZMassEtaLt1p0": 1.0,
        "ZMassEta1p0to1p48": 1.48,
        "ZMassEtaLt1p48": 1.48,
        "ZMassEta1p48to1p65": 1.65,
        "ZMassEta1p65to2p1": 2.1,
        "ZMassEta1p48to2p1": 2.1,
        "ZMassEtaGt2p1": np.inf,
    }


class TallinnTriggerSingleMuon(Trigger1DTo2D):

    sf_name = "Tallinn_sl_muon_1d"
    eta_boundaries = {
        "ZMassEtaLt0p9": 0.9,
        "ZMassEta0p9to1p2": 1.2,
        "ZMassEta1p2to2p1": 2.1,
        "ZMassEtaGt2p1": np.inf,
    }

    # 2016 eta bins:
    #     muons: [0, 0.9, 1.2, 2.1, 2.4])
    #     electrons: [0, 1.0, 1.48, 1.65, 2.1]


class Dir(IntEnum):
    down = -1
    nom = 0
    up = +1


class TriggerCorrections(CorrectionsBase):
    def requires(self):
        all = DownloadFiles.req(self, type="histo")
        prep = {
            "Tallinn_single_electron": TallinnTriggerSingleElectron.req(self),
            "Tallinn_single_muon": TallinnTriggerSingleMuon.req(self),
        }
        return {"all": all, "prep": prep}

    def run(self):
        self.output().parent.touch()
        corrections = {**self.input()["all"]["trigger_sf_files"], **self.input()["prep"]}
        self.ext.add_weight_sets(
            [
                self.template.format(prefix=f"{key}_trigger_", hist="*", path=target.path)
                for key, target in corrections.items()
            ]
        )
        self.ext.finalize()
        hevaluator = self.ext.make_evaluator()
        hevaluator = self.mod_evaluator(hevaluator)

        # https://gitlab.cern.ch/ttH_leptons/doc/blob/master/Legacy/data_to_mc_corrections.md#trigger-efficiency-scale-factors
        # fmt: off
        raw = {
            # 2016
            (2016, "ee", Dir.up): (np.array([0.98, 1.00]) * 1.02, np.array([-np.inf, 25.0, np.inf])),
            (2016, "ee", Dir.nom): (np.array([0.98, 1.00]), np.array([-np.inf, 25.0, np.inf])),
            (2016, "ee", Dir.down): (np.array([0.98, 1.00]) * 0.98, np.array([-np.inf, 25.0, np.inf])),
            (2016, "emu", Dir.up): (np.array([1.00]) * 1.01, np.array([-np.inf, np.inf])),
            (2016, "emu", Dir.nom): (np.array([1.00]), np.array([-np.inf, np.inf])),
            (2016, "emu", Dir.down): (np.array([1.00]) * 0.99, np.array([-np.inf, np.inf])),
            (2016, "mumu", Dir.up): (np.array([0.99]) * 1.01, np.array([-np.inf, np.inf])),
            (2016, "mumu", Dir.nom): (np.array([0.99]), np.array([-np.inf, np.inf])),
            (2016, "mumu", Dir.down): (np.array([0.99]) * 0.99, np.array([-np.inf, np.inf])),
            # 2017
            (2017, "ee", Dir.up): (np.array([0.98, 1.00]) * 1.01, np.array([-np.inf, 40.0, np.inf])),
            (2017, "ee", Dir.nom): (np.array([0.98, 1.00]), np.array([-np.inf, 40.0, np.inf])),
            (2017, "ee", Dir.down): (np.array([0.98, 1.00]) * 0.99, np.array([-np.inf, 40.0, np.inf])),
            (2017, "emu", Dir.up): (np.array([0.98, 0.99]) * 1.01, np.array([-np.inf, 40.0, np.inf])),
            (2017, "emu", Dir.nom): (np.array([0.98, 0.99]), np.array([-np.inf, 40.0, np.inf])),
            (2017, "emu", Dir.down): (np.array([0.98, 0.99]) * 0.99, np.array([-np.inf, 40.0, np.inf])),
            (2017, "mumu", Dir.up): (np.array([0.97, 0.995, 0.96, 0.94]) * 1.02, np.array([-np.inf, 40.0, 55.0, 70.0, np.inf])),
            (2017, "mumu", Dir.nom): (np.array([0.97, 0.995, 0.96, 0.94]), np.array([-np.inf, 40.0, 55.0, 70.0, np.inf])),
            (2017, "mumu", Dir.down): (np.array([0.97, 0.995, 0.96, 0.94]) * 0.98, np.array([-np.inf, 40.0, 55.0, 70.0, np.inf])),
            # 2018
            (2018, "ee", Dir.up): (np.array([0.98, 1.00]) * 1.01, np.array([-np.inf, 25.0, np.inf])),
            (2018, "ee", Dir.nom): (np.array([0.98, 1.00]), np.array([-np.inf, 25.0, np.inf])),
            (2018, "ee", Dir.down): (np.array([0.98, 1.00]) * 0.99, np.array([-np.inf, 25.0, np.inf])),
            (2018, "emu", Dir.up): (np.array([0.98, 1.00]) * 1.01, np.array([-np.inf, 25.0, np.inf])),
            (2018, "emu", Dir.nom): (np.array([0.98, 1.00]), np.array([-np.inf, 25.0, np.inf])),
            (2018, "emu", Dir.down): (np.array([0.98, 1.00]) * 0.99, np.array([-np.inf, 25.0, np.inf])),
            (2018, "mumu", Dir.up): (np.array([1.01, 0.995, 0.98]) * 1.01, np.array([-np.inf, 40.0, 70.0, np.inf])),
            (2018, "mumu", Dir.nom): (np.array([1.01, 0.995, 0.98]), np.array([-np.inf, 40.0, 70.0, np.inf])),
            (2018, "mumu", Dir.down): (np.array([1.01, 0.995, 0.98]) * 0.99, np.array([-np.inf, 40.0, 70.0, np.inf])),
        }
        # fmt: on
        revaluator = mk_dense_evaluator(raw)
        self.output().dump(dict(hist=hevaluator, raw=revaluator))


class PlotTriggerCorrections(PlotCorrectionsBase):
    version = TriggerCorrections.version

    def requires(self):
        return TriggerCorrections.req(self)

    def run(self):
        self.output().parent.touch()
        inp = self.input().load()["hist"]
        pb = tqdm(Map := set(k.replace("_error", "") for k in inp.keys()), total=len(Map), unit="Map")  # fmt: skip
        for m in pb:
            # if there are weird values, skip
            if (weird := ~np.isfinite(inp[m]._values)).any():
                tqdm.write(f"SKIP: {weird.sum()} non-finite values in {m}!")
                continue
            for ax in inp[m]._axes:
                if (weirdax := ~np.isfinite(ax)).any():
                    tqdm.write(f"SKIP: {weirdax.sum()} non-finite values in {m} (axis: {ax})!")
                    continue
            self.plot(inp, self.local_path(), m, guess_pt_eta=True)
        self.output().touch()
