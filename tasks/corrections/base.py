# coding: utf-8

import os

import luigi
import matplotlib.pyplot as plt
import mplhep as hep
import numpy as np
import uproot
from tqdm.auto import tqdm

from processor.sf import POGLeptonSF
from tasks.base import AnalysisCampaignTask
from tasks.files import DownloadFiles
from tools.numpy import array_to_hist
from utils.aci import rgb
from utils.coffea import mk_dense_evaluator


class Correction:
    available = True

    def __init_subclass__(cls) -> None:
        if "name" not in cls.__dict__:
            cls.name = cls.__name__.replace("Corrections", "").lower()

    def output(self):
        return self.local_target("corrections_reorder.coffea")


class CorrectionsBase(Correction, AnalysisCampaignTask):

    version = None

    template = "{prefix} {hist} {path}"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        from coffea.lookup_tools import extractor

        self._ext = extractor()
        self._files = self.campaign_inst.aux.get("files", {})

    @property
    def ext(self):
        return self._ext

    def finish(self):
        self.ext.finalize()
        evaluator = self.ext.make_evaluator()
        evaluator = self.mod_evaluator(evaluator)
        self.output().parent.touch()
        self.output().dump(evaluator)

    def mod_evaluator(self, evaluator):
        return evaluator

    def put_dense(self, values, edges=None):
        ev = mk_dense_evaluator(values, edges)

        self.output().parent.touch()
        self.output().dump(ev)


class SFPTEta(AnalysisCampaignTask):
    version = None

    type = "histo"
    eta_bins = np.array(
        [0, 0.5, 1.0, 1.5, 2.0, np.inf]
    )  # careful: must be the same for nominal and shift
    pt_bins = np.array([10, 15, 20, 25, 30, 35, 40, 45, 60, 80, np.inf])

    nominal_key = "EGamma_SF2D"  # same for electrons and muons
    error_key = "histo_eff_data"

    def output(self):
        return self.local_target("id_correction.root")

    def requires(self):
        return DownloadFiles.req(self, type=self.type)

    def to_path(self, target):
        return target.path

    @property
    def particle(self):
        raise NotImplementedError

    def run(self):
        sf_dict = self.input()

        main_key = "%s_sf_pteta_files" % self.particle
        nominal_path = sf_dict[main_key]["id_tight"].path
        error_pt_path = sf_dict[main_key]["id_tight_error_pt"].path
        error_eta_path = sf_dict[main_key]["id_tight_error_eta"].path

        nominal = uproot.open(nominal_path)[self.nominal_key].values
        error_pt = uproot.open(error_pt_path)[self.error_key].values
        error_eta = uproot.open(error_eta_path)[self.error_key].values

        shift_pt = np.abs(1 - error_pt)
        shift_eta = np.abs(1 - error_eta)

        n_bins_pt = shift_pt.shape[0]
        n_bins_eta = shift_eta.shape[0]

        shift_pt = np.repeat(shift_pt[np.newaxis, ...], repeats=n_bins_eta, axis=0)
        shift_eta = np.repeat(shift_eta[..., np.newaxis], repeats=n_bins_pt, axis=1)

        up = np.maximum(1 + shift_pt, 1 + shift_eta) * nominal
        down = np.minimum(1 - shift_pt, 1 - shift_eta) * nominal

        self.output().parent.touch()
        file = uproot.create(self.output().path)
        file["nominal"] = array_to_hist(nominal, (self.eta_bins, self.pt_bins))
        file["up"] = array_to_hist(up, (self.eta_bins, self.pt_bins))
        file["down"] = array_to_hist(down, (self.eta_bins, self.pt_bins))


class PlotCorrectionsBase(AnalysisCampaignTask):

    version = CorrectionsBase.version
    show_values = luigi.BoolParameter()
    equal_bin_widths = luigi.BoolParameter()
    with_errors = luigi.BoolParameter()

    @staticmethod
    @np.vectorize
    def _fmt(x, err=None):
        if x < 0.01 and x != 0.00:
            fmt = "<0.01"
        else:
            fmt = f"{x:.2f}"
        if err is not None:
            if err < 0.01 and err != 0.00:
                fmt += "\\pm<0.01"
            else:
                fmt += f"\\pm{err:.2f}"
        return rf"${fmt}$"

    def _path(self, path, suffix=".pdf"):
        assert isinstance(path, str)
        if self.show_values:
            path += ".show_values"
        if self.equal_bin_widths:
            path += ".equal_bin_widths"
        if self.with_errors:
            path += ".with_errors"
        return f"{path}{suffix}"

    def output(self):
        return self.local_target(self._path(".done", suffix=""))

    def _infer_axis_and_labels(self, SFaxis, ax, dim="x", guess_pt_eta=True):
        assert dim in ("x", "y"), "Only supports 'x' and 'y' axis!"
        ticklabels = list(self._fmt(SFaxis))
        # best guess axes:
        if guess_pt_eta:
            Ipt, Ieta, Aeta = POGLeptonSF._infer(SFaxis[None, :])
            if Ipt == 0:
                getattr(ax, f"set_{dim}label")(r"$p_{T}$ (GeV)")
                ticklabels[-1] = rf"$\infty$"
                ticklabels[0] = rf"$0.00$"
            if Ieta == 0:
                getattr(ax, f"set_{dim}label")(r"$|\eta|$" if Aeta else r"$\eta$")
                if Aeta:
                    ticklabels[-1] = rf"$\infty$"
                else:
                    ticklabels[-1] = rf"$\infty$"
                    ticklabels[0] = rf"$-\infty$"
        return ticklabels

    def _get_err(self, inp, Map, SFval):
        try:
            SFerr = inp[Map + "_error"]._values
            # try to infer if relative or absolute error
            # let's assume it is absolute if all values are close (~50%) to nominal value
            # this can probably be improved...
            if np.all((SFerr / SFval) >= 0.5):
                # assume absolute
                SFerr = np.abs(SFval - SFerr)
        except KeyError:
            SFerr = np.zeros_like(SFval)
        assert SFval.shape == SFerr.shape
        return SFerr

    def _bins4plot(self, bins: np.ndarray) -> np.ndarray:
        if self.equal_bin_widths:
            return np.arange(len(bins))
        if np.all(np.isfinite(bins)):
            return bins
        width = np.diff(bins)
        mean_width = np.mean(width[np.isfinite(width)])
        bins = bins.copy()
        if bins[0] == -np.inf:
            bins[0] = bins[1] - mean_width
        if bins[-1] == np.inf:
            bins[-1] = bins[-2] + mean_width

    def plot(
        self,
        inp,
        outdir,
        Map,
        guess_pt_eta=True,
        small_size=8,
        bigger_size=10,
        dynamic_display=True,
    ):
        plt.rcParams.update({"figure.max_open_warning": 0})
        plt.rcParams.update({"font.size": small_size})
        plt.rcParams.update({"axes.titlesize": bigger_size})
        plt.rcParams.update({"axes.labelsize": bigger_size})
        plt.rcParams.update({"xtick.labelsize": bigger_size})
        plt.rcParams.update({"ytick.labelsize": bigger_size})
        plt.rcParams.update({"legend.fontsize": bigger_size})
        plt.rcParams.update({"figure.titlesize": bigger_size})

        SF = inp[Map]
        fig, ax = plt.subplots()
        SFval = SF._values
        if (dim := SF._dimension) == 1:
            if self.with_errors:
                SFerr = self._get_err(inp, Map, SFval)
            else:
                SFerr = None

            opts = dict(edgecolor=(0, 0, 0, 0.5), color=rgb(0, 84, 159))  # RWTH blue
            hep.histplot(
                SFval,
                self._bins4plot(SF._axes),
                ax=ax,
                histtype="fill",
                **opts,
            )
            ticklabels = self._infer_axis_and_labels(
                SFaxis=SF._axes,
                ax=ax,
                dim="x",
                guess_pt_eta=guess_pt_eta,
            )

            if self.equal_bin_widths:
                ax.set_xticks(np.arange(len(SF._axes)))
            else:
                ax.set_xticks(SF._axes)
            ax.set_xticklabels(ticklabels)
            if self.show_values:
                xticks = ax.get_xticks()
                labels = self._fmt(SFval, SFerr)
                assert len(xticks) - len(labels) == 1
                for i, label in enumerate(labels):
                    h = SFval[i]
                    x = (xticks[i + 1] + xticks[i]) / 2
                    ax.text(
                        x,
                        h + 0.01,
                        label,
                        ha="center",
                        va="bottom",
                        fontsize=10,
                    )
                lo, hi = ax.get_ylim()
                ax.set_ylim(bottom=lo, top=hi * 1.1)
            ax.autoscale(axis="x", tight=True)
            self.finalize(fig, ax, os.path.join(outdir, Map))

        elif (dim := SF._dimension) == 2:
            if self.with_errors:
                # too long to display properly, so remove error display
                if dynamic_display and len(SF._axes[0]) > len(SF._axes[1]):
                    SFerr = None
                else:
                    SFerr = self._get_err(inp, Map, SFval)
            else:
                SFerr = None

            hep.hist2dplot(
                SFval,
                self._bins4plot(SF._axes[0]),
                self._bins4plot(SF._axes[1]),
                labels=self._fmt(SFval, SFerr) if self.show_values else None,
                ax=ax,
                cbarpad=0.1,
            )

            for i, dim in (0, "x"), (1, "y"):
                ticklabels = self._infer_axis_and_labels(
                    SFaxis=SF._axes[i],
                    ax=ax,
                    dim=dim,
                    guess_pt_eta=guess_pt_eta,
                )
                if self.equal_bin_widths:
                    getattr(ax, f"set_{dim}ticks")(np.arange(len(SF._axes[i])))
                else:
                    getattr(ax, f"set_{dim}ticks")(SF._axes[i])
                getattr(ax, f"set_{dim}ticklabels")(ticklabels)

            self.finalize(fig, ax, os.path.join(outdir, Map))

        else:
            tqdm.write(f"SKIP: {Map} has {dim} dimension(s), not supported!")

    def finalize(self, fig, ax, path):
        hep.cms.label(
            llabel="Private Work",
            year=self.year,
            lumi=np.round(self.campaign_inst.aux["lumi"] / 1000.0, 2),
            loc=0,
            ax=ax,
            fontname="sans-serif",
            fontsize=10,
        )
        fig.autofmt_xdate()
        fig.tight_layout()
        plt.savefig(self._path(path))
        ax.cla()
        plt.close(fig)
