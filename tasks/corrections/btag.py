# coding: utf-8

from collections import ChainMap
from functools import reduce
from operator import add, attrgetter
from types import SimpleNamespace
from typing import Callable, Dict, Iterable, Union

import awkward as ak  # noqa
import coffea.processor.accumulator as ACC
import hist
import law
import luigi
import luigi.util
import numpy as np

from processor import util  # noqa
from tasks.coffea import CoffeaProcessor
from tasks.files import DownloadFiles
from utils.coffea import BaseProcessor, Weights, hist_accumulator, hists2dense_evaluator
from utils.csv_reader import WP, BTagSFCSVReader, BTagSFGroup

from .base import CorrectionsBase


class BTagCorrections(CorrectionsBase):
    def requires(self):
        return DownloadFiles.req(self, type="btag")

    def run(self):
        self.output().dump(law.util.map_struct(lambda tar: BTagSFCSVReader(tar.path), self.input()))


@luigi.util.inherits(CoffeaProcessor)
class _BTag_Corrections(CorrectionsBase):
    version = CoffeaProcessor.version
    processor = "BTagSFNormEff"
    debug = False
    explorative = False

    def store_parts(self):
        return super().store_parts() + (self.analysis_choice, self.recipe)

    def requires(self):
        return CoffeaProcessor.req(self, processor=self.processor)


class BTagNormCorrections(_BTag_Corrections):
    def calculate(self, v0, v1):
        return np.divide(v0, v1, where=(v0 != 0) & (v1 != 0), out=np.ones_like(v0))

    def run(self):
        nJet = self.input().load()["nJet"]
        nj0 = nJet[0].hist.to_hist()
        nj1 = nJet[1].hist.to_hist()
        assert nj0.shape == nj1.shape
        self.put_dense(
            {
                d: self.calculate(
                    nj0[d, ...].view()["value"],
                    nj1[d, ...].view()["value"],
                )
                for d in nj0.axes["dataset"]
            },
            nj0.axes["nJet"].edges,
        )


class BTagSubjetCorrections(_BTag_Corrections):
    def requires(self):
        return dict(proc=super().requires(), corr=BTagCorrections.req(self))

    @property
    def available(self):
        return self.__class__.__name__ in self.analysis_inst.aux.get("non_common_corrections", [])

    def run(self):
        csv = self.input()["corr"].load()
        inp = self.input()["proc"].load()
        sgw = inp["sum_gen_weights"]
        sig = self.analysis_inst.processes.get(self.analysis_inst.aux["signal_process"])

        wp2h = {}
        for wp in ["medium"]:
            h = inp[f"subjet{wp[0].upper()}"].hist.to_hist()
            wp2h[wp] = reduce(
                add,
                (
                    h[dict(dataset=ds.name)]
                    * (ds.xs.nominal * self.campaign_inst.aux["lumi"] / sgw[ds.name, "nominal"])
                    for ds in map(self.campaign_inst.datasets.get, h.axes[0])
                    if ds.process.is_mc
                    and not ds.process.has_parent_process(sig, include_self=True)
                ),
            )

        self.output().dump(BTagSubjetHelper(wp2h=wp2h, csv=csv))

    @classmethod
    def prepare(cls, events: ak.Array, corrections: dict, wp: WP) -> None:
        if not hasattr(events.SubJet, "hadronFlavour") and hasattr(events.SubJet, "nBHadrons"):
            events["SubJet", "hadronFlavour"] = ak.where(
                events.SubJet.nBHadrons > 0,
                5,
                (events.SubJet.nCHadrons > 0) * 4,
            )
        if hasattr(events.SubJet, "hadronFlavour") and cls.name in corrections:
            wp = WP(wp).name
            events["SubJet", f"btagDeepB_SF_{wp}"] = (
                corrections["btag"]["subjet_deepb"]
                .get(wp)
                .eval_awk1(obj=events.SubJet, discr="btagDeepB")
            )

    @classmethod
    def apply(cls, proc: BaseProcessor, *args, **kwargs) -> None:
        corr: BTagSubjetHelper = proc.corrections.get(cls.name)
        if corr:
            return corr.apply(proc, *args, **kwargs)


class BTagSubjetHelper:
    def __init__(self, wp2h: Dict[WP, hist.Hist], csv: BTagSFGroup) -> None:
        self._ev = hists2dense_evaluator({wp: self._calculate(h) for wp, h in wp2h.items()})
        self._csv = csv
        self._wp2h = wp2h

    @classmethod
    def _calculate(self, h: hist.Hist) -> hist.Hist:
        r = h[dict(tagged=True)]
        v = r.view(True)

        t = h[dict(tagged=sum)]

        # v["value"] = np.divide(v["value"], t.view(True)["value"], out=np.ones_like(v), where=v)
        v["value"] /= t.view(True)["value"]
        v["variance"] = 0

        return r

    def apply(
        self,
        proc: BaseProcessor,
        subjets: ak.Array,
        weights: Weights,
        wp: WP,
        name: str = "btagWeight_subjet",
    ) -> None:
        # Method 1a
        # https://twiki.cern.ch/twiki/bin/viewauth/CMS/BTagSFMethods#1a_Event_reweighting_using_scale
        if hasattr(subjets, "hadronFlavour"):
            wp = WP(wp).name
            subjets = ak.flatten(subjets, axis=2)  # merge fatjet & subjets axes
            sf = subjets[f"btagDeepB_SF_{wp}"]
            tag = subjets.btagDeepB >= proc.campaign_inst.aux["working_points"]["subjet_deepb"][wp]
            eff = self._ev[wp](
                abs(subjets.eta),
                subjets.pt,
                ak.where(subjets.hadronFlavour, subjets.hadronFlavour - 3, subjets.hadronFlavour),
            )
            sf_event = {
                k: ak.prod(sf[k][tag], axis=1)
                * ak.prod(((1 - eff * sf[k]) / (1 - eff))[~tag], axis=1)
                for k in ak.fields(sf)
            }
        else:
            n_events = len(subjets)
            sf_event = dict(
                central=np.ones(n_events=n_events),
                up=np.ones(n_events=n_events),
                down=np.ones(n_events=n_events),
            )

        weights.add(
            name,
            weight=ak.to_numpy(sf_event["central"]),
            weightUp=ak.to_numpy(sf_event["up"]),
            weightDown=ak.to_numpy(sf_event["down"]),
        )


class BTagSFNormEff(BaseProcessor):
    njets: Union[Callable[[dict], ak.Array], str] = "njets"
    btagWeight: str = "btagWeight"
    subjets: Union[Callable[[dict], ak.Array], str, None] = None
    skip_cuts: Iterable[str] = ()

    jes_shifts = False
    individual_weights = True

    @classmethod
    def requires(cls, task):
        return task.base_requires(
            data_source="mc", btagnorm=False, btagsubjet=False, hhreweigthing=False
        )

    def __init__(self, task):
        super().__init__(task)
        self._accumulator["nJet"] = ACC.dict_accumulator(
            {
                sf: hist_accumulator(
                    hist.Hist(
                        self.dataset_axis,
                        hist.axis.Integer(0, 10, name="nJet", label="Number of Jets"),
                        storage=hist.storage.Weight(),
                    )
                )
                for sf in (0, 1)
            }
        )
        # only medium WP (for now)
        self._accumulator["subjetM"] = hist_accumulator(
            hist.Hist(
                self.dataset_axis,
                hist.axis.Variable([0, 2.4, 2.5], name="absEta", underflow=False),
                hist.axis.Variable([20, 30, 120, 180, 240, 450, 1000], name="pt"),
                hist.axis.IntCategory([0, 4, 5], name="hadronFlavour"),
                hist.axis.Boolean(name="tagged"),
                storage=hist.storage.Weight(),
            )
        )

    def process(self, events):
        select_output = next(self.select(events))
        assert select_output["unc"] == "nominal"

        dsname = self.get_dataset(events).name
        categories = select_output["categories"]
        selection = select_output["selection"]
        weights = select_output["weights"]
        output = select_output["output"]

        if callable(self.njets):
            njets = self.njets(select_output)
        else:
            njets = eval(
                self.analysis_inst.variables.get(self.njets).expression,
                globals(),
                ChainMap({}, select_output),
            )

        cut = cut_nob = False
        skip_cuts = set(c.lstrip("!") for c in self.skip_cuts)
        skip_cuts |= set(f"!{c}" for c in skip_cuts)
        for cuts in categories.values():
            cut |= selection.all(*cuts)
            cut_nob |= selection.all(*(set(cuts) - skip_cuts))

        if not np.any(cut_nob):
            cut_nob = np.s_[:1]
            njets = [-1]

        for o, w in [
            (output["nJet"][0], weights.partial_weight(exclude=[self.btagWeight])),
            (output["nJet"][1], wAll := weights.weight()),
        ]:
            o.hist.fill(weight=w[cut_nob], nJet=njets[cut_nob], dataset=dsname)

        if self.subjets is not None and self.get_dataset_shift(events) == "nominal":
            if callable(self.subjets):
                subjet = self.subjets(output)
            else:
                subjet = attrgetter(self.subjets)(SimpleNamespace(**select_output))
            ws = ak.flatten(ak.flatten(ak.zip(dict(w=wAll, subjet=subjet))[cut]))
            wp = self.campaign_inst.aux["working_points"]["subjet_deepb"]["medium"]

            output["subjetM"].hist.fill(
                weight=ws.w,
                absEta=abs(ws.subjet.eta),
                pt=ws.subjet.pt,
                hadronFlavour=ws.subjet.hadronFlavour,
                tagged=ws.subjet.btagDeepB >= wp,
                dataset=dsname,
            )

        return output


def BTagSF_reshape(processor, events, weights, jets, shift, unc):
    """Helper for applying DeepFlavour reshape BTagSF"""
    dataset_inst = processor.get_dataset(events)
    if not dataset_inst.is_mc:
        return

    # https://twiki.cern.ch/twiki/bin/viewauth/CMS/BTagSFMethods
    # AK4 Jet reshape SF
    corr = processor.corrections["btag"]["deepjet"][
        "POG_reduced"
        + (
            "_TuneCP5"
            if int(processor.year) == 2016 and "_TuneCP5_" in processor.get_lfn(events, default="")
            else ""
        )
    ].get("reshape")
    if shift is None:
        shifts = list(processor.analysis_inst.aux.get("btag_sf_shifts", []))
        # shifts = [] # no shifts -> speedup
        c = "central"
        corr = corr.reduce(shifts=shifts)
    else:
        shifts = []
        if unc in ("jer", "UnclustEn"):
            c = "central"
        else:
            # flip up/down direction for certain jec shifts, since they changed direction:
            # ReducedJECv1 (used for BTagSF) <-> ReducedJECv2 (applied here)
            # https://twiki.cern.ch/twiki/bin/viewauth/CMS/JECUncertaintySources#Run_2_reduced_set_of_uncertainty
            # if unc in ("RelativeSample", "RelativeBal"):
            if unc in ("FlavorQCD", "RelativeBal") or unc.startswith(
                ("RelativeSample_", "EC2")  # these are suffixed by their year
            ):
                btag_shift = dict(up="down", down="up")[shift]
            else:
                btag_shift = shift
            btag_unc = "HEMIssue" if unc == "HemIssue" else unc
            c = f"{btag_shift}_jes{btag_unc}"
        assert c in corr.shifts, (c, corr.shifts)
        corr = corr.reduce(shifts=[c], updown=False)
    sfs = ak.prod(corr.eval_awk1(obj=jets, discr="btagDeepFlavB"), axis=-1)
    sf0 = sfs[c]
    weights.add("btagWeight", sf0)
    for btag_shift in shifts:
        weights.add(
            "btagWeight_%s" % btag_shift,
            ak.ones_like(sf0),
            weightUp=sfs[f"up_{btag_shift}"] / sf0,
            weightDown=sfs[f"down_{btag_shift}"] / sf0,
        )

    if "btagnorm" in processor.corrections:
        btagnorm = processor.corrections["btagnorm"][dataset_inst.name]
        weights.add("btagNorm", btagnorm(ak.num(jets)))
