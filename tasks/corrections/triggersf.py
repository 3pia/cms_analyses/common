# coding: utf-8

from ast import Call
from numbers import Number
import re
from dataclasses import dataclass
from operator import mul
from functools import partial, reduce
from typing import Callable, Dict, Iterable, List, Literal, Optional, Sequence, Set, Tuple, Union

import awkward as ak
import hist
import luigi
import numpy as np
from scipy.stats import crystalball, norm
from coffea.processor.accumulator import iadd
import matplotlib.pyplot as plt

from tasks.coffea import CoffeaProcessor
from tasks.files import DownloadFiles
from utils.betaq import betaq
from utils.coffea import Efficency, Weights, akm, reduce_and, reduce_or

from .base import CorrectionsBase


class TriggerSFBase(CorrectionsBase):
    @property
    def available(self):
        return self.__class__.__name__ in self.analysis_inst.aux.get("non_common_corrections", [])

    def run(self):
        eff_data, eff_mc = self.load_eff()
        # from IPython import embed; embed(); 0/0  # fmt: skip

        output = {
            name: {
                step.key: self.process(name, step, eff_data[name][step.key], eff_mc[name][step.key])
                for step in trig.steps
            }
            for i, trig in enumerate(self.campaign_inst.aux["triggers"])
            for name in [trig.name, f"{trig.name}_AND"][: i + 1]
        }
        assert set(output.keys()) <= set(eff_data.keys()) == set(eff_mc.keys())

        self.output().dump(output)

    def process(
        self, name: str, step: "_Step", data: Efficency, mc: Efficency
    ) -> "TriggerDriver.DataMC":
        print("#" * 5, name, step.key)
        fig, axs = plt.subplots(
            nrows=3,
            figsize=(13, 9),
            sharex=True,
            gridspec_kw=dict(
                hspace=0.03,
                left=0.07,
                top=0.92,
                bottom=0.05,
                right=0.98,
            ),
        )
        fig.suptitle(name)
        axs[0].set_title(step.key)
        x = data.x_linspace()

        for what, eff, color in [
            ("data", data, "red"),
            ("MC", mc, "blue"),
        ]:
            for label, marker, tot, (eb,) in zip(
                ["Fail", "Pass"],
                "x.",
                eff.hist[::sum, :].view()["value"],
                eff.hist.plot(
                    ax=axs[0],
                    binwnorm=None,
                    histtype="errorbar",
                    markersize=4,
                    elinewidth=0.5,
                    capsize=0.5,
                    xerr=True,
                    color=color,
                ),
            ):
                tot = rf"{{{tot:.3g}}}".replace("e", r"}\cdot10^{")
                eb.set_label(rf"{what} {label} (${tot}$)")
                eb[0].set_marker(marker)

        axs[0].legend(ncol=2)
        axs[-1].set_xlabel(axs[0].get_xlabel())
        axs[0].set_xlabel("")
        axs[0].set_ylabel("Count")

        if isinstance(step, TagStep):
            axs[0].set_yscale("log")

        def _proc(eff: Efficency, label: str, color: str) -> Efficency.Func:
            eff.plot(axs[-2], label=label, color=color, fmt="none", elinewidth=0.5, capsize=0.5)

            print("#" * 3, label)
            fk = step.fit_kwargs
            # f0 = eff.Func(fk["fun"], fk["x0"], np.array(fk["x0"]) * 0)
            # f0.plot(axs[-2], x, color=color, linestyle=":", label="init")

            if step.with_shifts:
                fun = eff.fit(**fk)

                fun.up = eff.mode_shift(1).fit(**fk)
                fun.down = eff.mode_shift(-1).fit(**fk)
            else:
                fun = eff.fit(**fk)

            fun.plot(axs[-2], x, color=color, label=f"{label} func. param.:\n%s")

            return fun

        ret = TriggerDriver.DataMC(
            data=_proc(data, "data", "red"),
            mc=_proc(mc, "MC", "blue"),
        )

        axs[-2].set_ylabel("Efficency")
        axs[-2].set_ylim(0, 1)
        axs[-2].set_xlim(*data.axis1D.edges[[0, -1]])
        axs[-2].legend(*np.array(axs[-2].get_legend_handles_labels())[:, [2, 0, 3, 1]], ncol=2)

        ret.plot(axs[-1], x)

        # plot beta-quotient directly
        dataU, mcU = data.unify(mc, axis=0)
        ax = dataU.hist.axes[0]
        r = betaq.rvs(*dataU.ab(), *mcU.ab(), size=(10000, len(ax)))
        c, u, d = np.percentile(r, 100 * norm.cdf([0, 1, -1]), axis=0)
        axs[-1].errorbar(
            x=ax.centers,
            y=c,
            yerr=abs(np.stack([d, u]) - c),
            xerr=ax.widths / 2,
            fmt="none",
            elinewidth=0.5,
            capsize=0.5,
        )

        axs[-1].set_ylabel("data / MC")
        axs[-1].axhline(1, linestyle=":", linewidth=0.5, color="k")
        if axs[-1].get_ylim()[1] > 2.5:
            axs[-1].set_ymargin(0)
            axs[-1].set_ylim(top=2.5)

        tar = self.local_target(f"{name}/{step.key}.pdf")
        tar.parent.touch()
        fig.savefig(tar.path)

        return ret


@luigi.util.inherits(CoffeaProcessor)
class TriggerSFInternal(TriggerSFBase):
    version = CoffeaProcessor.version
    processor = "TriggerEfficency"
    debug = False
    explorative = False

    def store_parts(self):
        return super().store_parts() + (self.analysis_choice, self.recipe)

    def requires(self):
        return CoffeaProcessor.req(self, processor=self.processor)

    def load_eff(self):
        inp = self.input().load()

        eff_data = {}
        eff_mc = {}
        for ds, eff in inp["eff"].items():
            ds = self.campaign_inst.datasets.get(ds)
            if ds.is_data:
                iadd(eff_data, eff)
            else:
                f = ds.xs.nominal * self.campaign_inst.aux["lumi"]
                f /= inp["sum_gen_weights"][ds.name, "nominal"]
                eff = {trig: {bit: e * f for bit, e in bits.items()} for trig, bits in eff.items()}
                iadd(eff_mc, eff)

        return eff_data, eff_mc


class TriggerSFExternal(TriggerSFBase):
    variant = luigi.Parameter("matched")

    def store_parts(self):
        return super().store_parts() + (self.variant,)

    def requires(self):
        return DownloadFiles.req(self, type="triggersf")

    def load_eff(self):
        inp = self.input()[self.variant].load(formatter="uproot")

        eff_data = {}
        eff_mc = {}
        for key, tgae in inp.items(filter_classname="TGraphAsymmErrors", cycle=False):
            ds, trig, _, step = key.split("_")
            assert _ == "Efficiency"

            eff = dict(TTbar=eff_mc, SingleMuon=eff_data)[ds]
            trig = self.campaign_inst.aux["triggersf"]["lut_trigger"][trig]
            step = self.campaign_inst.aux["triggersf"]["lut_step"].get(step, f"hlt{step}")
            eff.setdefault(trig, {})[step] = Efficency.from_TGraphAsymmErrors(tgae)

        return eff_data, eff_mc


np.set_printoptions(precision=4, linewidth=250, suppress=True)  # HACK
# fitting functions
def toc(x, loc, scale, off, tot, m=None, beta=None, ew=None):
    assert (m is None) == (beta is None)
    if m is None:
        ret = norm.cdf(x, loc, scale)
    else:
        ret = crystalball.cdf(x, beta, m, loc, scale)
        if ew is not None:
            # ret *= 0.5 * (1 - erf((x-loc)/ew))
            ret *= norm.cdf(x, loc, ew)
    return (tot - off) * ret + off


def poly(x, *p):
    return np.polynomial.Polynomial(p)(x)
    return np.poly1d(p)(x)  # from roots, not desired - we need complex roots


# trigger steps
@dataclass(frozen=True)
class _Step:
    binInfo: Union[Tuple[int, float, float], np.ndarray]
    key: Union[str, Set[str]]
    with_shifts = True

    def __class_getitem__(cls, bi) -> Callable:
        return partial(cls, bi)

    def axis(self):
        bi = self.binInfo
        if isinstance(bi, slice):
            assert bi.step.real == 0 and bi.step.imag > 0
            return hist.axis.Regular(int(bi.step.imag), bi.start, bi.stop, name=self.var_name)
        elif isinstance(bi, tuple):
            bi += (np.array([b.stop for b in bi if isinstance(b, slice)]),)
            bi = np.unique(np.r_[bi])
            return hist.axis.Variable(bi, name=self.var_name)
        else:
            raise ValueError(f"unsupported binInfo: {bi!r}")

    @property
    def var_name(self):
        raise NotImplementedError

    def value(self, drv) -> ak.Array:
        raise NotImplementedError

    def cond(self, drv) -> ak.Array:
        raise NotImplementedError

    def measure(self, drv: "TriggerDriver", mask: np.ndarray) -> Tuple[np.ndarray, Efficency]:
        cond = ak.to_numpy(self.cond(drv))
        fa = dict(passing=cond[mask], weight=drv.weight_arr[mask])
        fa[self.var_name] = ak.to_numpy(self.value(drv)[mask])

        eff = Efficency.from_axes(self.axis())
        eff.fill(**fa)
        return mask & cond, eff

    def eff(self, drv: "TriggerDriver", efunc: "TriggerDriver.DataMC") -> np.ndarray:
        return efunc.ucd(ak.to_numpy(self.value(drv)))

    @staticmethod
    def _nth(obj, num: int, fill=0):
        assert 0 < num
        obj = ak.pad_none(obj, num)
        if fill is not None:
            obj = ak.fill_none(obj, fill)
        return obj[:, num - 1]

    @property
    def fit_kwargs(self) -> dict:
        lo, hi = self.axis().edges[[0, -1]]
        w = hi - lo
        return dict(
            fun=toc,
            x0=[w / 6, w / 15, 0.02, 0.99, 2, 1, w / 25],
            bounds=[
                (0, hi),  # originally not bound
                (1, hi),  # originally not bound
                (0, 1),
                (0, 1),
                (1.01, 10),
                (0.001, 10),
                (1, hi),  # original (10, 100),
            ],
        )


@dataclass(frozen=True)
class HTStep(_Step):
    kind: Literal["calo", "pf", "pure"]
    var_name = "HT"

    def value(self, drv):
        return getattr(drv, f"ht_{self.kind}")

    def cond(self, drv):
        return drv.trigObjMask(flag=self.key, alt="HT")


@dataclass(frozen=True)
class JetStep(_Step):
    nJet: int

    @property
    def var_name(self):
        return f"pt{self.nJet}"

    def value(self, drv):
        return self._nth(drv.jets_pt.pt, self.nJet)

    def cond(self, drv):
        return drv.trigObjMask(flag=self.key, jetCount=self.nJet)


@dataclass(frozen=True)
class TagStep(_Step):
    nTag: int
    nJet = 4
    var_name = "btagDeepFlavB"
    with_shifts = True

    def __post_init__(self):
        assert 0 < self.nTag <= self.nJet <= 4

    def value(self, drv):
        return self._nth(getattr(drv.jets_b, self.var_name), 1, fill=-1)

    def cond(self, drv):
        return drv.trigObjMask(flag=self.key, jetCount=self.nTag)

    def eff(self, drv: "TriggerDriver", efunc: "TriggerDriver.DataMC") -> np.ndarray:
        b = ak.fill(ak.pad_none(drv.jets_b, self.nJet, clip=True), -1)
        assert b.shape[1:] == (self.nJet,)
        eff = efunc.ucd(b)
        assert eff.shape == (3,) + b.shape

        if self.nTag == 0:
            return np.ones_like(eff[..., 0])

        if self.nTag == self.nJet:
            return eff.prod(axis=-1)

        ief = (1 - eff) / eff

        if self.nTag <= 2:
            pief = ief.prod(axis=-1)
            if self.nTag == 1:
                return 1 - pief
            elif self.nTag == 2:
                return 1 - pief - (pief[..., None] * eff).sum(axis=-1)

        if self.nJet == self.nTag + 1:
            peff = eff.prod(axis=-1)
            return peff + (peff[..., None] * ief).sum(axis=-1)

        raise ValueError(f"unsupported nTag/nJet combination ({self.nTag}/{self.nJet})")

    @property
    def fit_kwargs(self) -> dict:
        return dict(fun=poly, x0=list(range(5)))


class Trigger:
    def __init__(self, key: Union[str, Iterable[str]], *steps: List[_Step]) -> None:
        self.key = key
        self.steps = steps

    @property
    def name(self) -> str:
        if isinstance(self.key, str):
            return self.key
        return "_XOR_".join(sorted(self.key))

    def mask(self, HLT: ak.Array):
        key = self.key
        if isinstance(key, str):
            key = [key]
        res = {k: getattr(HLT, k) for k in key if hasattr(HLT, k)}
        assert res, f"no trigger found, of {self.key!r}"
        assert len(res) == 1, f"multiple triggers found: {list(res.keys())}"
        return next(iter(res.values()))

    def measure(self, drv: "TriggerDriver", mask: np.ndarray) -> Dict[str, Efficency]:
        curr = mask.copy()
        ret = {}
        for step in self.steps:
            assert step.key not in ret
            curr, ret[step.key] = step.measure(drv, curr)
        return ret

    def match(self, drv: "TriggerDriver") -> np.ndarray:
        return reduce_and(step.cond(drv) for step in self.steps)

    def eff(self, drv: "TriggerDriver", efunc: Dict[str, "TriggerDriver.DataMC"]) -> np.ndarray:
        return reduce(mul, (step.eff(drv, efunc[step.key]) for step in self.steps))


class TriggerDriver:
    def check(self, triggers: Iterable[Trigger], HLT: ak.Array) -> np.ndarray:
        return reduce_or(trig.mask(HLT) for trig in triggers)

    def __init__(
        self,
        triggers: List[Trigger],
        cand: ak.Array,
        jets: ak.Array,
        events: ak.Array,
        weights: Weights,
    ):
        self.triggers = triggers
        self.cand = cand
        self.events = events
        self.weights = weights

        # collect filter bits (for jets, near candidates)
        comb = ak.cartesian([cand, events.TrigObj[events.TrigObj.id == 1]], axis=1, nested=True)
        self.jet_bits = comb.slot1[comb.slot0.delta_r2(comb.slot1) < 0.5**2].filterBits

        # parse TrigObj.id table
        self.toik = {}
        for nv in events.TrigObj.id.__doc__.split(":", 1)[-1].strip().split(", "):
            n, v = nv.split(" = ")
            v = v.strip()
            assert v not in self.toik
            self.toik[v] = int(n)

        # parse TrigObj.filterBit table
        self.tofbt = {}
        for s in re.sub("/s+", " ", events.TrigObj.filterBits.__doc__).strip().split(";"):
            s = s.strip()
            if not s:
                continue
            s, kind = s.rsplit(" for ", 1)
            s = s.split(":", 1)[-1].strip()
            kt = self.tofbt.setdefault(kind, {})
            for p in re.split(r",|\bbit\b", s):
                p = p.strip()
                if not p:
                    continue
                try:

                    n, vv = re.search("(\d+) +(?:=|for) +(.+)", p).groups()
                except:
                    from IPython import embed; embed(); raise  # fmt: skip
                n = int(n)
                for v in vv.split(" or "):
                    assert v not in kt
                    kt[v] = n

        # jet collections
        self.jets_b = cand[ak.argsort(cand.btagDeepFlavB, ascending=False)][:, :4]
        self.jets_pt = cand[ak.argsort(cand.pt, ascending=False)][:, :4]

        # HT variables
        ht_jets = jets[(jets.pt > 30) & (abs(jets.pt) < 2.5)]
        self.ht_pf = ak.sum(jets.pt, axis=-1)
        ht_jets = ht_jets[~ak.any(ht_jets.matched_muons.pfRelIso04_all < 0.3, axis=-1)]
        self.ht_calo = ak.sum(jets.pt, axis=-1)
        ht_jets = ht_jets[~ak.any(ht_jets.matched_electrons.pfRelIso03_all < 0.3, axis=-1)]
        self.ht_pure = ak.sum(jets.pt, axis=-1)

    @property
    def weight_arr(self):
        return self.weights.weight()

    def trigObjMask(self, flag: str, jetCount: int = 0, alt: str = None) -> np.ndarray:
        good = False
        if jetCount > 0:
            bit: int = self.tofbt["Jet"][flag]
            good = good | (akm((self.jet_bits >> bit) & 1, bool, any, sum, np) >= jetCount)
        if alt is not None:
            typ: int = self.toik[alt]
            bit: int = self.tofbt[alt][flag]
            bits = self.events.TrigObj[self.events.TrigObj.id == typ].filterBits
            good = good | akm((bits >> bit) & 1, bool, any, np)
        assert good is not False
        return good

    def measure(self, mask: ak.Array) -> Dict[str, Dict[str, Efficency]]:
        ret = {}
        mask_and = None
        for trigger in self.triggers:
            trig = trigger.mask(self.events.HLT)
            ret[trigger.name] = trigger.measure(self, mask)
            if mask_and is None:
                mask_and = mask & trig
            else:
                ret[f"{trigger.name}_AND"] = trigger.measure(self, mask_and)
                mask_and &= trig

        return ret

    def check(self) -> np.ndarray:
        "Trigger bit check"
        return reduce_or(trig.mask(self.events.HLT) for trig in self.triggers)

    def match(self) -> np.ndarray:
        "Trigger object matching"
        return reduce_or(trig.match(self) for trig in self.triggers)

    @dataclass
    class DataMC:
        data: Efficency.Func
        mc: Efficency.Func

        def ucd(self, x: np.ndarray, **kwargs) -> np.ndarray:
            du, dc, dd = self.data.ucd(x, **kwargs)
            mu, mc, md = self.mc.ucd(x, **kwargs)
            return np.stack([du / md, dc / mc, dd / mu])

        def plot(
            self,
            axs,
            x: np.ndarray,
            fill: Union[Dict, None, Literal[False]] = None,
            ucd_kwargs: Dict = {},
            **kwargs,
        ):
            if fill is not False:
                up, nom, down = self.ucd(x, **ucd_kwargs)
            else:
                nom = self.data(x) / self.mc(x)

            axs.plot(x, nom, **kwargs)
            if fill is not False:
                if fill is None:
                    fill = dict(alpha=0.3, label=None, linewidth=0)
                up, down = np.minimum(up, down), np.maximum(up, down)
                axs.fill_between(x, down, up, **dict(kwargs, **fill))

    def eff_sf(self, efunc: Dict[str, Dict[str, DataMC]]) -> np.ndarray:
        eff_tot = 0
        eff_and = None
        for trigger in self.triggers:
            eff_tot += (eff := trigger.eff(self, efunc[trigger.name]))
            if eff_and is None:
                eff_and = eff
            else:
                eff_tot -= eff_and * trigger.eff(self, efunc[f"{trigger.name}_AND"])
                eff_and *= eff

        return np.divide(
            eff_tot[0],
            eff_tot[1],
            out=np.ones_like(eff_tot[1]),
            where=eff_tot[1] > 0,
        )

    def apply_sf(self, sf: np.ndarray, name="triggerSF"):
        assert sf.shape[0] == 3
        self.weights.add(name, sf[0], weightUp=sf[1], weightDown=sf[2])
