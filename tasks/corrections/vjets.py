# coding: utf-8

from collections import defaultdict
from functools import partial

import awkward as ak
import hist
import luigi
import luigi.util
import numpy as np
from coffea.processor import Weights

from config.common import get as Pget
from processor.util import reduce_and, reduce_or
from tasks.coffea import CoffeaProcessor
from utils.coffea import BaseProcessor, hist_accumulator, mk_dense_evaluator

from .base import CorrectionsBase

"""
    dsname = self.get_dataset(events).name
    if dsname in corr:
        SF = corr[dsname](events.LHE.HT, events.LHE.nJet)
"""


@luigi.util.inherits(CoffeaProcessor)
class VJetsCorrections(CorrectionsBase):
    version = CoffeaProcessor.version
    processor = "VJetsStitchCount"
    debug = False
    explorative = False

    def group_to_procs(self, group):
        incl, excl = group
        incl_proc = self.analysis_inst.processes.get(incl)
        excl_procs = [self.analysis_inst.processes.get(ex) for ex in excl]
        return incl_proc, excl_procs

    def check_groups(self, groups):
        for group in groups.values():
            incl_proc, excl_procs = self.group_to_procs(group)
            assert "binned" not in incl_proc.aux
            assert any(ex.aux.get("binned", None) for ex in excl_procs)

    def process(self, data, group):
        incl_proc, excl_procs = self.group_to_procs(group)
        root_procs = [incl_proc] + excl_procs

        data = data.project(
            "dataset",
            *set(
                b[0] if isinstance(b, tuple) else b for ex in excl_procs for b in ex.aux["binned"]
            ),
        )
        bins = []
        for dsname in data.axes["dataset"]:
            ds = self.campaign_inst.datasets.get(dsname)
            (proc,) = ds.processes.values
            root = VJetsStitchHelper.proc2root(root_procs, proc)
            if root is None:
                continue
            bins.append((ds, proc, root))

        bins.sort(key=lambda x: (root_procs.index(x[2]), x[0].id))

        roots = [x[2] for x in bins]
        if incl_proc in roots:
            assert incl_proc == roots[0]  # can only fail if previous sort failed
            assert incl_proc not in roots[1:], "illegal multiple datasets of inclusive process"
        else:
            assert len(set(roots)) <= 1, "multiple to-be-stitched datasets, but inclusive missing"
            return None, None, None  #

        filled = defaultdict(int)

        dsnames = []
        count = []
        xsec = []
        for ds, proc, root in bins:
            v = data[ds.name, ...].view(flow=True)["value"]
            xs = proc.xsecs[13].nominal

            filled[root.name] |= v != 0

            count.append(v)
            xsec.append(xs)
            dsnames.append(ds.name)

        count = np.array(count)
        daxes = tuple(range(1, count.ndim))
        filled = sum(filled.values())
        xsec = np.expand_dims(xsec, daxes)

        if 0 and incl_proc == self.analysis_inst.processes.get("dy_lep_50ToInf"):
            count[:] = 0
            count[dsnames.index("DYJetsToLL_0J"), 0:4] = [6.55e7, 1.00e7, 0, 0]
            count[dsnames.index("DYJetsToLL_1J"), 0:4] = [0, 4.36e7, 6.18e3, 0]
            count[dsnames.index("DYJetsToLL_2J"), 0:4] = [0, 0, 2.16e7, -2.80e6]
            count[dsnames.index("DYJetsToLL_M-50"), 0:4] = [1.01e8, 2.41e7, 7.89e6, -1.51e6]
            xsec[:] = 0
            xsec[dsnames.index("DYJetsToLL_0J")] = 4843.6
            xsec[dsnames.index("DYJetsToLL_1J")] = 897.8
            xsec[dsnames.index("DYJetsToLL_2J")] = 335.8
            xsec[dsnames.index("DYJetsToLL_M-50")] = 6077.2
            filled[:] = 0
            filled[0:4] = 2

        weights = VJetsStitchHelper.calculate(
            na=count,
            nc=count.sum(axis=0, keepdims=True),
            nd=count.sum(axis=daxes, keepdims=True),
            fa=filled,
            xd=xsec,
        )

        axes = [ax for ax in data.axes if ax.name != "dataset"]
        return (
            dict(zip(dsnames, weights)),
            [
                np.r_[
                    -np.inf if ax.traits.underflow else slice(None),
                    ax.edges,
                    np.inf if ax.traits.overflow else slice(None),
                ]
                for ax in axes
            ],
            [ax.name for ax in axes],
        )

    def prepare(self, data, groups):
        axes = {}
        evdata = {}
        for group in groups.values():
            dat, e, a = self.process(data, group)
            if dat is None and e is None and a is None:
                print(f"skipping group: {group!r}")
                continue
            if len(e) == 1:
                e = e[0]
            for ds, d in dat.items():
                evdata[ds] = (d, e)
                axes[ds] = a
        return evdata, axes

    def requires(self):
        return CoffeaProcessor.req(self, processor=self.processor)

    @property
    def available(self):
        return self.__class__.__name__ in self.analysis_inst.aux.get("non_common_corrections", [])

    def run(self):
        data = self.input().load()["LHE_vJets"].hist.to_hist()
        self.check_groups(groups)
        out = VJetsStitchSF(*self.prepare(data, groups))
        self.output().dump(out)


groups = dict(
    dy_50to_nlo=(
        "dy_lep_50ToInf",
        [
            "dy_lep_nj",
            # LO samples
            "DYJetsToLL_M_50_incl",
            "DYJetsToLL_M_50_HT",
            # b-enriched samples
            "DYBJetsToLL_M-50",
            "DYJetsToLL_BGenFilter_M-50",
        ],
    ),
    wjets=(
        "WJetsToLNu",  # LO & NLO samples here
        [
            # NLO samples
            "WJetsToLNu_nJ",
            # LO samples
            "WnJetsToLNu",
            "WJetsToLNu_HT",
            # b-enriched samples
            "WBJetsToLNu_Wpt",
            "WJetsToLNu_BGenFilter_Wpt",
        ],
    ),
)


class VJetsStitchHelper:
    def proc2root(roots, proc):
        for root in roots:
            if proc is root or proc.has_parent_process(root):
                return root

    def calculate(na, nc, nd, fa, xd):
        # names: 12
        # 1: Xsec, Number, Weight, Filled bins
        # 2: per Dataset, per Condition/bin, All
        wd = xd / nd
        xc = (wd * na / fa).sum(axis=0, keepdims=True)
        wc = xc / nc
        wa = wc / wd
        return np.where(na, wa, 0)

    def gen2bStatus(events):
        gpPDG = np.abs(events.GenPart.pdgId)
        return reduce_and(
            events.GenPart.status == 2,
            reduce_or(
                reduce_and(500 <= gpPDG, gpPDG < 600),
                reduce_and(5000 <= gpPDG, gpPDG < 6000),
            ),
        )


class VJetsStitchCount(BaseProcessor):
    @classmethod
    def want_dataset(cls, ds, group, config):
        incl, excl = group
        root = [incl] + excl
        root_procs = [config.processes.get(proc) for proc in root]
        ds_procs = ds.processes.values
        return any(VJetsStitchHelper.proc2root(root_procs, proc) is not None for proc in ds_procs)

    @classmethod
    def requires(cls, task):
        return task.base_requires(
            data_source="mc",
            data_filter=lambda ds: any(
                cls.want_dataset(ds, g, task.analysis_inst) for g in groups.values()
            ),
            corrections=False,
            pileup={},
            # prefiring={},
        )

    def __init__(self, task):
        super().__init__(task)
        self._accumulator["LHE_vJets"] = hist_accumulator(
            hist.Hist(
                self.dataset_axis,
                hist.axis.Variable(
                    [0, 70, 100, 200, 400, 600, 800, 1200, 2500], name="HT", underflow=False
                ),
                hist.axis.Regular(1, 100, 200, name="Vpt"),
                hist.axis.Integer(0, 6, name="Njets", underflow=False),
                hist.axis.Integer(0, 1, name="Nb", underflow=False),
                hist.axis.Integer(0, 1, name="nGen2bStatus", underflow=False),
                storage=hist.storage.Weight(),
            )
        )

    def process(self, events):
        output = self.accumulator.identity()
        # TODO: add PU, L1prefireing, top_pt weights
        w = Weights(len(events))
        w.add("gen", ak.to_numpy(events.Generator.weight))
        w.add(
            "pu",
            **self.corrections["pileup"](
                pu_key=self.get_pu_key(events),
                nTrueInt=events.Pileup.nTrueInt,
            ),
        )
        VptSF()(self.get_dataset(events), events, w)
        # if "prefiring" in self.corrections:
        #     weight *= self.corrections["prefiring"](events.Jet, events.Photon, dir=0)
        # weight *= top_pT_reweighting(events.GenPart).flatten()  # who tho?

        output["LHE_vJets"].hist.fill(
            dataset=self.get_dataset(events).name,
            HT=ak.to_numpy(events.LHE.HT),
            Vpt=ak.to_numpy(events.LHE.Vpt),
            Njets=ak.to_numpy(events.LHE.Njets),
            Nb=ak.to_numpy(events.LHE.Nb),
            nGen2bStatus=ak.to_numpy(ak.sum(VJetsStitchHelper.gen2bStatus(events), axis=-1)),
            weight=w.weight(),
        )
        return output


class FakeLHE:
    Vpt = None
    Nb = None


class VptSF:
    def __call__(self, dataset, events, weights, bjet=True, bjet_norm=True, nlo=True):
        LHE = getattr(events, "LHE", FakeLHE)
        if bjet and dataset.campaign.aux["year"] in (2017, 2018):
            bjet = self.bjet(dataset, LHE.Vpt, norm=bjet_norm)
            if bjet is not None:
                weights.add("Vpt_bjets", bjet)
        # Disable, see: https://git.rwth-aachen.de/3pia/cms_analyses/common/-/issues/293
        if False and nlo:
            nlo = self.nlo(dataset, LHE.Vpt, LHE.Nb, shifts=True)
            if nlo is None:
                sf = np.ones((len(events),))
                shift = 0
            else:
                sf, shift = nlo
            weights.add(
                "Vpt_nlo",
                weight=np.array(sf).astype(np.float32),
                weightUp=np.array(sf + shift).astype(np.float32),
                weightDown=np.array(sf - shift).astype(np.float32),
            )

    def bjet(self, dataset, vpt, norm=True):
        coeff = self._get(dataset, self.coeff_bjet)
        if coeff is None:
            return
        n = self._get(dataset, self.norm_bjet, 1) if norm else 1
        vpt = ak.to_numpy(vpt)
        return self._poly(vpt[:], coeff) * n

    def nlo(self, dataset, vpt, Nb, shifts=True):
        target = self._get(dataset, self.xfer_nlo)
        if target is None:
            return
        coeff = self.coeff_nlo[target]
        if coeff is None:
            return
        if target == "ZJetsToNuNu":
            raise NotImplementedError("VptSF ZJetsToNuNu deltaEta reweigthing")
        Nb = ak.to_numpy(Nb)
        Nb = np.clip(Nb, 0, coeff.shape[0] - 1, dtype=np.intp)
        if not shifts:
            coeff = coeff[..., 0]
        vpt = ak.to_numpy(vpt)
        sf = self._poly(vpt[:], np.moveaxis(coeff[Nb], 0, -1))
        return sf

    def _get(self, dataset, obj, default=None):
        for proc in dataset.processes.values:
            if proc.name in obj:
                return obj[proc.name]
            for _, pproc in proc.walk_parent_processes():
                if pproc.name in obj:
                    return obj[pproc.name]
        return default

    def _poly(self, x, coeff):
        ret = coeff[-1]
        for c in coeff[-2::-1]:
            ret *= x
            ret += c
        return ret

    # see AN2019_229_v6, Table 11-13
    norm_bjet = {
        #  [dataset]
        "zjets_nunu_bgen": 1.242 * 3,
        "WJetsToLNu_BGenFilter_Wpt": 1.248,
        "DYJetsToLL_BGenFilter_M-50": 1.171,
        "zbjets_nunu": 1.332,
        "WBJetsToLNu_Wpt": 0.977,
        "DYBJetsToLL_M-50": 1.259,
    }
    coeff_bjet = {
        #  [dataset][coeff] poly(Vpt)
        "zjets_nunu_bgen": [7.7e-1, 1.184e-3, -9.181e-7],
        "WJetsToLNu_BGenFilter_Wpt": [8.325e-1, 1.054e-3, -1.067e-6],
        "DYJetsToLL_BGenFilter_M-50": [7.825e-1, 1.529e-3, -9.667e-7],
        "zbjets_nunu": [6.968e-1, 1.764e-3, -1.526e-6],
        "WBJetsToLNu_Wpt": [1.005, 5.043e-4, -3.894e-7],
        "DYBJetsToLL_M-50": [7.519e-1, 1.975e-3, -1.836e-6],
    }
    xfer_nlo = {
        # BGenFilter
        "zjets_nunu_bgen": "ZJetsToNuNu",
        "WJetsToLNu_BGenFilter_Wpt": "WJetsToLNu",
        "DYJetsToLL_BGenFilter_M-50": "DYJetsToLL",
        # plain LO
        "WnJetsToLNu": "WJetsToLNu",
        "WJetsToLNu_HT": "WJetsToLNu",
        "DYJetsToLL_M_50_incl": "DYJetsToLL",
        "DYJetsToLL_M_50_HT": "DYJetsToLL",
        # BJets
        "zbjets_nunu": "ZJetsToNuNu",
        "WBJetsToLNu_Wpt": "WJetsToLNu",
        "DYBJetsToLL_M-50": "DYJetsToLL",
    }
    coeff_nlo = {
        # [dataset][nB][coeff][0: nominal, 1: uncertainty] poly(Vpt)
        "ZJetsToNuNu": np.array(
            [
                [[1.688, 0.002], [-1.785e-3, 0.007e-3]],
                [[1.575, 0.007], [-1.754e-3, 0.027e-3]],
                [[1.424, 0.010], [-1.539e-3, 0.041e-3]],
            ]
        ),
        "WJetsToLNu": np.array(
            [
                [[1.628, 0.005], [-1.339e-3, 0.020e-3]],
                [[1.586, 0.027], [-1.531e-3, 0.112e-3]],
                [[1.440, 0.048], [-0.925e-3, 0.203e-3]],
            ]
        ),
        "DYJetsToLL": np.array(
            [
                [[1.650, 0.002], [-1.707e-3, 0.020e-3]],
                [[1.534, 0.010], [-1.485e-3, 0.080e-3]],
                [[1.519, 0.019], [-1.916e-3, 0.140e-3]],
            ]
        ),
    }
    coeff_deta = np.array(
        [
            # only for ZJetsToNuNu
            # [nB][Vpt bin][coeff] poly(x)
            # Vpt edges: 150, 170, 200, 250, 300, 400
            [
                [8.902e-1, +1.171e-01, -4.028e-02, +5.413e-03],
                [8.902e-1, +1.171e-01, -4.028e-02, +5.413e-03],
                [9.055e-1, +1.016e-01, -4.262e-02, +6.732e-03],
                [9.258e-1, +6.067e-02, -2.653e-02, +4.067e-03],
                [9.446e-1, +8.139e-02, -4.740e-02, +8.163e-03],
            ],
            [
                [9.509e-1, +4.284e-02, -2.355e-02, +6.062e-03],
                [9.476e-1, +6.023e-02, -4.144e-02, +9.121e-03],
                [9.424e-1, +8.237e-02, -4.904e-02, +9.142e-03],
                [9.838e-1, -5.520e-03, -1.425e-02, +4.843e-03],
                [1.012e0, +5.707e-03, -3.566e-02, +1.008e-02],
            ],
            [
                [8.635e-1, +4.481e-02, +8.009e-02, -1.883e-02],
                [8.942e-1, +6.551e-02, +2.721e-02, -7.706e-03],
                [8.709e-1, +8.808e-02, +1.561e-02, -6.648e-03],
                [9.174e-1, +2.229e-02, +4.194e-02, -1.010e-02],
                [9.712e-1, -1.882e-02, +5.351e-02, -1.159e-02],
            ],
        ]
    )


class VptSFCount(BaseProcessor):
    @classmethod
    def requires(cls, task):
        return task.base_requires(
            data_source="mc",
            data_filter=lambda ds: any(
                VJetsStitchCount.want_dataset(ds, g, task.analysis_inst) for g in groups.values()
            ),
            corrections=False,
        )

    def process(self, events):
        return (
            hist.Hist.new.StrCat("", name="dataset", growth=True)
            .Int(0, 2, name="Nb", underflow=False)
            .Reg(100, 0, 1000, name="Vpt")
            .Weight()
            .fill(
                dataset=self.get_dataset(events).name,
                Nb=ak.to_numpy(events.LHE.Nb),
                Vpt=ak.to_numpy(events.LHE.Vpt),
                weight=ak.to_numpy(events.Generator.weight),
            )
        )


@luigi.util.inherits(CoffeaProcessor)
class RecalcVptSF(CorrectionsBase):
    version = CoffeaProcessor.version
    processor = "VptSFCount"
    debug = False
    explorative = False

    bins = luigi.IntParameter(
        default=25, description="Number of bins used for rebinning in 0 <= Vpt <= 1000."
    )

    def requires(self):
        return CoffeaProcessor.req(self, processor=self.processor)

    def run(self):
        import matplotlib.pyplot as plt
        from scinum import DOWN, UP, Number
        from scipy.optimize import curve_fit

        self.output().parent.touch()

        fun = lambda x, a, b: b - a * x

        h = self.input().load()
        hview = h.view(True)

        # rebin
        hnew = hist.Hist(
            h.axes["dataset"],
            h.axes["Nb"],
            hist.axis.Regular(self.bins, 0, 1000, name="Vpt"),
            storage=hist.storage.Weight(),
        )
        loc = h.axes["Vpt"].index
        ax = hnew.axes["Vpt"]
        for bin in range(ax.size):
            hnew[..., bin] = hview[..., loc(ax[bin][0]) : loc(ax[bin][1])].sum(axis=-1)

        h = hnew
        x = h.axes["Vpt"].centers
        coeffs = {}

        # apply xsec weight
        for d in h.axes["dataset"]:
            xs = self.campaign_inst.datasets.get(d).process.xsecs[13].nominal
            print(d, xs)
            h[d, ...] = h[d, ...].view(True) * xs

        for k, v in VptSF.xfer_nlo.items():
            lo = self.analysis_inst.processes.get(k)
            nlo = self.analysis_inst.processes.get(v)
            datasets = {
                order: [
                    d
                    for d in h.axes["dataset"]
                    if self.campaign_inst.datasets.get(d).process.has_parent_process(
                        ds, include_self=True
                    )
                ]
                for order, ds in [("lo", lo), ("nlo", nlo)]
            }

            # slice away overflow and underflow of Vpt axis
            hv = h.view(True)[..., 1:-1]
            dssum = partial(np.sum, axis=0)
            hvnlo = Number(
                dssum(
                    (arr := hv[[h.axes["dataset"].index(d) for d in datasets["nlo"]], ...])["value"]
                ),
                np.sqrt(dssum(arr["variance"])),
            )
            # norm
            hvnlo = hvnlo / Number(n := np.sum(hvnlo.nominal, axis=-1)[:, None], np.zeros_like(n))
            hvlo = Number(
                dssum(
                    (arr := hv[[h.axes["dataset"].index(d) for d in datasets["lo"]], ...])["value"]
                ),
                np.sqrt(dssum(arr["variance"])),
            )
            # norm
            hvlo = hvlo / Number(n := np.sum(hvlo.nominal, axis=-1)[:, None], np.zeros_like(n))
            r = hvnlo / hvlo
            if np.all(bad := ~np.isfinite(r.nominal)):
                continue

            good = ~bad & (r.nominal != 0)

            # up == down
            assert np.all(r.u(direction=UP)[good] == r.u(direction=DOWN)[good])

            # initialize output values
            out = np.empty((3, 2, 2))
            # fit and extract best fit values for each Nb:
            for Nb in 0, 1, 2:
                valid = good[Nb]
                popt, pcov = curve_fit(
                    fun,
                    x[valid],
                    r.nominal[Nb, ...][valid],
                    sigma=r.u(direction=UP)[Nb, ...][valid],
                    # absolute_sigma=True,
                )
                print(Nb, r.nominal[Nb, ...][valid])
                out[Nb, 0, 0] = popt[0]
                out[Nb, 0, 1] = pcov[0, 0] ** 0.5
                out[Nb, 1, 0] = popt[1]
                out[Nb, 1, 1] = pcov[1, 1] ** 0.5
                # plot normed distributions
                plt.errorbar(
                    x[valid],
                    hvlo.nominal[Nb, ...][valid],
                    yerr=hvlo.u(direction=UP)[Nb, ...][valid],
                    fmt="o",
                    label=f"LO",
                    color="green",
                )
                plt.errorbar(
                    x[valid],
                    hvnlo.nominal[Nb, ...][valid],
                    yerr=hvnlo.u(direction=UP)[Nb, ...][valid],
                    fmt="o",
                    label=f"NLO",
                    color="orange",
                )
                plt.legend()
                plt.xscale("log")
                plt.title("2018")
                plt.xlabel("LHE Vpt")
                plt.ylabel("a.u.")
                plt.savefig(self.output().parent.path + f"/{k}-{v}-Nb{Nb}-lo_vs_nlo.pdf")
                plt.close()

                # plot fit
                plt.errorbar(
                    x[valid],
                    r.nominal[Nb, ...][valid],
                    yerr=r.u(direction=UP)[Nb, ...][valid],
                    fmt="o",
                    label="Ratio",
                    color="black",
                )
                plt.plot(x[valid], fun(x[valid], popt[0], popt[1]), "r-", label="Fit")
                plt.ylim(0, 10)
                plt.title("2018")
                plt.xlabel("LHE Vpt")
                plt.ylabel("Ratio factor NLO/LO")
                plt.savefig(self.output().parent.path + f"/{k}-{v}-Nb{Nb}.pdf")
                plt.close()
            coeffs[(k, v)] = out
        print(coeffs)
        print(self.output().parent.path)
        from IPython import embed

        embed()


class VJetsStitchSF:
    name = "VJetsStitching"
    vptSF = VptSF()

    def __init__(self, evdata, axes):
        self.ev = mk_dense_evaluator(evdata)
        self.axes = axes

    def __call__(self, dataset, events, weights):
        self.vptSF(dataset, events, weights)
        if dataset.name in self.ev:
            if missing := [ds for ds in dir(self.ev) if dataset.campaign.datasets.get(ds) is None]:
                raise RuntimeError("missing VJets datasets: " + ", ".join(missing))
            vars = [
                (
                    ak.to_numpy(ak.sum(VJetsStitchHelper.gen2bStatus(events), axis=-1))
                    if ax == "nGen2bStatus"
                    else ak.to_numpy(getattr(events.LHE, ax))
                )
                for ax in self.axes[dataset.name]
            ]
            weights.add(self.name, self.ev[dataset.name](*vars))
