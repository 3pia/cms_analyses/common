# coding: utf-8

import awkward as ak
import numpy as np
import uproot
from coffea.processor.accumulator import (
    defaultdict_accumulator,
    dict_accumulator,
    set_accumulator,
)

from tasks.base import AnalysisCampaignTask, PoolMap
from tasks.files import LHAPDF, DownloadNanoAODs, LHAPDFCompact
from utils.coffea import ProcessorABC
from utils.lhapdfv import PDFInput

from .base import CorrectionsBase

"""
    events = corr.filterBad(events, output)

    corr(events, weights)
"""


class PDFCorrections(CorrectionsBase):
    version = AnalysisCampaignTask.version

    def requires(self):
        return dict(
            full=LHAPDF.req(self),
            need=PDFSeen.req(self),
        )

    def run(self):
        target = self.campaign_inst.aux["PDFSet"]
        need = set(self.input()["need"].load())
        need -= {None, False}  # filter out bad ones
        need.add(target)

        lhapdf = self.input()["full"].load()
        names = set(lhapdf.getSet(key).name for key in need)

        compact = yield LHAPDFCompact.req(self, names=sorted(names))

        self.output().dump(PDFSF(compact.load(), target=target))


class PDFSeen(AnalysisCampaignTask, PoolMap):
    resources = {"full_load": 1}

    def requires(self):
        return DownloadNanoAODs.req(
            self, datasets=[ds.name for ds in self.campaign_inst.datasets.values]
        )

    def output(self):
        return self.local_target("data.json")

    def run(self):
        fns = set(
            target.path
            for files in self.input().values()
            for target in files.targets
            if target.exists()
        )
        ids = set()
        for pdftitle in self.pmap(_fn2pdftitle, fns, unit="file"):
            try:
                ids.add(PDFSF._parseId(pdftitle))
            except PDFSF.PDFError:
                pass

        self.output().dump(sorted(ids))


def _fn2pdftitle(fn):
    with uproot.open(fn) as f:
        e = f["Events"]
        if "LHEPdfWeight" in e:
            return e["LHEPdfWeight"].title
        else:
            None


class PDFSF:
    events_prefix = "PDFWeights_"

    class PDFError(RuntimeError):
        """Critical problem"""

    class PDFIssue(PDFError):
        """Ignored by assuming PDFweights = 1"""

    def __init__(self, lhapdf, target):
        self.lhapdf = lhapdf
        self.target = lhapdf.getSet(target)

    @classmethod
    def _parseId(self, title):
        if not title:
            raise self.PDFIssue("title missing")
        if not title.startswith("LHE pdf variation weights"):
            raise self.PDFIssue("bad title", title)
        try:
            return int(title.split()[-3])
        except (IndexError, ValueError) as e:
            raise self.PDFIssue("malformatted id") from e

    def _parse(self, events):
        vals = getattr(events, "LHEPdfWeight", None)
        if vals is None:
            raise self.PDFIssue("weights missing")

        title = events.metadata.setdefault("PDFtitle", vals.__doc__)
        id0 = self._parseId(title)

        set = self.lhapdf.getSet(id0)
        if set is None:
            raise self.PDFError("unknown set", id0)

        off = id0 - set.lhapdfID
        if off not in (0, 1):
            raise self.PDFError("illegal offset", off)

        return vals, set, off

    def filterBad(self, events):
        try:
            vals, set, off = self._parse(events)
        except self.PDFError:
            return events

        bad = ak.to_numpy(ak.num(vals)) != len(set) - off
        if np.any(bad):
            self.prepare(events)
            return events[~bad]

        return events

    @staticmethod
    def _regular(arr):
        return arr if isinstance(arr, np.ndarray) else ak.to_numpy(arr)

    def calc(self, events, use_xpdf=False):
        try:
            vals, set, off = self._parse(events)
        except self.PDFIssue:
            return
        assert set.hasReplicas or off == 0  # no nominal values available
        # example: https://cernbox.cern.ch/index.php/s/mptWEseLCFnSytA

        valsT = ak.to_numpy(vals).T
        if off:
            valsT = [None] * off + list(valsT)

        if set == self.target:
            return self.target.uncertainty(valsT), 1
        else:
            I = self.mk_input(events)

            set.config["ForcePositive"] = 2  # clip >0
            factor = set.central(valsT) / set.central(I)

            if use_xpdf:
                ref = events.Generator.xpdf1 * events.Generator.xpdf2
                factor = np.where(ref > 0, 1 / ref, factor)

            return self.target.uncertainty(I), factor

    def mk_input(self, events):
        G = events.Generator
        return PDFInput(
            pdgId=np.stack((self._regular(G.id1), self._regular(G.id2)), axis=0),
            x=np.stack((self._regular(G.x1), self._regular(G.x2)), axis=0),
            q=self._regular(G.scalePDF),
            _axis=0,
        )

    def prepare(self, events, **kwargs):
        if not hasattr(events, self.events_prefix + "weight"):
            res = self.calc(events, **kwargs)
            if res is None:
                ones = np.ones(len(events))
                ones.flags.writeable = False
                for k in "weight", "weightUp", "weightDown":
                    events[self.events_prefix + k] = ones
            else:
                res, factor = res
                good = np.isfinite(factor)  # TODO: do we actually still need this?
                for k, v in dict(
                    weight=res.central,
                    weightUp=res.errmax,
                    weightDown=np.maximum(res.errmin, 0),
                ).items():
                    events[self.events_prefix + k] = np.where(good, v * factor, 1)

            # unload PDFs
            self.target._pdfs.clear()

    def __call__(
        self,
        events,
        weights,
        clip=None,
        relative=False,
        **kwargs,
    ):
        self.prepare(events, **kwargs)
        if clip is None:
            clip = (-np.inf, np.inf)
        else:
            if isinstance(clip, (float, int)):
                clip = (1 / clip, clip)
            clip = min(clip), max(clip)
            assert 0 <= clip[0] < 1 < clip[1]

        w, wUp, wDown = [
            np.clip(ak.to_numpy(events[self.events_prefix + k]), *clip)
            for k in ("weight", "weightUp", "weightDown")
        ]
        if relative:
            w1 = np.ones_like(w)
            w1.flags.writeable = False
            weights.add(name="PDFSet_off", weight=w1, weightUp=w)
            weights.add(name="PDFSet_rel", weight=w1, weightUp=wUp / w, weightDown=wDown / w)
        else:
            weights.add(name="PDFSet", weight=w, weightUp=wUp, weightDown=wDown)
