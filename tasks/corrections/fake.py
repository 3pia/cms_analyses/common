# coding: utf-8
import itertools
from functools import partial, reduce
from operator import mul
from copy import deepcopy

from tqdm.auto import tqdm
import numpy as np

from tasks.files import DownloadFiles
from utils.util import pscan_vispa

from .base import CorrectionsBase, PlotCorrectionsBase


def bin_mask(boundaries, *features):
    conditions = []
    for feature, (low, high) in zip(features, boundaries):
        conditions.append(low <= feature)
        conditions.append(feature < high)
    return reduce(mul, conditions)


class HistBinLookup:
    def __init__(self, histo):
        self.histo = histo

    @property
    def axes(self):
        _axes = deepcopy(self.histo._axes)
        ax = []
        # insert under-(0) and over-(inf) flow
        for a in _axes:
            a[0], a[-1] = 0, np.inf
            ax.append(a)
        return ax

    @property
    def bin_masks(self):
        """Prepares list of tuples of unc. names and respective masking functions"""
        binned_axes = []
        for ax in self.axes:
            startsandstops = [*zip(ax, ax[1:])]
            names = list(map(lambda x: f"{x[0]}-{x[1]}", startsandstops))
            binned_axes.append(list(zip(names, startsandstops)))

        bin_masks = []
        for _binned_axes in itertools.product(*binned_axes):
            bin_name = "_".join(b[0] for b in _binned_axes)
            boundaries = [b[1] for b in _binned_axes]
            _bin_mask = partial(bin_mask, boundaries)
            bin_masks.append((bin_name, _bin_mask))

        return bin_masks


class Fake(CorrectionsBase):
    def requires(self):
        return DownloadFiles.req(self, type="histo")

    @property
    def available(self):
        return self.__class__.__name__ in self.analysis_inst.aux.get("non_common_corrections", [])

    def run(self):
        self.ext.add_weight_sets(
            [
                self.template.format(prefix=f"{key}_", hist="*", path=target.path)
                for key, target in self.input()["fake"].items()
            ]
        )
        self.finish()


class PlotFakeCorrections(PlotCorrectionsBase):
    def requires(self):
        return Fake.req(self)

    def run(self):
        self.output().parent.touch()
        inp = self.input().load()
        pb = tqdm(Map := set(k.replace("_error", "") for k in inp.keys()), total=len(Map), unit="Map")  # fmt: skip
        for m in pb:
            self.plot(
                inp, self.local_path(), m, small_size=12, bigger_size=12, dynamic_display=False
            )
        self.output().touch()
        self.logger.info(
            pscan_vispa(
                directory=self.output().parent.path,
                regex=r"Tallinn_fakerates_(?P<Analysis>.+)_FR_mva0(?P<MVA>.+)_(?P<Lepton>.+)_data_comb_?(?P<Kind>.+)?"
                + self._path(""),
            )
        )
