# coding: utf-8

from functools import wraps

import luigi
import numpy as np
from hist import Hist
from scinum import Number as SciNum
from tqdm.auto import tqdm

from tasks.coffea import CoffeaProcessor
from tasks.files import DownloadFiles
from utils.bh5 import Histogram as Hist5
from utils.bh5 import rebin
from utils.coffea import hists2dense_evaluator
from utils.order import dataset_in_group

from .base import CorrectionsBase, PlotCorrectionsBase


class DYEstExternal(CorrectionsBase):
    @property
    def available(self):
        return "DYEstimation" in self.campaign_inst.aux.get("files", {}).get("histo", {})

    def requires(self):
        return DownloadFiles.req(self, type="histo")

    def run(self):
        self.ext.add_weight_sets(
            [
                self.template.format(
                    prefix=f"DYEstimation_{dt}_{ch}_{var}_",
                    hist="*",
                    path=tar.path,
                )
                for (dt, ch, var), tar in self.input()["DYEstimation"].items()
            ]
        )
        self.finish()


def hist_sn(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        for key, arr in kwargs.items():
            dtype = arr.dtype
            sn = SciNum(arr["value"], dict(stat=np.sqrt(arr["variance"])))
            kd = arr.ndim > 1
            sn.sum = SciNum(
                arr["value"].sum(axis=-1, keepdims=kd),
                dict(stat=np.sqrt(arr["variance"].sum(axis=-1, keepdims=kd))),
            )
            kwargs[key] = sn

        ret = func(*args, **kwargs)

        arr = np.empty(shape=ret.n.shape, dtype=dtype)
        arr["value"] = ret.nominal
        arr["variance"] = ret.u("stat", "up")
        return arr

    return wrapper


@luigi.util.inherits(CoffeaProcessor)
class DYEstFull(CorrectionsBase):
    version = CoffeaProcessor.version
    processor = "DYCount"
    debug = False
    explorative = False

    @property
    def available(self):
        return "DYEstFull" in self.analysis_inst.aux.get("non_common_corrections", [])

    def store_parts(self):
        return super().store_parts() + (self.analysis_choice, self.recipe)

    def requires(self):
        return CoffeaProcessor.req(self, processor=self.processor)

    def run(self):
        inp = self.input().load()
        sgw = inp["sum_gen_weights"]
        chan = ["ee", "mumu", "emu", "ll"]

        out = {}
        for var, (xfer, binning) in dict(
            HT=(
                {"resolved_0b_to1b": "resolved_1b", "resolved_0b_to2b": "resolved_2b"},
                np.r_[
                    0,
                    50:100:5,
                    100:200:10,
                    200:300:20,
                    300:400:25,
                    400:600:50,
                    600:800:100,
                    800,
                    1000,
                ],
            ),
            fatjet1_msoftdrop=(
                {"boosted_0b": "boosted_1b"},
                [30, 50, 70, 100, 150, 300],
            ),
        ).items():
            hist = inp["histograms"][var].hist.to_hist()
            assert hist.axes.name == ("dataset", "category", "systematic", var)
            s2d = {f"{ch}_{s}": f"{ch}_{d}" for s, d in xfer.items() for ch in chan}

            # sum data - mc_{w/o DY/signal}
            i_sys = hist.axes[2].index("nominal")
            hdd = Hist(hist.axes["category"], hist.axes[var], storage=hist._storage_type())
            for i, ds in enumerate(hist.axes["dataset"]):
                ds_inst = self.campaign_inst.datasets.get(ds)

                if dataset_in_group(ds_inst, [self.analysis_inst.processes.get("data")]):
                    f = 1
                elif dataset_in_group(
                    ds_inst,
                    [
                        self.analysis_inst.processes.get("diHiggs"),
                        self.analysis_inst.processes.get("dy"),
                    ],
                ):
                    continue
                else:
                    f = -1
                    # apply sum_gen_weight, xsec, lumi
                    (ds_proc,) = ds_inst.processes.values
                    f /= sgw[ds, "nominal"]
                    f *= ds_proc.xsecs[13].nominal * self.campaign_inst.aux["lumi"]

                v = hist.view(True)[hist.axes[0].index(ds), :, i_sys, :].copy()
                v["value"] *= f
                v["variance"] *= f * f
                hdd += v

            hdd = rebin(hdd, binning, var)
            # print("bins", list(binning))

            # output histogram
            hfac = Hist5.regrow(
                hdd,
                dict(category=list(s2d.keys())),
                copy=False,
            )

            for src, dst in s2d.items():
                # print(f"xfer({var}): {src} -> {dst}")
                hfac[src, ...] = self.abcd(
                    a=hdd[f"{src}_sr_zpeak", ...].view(True),
                    b=hdd[f"{dst}_sr_zpeak", ...].view(True),
                )

            out[var] = hfac

        # export histograms
        self.output().dump(
            hists2dense_evaluator(
                {
                    f"{cat}_{var}": hfac[cat, ...]
                    for var, hfac in out.items()
                    for cat in hfac.axes["category"]
                },
                err_suffix="_error",
            )
        )

    @hist_sn
    def abcd(self, a, b):
        ret = b / a

        # def f(arr):
        #     return ", ".join("%5.3f" % x for x in arr)
        #
        # def pf(label, arr):
        #     n = arr.n
        #     u = arr.u("stat", "up")
        #     print(f"{label}:\t{f(n)}\n\t{f(u)}\n\t{f(u / abs(n))}")
        #
        # pf("", ret)

        return ret


@luigi.util.inherits(DYEstFull)
class PlotDYEstFull(PlotCorrectionsBase):
    version = DYEstFull.version

    def store_parts(self):
        return super().store_parts() + (self.recipe,)

    def requires(self):
        return DYEstFull.req(self)

    def run(self):
        self.output().parent.touch()
        inp = self.input().load()
        for m in tqdm(sorted(k for k in inp.keys() if not k.endswith("_error")), unit="SF"):
            self.plot(inp, self.local_path(), m)
        self.output().touch()
