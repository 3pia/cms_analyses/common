# coding: utf-8

from collections import defaultdict

import law
import luigi
from tqdm.auto import tqdm

from tasks.base import AnalysisCampaignTask, PoolMap
from tasks.group import GroupCoffeaProcesses
from tasks.mixins import ModelMixin, PGroupMixin, RecipeMixin
from utils.util import ulimit


class BinningCalculation(ModelMixin, PGroupMixin, RecipeMixin, AnalysisCampaignTask, PoolMap):
    resources = {"lotsofram": 1}

    def requires(self):
        return GroupCoffeaProcesses.req(self, process_group=self.process_group)

    def output(self):
        return self.local_target("binning.json")

    @property
    def n_parallel_max(self):
        return len(self.analysis_inst.variables)

    @law.decorator.timeit(publish_message=True)
    @law.decorator.safe_output
    def run(self):
        ulimit(AS="hard")

        self.output().parent.touch()
        inp = self.input().load()

        # remove cutflow
        inp.pop("cutflow")
        inp.pop("cutflow_raw", None)

        # do rebinning
        binnings = {}
        for variable, hist in tqdm(inp.items(), desc="calculate binning", unit="variable"):
            variable = self.analysis_inst.variables.get(variable)
            binning = self.StatModel.calculate_binnings(variable, hist, self)
            binnings[variable.name] = binning

        self.output().dump(binnings)


class TrimBinnings(ModelMixin, PGroupMixin, RecipeMixin, AnalysisCampaignTask):
    """
    Trims the binning to the same binning for all years. Enable with '--Rebin-trim-binnings True'
    """

    @property
    def years(self):
        return [str(c.aux["year"]) for c in self.analysis_inst.campaigns]

    def requires(self):
        return {
            year: BinningCalculation.req(
                self,
                process_group=self.process_group,
                year=year,
            )
            for year in self.years
        }

    def output(self):
        return self.local_target("trimmed_binning.json")

    @law.decorator.timeit(publish_message=True)
    @law.decorator.safe_output
    def run(self):
        ulimit(AS="hard")

        self.output().parent.touch()
        # variable -> category -> binning
        inputs = {year: inp.load() for year, inp in self.input().items()}
        this = inputs[self.year]

        nested_dict = lambda: defaultdict(nested_dict)

        # year -> variable -> category -> binning
        binnings_trimmed = nested_dict()
        for variable, d in this.items():
            for category, _ in d.items():
                binnings = [(y, d_[variable][category]) for y, d_ in inputs.items()]
                minl = min(*[len(binning) for _, binning in binnings])
                for year, bins in binnings:
                    if len(bins) > minl:
                        bins = self.StatModel.trim_binning(
                            year=year,
                            variable=variable,
                            category=category,
                            bins=bins,
                            minl=minl,
                            task=self,
                        )
                    binnings_trimmed[year][variable][category] = bins

        # variable -> category -> binning
        self.output().dump(binnings_trimmed[self.year])


class Rebin(ModelMixin, PGroupMixin, RecipeMixin, AnalysisCampaignTask, PoolMap):
    resources = {"lotsofram": 1}
    trim_binnings = luigi.BoolParameter(
        default=False,
        description="Trim binnings for Run 2, requires binning calculation to be done for all years",
    )

    def requires(self):
        return {
            "hists": GroupCoffeaProcesses.req(self, process_group=self.process_group),
            "binnings": (TrimBinnings if self.trim_binnings else BinningCalculation).req(
                self,
                process_group=self.process_group,
            ),
        }

    def output(self):
        return self.local_target("rebinned_hists.coffea")

    @property
    def n_parallel_max(self):
        return len(self.analysis_inst.variables)

    @law.decorator.timeit(publish_message=True)
    @law.decorator.safe_output
    def run(self):
        ulimit(AS="hard")

        self.output().parent.touch()
        binnings = self.input()["binnings"].load()
        hists = self.input()["hists"].load()

        # remove cutflow
        hists.pop("cutflow")
        hists.pop("cutflow_raw", None)

        # do rebinning
        rebinned_hists = {}
        for variable, hist in tqdm(hists.items(), desc="rebin", unit="variable"):
            variable = self.analysis_inst.variables.get(variable)
            rebinned = self.StatModel.rebin(variable, hist, binnings[variable.name], self)
            rebinned_hists[variable.name] = rebinned

        self.output().dump(rebinned_hists)
