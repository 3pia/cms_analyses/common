# coding: utf-8

import os
import re
import shutil
import warnings
from collections import Counter, defaultdict
from contextlib import contextmanager
from fnmatch import filter as fnfilter
from functools import cached_property, partial
from itertools import chain
from operator import itemgetter
from pathlib import Path

import hist
import law
import luigi
import matplotlib
import matplotlib.pyplot as plt
import mplhep as hep
import numpy as np
from cycler import cycler
from luigi import local_target
from matplotlib.legend_handler import HandlerTuple
from matplotlib.patches import Rectangle
from matplotlib.ticker import AutoMinorLocator
from matplotlib.transforms import blended_transform_factory
from tqdm.auto import tqdm

from config.constants import LAMBDA0
from config.rename_nuisance import rename_nuisance
from tasks.base import (
    AnalysisCampaignTask,
    AnalysisTask,
    BaseTask,
    FileProvider,
    FitDistributionsWrapper,
    PoolMap,
)
from tasks.binning import Rebin
from tasks.group import GroupCoffeaProcesses
from tasks.mixins import (
    CategoryMixin,
    CombinedMixin,
    ModelMixin,
    PGroupMixin,
    RecipeMixin,
    VariableMixin,
    CMSLabelMixin,
)
from tasks.neutrino import NeutrinoReconstructionFit1
from utils.bh5 import Histogram as Hist5
from utils.plot import (
    LegendMerger,
    auto_ratio_ylim,
    autoscale,
    axis_margin,
    cms_label,
    legend_fcf,
    lumi_label,
    plot1d,
    plotratio,
)
from utils.sandbox import OpenportsTask
from utils.tf_serve import autoClient
from utils.util import defix, pscan_vispa, ulimit


class PlotBase(AnalysisCampaignTask):
    # cyclers
    scyc = cycler(hatch=["\\" * 3, "." * 3]) * cycler(color=plt.get_cmap("Pastel2").colors)
    lcyc = cycler(linestyle=[(0, (1, 1)), "-.", "--"]) * cycler(color=plt.get_cmap("Set1").colors)
    cms_label_suffix = "Work in progress"

    # opts
    lopt = dict(linestyle="-", linewidth=1.5), lcyc
    popt = (
        dict(
            linestyle="none",
            markersize=10.0,
            color="k",
            elinewidth=1,
        ),
        cycler(marker=".1234Ds*"),
    )
    eopt = dict(
        label="Stat. Unc.",
        hatch="/" * 7,
        facecolor="none",
        edgecolor=(0, 0, 0, 0.5),
        linewidth=0,
    )
    bopt = {}

    ratio_max_delta = None
    ratio_extra_offset_lines = False

    def group_hist(self, hist, plotting_process_groups: dict, ignore=()) -> dict:
        get = self.analysis_inst.processes.get

        assert hist.axes[0].name == "process"
        # planning
        hview = hist.view(True)
        hists = {
            group: Hist5.regrow(hist, dict(process=process_group), copy=False)
            for group, process_group in plotting_process_groups.items()
        }
        unassigned = set()
        undefined = set()
        for process in hist.axes["process"]:
            if process in ignore:
                continue
            if get(process) is None:
                undefined.add(process)
                continue
            for group, process_group in plotting_process_groups.items():
                root = self.getPGroot(get(process), process_group)
                if root is not None:
                    hnew = hists[group]
                    hnew.view(True)[hnew.axes[0].index(root.name)] += hview[
                        hist.axes[0].index(process)
                    ]
                    break
            else:
                unassigned.add(process)
        if (unassigned or undefined) and getattr(self, "n_parallel", 1) == 1:
            warnings.warn(
                "\t\n".join(
                    [
                        "some processes were not assigned or undefined!",
                        f"unassigned: {sorted(unassigned)}",
                        f"grouping: {plotting_process_groups}",
                        f"assigned: {sorted(set(hist.axes['process']) - unassigned - undefined)}",
                        f"undefined in ACI: {sorted(undefined)}",
                    ]
                )
            )

        return hists

    def regularize(self, orig):
        ax = orig.axes[-1]
        if not isinstance(ax, hist.axis.Variable):
            return orig
        if ax.widths.std() < 1e-3:
            return orig
        tr = ax.traits
        if tr.circular or tr.growth:
            return orig

        hnew = hist.Hist(
            *orig.axes[:-1],
            hist.axis.Integer(
                start=0,
                stop=ax.size,
                name=ax.name,
                label=f"{ax.label} bin (a.u.)",
                underflow=tr.underflow,
                overflow=tr.overflow,
            ),
            storage=orig._storage_type(),
        )
        hnew.view(True)[:] = orig.view(True)
        return hnew

    def blind(self, background, signal, data, std_margin=1):
        if not self.unblinded:
            return

        assert background.axes[-1] == signal.axes[-1] == data.axes[-1]

        bkg = np.sum(background.view(True), axis=tuple(range(background.ndim - 1)))
        sig = np.sum(signal.view(True), axis=tuple(range(signal.ndim - 1)))

        # use safety value for empty bins
        if sig0v := sig["value"].sum():
            sig0 = np.full((), sig["variance"].sum() / sig0v, sig.dtype)
            if sig0["value"] > 0:
                sig0["variance"] *= LAMBDA0**0.5
                sig[sig["variance"] <= 0] = sig0

        bkg_eff = np.maximum(bkg["value"] - std_margin * np.sqrt(bkg["variance"]), 0)
        sig_eff = np.maximum(sig["value"] + std_margin * np.sqrt(sig["variance"]), 0)

        mask = bkg_eff < self.blind_thresh * sig_eff
        if not np.any(mask):
            return data

        dview = data.view(True)
        dview["value"][..., mask] = np.nan
        dview["variance"][..., mask] = 0

        # we can inject the "Data blinded" process, with x-markers
        if (
            set(data.axes[1:-1].extent) <= {1}
            and data.axes[0].name == "process"
            and "data_blind" not in data.axes[0]
        ):
            blind = Hist5.regrow(data, dict(process=["data_blind"]), copy=False)

            bview = blind.view(True)
            bview["value"] = np.where(mask, bkg["value"], np.nan)
            bview["variance"] = 0

            data = data + blind
            data.axes["process"].labels = dict(data_blind="")
            data.axes["process"].styles = dict(
                data_blind=dict(marker="x", elinewidth=0, markersize=5)
            )

        return data

    def plot(
        self,
        targets,
        stack=None,
        lines=None,
        points=None,
        ratio=None,
        hstack_ratio=None,
        hstack_labels=None,
        hstack_legend_slice=-1,
        legend_fraction=0.2,
        x_label="",
        sup_title="",
        legend_title="",
        lumi_prefix_label="",
    ):

        ahist = dict(stack=stack, lines=lines, points=points, ratio=ratio)
        del stack, lines, points, ratio
        nhist = set(len(h) for h in ahist.values() if isinstance(h, (list, tuple)))
        if nhist:
            nhist -= {1}
            if nhist:
                assert len(nhist) == 1, f"inconsistent number histograms: {nhist}"
                (nhist,) = nhist
                for k, h in ahist.items():
                    if not isinstance(h, (list, tuple)):
                        h = [h]
                    if len(h) == 1:
                        ahist[k] = h * nhist
                nbins = [
                    set(
                        harr[i].axes[-1].size
                        for harr in ahist.values()
                        if isinstance(harr[i], hist.Hist)
                    )
                    for i in range(nhist)
                ]
                if max(map(len, nbins)) > 1:
                    raise RuntimeError(f"inconsistent number of bins: {nbins}")
                if hstack_ratio == "bins":
                    hstack_ratio = [list(nb)[0] for nb in nbins]
            else:
                nhist = None
                for k, h in ahist.items():
                    if isinstance(h, (list, tuple)):
                        ahist[k] = h[0]

        has_ratio = (
            any(r is not None for r in ahist["ratio"])
            if isinstance(ahist["ratio"], (list, tuple))
            else (ahist["ratio"] is not None)
        )

        if not nhist:
            hstack_ratio = None

        fig, aax = plt.subplots(
            2 if has_ratio else 1,
            nhist or 1,
            squeeze=False,
            figsize=(8, 6),
            sharex="col",
            sharey="row",
            gridspec_kw=dict(
                left=0.1,
                top=0.9,
                bottom=0.1,
                right=0.95,
                height_ratios=(3, 1) if has_ratio else None,
                width_ratios=hstack_ratio,
                hspace=0.04,
                wspace=0,
            ),
        )

        for ax, hd in zip(aax.T, cycler(**ahist)) if nhist else [(aax[:, 0], ahist)]:
            rat = hd.pop("ratio")
            ropt = self.popt
            if rat is not None:
                if isinstance(rat, str):
                    if rat == "lines":
                        rat = hd["lines"]
                        ropt = self.lopt
                    elif rat == "points":
                        rat = hd["points"]
            self._plot(*ax, **hd, ratio=rat, ropt=ropt)

        # autoscaleing
        for ax in aax[-1]:  # last row
            ax.autoscale(axis="x", tight=True)
        autoscale(aax[0], skip="Unc", margins=dict(y=0.05, tight=False), use_sticky_edges=False)

        if has_ratio:
            auto_ratio_ylim(aax[-1], ratio_max_delta=self.ratio_max_delta)

        # axis ticks
        for ax in aax.flat:  # all
            # ax.xaxis.set_minor_locator(AutoMinorLocator())
            ax.xaxis.set_ticks_position("both")
            ax.yaxis.set_label_position("left")
            ax.tick_params(which="both", direction="in")
            ax.yaxis.set_offset_position("left")
            ax.yaxis.offsetText.set_ha("right")
            # align offset text with CMS label (magic, no idea why this works)
            ax.yaxis.OFFSETTEXTPAD = 3 + 2 * (ax.yaxis.figure.dpi / 72)
        for col, cax in enumerate(aax.T):
            # where do we want ticks?
            if nhist:
                if col == 0:
                    pos = "left"
                elif col == nhist - 1:
                    pos = "right"
                else:
                    pos = "none"
            else:
                pos = "both"
            for ax in cax:
                ax.yaxis.set_ticks_position(pos)
            if col:
                cax[-1].xaxis.get_major_locator().set_params(prune="lower")

        # axis tick labels
        for ax in aax[:, 1:][:, -1:].flat:  # last column
            for label in ax.yaxis.get_ticklabels():
                label.set_visible(False)

        # remove "major tick"-limit of 9 for log axes
        for ax in aax[0] if self.log_scale else ():
            yax = aax[0, 0].yaxis
            nt = yax.get_tick_space()
            yax.get_major_locator().numticks = nt
            yax.get_minor_locator().numticks = nt

        # axis labels
        for ax in aax.flat[:-1]:  # all, except bottom right
            ax.xaxis.label.set_visible(False)
        for ax in aax[:, 1:].flat:  # all but the left column
            ax.yaxis.label.set_visible(False)
        # aax[0, 0].yaxis.set_label_position("right")  # upper left
        aax[0, 0].set_ylabel(aax[0, 0].get_ylabel(), loc="top")
        aax[-1, -1].set_xlabel(x_label, loc="right")  # bottom right

        if hstack_labels:
            for ax, label in zip(aax[0], defix(hstack_labels)):
                ax.text(
                    np.mean(ax.get_xlim()),
                    ax.get_ylim()[-1],
                    label,
                    horizontalalignment="center",
                    verticalalignment="bottom",
                )
            axis_margin(aax[0, 0], y=0.05)

        lm = LegendMerger()
        for i, axs in enumerate(aax):
            for ax in axs:
                lm.add(ax, order=i)
        lm.reposition(lambda label: "\n" in label, 1e10)

        axis_margin(aax[0, 0], y=legend_fraction)
        legend_fcf(
            legend := fig.legend(
                *lm.result(hstack_legend_slice, reversed=True),
                mode="expand",
                ncol=5,
                title=legend_title,
                title_fontsize="large",
                fancybox=False,
                loc="upper left",
                # not exact number since otherwise axis-ticks are slightly visible
                bbox_to_anchor=(0.0995, 1.001 - legend_fraction, 0.85125, legend_fraction),
                bbox_transform=blended_transform_factory(fig.transFigure, aax[0, 0].transAxes),
                borderaxespad=0.1,
                borderpad=0.4,
                labelspacing=0.1,
                framealpha=1,
                edgecolor="white",
                handler_map={tuple: HandlerTuple(ndivide=None, pad=0)},
            )
        )
        if legend_title:
            legend._legend_box.sep = 10

        cms_label(aax[0, 0], label=self.cms_label_suffix, data=self.unblinded)
        lumi_label(
            aax[0, -1],
            data=self.unblinded,
            campaign=self.campaign_inst,
            lumi_prefix_label=lumi_prefix_label,
            year="",
        )

        if sup_title:
            fig.suptitle(sup_title)

        # save them
        for target in targets:
            target.parent.touch()
            fig.savefig(target.path)

        plt.close(fig)

    def _plot(self, ax, rax=None, *, stack=None, lines=None, points=None, ratio=None, ropt={}):
        # main plots
        spo = self.plotopts(stack, order=sum, fill_opts=({}, self.scyc))
        self.plot1d(ax=ax, stack=True, error_opts=self.eopt, **spo)
        self.plot1d(ax=ax, **self.plotopts(lines, order="label", line_opts=self.lopt))
        self.plot1d(ax=ax, **self.plotopts(points, order="label", error_opts=self.popt))

        if ratio is not None and stack is not None:
            overlay = spo["overlay"]
            denom = stack[{overlay: slice(None, None, sum)}]
            offset = denom.copy()
            offset.view(True)["variance"][...] = 0
            if self.bopt is None:
                dfopt = None
            else:
                if self.eopt is None:
                    dfopt = self.bopt
                else:
                    dfopt = dict(self.bopt, label=self.eopt.get("label", ""))

            for src, do_offset, (defaults, cyc) in [
                (lines if self.ratio_extra_offset_lines else None, True, self.lopt),
                (ratio, False, ropt),
            ]:
                if src is None:
                    continue
                cyciter = iter(cyc)
                axis = src.axes[overlay]
                for r in axis:
                    style = self.get_style(axis.name, r, axis)
                    if style is None:
                        style = next(cyciter)
                    style = dict(defaults, **style)
                    nom = src[{overlay: r}]
                    if do_offset:
                        nom = nom + offset
                    is_last = r == axis[-1] and (ratio is None or ratio is src)
                    plotratio(
                        nom,
                        denom,
                        ax=rax,
                        error_opts=style,
                        denom_fill_opts=dfopt if is_last else None,
                        guide_opts={},
                        unc="scale",
                        clear=False,
                    )

    def plotopts(self, hist, overlay=None, order=None, **kwargs) -> dict:
        if hist is None:
            return dict(h=None)
        if overlay is None:
            overlay = hist.axes[0].name or 0
        ret = dict(h=hist, overlay=overlay)
        ax = hist.axes[overlay]
        if not ax.extent:
            return dict(h=None)
        # labels
        if overlay == "process":
            signal = self.analysis_inst.processes.get(self.analysis_inst.aux["signal_process"])
            labels = {
                key: key
                if process is None
                else (
                    process.parent_processes.values[0]
                    if process is signal and process.parent_processes
                    else process
                ).label_short
                for key, process in ((p, self.analysis_inst.processes.get(p)) for p in ax)
            }
        else:
            labels = {s: s for s in list(ax)}
        labels.update(getattr(ax, "labels", {}))
        if labels:
            ret["legend_labels"] = labels
        # ordering
        if order is sum:
            sax = list(range(hist.ndim))
            del sax[tuple(hist.axes).index(ax)]
            lut = dict(zip(ax, hist.view(True)["value"].sum(axis=tuple(sax)).tolist()))
        elif order == "label":
            assert labels
            lut = labels
        else:
            lut = None
        if lut:
            ret["order"] = sorted(ax, key=lut.get)
        # build styles
        for key, (defaults, cyc) in kwargs.items():
            by_key = {}
            cyciter = iter(cyc)
            for i, item in enumerate(ret["order"] if lut else ax):
                style = self.get_style(overlay, item, ax)
                if style is None:
                    style = next(cyciter)
                style = dict(defaults, **style)
                for k, v in style.items():
                    if k not in by_key:
                        by_key[k] = ["k" if k == "color" else ""] * i
                    by_key[k].append(v)
            ret[key] = by_key

        return ret

    def get_style(self, kind, key, axis):
        if hasattr(axis, "styles"):
            styles = axis.styles
            if isinstance(styles, (tuple, list)):
                style = styles[axis.index(key)]
            else:
                style = styles.get(key)
            if style is not None:
                return style
        if kind == "process":
            p0 = self.analysis_inst.processes.get(key)
            for _, p in p0.walk_parent_processes(include_self=True):
                style = p.aux.get("style") or {}
                if any(p.color):
                    style["color"] = p.color
                if style:
                    return style
        elif kind == "category":
            cat = self.analysis_inst.categories.get(key, None)
            return cat and cat.aux.get("style", None)

    def plot1d(self, h, **kwargs):
        if h is None:
            return
        if not all(ax.extent for ax in h.axes):
            return

        sli = [ax[0] if ax.extent == 1 else slice(None) for ax in h.axes]
        sli[tuple(h.axes).index(h.axes[kwargs["overlay"]])] = slice(None)
        h = h[tuple(sli)]

        assert len(h.axes) == 2

        plot1d(
            h,
            overflow=False,
            binwnorm=None,
            clear=False,
            log=self.log_scale,
            legend_opts=None,
            **kwargs,
        )

    @staticmethod
    @contextmanager
    def plot_silencer(extra=(), active=True):
        with warnings.catch_warnings():
            if active:
                warnings.filterwarnings(
                    "ignore",
                    message="|".join(
                        [
                            r".*converting a masked element to nan",
                            r"invalid value encountered",
                            r"divide by zero encountered",
                            r"All sumw are zero!  Cannot compute meaningful error bars",
                            r"Data has no positive values, and therefore cannot be log-scaled.",
                        ]
                        + list(extra)
                    ),
                )
            yield


class CutflowPlot(PGroupMixin, RecipeMixin, AnalysisCampaignTask):
    def requires(self):
        return GroupCoffeaProcesses.req(self)

    def output(self):
        return self.local_target(".done")

    def outcoll(self):
        return {c: self.local_target(f"{c}.pdf") for c in self.categories}

    @property
    def categories(self):
        return [*self.cutflow.axes["category"]]

    @law.decorator.timeit(publish_message=True)
    @law.decorator.safe_output
    def run(self):
        f, ax = plt.subplots()
        self.cutflow = self.input().load()["cutflow"]

        output = self.outcoll()
        for c in tqdm(self.categories, unit="category"):
            h = self.cutflow[:, c, :]
            cat = self.analysis_inst.categories.get(c)
            legend_labels = {}
            for p in h.axes["process"]:
                if p == self.analysis_inst.aux["signal_process"]:
                    # get first
                    l = self.analysis_inst.processes.get(p).parent_processes.values[0].label_short
                else:
                    l = self.analysis_inst.processes.get(p).label_short
                legend_labels[p] = l

            ax = plot1d(
                h,
                overlay="process",
                stack=False,
                overflow=False,
                ax=ax,
                clear=True,
                legend_labels=legend_labels,
                legend_title=cat.label,
                log=True,
            )
            hep.cms.label(
                llabel="Private Work",
                year=self.year,
                lumi=np.round(self.campaign_inst.aux["lumi"] / 1000.0, 2),
                loc=0,
                ax=ax,
                fontname="sans",
            )
            plt.tight_layout()
            output[c].parent.touch()
            plt.savefig(output[c].path)
            ax.cla()

        self.output().touch()
        self.logger.info(pscan_vispa(directory=self.local_path(), regex=r"(?P<Category>.+).pdf"))


class PlotHistsBase(ModelMixin, PGroupMixin, RecipeMixin, PlotBase):
    log_scale = luigi.BoolParameter()
    blind_thresh = luigi.FloatParameter(default=np.inf)
    format = law.CSVParameter(default=["pdf", "png"])
    group_ignore = ()

    @property
    def unblinded(self):
        return self.blind_thresh < np.inf

    @cached_property
    def basename(self):
        if self.unblinded:
            b = f"{self.blind_thresh:e}"
            b = re.sub(r"\.?0+([Ee])", r"\1", b)
            b = re.sub(r"([Ee])(?:\+|(-))0*", r"\1\2", b)
        else:
            b = "blind"

        return f"{b}_{'log' if self.log_scale else 'lin'}"

    def output(self):
        return self.local_target(f".{self.basename}_done")

    @cached_property
    def plotting_process_groups(self):
        groups = dict(background=[], signal=[], data=[])
        signal = self.analysis_inst.processes.get(self.analysis_inst.aux["signal_process"])
        data = self.analysis_inst.processes.get("data")
        for n in self.legacy_processes:
            p = self.analysis_inst.processes.get(n)
            if p is None:
                raise ValueError(f"unknown process: {n!r}")
            # if process.is_data: # data driven processes are "data" flagged
            if data.has_process(p) or data is p:
                groups["data"].append(p.name)
            elif signal.has_process(p) or signal is p:
                groups["signal"].append(p.name)
            else:
                groups["background"].append(p.name)
        return groups

    def collate(self, work, collate, ctx_filter="", skip_single=True):
        # collate stuff
        cc2d = defaultdict(dict)
        for ctx, cat, d in work:
            cc2d[ctx][cat] = d
        new_work = []
        for ctx, c2d in cc2d.items():
            if not re.search(ctx_filter, ctx):
                continue
            for search, replace in collate.items():
                if isinstance(replace, tuple):
                    replace, custom = replace
                else:
                    custom = replace
                if isinstance(search, str):
                    search = (search,)

                names = {}
                coll = defaultdict(dict)
                for cat, d in c2d.items():
                    for idx, needle in enumerate(search):
                        recat = re.sub(needle, replace, cat)
                        if recat != cat:
                            coll[recat][idx] = (cat, d)
                            name = re.sub(needle, custom, cat)
                            assert names.setdefault(recat, name) == name
                            break
                coll = {
                    recat: list(map(itemgetter(1), sorted(data.items(), key=itemgetter(0))))
                    for recat, data in coll.items()
                }
                new_work.extend(
                    (ctx, (recat, names[recat]), data)
                    for recat, data in coll.items()
                    if not skip_single or len(data) > 1
                )
        return new_work

    def worker(self, cch):
        ctx, cat, data = cch

        if isinstance(data, list):
            cat, name = cat
            titles, hists = zip(*(self.unpack_data(ctx, *d) for d in data))
        else:
            name = cat
            titles = None
            hists = [self.unpack_data(ctx, cat, data)[1]]

        x_label_orig = hists[0].axes[-1].label
        hists = [self.regularize(hist) for hist in hists]
        x_label = hists[0].axes[-1].label

        ghist = defaultdict(list)
        for h in hists:
            gh = self.group_hist(h, self.plotting_process_groups, ignore=self.group_ignore)
            gh["ratio"] = d = self.blind(**gh)

            if d and "data_blind" in d.axes[0]:
                d = d.copy()
                d.view(True)[d.axes[0].index("data_blind"), ...]["value"] = np.nan

            gh["data"] = d

            for k, v in gh.items():
                ghist[k].append(v)

        with self.plot_silencer():
            self.plot(
                targets=[self.local_target(ctx, name, f"{self.basename}.{f}") for f in self.format],
                stack=ghist["background"],
                lines=ghist["signal"],
                points=ghist["data"],
                ratio=ghist["ratio"],
                x_label=x_label,
                sup_title=self.get_title(ctx, cat),
                hstack_labels=titles,
                hstack_ratio="bins" if x_label_orig != x_label else None,
            )


class PlotFakeNC(PlotHistsBase):
    def requires(self):
        from tasks.corrections.fake import FakeNonClosureCorrection

        req = {"rebin": Rebin.req(self, process_group=["tt_fh"])}
        if self.analysis_choice == "bbww_sl":
            req["fake"] = FakeNonClosureCorrection.req(self, process_group=["tt_fh"])

        return req

    def output(self):
        return self.local_target(".done")

    def run(self):
        hists = self.input()["rebin"].load()["dnn_score_max"]
        sf = self.input()["fake"].load(allow_pickle=True)[()]["dnn_score_max"]

        cats = set()
        for cat in hists.keys():
            c = re.search(f"^(.+)_(.+)_[sf]r_nonprompt(.*)$", cat)
            if c is not None:
                cats.add(c.groups())

        self.output().parent.touch()
        for _ in cats:
            flavour, cat, proc = _

            mc_fakes = hists[f"{flavour}_{cat}_sr_nonprompt{proc}"][:, "nominal", :]
            mc_closure = hists[f"{flavour}_{cat}_fr_nonprompt{proc}"][:, "nominal", :]

            mc_fakes.axes["process"].labels = {"tt_fh": "mc_fakes"}
            mc_closure.axes["process"].labels = {"tt_fh": "mc_closure"}
            mc_fakes.axes["process"].styles = [dict(color="gray")]
            mc_closure.axes["process"].styles = [dict(color="red")]
            mc_closure_ratio = mc_closure.copy()
            mc_closure_ratio.axes["process"].styles = [dict(color="red", marker="+")]

            try:
                fit_vals = sf[f"{flavour}_{cat}_sr_prompt{proc}"]
                description = f"cog/nom/slope: {fit_vals['cog']:.2f}/{fit_vals['nom']:.2f}/{fit_vals['slope']:.2f}"
            except:
                description = ""
            with self.plot_silencer():
                self.plot(
                    targets=[self.local_target(f"{flavour}_{cat}{proc}.{f}") for f in self.format],
                    stack=mc_fakes,
                    lines=mc_closure,
                    ratio=mc_closure_ratio,
                    legend_title=f"{flavour}_{cat}{proc}\n{description}",
                )
        self.output().touch()


class PlotProducer(PlotHistsBase, PoolMap):
    n_parallel_max = 20
    # include signal (lines) in ratio (with offset, i.e. ontop of total bg)
    ratio_extra_offset_lines = True

    def requires(self):
        return self.StatModel.requires(self)

    @law.decorator.timeit(publish_message=True)
    @law.decorator.safe_output
    def run(self):
        from utils.util import ulimit

        ulimit(AS="hard")

        if 0 <= self.blind_thresh < 1e3:
            warnings.warn("Plot will be aggressively unblinded!")

        work = [
            (
                var,
                cat,
                hist[dict(systematic="nominal")] if "systematic" in hist.axes.name else hist,
            )
            for var, data in self.input().load().items()
            if var != "cutflow"
            for cat, hist in data.items()
            if cat in self.analysis_inst.categories.keys
        ]

        work += self.collate(
            work,
            self.analysis_inst.aux["collate_cats"],
            self.analysis_inst.aux.get("collate_vars", ""),
        )

        # touch cached_property
        self.basename
        self.plotting_process_groups

        for _ in self.pmap(self.worker, work, unit="plot", unordered=True):
            pass

        self.output().touch()
        self.logger.info(
            pscan_vispa(
                directory=self.local_path(),
                regex=r"(?P<variable>.+)/(?P<channel>[^_]+)_(?P<category>.+)/(?P<blind_thresh>.+)_(?P<scale>.+)\.(?P<format>[^\.]+)",
            )
        )

    def unpack_data(self, var, cat, hist):
        hist.axes[-1].label = self.analysis_inst.variables.get(var).x_title
        title = self.analysis_inst.categories.get(cat).label_short
        return title, hist

    def get_title(self, ctx, cat):
        return self.analysis_inst.categories.get(cat).label_short


class PlotSysts(
    CMSLabelMixin, VariableMixin, CategoryMixin, PlotHistsBase, AnalysisCampaignTask, PoolMap
):
    n_parallel_max = 8
    systematics = law.CSVParameter(["*"])
    processes = law.CSVParameter(["*"])

    @cached_property
    def rnuisances(self):
        return self.StatModel.get_rnuisances(self)

    def rename_syst(self, nuisance):
        nuisance = self.rnuisances[nuisance]
        nuisance = rename_nuisance(nuisance)
        return nuisance

    def worker(self, cch):
        process, systematic, hists = cch

        syst_label = self.rename_syst(systematic)
        syst_up = f"{systematic}Up"
        syst_down = f"{systematic}Down"

        nom, shifts = [self.regularize(hist) for hist in hists]

        yield_nom = nom["nominal", :].sum()["value"]
        yield_up = shifts[syst_up, :].sum()["value"]
        yield_down = shifts[syst_down, :].sum()["value"]
        shift_up = (yield_up - yield_nom) / yield_nom
        shift_down = (yield_down - yield_nom) / yield_nom

        # override some styles
        nom.axes["systematic"].styles = [
            dict(edgecolor="black", facecolor=(0, 0, 0, 0.0)),
        ]
        shifts.axes["systematic"].labels = {
            syst_up: f"Up ({shift_up * 100:.1f}%)",
            syst_down: f"Down ({shift_down * 100:.1f}%)",
        }
        shifts.axes["systematic"].styles = [
            dict(color="red"),
            dict(color="green"),
        ]

        # do not plot statistical uncs. for systematic shifts
        self.eopt = None
        nom.view()["variance"] = 0
        shifts.view()["variance"] = 0
        cat = self.analysis_inst.categories.get(self.category)

        with self.plot_silencer():
            self.plot(
                targets=[
                    self.local_target(process, systematic, f"{self.basename}.{f}")
                    for f in self.format
                ],
                stack=nom,
                lines=shifts,
                ratio="lines",
                x_label=nom.axes[-1].label,
                legend_title=f"{cat.label}: {syst_label}",
            )

    def requires(self):
        return self.StatModel.requires(self)

    @property
    def everything(self):
        return "*" in self.systematics and "*" in self.processes

    def output(self):
        if self.everything or not self.input().exists():
            return self.local_target(".done")
        return {
            proc: {
                syst: {
                    fmt: self.local_target(proc, syst, f"{self.basename}.{fmt}")
                    for fmt in self.format
                }
                for syst in self.systs
            }
            for proc in self.used_procs
        }

    @property
    def systs(self):
        ret = set([s.replace("Up", "").replace("Down", "") for s in self.hist.axes["systematic"]])
        ret -= {"nominal"}
        return set().union(*(fnfilter(ret, syst) for syst in self.systematics))

    @property
    def available_procs(self):
        return set([*self.hist.axes["process"]])

    @property
    def used_procs(self):
        ret = self.available_procs - {"data"}
        procs = set().union(*(fnfilter(ret, proc) for proc in self.processes))
        if "sum_background" in self.processes:
            procs.add("sum_background")
        return procs

    @cached_property
    def hist(self):
        data = self.input().load()
        try:
            data = data[self.var]
        except KeyError as e:
            raise KeyError(f"variable {self.var} not in {sorted(data.keys())}") from e
        try:
            data = data[self.category]
        except KeyError as e:
            raise KeyError(f"category {self.category} not in {sorted(data.keys())}") from e
        return data

    def run(self):
        hist = self.hist
        work = []

        available_procs = self.available_procs
        used_procs = self.used_procs
        background_procs = (
            set(self.StatModel.background_processes) if "sum_background" in used_procs else set()
        )

        for syst in self.systs:
            bgs_nominal = bgs_shifts = None
            for proc in available_procs:
                if not proc in (background_procs | used_procs):
                    continue

                up, down = syst + "Up", syst + "Down"

                nom = hist.axes[1].index("nominal")
                up = hist.axes[1].index(up)
                down = hist.axes[1].index(down)

                nominal = hist[proc, nom : nom + 1, :]
                shifts = hist[proc, up : up + 1, :] + hist[proc, down : down + 1, :]

                if proc in used_procs:
                    work.append((proc, syst, [nominal, shifts]))

                if proc in background_procs:
                    if bgs_nominal is None:
                        bgs_nominal = nominal.copy()
                        bgs_shifts = shifts.copy()
                    else:
                        bgs_nominal += nominal
                        if not (np.any(shifts.values()) or np.any(shifts.variances())):
                            nominal_shifts = shifts.copy()
                            nominal_shifts_view = nominal_shifts.view(True)
                            nominal_shifts_view += nominal.view(True)
                            bgs_shifts += nominal_shifts
                        else:
                            bgs_shifts += shifts

            if background_procs:
                work.append(("sum_background", syst, [bgs_nominal, bgs_shifts]))

        # touch cached_property
        self.basename
        for _ in self.pmap(self.worker, work, unit="plot", unordered=True):
            pass

        self.logger.info(
            pscan_vispa(
                directory=self.local_path(),
                regex=r"(?P<Process>.+)/(?P<Systematic>.+)/(?P<blind_thresh>.+)_(?P<scale>.+)\.(?P<format>[^\.]+)",
            )
        )
        if self.everything:
            self.output().touch()


class PlotSystsWrapper(ModelMixin, RecipeMixin, FitDistributionsWrapper):
    log_scale = PlotHistsBase.log_scale
    blind_thresh = PlotHistsBase.blind_thresh
    format = PlotHistsBase.format
    systematics = PlotSysts.systematics
    processes = PlotSysts.processes
    _task = PlotSysts


class CheckSysts(VariableMixin, CategoryMixin, ModelMixin, RecipeMixin, AnalysisCampaignTask):
    """
    This task creates a html table which can be used to investigate systematic shifts.
    It marks one-sided systematics according if one of two triggers fires.
    The triggers are:
        1. At least one bin with a one-sided difference greater than thresh_single (default: 10%)
        2. At least a percentage of thresh_intregral (default: 20%) of the hist is one-sided
    By hovering over the table, the html shows the original plots.
    """

    triggers = ["single", "amount"]
    thresh_single = luigi.FloatParameter(
        default=0.1, description="Threshold for one-sided discrepany in single bins"
    )
    thresh_integral = luigi.FloatParameter(
        default=0.2, description="Threshold for one-sided percentage of whole hist"
    )

    def html(self, thead, trs):
        return f"""
            <!DOCTYPE html>
            <html>
            <head>
            <style>
            table {{
                table-layout: fixed;
            }}

            thead th {{
                position: -webkit-sticky;
                position: sticky;
                top: 0;
            }}

            tbody th {{
                position: -webkit-sticky;
                position: sticky;
                left: 0;
            }}

            th.rotate {{
                height: 140px;
                white-space: nowrap;
            }}

            th.rotate>div {{
                transform:
                    translate(25px, 51px)
                    rotate(315deg);
                width: 30px;
            }}

            th.rotate>div>span {{
                border-bottom: 1px solid #ccc;
                padding: 5px 10px;
            }}

            th>div>span {{
                background: white;
            }}

            .hover-title {{
                display: inline;
                pointer-events: auto;
                cursor: pointer;
            }}

            .hover-image {{
                visibility: hidden;
            }}

            body:not(.mobile) .hover-title:hover + .hover-image {{
                visibility: visible;
                pointer-events: none;
            }}

            .hover-image {{
                display: flex;
                position: fixed;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
                pointer-events: none;
                flex-direction: column;
                align-items: center;
                justify-content: center;
                width: 90vw;
                height: 90vh;
            }}

            .hover-image img {{
                max-width: 100% !important;
                max-height: 100% !important;
                width: auto !important;
                height: auto !important;
                margin-bottom: 0;
            }}
            </style>
            </head>
            <body>

            <table>
            <thead>
                {thead}
            </thead>
            <tbody>
                {"".join(trs)}
            </tbody>
            </table>
            </body>
            </html>
        """

    def thead(self, procs, title=""):
        out = f"""<tr><td>{title}</td>{"".join(self.th(proc) for proc in procs)}</tr>"""
        return out

    def th(self, proc):
        return f"""<th class="rotate"><div><span>{proc}</span></div></th>"""

    def tr(self, values, paths, title=""):
        return f"""<tr><th><div><span>{title}</span></div></th>{"".join(self.td(value, path) for (value, path) in zip(values, paths))}</tr>"""

    def coloring(self, *args):
        if all(arg == True for arg in args):
            color = "red"
        elif any(arg == True for arg in args):
            color = "yellow"
        else:
            color = "lightgreen"
        return "background-color: %s;" % color

    def td(self, value, path):
        val1, val2 = value
        return f"""
            <td style="{self.coloring(val1>0, val2>self.thresh_integral)}">
            <div class="hover-title">{val1} / {int(100*val2)}%</div>
            <div class="hover-image">
            <img src="{path}"/>
            </div>
            </td>
        """

    def requires(self):
        return {"data": self.StatModel.requires(self), "plots": PlotSysts.req(self, format=["jpg"])}

    def output(self):
        return {
            "html": self.local_target("systs.html"),
            "plots": self.local_directory_target("plots"),
            "pkl": self.local_target("systs.pkl"),
        }

    @property
    def systs(self):
        return set([s.replace("Up", "").replace("Down", "") for s in self.h.axes["systematic"]]) - {"nominal"}  # fmt: skip

    @property
    def procs(self):
        return set([*self.h.axes["process"]]) - {"data"}

    @cached_property
    def inp(self):
        return self.input()["data"].load()[self.var]

    @property
    def h(self):
        return self.inp[self.category]

    def run(self):
        import pandas as pd

        df = pd.DataFrame(
            columns=[f"{proc}" for proc in self.procs],
            index=self.systs,
        )

        items = set(
            [
                (
                    p,
                    self.category,
                    s.replace("Up", "").replace("Down", ""),
                )
                for p in self.procs
                for s in self.systs
            ]
        )
        for (process, cat, syst) in tqdm(items):
            h = self.h[process, ...]
            dir_u = "Up"
            dir_d = "Down"
            nominal = h["nominal", :].view()["value"]
            up = h[syst + dir_u, :].view()["value"]
            down = h[syst + dir_d, :].view()["value"]

            is_shifted = (up != nominal) | (down != nominal)
            one_sided = is_shifted & (np.sign(up - nominal) == np.sign(down - nominal))

            # single one-sided bins
            def abs_rel_err(nominal, shift):
                nom, denom = nominal - shift, nominal
                val = np.divide(nom, denom, out=np.zeros_like(nom), where=denom != 0)
                return np.abs(val)

            discr_up = abs_rel_err(nominal, up)
            discr_down = abs_rel_err(nominal, down)
            one_sided_thresh = np.sum(
                (discr_up > self.thresh_single) & (discr_down > self.thresh_single) & one_sided
            )

            # percentage of one-sided distribution
            e = h["nominal", :].axes[0].edges
            sizes = e[1:] - e[:-1]
            total_one_sided = np.sum(sizes * one_sided)
            total = np.max(e) - np.min(e)
            percentage_one_sided = total_one_sided / total
            df.loc[syst, f"{process}"] = (one_sided_thresh, percentage_one_sided)

        df.sort_index(inplace=True)
        trs = []
        for label, value in df.iterrows():
            trs.append(
                self.tr(
                    *zip(
                        *list(
                            (it, f"plots/{val}/{label}/blind_lin.jpg") for val, it in value.items()
                        )
                    ),
                    title=label,
                )
            )
        html = self.html(self.thead(self.procs, title="Systematic"), trs)
        self.output()["html"].parent.touch()
        with open(self.output()["html"].path, "w") as f:
            f.write(html)

        shutil.copytree(
            self.input()["plots"].parent.path, self.output()["plots"].path, dirs_exist_ok=True
        )
        df.to_pickle(self.output()["pkl"].path)


class CheckSystsWrapper(ModelMixin, RecipeMixin, FitDistributionsWrapper):
    _task = CheckSysts


class CheckSystsCombined(ModelMixin, RecipeMixin, CombinedMixin, AnalysisTask, law.WrapperTask):
    def requires(self):
        return {
            year: CheckSystsWrapper.req(self, year=year) for year in self.years if year != "run2"
        }


class NeutrinoClient(BaseTask):
    def output(self):
        return self.local_target("client.pkl")

    def run(self):
        """
        Convenience function to start server with neutrino model on a machine.
        Steps:
        - Open server and client
        - Save client to output
        - Wait (embed)
        - (do executions in other task)
        - Delete client
        """

        client = autoClient(
            "/net/scratch/cms/dihiggs/store/NeutrinoReconstructionFit1/prod27/optimizer_Adam/batch_size_100000/model",
            remote=False,
            sargs=dict(exe="/net/scratch/BFischer/tfsrv/gpu_2.6.0/tensorflow_model_server"),
        )
        self.output().dump(client)
        from IPython import embed; embed()  # fmt: skip
        self.output().remove()


class EvalNeutrinoTTH(OpenportsTask, BaseTask):
    n_events = luigi.IntParameter(default=1_000_000, description="Size of data to evaluate on.")
    place_of_execution = luigi.ChoiceParameter(choices=["local", "remote"], default="local")
    device = luigi.ChoiceParameter(choices=["gpu-cold", "gpu-hot", "cpu"], default="gpu-cold")

    optimizer = luigi.ChoiceParameter(choices=["SGD", "Adam", "Adadelta", "RMSprop"], default="SGD")
    batch_size = luigi.IntParameter(default=100_000, description="Batch size of evaluation.")

    def requires(self):
        from tasks.neutrino import NeutrinoReconstructionFit1

        return {
            "reco": NeutrinoReconstructionFit1.req(
                self, optimizer=self.optimizer, batch_size=self.batch_size
            ),
            "data": FileProvider(filepath="/net/scratch/cms/data/lbn_nu/data_ttH.npy"),
        }

    def store_parts(self):
        return super().store_parts() + (
            f"n_events_{self.n_events}",
            f"executed_{self.place_of_execution}ly",
            f"on_{self.device}",
            f"optimizer_{self.optimizer}",
            f"batch_size_{self.batch_size}",
        )

    @cached_property
    def dnn_predict(self):
        if self.place_of_execution == "local":
            if self.device == "cpu":
                os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
            elif self.device == "gpu-cold":
                os.environ["CUDA_VISIBLE_DEVICES"] = "0"
            else:
                raise NotImplementedError
            import tensorflow as tf

            model = tf.keras.models.load_model(self.input()["reco"].parent.path)
            return lambda x: model.predict(x, batch_size=self.batch_size)
        elif self.place_of_execution == "remote":
            if self.device == "gpu-hot":
                import pickle

                # needs started and warm server, please use NeutrinoClient Task on vispa-worker01
                client = pickle.load(
                    open("/net/scratch/cms/dihiggs/store/NeutrinoClient/prod27/client.pkl", "rb")
                )
            elif self.device == "gpu-cold":
                client = autoClient(
                    self.input()["reco"].parent.path,
                    remote=True,
                    sargs=dict(exe="/net/scratch/BFischer/tfsrv/gpu_2.6.0/tensorflow_model_server"),
                )
                self.server = client.server
            else:
                raise NotImplementedError
            return lambda x: client.batched(x, batch_size=self.batch_size)
        else:
            raise NotImplementedError

    @property
    def raw_data(self):
        return self.input()["data"].load()[: self.n_events]

    @property
    def data(self):
        ulimit(AS="hard")
        _raw_data = self.raw_data

        _data = np.concatenate(
            [
                _raw_data["lep_E"][..., None],
                _raw_data["lep_Px"][..., None],
                _raw_data["lep_Py"][..., None],
                _raw_data["lep_Pz"][..., None],
                _raw_data["met_Px"][..., None],
                _raw_data["met_Py"][..., None],
            ],
            axis=-1,
        )

        if self.n_events > 1_000_000:
            assert self.n_events % 1_000_000 == 0, "For large values only support multiples of 1M"
            scale_up = int(self.n_events / 1_000_000)
            _data = np.concatenate([_data[:1_000_000] for i in range(scale_up)], axis=0)
        return _data


class PlotNeutrinoTTH(EvalNeutrinoTTH, PlotHistsBase):
    vars = ("x", "y", "z")
    transforms = ("plain", "diff")
    recos = ("all", "real", "virt")

    def output(self):
        return {
            reco: {
                f"{var}_{transform}": self.local_target(f"{reco}/{var}_{transform}.pdf")
                for var in self.vars
                for transform in self.transforms
            }
            for reco in self.recos
        }

    def run(self):
        """Run function of TestNeutrino1
        Does:
        - Evaluates DNN on ttH (testing) dataset
        - Produces plots for comparison between diffferent reconstructions, among them analytic reco using tensorflow and analytic reco using ROOT

        Here is some debug code you may use:

        # calculate constrain value of optimization
        constr_val = -(mc ** 2) / (4 * lepx)

        # print inputs used for one event
        def inspect(id):
            print(
                f"event {id}",
                f"lepx {lepx[id]}",
                f"lepy {lepy[id]}",
                f"lepz {lepz[id]}",
                f"metx {metx[id]}",
                f"mety {mety[id]}",
                f"hval {hval[id]}",
                f"analytic_nu_Pz1 {data['analytic_nu_Pz1'][id]}",
                f"analytic_nu_Pz2 {data['analytic_nu_Pz2'][id]}",
            )

        # write inputs to root file
        d = {"lep_E": inps[:, 0], "lep_Px": inps[:, 1], "lep_Py": inps[:, 2], "lep_Pz": inps[:, 3], "met_Px": inps[:, 4], "met_Py": inps[:, 5]}
        with uproot3.recreate("tth.root") as file:
            file["tree"] = uproot3.newtree({n: v.dtype for n, v in d.items()})
            file["tree"].extend(d)


        """
        import time

        from hist import Hist
        from rich import print

        from config.constants import H_MASS, W_MASS
        from tasks.neutrino import get_h
        from utils.tf_serve import autoClient

        data = self.raw_data
        mc = W_MASS.nominal
        lepe = data["lep_E"]
        lepx = data["lep_Px"]
        lepy = data["lep_Py"]
        lepz = data["lep_Pz"]
        metx = data["met_Px"]
        mety = data["met_Py"]

        mc = W_MASS.nominal
        hval = get_h(lepx, lepy, metx, mety, mc).numpy()

        inps = np.concatenate(
            [
                lepe[..., None],
                lepx[..., None],
                lepy[..., None],
                lepz[..., None],
                metx[..., None],
                mety[..., None],
            ],
            axis=-1,
        )
        start = time.time()
        reco_nu = {k: v for k, v in zip(self.vars, self.dnn_predict(inps))}
        runtime = time.time() - start
        print(f"Evaluation {inps.shape[0]} events, batch_size {self.batch_size}: {runtime:.2f}s")

        for reco, mask in [
            ("all", np.ones_like(hval, dtype=bool)),
            ("real", hval <= 1),
            ("virt", hval > 1),
        ]:
            for var in self.vars:
                gen = data[f"gen_nu_P{var}"][mask]
                rec = reco_nu[f"{var}"][mask]
                if var == "z":
                    ana = data[f"analytic_nu_P{var}1"][mask]
                else:
                    ana = data[f"analytic_nu_P{var}"][mask]
                    met = data[f"met_P{var}"][mask]

                h = (
                    Hist.new.StrCat(["met", "gen", "rec", "ana"], name="id")
                    .Reg(200, -1000, +1000, name="var")
                    .Weight()
                )
                h_diff = (
                    Hist.new.StrCat(["rec-gen", "ana-gen", "rec-ana"], name="id")
                    .Reg(200, -100, +100, name="var")
                    .Weight()
                )

                h.fill(id="gen", var=gen)
                h.fill(id="rec", var=rec)
                h_diff.fill(id="rec-gen", var=rec - gen)

                h.fill(id="ana", var=ana)
                h_diff.fill(id="ana-gen", var=ana - gen)
                h_diff.fill(id="rec-ana", var=rec - ana)

                if var != "z":
                    h.fill(id="met", var=met)

                h.axes["id"].styles = [dict(linestyle="-") for i in range(4)]
                h_diff.axes["id"].styles = [dict(linestyle="-") for i in range(3)]

                for histogram, transform in zip([h, h_diff], self.transforms):
                    self.plot([self.output()[reco][f"{var}_{transform}"]], lines=histogram)

        # metrics
        absurd_neutrinos = np.where(np.abs(reco_nu["z"]) > 100000)[0]
        if len(absurd_neutrinos) > 0:
            absurd_pz_abs = np.abs(reco_nu["z"][absurd_neutrinos])
            print(f"Number of absurd_neutrino: {len(absurd_neutrinos)}")
            print(f"  pz mean: {np.nanmean(absurd_pz_abs)}")
            print(f"  pz std: {np.nanstd(absurd_pz_abs)}")
            print(f"  pz max: {np.nanmax(absurd_pz_abs)}")
        else:
            print("No absurd_neutrinos found.")


class PlotNeutrinoTTHTestOptimizers(PlotNeutrinoTTH, law.WrapperTask):
    def requires(self):
        return [
            PlotNeutrinoTTH.req(self, optimizer=opt) for opt in PlotNeutrinoTTH.optimizer._choices
        ]

    def output(self):
        return

    def run(self):
        return


class BenchmarkNeutrinoTTH(EvalNeutrinoTTH):
    execution_number = luigi.IntParameter(default=0, description="Run number")

    def store_parts(self):
        return super().store_parts() + (f"execution_{self.execution_number}",)

    def output(self):
        return self.local_target(f"metrics.json")

    def run(self):
        import time

        import nvidia_smi

        data = self.data

        start = time.time()
        self.dnn_predict(data)
        runtime = time.time() - start

        # get gpu information
        nvidia_smi.nvmlInit()
        handle = nvidia_smi.nvmlDeviceGetHandleByIndex(0)
        gpu_info = nvidia_smi.nvmlDeviceGetMemoryInfo(handle)
        nvidia_smi.nvmlShutdown()

        # write metrics
        self.output().dump(
            {
                "n_events": self.n_events,
                "batch_size": self.batch_size,
                "runtime": runtime,
                "gpu_info": {
                    "total": gpu_info.total,
                    "free": gpu_info.free,
                    "used": gpu_info.used,
                },
            }
        )
        print(f"Evaluation {data.shape[0]} events, batch_size {self.batch_size}: {runtime:.2f}s")


class PlotNeutrinoTTHAcatConference2021(AnalysisTask):
    def output(self):
        return {
            "n_events": self.local_target("n_events.pdf"),
            "batch_size": self.local_target("batch_size.pdf"),
        }

    def requires(self):
        return {
            # "batch_size": {
            #     bs: [
            #         BenchmarkNeutrinoTTH.req(
            #             self,
            #             n_events=10_000_000,
            #             batch_size=bs,
            #             execution_number=i,
            #         )
            #         for i in range(5)
            #     ]
            #     for bs in [1_000, 10_000, 100_000, 200_000, 500_000]
            # },
            "n_events": {
                (exe, device): {
                    n_events: [
                        BenchmarkNeutrinoTTH.req(
                            self,
                            n_events=n_events,
                            batch_size=100_000,
                            device=device,
                            place_of_execution=exe,
                            execution_number=i,
                        )
                        for i in range(5)
                    ]
                    for n_events in [
                        100,
                        10_000,
                        1_000_000,
                        10_000_000,
                        30_000_000,
                        100_000_000,
                    ]  # , 1_000_000_000]
                }
                for exe, device in [
                    ("remote", "gpu-cold"),
                    ("remote", "gpu-hot"),
                    ("local", "gpu-cold"),
                    ("local", "cpu"),
                ]
            },
        }

    def prepare(self, target_dictionary, feat="n_events"):
        def load_metrics(target):
            metrics = target.load()
            return metrics[feat]

        vload_metrics = np.vectorize(load_metrics, otypes=[np.ndarray])
        a = {key: np.stack(vload_metrics(np.array(val))) for key, val in target_dictionary.items()}
        x = np.array(list(a.keys()))

        y_mean = np.array([np.mean(a[key]) for key in x])
        y_std = np.array([np.std(a[key]) for key in x])

        return x, y_mean, y_std

    def run(self):
        x, y_mean, y_std = self.prepare(self.input()["n_events"][('local', 'cpu')], feat="runtime")  # fmt: skip
        plt.plot(x, y_mean / x, label="Graph: On-board CPU (Cold)", color="#0068A4")
        plt.fill_between(x, (y_mean - y_std) / x, (y_mean + y_std) / x, color="#0068A4", alpha=0.4)
        x, y_mean, y_std = self.prepare(self.input()["n_events"][('local', 'gpu-cold')], feat="runtime")  # fmt: skip
        plt.plot(x, y_mean / x, label="Graph: On-board GPU (Cold)", color="#FEAD00")
        plt.fill_between(x, (y_mean - y_std) / x, (y_mean + y_std) / x, color="#FEAD00", alpha=0.4)
        x, y_mean, y_std = self.prepare(self.input()["n_events"][('remote', 'gpu-cold')], feat="runtime")  # fmt: skip
        plt.plot(x, y_mean / x, label="Graph: Server GPU (Cold)", color="#A11035")
        plt.fill_between(x, (y_mean - y_std) / x, (y_mean + y_std) / x, color="#A11035", alpha=0.4)
        x, y_mean, y_std = self.prepare(self.input()["n_events"][('remote', 'gpu-hot')], feat="runtime")  # fmt: skip
        plt.plot(x, y_mean / x, label="Graph: Server GPU (Warm)", color="#006164", linestyle=":")
        # plt.fill_between(x, (y_mean - y_std) / x, (y_mean + y_std) / x, color="#006164", alpha=0.4)

        plt.axhline(y=0.001, color="#000000", linestyle="--", label="Typical Executable")

        plt.xlabel("N Events [#]")
        plt.ylabel("Runtime / Event [s]")
        plt.xscale("log")
        plt.yscale("log")
        plt.legend()

        self.output()["n_events"].parent.touch()
        plt.savefig(self.output()["n_events"].path)
        plt.close("all")

        # x, y_mean, y_std = self.prepare(self.input()["batch_size"], feat="runtime")
        # n_events = 10_000_000
        # print(x, y_mean / n_events, y_std / n_events)

        # plt.plot(x, y_mean / n_events)
        # plt.fill_between(x, (y_mean - y_std) / n_events, (y_mean + y_std) / n_events, alpha=0.4)
        # plt.xlabel("Batch Size [#]")
        # plt.ylabel("Runtime / Event [s]")
        # plt.xscale("log")
        # plt.yscale("log")

        # plt.savefig(self.output()["batch_size"].path)


class TestNeutrino2(PlotHistsBase):

    vars = ("x", "y", "z")

    def requires(self):
        from tasks.coffea import CoffeaProcessor
        from tasks.neutrino import NeutrinoReconstructionFit2

        return {
            "reco": NeutrinoReconstructionFit2.req(self),
            "data": CoffeaProcessor.req(
                self,
                processor="NeutrinoExporter",
                debug=True,
            ),
        }

    def output(self):
        return {var: self.local_target(f"{var}.pdf") for var in self.vars + ("mass", "pt")}

    def run(self):
        import vector
        from hist import Hist

        from utils.tf_serve import autoClient

        data = self.input()["data"].load()
        dnn = autoClient(self.input()["reco"].parent.path, remote=False)

        inps = np.concatenate(
            [
                data.get(("dimu", "mumu", "e"))[..., None],
                data.get(("dimu", "mumu", "x"))[..., None],
                data.get(("dimu", "mumu", "y"))[..., None],
                data.get(("dimu", "mumu", "z"))[..., None],
                data.get(("met", "mumu", "x"))[..., None],
                data.get(("met", "mumu", "y"))[..., None],
            ],
            axis=-1,
        )
        reco_dinu = dnn(inps)

        for var in self.vars:
            h = (
                Hist.new.StrCat(["met", "gen", "rec"], name="id")
                .Reg(60, -200, 200, name="var")
                .Weight()
            )
            h.fill(id="rec", var=reco_dinu[f"{var}"])
            h.fill(id="gen", var=data.get(("dinugen", "mumu", var)))
            if var != "z":
                h.fill(id="met", var=data.get(("met", "mumu", var)))
            else:
                h.fill(
                    id="met",
                    var=data.get(("met", "mumu", var)),
                    weight=1 / len(data.get(("met", "mumu", var))),
                )
            h.axes["id"].styles = [dict(linestyle="-") for i in range(3)]
            self.plot([self.output()[var]], lines=h)
        h = (
            Hist.new.StrCat(["vv(gen)", "ll", "vv(reco)", "ll+vv(reco)", "ll+MET"], name="id")
            .Reg(60, 0, 300, name="mass")
            .Weight()
        )
        h.fill(id="vv(gen)", mass=data.get(("dinugen", "mumu", "m")))
        h.fill(id="ll", mass=data.get(("dimu", "mumu", "m")))

        # assume m_vv = 0
        energy = np.sqrt(reco_dinu["x"] ** 2 + reco_dinu["y"] ** 2 + reco_dinu["z"] ** 2)

        p4dinu = vector.arr(
            {
                "energy": energy,
                "x": reco_dinu["x"],
                "y": reco_dinu["y"],
                "z": reco_dinu["z"],
            }
        )

        h.fill(id="vv(reco)", mass=p4dinu.mass)

        p4dimu = vector.arr(
            {
                "energy": data.get(("dimu", "mumu", "e")),
                "x": data.get(("dimu", "mumu", "x")),
                "y": data.get(("dimu", "mumu", "y")),
                "z": data.get(("dimu", "mumu", "z")),
            }
        )

        p4met = vector.arr(
            {
                "energy": data.get(("met", "mumu", "e")),
                "x": data.get(("met", "mumu", "x")),
                "y": data.get(("met", "mumu", "y")),
                "z": data.get(("met", "mumu", "z")),
            }
        )

        # (dinu + dimu).mass ~= Hmass
        h.fill(id="ll+vv(reco)", mass=(p4dinu + p4dimu).mass)
        h.fill(id="ll+MET", mass=(p4met + p4dimu).mass)

        h.axes["id"].styles = [dict(linestyle="-") for i in range(5)]
        self.plot([self.output()["mass"]], lines=h)

        # pT
        h = (
            Hist.new.StrCat(["vv(gen)", "ll", "vv(reco)", "MET"], name="id")
            .Reg(60, 0, 300, name="pt")
            .Weight()
        )
        h.fill(
            id="vv(gen)",
            pt=np.sqrt(
                data.get(("dinugen", "mumu", "x")) ** 2 + data.get(("dinugen", "mumu", "y")) ** 2
            ),
        )
        h.fill(id="ll", pt=p4dimu.pt)
        h.fill(id="vv(reco)", pt=p4dinu.pt)
        h.fill(id="MET", pt=p4met.pt)
        h.axes["id"].styles = [dict(linestyle="-") for i in range(4)]
        self.plot([self.output()["pt"]], lines=h)
