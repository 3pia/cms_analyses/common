# -*- coding: utf-8 -*-

import json
from collections import defaultdict
from functools import reduce
from hashlib import sha1
from importlib import import_module
from pathlib import Path
from subprocess import Popen
from typing import Dict, Tuple

import hist
import law
import luigi
import numpy as np
import uproot as uproot4
import uproot3
from tqdm.auto import tqdm

from tasks.base import AnalysisTask, CombinationTask, DHITask, EOSFileUpload, PoolMap
from tasks.datacard import DatacardProducer, DatacardsVariablesWrapper, DatacardsWrapper
from tasks.mixins import CMSLabelMixin, CreateIssueMixin, ModelMixin, RecipeMixin
from tasks.plotting import CheckSystsCombined
from utils.bh5 import Histogram as Hist5
from utils.uproot import decycle
from utils.util import outputs_to_issue

years = [2016, 2017, 2018]


class BlindingMixin:
    unblind = luigi.BoolParameter()

    def store_parts(self):
        if self.unblind:
            blind = "unblind"
        else:
            blind = "blind"

        return super().store_parts() + (blind,)


class FitDiagnostics(DHITask, DatacardProducer):
    def requires(self):
        return DatacardProducer.req(self, transfer=True)

    outmap = {"fitdiagnostics.root": "*/fitdiagnostics__*.root"}

    def command(self):
        return [
            "FitDiagnostics",
            "--version",
            self.version,
            "--datacards",
            self.remote_path(self.input()["datacard_eos"]),
            "--skip-save",
            "Toys",
            "--custom-args",
            "--ignoreCovWarning --skipSBFit",
        ]


class FitDiagnosticsWrapper(DatacardsWrapper):
    _task = FitDiagnostics


class FitDiagnosticsVariablesWrapper(DatacardsVariablesWrapper):
    _producer = FitDiagnostics
    # maybe set category by default to "all_incl_sr"


class DHI(DHITask, ModelMixin, RecipeMixin, BlindingMixin):
    outmap = {"plot.pdf": "*.pdf", "plot.png": "*.png", "plot.C": "*.C"}
    no_poll = luigi.BoolParameter(significant=False)
    wf_local = luigi.BoolParameter(significant=False)
    _cms_label_cmd = []

    @property
    def datacards_hash(self):  # TODO: issue #405
        stringified_datacards = json.dumps(self.datacards, sort_keys=True).encode("utf8")
        _datacards_hash = sha1(stringified_datacards).hexdigest()[:10]
        return _datacards_hash

    def complete(self):
        try:
            self.datacards_hash
        except:
            return False
        return super().complete()

    def store_parts(self):
        parts = super().store_parts()
        parts += (self.datacards_hash,)
        return parts

    def base_command(self, name=None, workflows=()):
        if name is None:
            name = type(self).__name__
        ret = [
            name,
            "--hh-model",
            "hh_model.model_default",
            "--Snapshot-workflow",
            "local",
            "--use-snapshot",
            "--Snapshot-custom-args",
            " --setParameterRanges r=-100,100",
            "--version",
            f"{self.version}__{self.model_version}",
            "--campaign",
        ]
        if hasattr(self, "years"):
            if len(self.years) == 1:  # 1 year
                ret += [str(self.years[0])]
            elif len(self.years) == 3 and (
                tuple(sorted(self.years)) == ("2016", "2017", "2018")
            ):  # FR 2
                ret += ["run2"]
            else:
                ret += [f"({', '.join(self.years)})"]  # any 2-year combination
        else:
            ret += [self.model_version]
        if not isinstance(workflows, dict):
            workflows = {wf: None for wf in workflows}
        for wf, runtime in workflows.items():
            if self.wf_local:
                ret += [f"--{wf}-workflow", "local"]
                continue
            ret += [f"--{wf}-workflow", "htcondor", f"--{wf}-poll-interval", "300sec"]
            if runtime and runtime is not True:
                ret += [f"--{wf}-max-runtime", runtime]
            if self.no_poll:
                ret.append(f"--{wf}-no-poll")

        # add cms label
        ret += self._cms_label_cmd
        return ret

    def wrapper2paths(self, wrapper, prefix=""):
        if isinstance(wrapper, dict):
            return sum((self.wrapper2paths(w, prefix=y) for y, w in wrapper.items()), [])
        elif isinstance(wrapper, (list, tuple)):
            return sum((self.wrapper2paths(w, prefix=prefix) for w in wrapper), [])
        else:
            _prefix = (
                lambda p, s: f"y{p}_{s}" if p.startswith(("2016", "2017", "2018")) else f"{p}_{s}"
            )
            return sorted(
                f"{_prefix(prefix, key)}={self.remote_path(output['datacard_eos'])}"
                for key, output in wrapper.input().items()
            )


class DatacardProvider(CombinationTask):
    def requires(self):
        return {
            f"{analysis}__{year}": DatacardsWrapper.req(
                self,
                year=str(year),
                analysis_choice=analysis,
                transfer=True,
                categories=self.categories[analysis],
            )
            for analysis in self.analyses
            for year in self.years
        }

    @property
    def datacards(self):
        def condition(outp):
            return isinstance(outp, dict) and all(
                isinstance(o, (DatacardsWrapper, EOSFileUpload)) for o in outp.values()
            )

        req = {
            k: v
            for k, v in self.deep_requires(condition=condition).items()
            if isinstance(v, DatacardsWrapper)
            and k in [f"{analysis}__{year}" for analysis in self.analyses for year in self.years]
        }
        # individual
        datacards = {
            key: self.wrapper2paths(
                value,
                prefix=key,
            )
            for key, value in req.items()
        }
        # all years
        if len(self.years) > 1:
            for year in self.years:
                datacards[year] = self.wrapper2paths(
                    [value for k, value in req.items() if year in k],
                    prefix=year,
                )
        # all analysis
        if len(self.analyses) > 1:
            for analysis in self.analyses:
                datacards[analysis] = self.wrapper2paths(
                    [value for k, value in req.items() if analysis in k],
                    prefix=analysis,
                )
        # in case 1 year and 1 analysis is given we can have the categories individually:
        if len(self.analyses) == len(self.years) == 1:
            analysis = self.analyses[0]
            year = self.years[0]
            datacards.update(
                {
                    p.split("=")[0].replace(f"{analysis}__{year}_", "", 1): [p]
                    for p in self.wrapper2paths(req)
                }
            )
        # all combined
        datacards["combined"] = self.wrapper2paths(req)
        return datacards


class FlatDatacardsCommand:
    @property
    def datacards(self):
        datacards = super().datacards
        return datacards["combined"]

    def base_command(self, name=None, workflows=()):
        ret = super().base_command(name=name, workflows=workflows)
        ret += ["--datacards", ",".join(self.datacards)]
        return ret

    def _repr_flags(self):
        flags = super()._repr_flags()
        flags[-1] = flags[-1] + " (flat)"
        return flags


class MultiDatacardsCommand:
    @property
    def datacards(self):
        datacards = super().datacards
        if len(self.years) == len(self.analyses) == 1:
            datacards.pop("combined", 1)
            return datacards
        # remove individual if multiple analyses are given
        if len(self.analyses) > 1:
            return {k: v for k, v in datacards.items() if k in k in self.analyses + ("combined",)}
        else:
            if len(self.years) > 1:
                return {k: v for k, v in datacards.items() if k in k in self.years + ("combined",)}

    def _prettify_datacard_name(self, name: str):
        # combined
        if name == "combined":
            return "Combined"
        # bbww_sl, bbww_dl, ...
        elif name in self.analyses:
            return self.analysis_insts[name].label_short
        # bbww_sl__2017, bbww_sl__2018, ...
        elif "__" in name:
            n, y = name.split("__")
            return f"{self.analysis_insts[n].label_short} - {y}"
        else:
            # technical category names, e.g.: all_boosted_sr_prompt_wjets_other
            # should only occur when 1 analysis and 1 year are given
            if (cat := self.analysis_insts[self.analyses[0]].categories.get(name)) is not None:
                # law CSVParameter can not really handle these nicely
                return cat.label_short.replace(",", "")
        # # 2016, 2017, 2018, somehow it did not match?
        return name

    def base_command(self, name=None, workflows=()):
        datacards = self.datacards
        ret = super().base_command(name=name, workflows=workflows)
        ret += [
            "--multi-datacards",
            ":".join(map(",".join, datacards.values())),
            "--datacard-names",
            ",".join(map(self._prettify_datacard_name, datacards.keys())),
        ]
        if not self.no_poll:
            ret += ["--workers", str(len(datacards.keys()))]
        return ret

    def _repr_flags(self):
        flags = super()._repr_flags()
        flags[-1] = flags[-1] + " (multi)"
        return flags


class FlatDatacardsTask(FlatDatacardsCommand, DHI, DatacardProvider):
    pass


class MultiDatacardsTask(MultiDatacardsCommand, DHI, DatacardProvider):
    pass


class FitDiagnosticsMixin:
    skip_b_only = luigi.BoolParameter()
    # unblind = True  # FitDiagnostic without data does not make sense

    @property
    def _skip_b_only(self):
        return "True" if self.skip_b_only else "False"

    def store_parts(self):
        return super().store_parts() + (f"skip_b_only_{self._skip_b_only}",)


class CreateWorkspace(FlatDatacardsTask):
    outmap = {"workspace.root": "*/workspace.root"}

    def command(self):
        bc = self.base_command(name="CreateWorkspace")
        bc.remove("--use-snapshot")
        idx = bc.index("--campaign")
        del bc[idx : idx + 2]
        return bc


class FitDiagnosticsCombined(FitDiagnosticsMixin, FlatDatacardsTask):
    outmap = {"fitdiagnostics.root": "*/fitdiagnostics__*.root"}
    priority = 200

    def command(self):
        bc = self.base_command(
            name="FitDiagnostics", workflows=dict(FitDiagnostics="336h")
        )  # 2 weeks
        # remove snapshot mechanism here, otherwise prefit shapes are broken
        bc.remove("--use-snapshot")
        idx = bc.index("--campaign")
        del bc[idx : idx + 2]
        bc += [
            "--unblinded",
            str(self.unblind),
            "--skip-save",
            "Toys",
        ]
        bc += ["--skip-b-only", self._skip_b_only]

        # Caution with '--ignoreCovWarning':
        # this option can lead to unnoticed
        # incorrect results if the fit fails
        bc += [
            "--custom-args",
            " --ignoreCovWarning --robustHesse=1 --numToysForShapes=50",
        ]
        return bc


class FitDiagnosticsFrequentistToysMixin:
    toys = luigi.IntParameter(default=200, description="Number of toys")
    postfit_unc_frequentist_toys = luigi.BoolParameter(default=False)

    def store_parts(self):
        fp = super().store_parts()
        if self.postfit_unc_frequentist_toys:
            fp += (f"postfit_unc_frequentist_toys_{self.toys}",)
        return fp


class FitDiagnosticsFrequentistToys(FitDiagnosticsFrequentistToysMixin, FitDiagnosticsCombined):
    toys = luigi.IntParameter(default=200)
    # outmap = {"fitdiagnostics.root": "*/fitdiagnostics__*.root"}
    priority = 200

    @property
    def outmap(self):
        return {
            f"fitdiagnostics.toy.seed{toyid}.root": f"*/fitdiagnostics.toy.seed{toyid}*.root"
            for toyid in list(range(1000, 1000 + self.toys))
        }

    def command(self):
        bc = self.base_command(
            name="FitDiagnosticsFrequentistToys",
            workflows=dict(FitDiagnosticsFrequentistToys="336h"),
        )  # 2 weeks
        # remove snapshot mechanism here, otherwise prefit shapes are broken
        bc.remove("--use-snapshot")
        idx = bc.index("--campaign")
        del bc[idx : idx + 2]
        bc += ["--unblinded", str(self.unblind), "--toys", str(self.toys)]
        bc += ["--skip-b-only", self._skip_b_only]

        # Caution with '--ignoreCovWarning':
        # this option can lead to unnoticed
        # incorrect results if the fit fails
        bc += [
            "--custom-args",
            " --ignoreCovWarning --robustHesse=1 --numToysForShapes=1",
        ]
        return bc


class FitDiagnosticsPostprocessed(FitDiagnosticsMixin, CombinationTask):
    def requires(self):
        raise NotImplementedError()

    def output(self):
        return self.local_target("fitdiagnostics_postprocessed.coffea")

    def cat2vax_pmap(
        self, analysis, campaign, category
    ) -> Tuple[hist.axis.AxisProtocol, Dict[str, str]]:
        """
        Returns a tuple of:
            - the variable axis for the given category
            - a dict to translate datacard processes into aci.Process names
        """

        (dcprod,) = self.find_datacard_reqs(category=category.name, year=str(campaign.aux["year"]))
        variable = analysis.variables.get(dcprod.variable)
        horig = dcprod.input().load()
        vax = horig[variable.name][category.name].axes[-1]

        model = analysis.aux.get("stat_model", self.model)
        module = ".".join([dcprod.analysis_choice, "models", model])
        sm = getattr(import_module(module), self.statmodel)
        pmap = {v: k for k, v in sm.rprocesses.fget(None).items()}

        return vax, pmap

    def find_datacard_reqs(self, **kwargs):
        reqs = [self]
        while not any(isinstance(r, DatacardProducer) for r in reqs):
            reqs = law.util.flatten([r.requires() for r in reqs])
        return [
            r
            for r in reqs
            if isinstance(r, DatacardProducer)
            and all(getattr(r, k, None) == v for k, v in kwargs.items())
        ]

    def add_fr2_hists(self, postprocessed_hists):
        for shape, hists in postprocessed_hists.items():
            # find histograms which can be merged (present throughout fr2)
            merged = defaultdict(set)
            for year in [2016, 2017, 2018]:
                for k in hists.keys():
                    if str(year) in k:
                        merged[k.replace(str(year), "run2")].add(k)

            for k, v in merged.items():
                if len(v) <= 1:
                    continue
                # get all processes, must not be the same in all histograms
                all_procs = reduce(lambda a, b: a | b, [set(hists[i].axes[0]) for i in v])

                edges = np.array([hist.axes[-1].edges for hist in hists.values()])
                all_same_edges = np.all(edges[:-1] == edges[1:])

                orig = hists[list(v)[0]]
                orig_var_axis = orig.axes[1]

                if all_same_edges:
                    new_var_axis = orig_var_axis
                    var_name = new_var_axis.name
                    variable = self.analysis_insts[self.analyses[0]].variables.get(var_name)
                    unit = variable.unit
                    if unit and unit != "1":
                        new_var_axis.label += f" ({unit})"
                else:
                    # in case that axis are different use integer axis
                    new_var_axis = hist.axis.Integer(
                        start=0,
                        stop=orig_var_axis.size,
                        name=orig_var_axis.name,
                        label=f"{orig_var_axis.label} bin (a.u.)",
                        underflow=orig_var_axis.traits.underflow,
                        overflow=orig_var_axis.traits.overflow,
                    )

                # create merge histogram with all procs
                hnew = hist.Hist(
                    hist.axis.StrCategory(all_procs, name="process", growth=True),
                    new_var_axis,
                    storage=orig._storage_type(),
                )

                # merge histograms on var axis with views
                hview = hnew.view(True)
                hview += sum(
                    Hist5.regrow(
                        hists[k],
                        dict(process=all_procs),
                    ).view(True)
                    for k in v
                )

                # add merged histogram to histograms
                hists[k] = hnew

    def get_fit_results(self, path):
        """get sb fit result

        Args:
            path (str): Path of fitdiagnostics fit file
        """
        with uproot4.open(path) as f:
            fit_results = {}
            for key in f.keys(recursive=False, cycle=False, filter_classname="RooFitResult"):
                try:
                    o = f[key]
                except uproot4.DeserializationError as e:
                    o = e.context["breadcrumbs"][0]
                assert "_covQual" in o._members
                fit_results[key] = o._members.copy()

            # extract 'r' value
            if self.unblind:
                fit_results["r"] = f["tree_fit_sb"]["r"].array(library="np")
        return fit_results

    def postprocess_fitdiagnostics(self, path, skip_cat=lambda cat: False):
        postprocessed_hists = defaultdict(dict)
        with uproot3.open(path) as f:
            work = [
                (decycle(shape), decycle(cat))
                for shape, d in f.items(filtername=lambda x: x.startswith(b"shapes"))
                for cat, cls in d.classnames()
                if cls == "TDirectory"
            ]
            for shape, cat in work:
                if skip_cat(cat):
                    continue
                analysis, campaign, category = self.cat2acc(cat)
                vax, pmap = self.cat2vax_pmap(analysis, campaign, category)

                d = f[shape][cat]
                procs = [
                    pmap.get(decycle(p), decycle(p))
                    for p in d.keys(filterclass=lambda cls: cls.__name__.startswith("TH1"))
                ]
                if "data" in d:
                    assert "data_err" not in d
                    procs += ["data", "data_err"]

                # Create Histogram with needed processes (from datacard translated with pmap)
                hnew = hist.Hist(
                    hist.axis.StrCategory(procs, name="process", growth=True),
                    vax,
                    storage=hist.storage.Weight(),
                )
                for p, h in d.items():
                    p = pmap.get(decycle(p), decycle(p))
                    if p not in hnew.axes[0]:
                        continue
                    if p == "data":
                        # HACK: histograms may have superfluous (empty) bins; cut them off
                        ce = h.yvalues[: len(vax)]
                        hi = ce + h.yerrorshigh[: len(vax)]
                        lo = ce - h.yerrorslow[: len(vax)]

                        view = hnew.view(False)[hnew.axes[0].index("data")]
                        view["value"] = ce
                        view = hnew.view(False)[hnew.axes[0].index("data_err")]
                        view["value"] = 0.5 * (lo + hi)
                        view["variance"] = np.square(0.5 * (hi - lo))
                    else:
                        view = hnew.view(False)[hnew.axes[0].index(p)]
                        view["value"] += h.values[: len(vax)]
                        view["variance"] += h.variances[: len(vax)]
                postprocessed_hists[shape][cat] = hnew

            # add histograms for fr2
            self.add_fr2_hists(postprocessed_hists)
        return postprocessed_hists

    @law.decorator.safe_output
    def run(self):
        postprocessed_hists = self.postprocess_fitdiagnostics(self.input().path)

        assert "fit_results" not in postprocessed_hists
        # extract fit quality infos
        postprocessed_hists["fit_results"] = self.get_fit_results(self.input().path)
        self.output().dump(postprocessed_hists)


class FitDiagnosticsCombinedPostprocessed(FitDiagnosticsPostprocessed, FlatDatacardsTask):
    def requires(self):
        return FitDiagnosticsCombined.req(self)


class FitDiagnosticsFrequentistToysPostprocessed(
    FitDiagnosticsFrequentistToysMixin, FitDiagnosticsPostprocessed, FlatDatacardsTask, PoolMap
):

    n_parallel_max = 20

    def requires(self):
        return FitDiagnosticsFrequentistToys.req(self)

    def worker(self, work):
        i, toy = work
        postprocessed_hists = self.postprocess_fitdiagnostics(toy.path)

        assert "fit_results" not in postprocessed_hists
        # extract fit quality infos
        postprocessed_hists["fit_results"] = self.get_fit_results(toy.path)
        return i, postprocessed_hists

    @law.decorator.safe_output
    def run(self):
        out_hists = {}

        work = [(i, toy) for i, toy in enumerate(self.input())]
        for i, postprocessed_hists in self.pmap(
            self.worker,
            work,
            unit="postprocess toy",
            unordered=True,
        ):
            out_hists[i] = postprocessed_hists
        self.output().dump(out_hists)


class AddFitDiagsMixin:
    fit_version = luigi.Parameter(description="task version")
    fit_model_version = luigi.Parameter(default="")
    fit_statmodel = luigi.Parameter(default="StatModel")

    fit_analyses = law.CSVParameter(
        default=("bbww_dl", "bbww_sl"),
        description="Analyses for combination",
    )
    fit_years = law.CSVParameter(
        default=("2016", "2017", "2018"),
        description="Years for combination",
    )
    fit_categories = luigi.DictParameter(default={}, description="Categories")
    fit_categories_tags = law.CSVParameter(default=("fit",), description="Categories tags")
    fit_categories_regex = luigi.Parameter(default=".+", description="Categories name regex")


class PostfitShapesFromWorkspace(AddFitDiagsMixin, CreateWorkspace):
    """Creates postfit shapes from (potentially) different datacards
    Uses Workspace from CreateWorkspace and FitDiags from FitDiagnosticsCombined.
    The datacards used for Workspace and FitDiagnostics must not be the same.
    Currently we are fixing the FitDiagnostics Result to a specific SL+DL FR2 fit
    and we are using the usual parameters for the CreateWorkspace task.
    The output of the task thus is defined by the CreateWorkspace set of datacards.
    We could also "split" the parameters for the two tasks in the future.
    The datacard sets of the tasks could be connected like this:

    @property
    def datacards_hash(self):
        keys = ("fitdiags", "workspace")
        def is_postfit_shapes_from_workspace(req):
            if isinstance(req, dict):
                if all(key in req.keys() for key in keys):
                    return True
        reqs = self.deep_requires(condition=is_postfit_shapes_from_workspace)
        _hashes = []d
        for key in keys:
            req = reqs[key]
            stringified_datacards = json.dumps(req.datacards, sort_keys=True).encode("utf8")
            hashified_datacards = sha1(stringified_datacards).hexdigest()[:10]
            _hashes.append(f"{key}_{hashified_datacards}")
        hashes = "__".join(_hashes)
        return hashes
    """

    @property
    def datacards(self):
        return CreateWorkspace.req(self).datacards

    def requires(self):
        return {
            "fitdiags": FitDiagnosticsCombined.req(
                self,
                version=self.fit_version,
                model_version=self.fit_model_version,
                statmodel=self.fit_statmodel,
                years=self.fit_years,
                analyses=self.fit_analyses,
                categories=self.fit_categories,
                categories_tags=self.fit_categories_tags,
                categories_regex=self.fit_categories_regex,
            ),
            "workspace": CreateWorkspace.req(self),
        }

    def output(self):
        return self.local_target("postfit.root")

    @law.decorator.safe_output
    def run(self):
        inp = self.input()
        workspace = inp["workspace"]
        fitdiags = inp["fitdiags"]

        workspace_path = workspace.path
        fitdiags_path = fitdiags.path

        self.output().parent.touch()
        output_path = self.output().path

        process = Popen(
            [
                f"submit -f -i cmssw/cc7:x86_64 sh -c 'cd /net/scratch/cms/dihiggs/software/dn801636/cc7/CMSSW/CMSSW_10_2_13 && source '/cvmfs/cms.cern.ch/cmsset_default.sh' && cmsenv && PostFitShapesFromWorkspace -w {workspace_path} -f {fitdiags_path}:fit_s --print --postfit --sampling --total-shapes --output {output_path}'"
            ],
            shell=True,
        )
        print(process.communicate())


class PostfitShapesFromWorkspacePostprocessed(
    FitDiagnosticsFrequentistToysMixin,
    FitDiagnosticsPostprocessed,
    PostfitShapesFromWorkspace,
):
    def requires(self):
        reqs = {"postfit_shapes": PostfitShapesFromWorkspace.req(self)}
        if self.postfit_unc_frequentist_toys:
            reqs.update(
                {
                    "postfit_unc_frequentist_toys": FitDiagnosticsFrequentistToysPostprocessed.req(
                        self,
                        version=self.fit_version,
                        model_version=self.fit_model_version,
                        years=self.fit_years,
                        analyses=self.fit_analyses,
                        categories=self.fit_categories,
                        categories_tags=self.fit_categories_tags,
                        categories_regex=self.fit_categories_regex,
                    )
                }
            )
        return reqs

    def output(self):
        return self.local_target("postprocessed.coffea")

    def decipher_shape(self, cat: str):
        cat, fit_type = cat.rsplit("_", 1)
        analysis, campaign, category = self.cat2acc(cat)
        return fit_type, analysis, campaign, category

    def find_datacard_reqs(self, **kwargs):
        reqs = [self.requires()["postfit_shapes"].requires()["workspace"]]
        while not any(isinstance(r, DatacardProducer) for r in reqs):
            reqs = law.util.flatten([r.requires() for r in reqs])
        return [
            r
            for r in reqs
            if isinstance(r, DatacardProducer)
            and all(getattr(r, k, None) == v for k, v in kwargs.items())
        ]

    @law.decorator.safe_output
    def run(self):
        postprocessed_hists = defaultdict(dict)
        with uproot3.open(self.input()["postfit_shapes"].path) as f:
            for (cat, d) in f.items():
                cat = decycle(cat)
                if cat in ["prefit", "postfit"]:
                    continue
                fit_type, analysis, campaign, category = self.decipher_shape(cat)
                vax, pmap = self.cat2vax_pmap(analysis, campaign, category)
                pmap.update({"data_obs": "data"})

                d = f[cat]
                procs = [
                    pmap.get(decycle(p), decycle(p))
                    for p in d.keys(filterclass=lambda cls: cls.__name__.startswith("TH1"))
                ]

                procs += ["data_err"]

                # Create Histogram with needed processes (from datacard translated with pmap)
                hnew = hist.Hist(
                    hist.axis.StrCategory(procs, name="process", growth=True),
                    vax,
                    storage=hist.storage.Weight(),
                )
                for p_dc, h in d.items():
                    p = pmap.get(decycle(p_dc), decycle(p_dc))
                    if p not in hnew.axes[0]:
                        continue

                    view = hnew.view(False)[hnew.axes[0].index(p)]

                    # cut of zeros from PSFW
                    assert np.all(h.values[len(vax) :] == 0)
                    assert np.all(np.nan_to_num(h.variances[len(vax) :]) == 0)

                    view["value"] += h.values[: len(vax)]
                    view["variance"] += h.variances[: len(vax)]

                    if p == "data":
                        view = hnew.view(False)[hnew.axes[0].index("data_err")]
                        view["value"] += h.values[: len(vax)]
                        view["variance"] += h.variances[: len(vax)]

                fit_shapes = {
                    "prefit": "shapes_prefit",
                    "postfit": "shapes_fit_sb",
                }
                fit_shape = fit_shapes[fit_type]
                postprocessed_hists[fit_shape][cat] = hnew

            # add histograms for fr2
            self.add_fr2_hists(postprocessed_hists)
            for fit_shape, hists in postprocessed_hists.items():
                if fit_shape == "shapes_prefit":
                    continue
                for cat, h in hists.items():
                    # we scale the total background...
                    scale_by_hand = False
                    if scale_by_hand:
                        if "run2" in cat:
                            sum_bkg = h["TotalBkg", :].view(False)
                            total_bkg_val, total_bkg_var = (
                                f["postfit"]["TotalBkg"].values,
                                f["postfit"]["TotalBkg"].variances,
                            )
                            if np.allclose(
                                sum_bkg.value,
                                total_bkg_val,
                            ):
                                sum_bkg.variance *= np.divide(
                                    total_bkg_var,
                                    sum_bkg.variance,
                                    out=np.ones_like(sum_bkg.variance),
                                    where=sum_bkg.variance > 0,
                                )
        assert "fit_results" not in postprocessed_hists

        # use frequentist toys to get postfit uncertainties
        if self.postfit_unc_frequentist_toys:
            toys = self.input()["postfit_unc_frequentist_toys"].load()
            cats = [c.rsplit("_", 1) for c in postprocessed_hists["shapes_fit_sb"].keys()]
            for cat, fit_type in cats:
                if fit_type == "prefit":
                    continue
                toys_cat = []
                for toy in toys.values():
                    # skip toys that did not converge
                    if len(toy["shapes_fit_s"]) == 0:
                        continue
                    toys_cat.append(
                        toy["shapes_fit_s"][cat]["total_background", :].view(False).value
                    )
                toys_cat = np.array(toys_cat)  # (n_toys x n_bins)
                unc = np.std(toys_cat, axis=0)
                h = postprocessed_hists["shapes_fit_sb"][f"{cat}_{fit_type}"]
                h.view(False)[h.axes[0].index("TotalBkg"), :].variance = unc**2

        # extract fit quality infos
        used_fitdiags = self.requires()["postfit_shapes"].input()["fitdiags"]
        postprocessed_hists["fit_results"] = self.get_fit_results(used_fitdiags.path)
        self.output().dump(postprocessed_hists)


class PlotPullsAndImpacts(CMSLabelMixin, FlatDatacardsTask, CreateIssueMixin):
    """https://cms-hh.web.cern.ch/cms-hh/tools/inference/tasks/pullsandimpacts.html"""

    mc_stats = luigi.BoolParameter()
    show_best_fit = luigi.BoolParameter(default=False)
    order_by_impact = luigi.BoolParameter(default=True)
    priority = 300

    def requires(self):
        req = super().requires()
        req.update(
            {
                "nuisance_renaming": UploadRenamings.req(
                    self,
                    filepath=str(Path(__name__).parent / "config" / "rename_nuisance.py"),
                )
            }
        )
        return req

    def command(self):
        ret = self.base_command(workflows={"PullsAndImpacts": "36h"}) + [
            "--keep-failures",
            "--left-margin",
            "750",
            "--pad-width",
            "1500",
            "--label-size",
            "18",
            "--file-types",
            "pdf,png,C",
            "--labels",
            self.remote_path(self.input()["nuisance_renaming"]),
            "--PullsAndImpacts-custom-args",
            " --robustFit=1 --cminDefaultMinimizerPrecision=1E-12 --setParameterRanges r=-100,100",
            "--PullsAndImpacts-retry-no-analytic",  # https://gitlab.cern.ch/hh/tools/inference/-/merge_requests/61
            "True",
        ]
        if self.unblind:
            ret += ["--unblinded", "True"]
        if self.mc_stats:
            ret.append("--mc-stats")
        if self.show_best_fit:
            ret += ["--show-best-fit", "True"]
        else:
            ret += ["--show-best-fit", "False"]
        if self.order_by_impact:
            ret.append("--order-by-impact")
        return ret

    def store_parts(self):
        sp = super().store_parts()
        if self.mc_stats:
            sp += ("mc_stats",)
        return sp

    @outputs_to_issue(
        important_params=["analyses", "years", "unblind", "mc_stats"], collapsibles=r".*.png$"
    )
    def run(self):
        super().run()


class UploadRenamings(EOSFileUpload):
    version = PlotPullsAndImpacts.version
    model_version = PlotPullsAndImpacts.model_version

    def store_parts(self):
        return (self.version, self.model_version)


class PlotMultipleGoodnessOfFits(CMSLabelMixin, MultiDatacardsTask, CreateIssueMixin):
    """https://cms-hh.web.cern.ch/cms-hh/tools/inference/tasks/gof.html#testing-multiple-datacards"""

    algo = luigi.ChoiceParameter(choices=["saturated", "AD", "KS"], default="saturated")
    priority = 400

    toys = luigi.IntParameter(default=300)

    def store_parts(self):
        return super().store_parts() + (self.algo,) + (f"toys_{self.toys}",)

    @property
    def outmap(self):
        # only pdf
        outmap = dict(super().outmap)
        new_outmap = {k: v for k, v in outmap.items() if k.endswith(".pdf")}
        return new_outmap

    def command(self):
        cmd = self.base_command(workflows={"GoodnessOfFit": "36h"})
        cmd += [
            "--file-types",
            "pdf,png",
            "--toys",
            f"{self.toys}",
            "--toys-per-branch",
            "1",
            "--algorithm",
            f"{self.algo}",
        ]
        if self.algo == "saturated":
            cmd += ["--frequentist-toys"]

        # remove snapshot mechanism here, hits walltime for combination
        cmd.remove("--use-snapshot")
        idx = cmd.index("--Snapshot-workflow")
        del cmd[idx : idx + 2]
        idx = cmd.index("--Snapshot-custom-args")
        del cmd[idx : idx + 2]
        return cmd

    @outputs_to_issue(important_params=["analyses", "years", "unblind", "algo"])
    def run(self):
        super().run()


class PlotUpperLimitsAtPoint(CMSLabelMixin, MultiDatacardsTask, CreateIssueMixin):
    """https://cms-hh.web.cern.ch/cms-hh/tools/inference/tasks/limits.html#multiple-limits-at-a-certain-point-of-parameters"""

    poi = luigi.ChoiceParameter(default="r", choices=["r", "r_gghh", "r_qqhh"])
    workflows = {"UpperLimits": "36h"}
    priority = 500

    def store_parts(self):
        return super().store_parts() + (self.poi,)

    def command(self):
        cmd = self.base_command(workflows=self.workflows)
        cmd += ["--file-types", "pdf,png,C"]
        cmd += ["--pois", self.poi]
        if self.unblind:
            cmd += ["--unblinded", "True"]
        cmd += ["--sort-by", "expected"]
        cmd += ["--show-parameters", "kl,kt:CV,C2V"]
        return cmd

    @outputs_to_issue(important_params=["analyses", "years", "unblind", "poi", "statmodel"])
    def run(self):
        super().run()


class ParameterScan(CreateIssueMixin):
    scan_parameter = luigi.Parameter(default="kl")
    y_log = luigi.BoolParameter(default=True)

    y_min = luigi.FloatParameter(
        default=-1000.0,
        significant=False,
        description="the lower y-axis limit; no default",
    )
    y_max = luigi.FloatParameter(
        default=-1000.0,
        significant=False,
        description="the upper y-axis limit; no default",
    )

    @property
    def workflows(self):
        raise NotImplementedError

    def store_parts(self):
        return super().store_parts() + (self.scan_parameter.replace(":", "__").replace(",", "_"),)

    def command(self):
        cmd = self.base_command(workflows=self.workflows)
        cmd += ["--file-types", "pdf,png,C"]
        if self.y_log:
            cmd += ["--y-log"]
        cmd += ["--scan-parameters", f"{self.scan_parameter}"]
        if self.unblind:
            cmd += ["--unblinded", "True"]
        if self.y_min is not None:
            cmd += ["--y-min", str(self.y_min)]
        if self.y_max is not None:
            cmd += ["--y-max", str(self.y_max)]
        return cmd

    @outputs_to_issue(
        important_params=["analyses", "years", "unblind", "scan_parameter", "statmodel"]
    )
    def run(self):
        super().run()


class LikelihoodScan(ParameterScan):
    workflows = {"LikelihoodScan": "36h"}

    @property
    def priority(self):
        return len(self.pois.split(",")) * 400

    @property
    def Nsp(self):
        return len(self.scan_parameter.split(":")[:2])

    @property
    def pois(self):
        sp = self.scan_parameter.split(":")[:2]
        if len(sp) == 1:
            return sp[0].split(",")[0]
        else:
            assert len(sp) == 2
            return ",".join([sp_.split(",")[0] for sp_ in sp])

    def command(self):
        cmd = super().command()
        cmd += ["--pois", self.pois]
        if self.Nsp == 2:
            cmd += ["--shift-negative-values", "True"]
        return cmd


class LimitScan(ParameterScan):
    priority = 400
    workflows = {"UpperLimits": "36h"}

    @property
    def outmap(self):
        outmap = dict(super().outmap)
        outmap["exclusion_ranges.json"] = "*.json"
        return outmap

    def command(self):
        cmd = super().command()
        if self.scan_parameter == "C2V":
            cmd += ["--pois", "r_qqhh"]
            cmd += ["--show-parameters", "kt,C2V,CV"]
        else:
            cmd += ["--pois", "r"]
            cmd += ["--show-parameters", "kl,kt,CV"]
        cmd += ["--xsec", "fb"]
        cmd += ["--save-ranges", "True"]
        return cmd


class ExclusionAndBestFit:
    pois = PlotUpperLimitsAtPoint.poi
    workflows = {"UpperLimits": "23h", "LikelihoodScan": "23h"}
    y_log = False
    priority = 1000

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        if "2D" in type(self).__name__:
            assert self.Nsp == 2, "This task only works for 2D scans!"
        else:
            assert self.Nsp == 1, "This task only works for 1D scans!"


class PlotExclusionAndBestFit(
    ExclusionAndBestFit, LikelihoodScan, CMSLabelMixin, MultiDatacardsTask
):
    """https://cms-hh.web.cern.ch/cms-hh/tools/inference/tasks/exclusion.html"""

    y_min = None
    y_max = None


class PlotExclusionAndBestFit2D(
    ExclusionAndBestFit, LikelihoodScan, CMSLabelMixin, FlatDatacardsTask
):
    """https://cms-hh.web.cern.ch/cms-hh/tools/inference/tasks/exclusion.html#2d-parameter-exclusion"""

    def command(self):
        cmd = super().command()
        # more than ~6k jobs points breaks lxplus
        # see: https://lists.openafs.org/pipermail/openafs-info/2002-September/005812.html
        cmd += ["--UpperLimits-tasks-per-job", "2"]
        cmd += ["--LikelihoodScan-tasks-per-job", "2"]

        idx = cmd.index("--shift-negative-values")
        del cmd[idx : idx + 2]

        cmd += ["--workers", "2"]

        sp = self.scan_parameter.split(":")[:2]
        sp = tuple([sp_.split(",")[0] for sp_ in sp])
        if sp == ("kl", "kt"):
            cmd += ["--show-parameters", "CV,C2V"]
            cmd += ["--y-min=-7.5", "--y-max=10", "--x-min=-30", "--x-max=30"]
        elif sp == ("C2V", "CV"):
            cmd += ["--show-parameters", "kl,kt"]
            cmd += ["--y-min=-3", "--y-max=5", "--x-min=-5", "--x-max=6"]
        elif sp == ("kl", "C2V"):
            cmd += ["--show-parameters", "kt,CV"]
            cmd += ["--y-min=-3.5", "--y-max=7", "--x-min=-17", "--x-max=23"]
        return cmd


class PlotMultipleUpperLimits(LimitScan, CMSLabelMixin, MultiDatacardsTask):
    """https://cms-hh.web.cern.ch/cms-hh/tools/inference/tasks/limits.html#multiple-limits-on-poi-vs-scan-parameter"""


class PlotUpperLimits(LimitScan, CMSLabelMixin, FlatDatacardsTask):
    """https://cms-hh.web.cern.ch/cms-hh/tools/inference/tasks/limits.html#limit-on-poi-vs-scan-parameter"""


class PlotUpperLimits2D(LimitScan, CMSLabelMixin, FlatDatacardsTask):
    """https://cms-hh.web.cern.ch/cms-hh/tools/inference/tasks/limits.html#limit-on-poi-vs-scan-parameter"""

    outmap = {"plot.pdf": "*.pdf", "plot.png": "*.png"}

    y_log = False
    y_min = None
    y_max = None

    def command(self):
        cmd = super().command()
        for q in ["--xsec", "--save-ranges"]:
            i = cmd.index(q)
            del cmd[i : i + 2]
        # more than ~6k jobs points breaks lxplus
        # see: https://lists.openafs.org/pipermail/openafs-info/2002-September/005812.html
        cmd += ["--UpperLimits-tasks-per-job", "2"]
        return cmd


class PlotMultipleLikelihoodScans(LikelihoodScan, CMSLabelMixin, MultiDatacardsTask):
    """https://cms-hh.web.cern.ch/cms-hh/tools/inference/tasks/likelihood.html#1d_1"""


class PlotLikelihoodScan(LikelihoodScan, CMSLabelMixin, FlatDatacardsTask):
    """https://cms-hh.web.cern.ch/cms-hh/tools/inference/tasks/likelihood.html#1d"""

    priority = -10


class InferenceCombined(
    ModelMixin,
    RecipeMixin,
    CreateIssueMixin,
    CombinationTask,
    law.WrapperTask,
):
    dhi_command = DHITask.dhi_command
    no_poll = DHI.no_poll
    mode = luigi.Parameter(
        default="lrGIi",
        description="""One or more of these (lrGPIpigs):
        l = limit asimov @SM
        r = limit scans
        G = gof [unblinded and only in background regions]
        P = postfit [unblinded and only in background regions]
        I = impacts [unblinded and only in background regions]
        p = prefit
        i = impacts
        g = gof other algorithms
        s = systematic envelopes
        """,
    )

    def fdiag(self, ub=False):
        from tasks.prefit import PlotFitDiagnosticsCombined

        if tuple(sorted(self.years)) == ("2016", "2017", "2018"):
            years = ("run2",)
        elif len(self.years) == 2:
            raise ValueError(
                "Can not produce Prefit/Postfit plots from only 2 years. Either one year or FR2!"
            )
        else:
            years = self.years
        if self.dhi_command:
            req = {
                (a, ub): FitDiagnosticsCombined.req(
                    self,
                    years=self.years,
                    unblind=ub,
                    analyses=(a,),
                )
                for a in self.analyses
            }
            return req
        else:
            # TODO: this is not yet compatible with analyses combination!
            return {
                (a, y, ub): PlotFitDiagnosticsCombined.req(
                    self,
                    year=y,
                    analysis_choice=a,
                    log_scale=True,
                    blind_thresh=1e4,
                    process_group=dict(bbww_dl=("plotting_DYest",), bbww_sl=("plotting",))[a],
                    unblind=ub,
                )
                for y in years
                for a in self.analyses
            }

    def requires(self):
        self.logger.info(f"Running mode {self.mode} with year(s) {self.years}")
        reqs = {}
        if "l" in self.mode:
            reqs["limits"] = [
                PlotUpperLimitsAtPoint.req(self, poi="r"),
                PlotUpperLimitsAtPoint.req(self, poi="r_qqhh"),
            ]
        if "r" in self.mode:
            reqs["scans"] = [
                PlotUpperLimits.req(self, scan_parameter="kl"),
                PlotUpperLimits.req(self, scan_parameter="C2V"),
            ]
        if "G" in self.mode:
            reqs["gof"] = {"saturated": PlotMultipleGoodnessOfFits.req(self, algo="saturated")}
        if "P" in self.mode:
            reqs["fdiag_ub"] = self.fdiag(ub=True)
        if "I" in self.mode:
            reqs["pulls"] = {
                "unblind": {
                    (a, y): PlotPullsAndImpacts.req(
                        self,
                        years=(y,),
                        unblind=True,
                        analyses=(a,),
                    )
                    for y in self.years
                    for a in self.analyses
                }
            }
            if len(self.years) > 1 or len(self.analyses) > 1:
                # add combination
                reqs["pulls"]["unblind"].update(
                    {
                        ("+".join(self.analyses), "+".join(self.years)): PlotPullsAndImpacts.req(
                            self, unblind=True
                        )
                    }
                )
        if "p" in self.mode:
            reqs["fdiag_b"] = self.fdiag(ub=False)
            # remove combined prefit if exists
            if "run2" in self.years:
                reqs["fdiag_b"].pop(("run2", False))
        if "i" in self.mode:
            reqs["pulls"].update(
                {
                    "asimov": {
                        (a, y): PlotPullsAndImpacts.req(
                            self,
                            years=(y,),
                            analyses=(a,),
                        )
                        for y in self.years
                        for a in self.analyses
                    }
                }
            )
            if len(self.years) > 1 or len(self.analyses) > 1:
                # add combination
                reqs["pulls"]["asimov"].update(
                    {("+".join(self.analyses), "+".join(self.years)): PlotPullsAndImpacts.req(self)}
                )
        if "g" in self.mode:
            reqs["gof"] = {
                algo: PlotMultipleGoodnessOfFits.req(self, algo=algo)
                for algo in PlotMultipleGoodnessOfFits.algo._choices
            }
        if "s" in self.mode:
            reqs["sys"] = CheckSystsCombined.req(self)
        return reqs
