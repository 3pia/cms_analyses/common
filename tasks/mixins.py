# coding: utf-8

import hashlib
from functools import cached_property, wraps
from itertools import chain
from operator import itemgetter

import law
import luigi

from utils.datacard import Datacard

years = [2016, 2017, 2018]


class RecipeMixin:
    recipe = luigi.Parameter()

    def store_parts(self):
        return super().store_parts() + (self.recipe,)


class ModelMixin:
    model = luigi.Parameter(default="nlo")
    model_version = luigi.Parameter(default="")
    statmodel = luigi.Parameter(default="StatModel")

    @cached_property
    def StatModel(self):
        model = self.analysis_inst.aux.get("stat_model", self.model)
        sm = getattr(self._import("models", model), self.statmodel)
        assert issubclass(sm, Datacard)
        return sm

    def store_parts(self):
        parts = super().store_parts() + (self.model,) + (self.statmodel,)
        if self.model_version:
            parts += (self.model_version,)
        return parts

    @classmethod
    def req_params(cls, inst, **kwargs):
        """
        Returns parameters that are jointly defined in this class and another task instance of some
        other class. The parameters are used when calling ``Task.req(self)``.
        In explicit: allows to run GroupCoffeaProcesses task with another `model_version`, since grouping is expensive.
        e.g.:
            - Check first binning: law run ... --model-version 'new_binning1' --GroupCoffeaProcesses-model-version 'dont_rerun_grouping'
            - Check second binning: law run ... --model-version 'new_binning2' --GroupCoffeaProcesses-model-version 'dont_rerun_grouping'
            ...
        """
        _prefer_cli = set(law.util.make_list(kwargs.get("_prefer_cli", [])))
        _prefer_cli.add("model_version")
        _prefer_cli.add("statmodel")
        kwargs["_prefer_cli"] = _prefer_cli
        params = super().req_params(inst, **kwargs)
        return params


class VariableMixin:
    variable = luigi.Parameter(default="", description="Variable to fit")

    @property
    def var(self):
        if self.variable:
            var = self.variable
        else:
            category_inst = self.analysis_inst.categories.get(self.category)
            var = category_inst.aux["fit_variable"]
            self.logger.info(f"variable for {self.category} set to fit_variable ({var})")
        return var

    def store_parts(self):
        return super().store_parts() + (self.var,)


class CategoryMixin:
    category = luigi.Parameter(default="all_incl_sr")

    def store_parts(self):
        return super().store_parts() + (self.category,)


class PGroupMixin:
    process_group = law.CSVParameter(default="default")

    @property
    def is_config_pgroup(self):
        return self.process_group[0] in self.process_groups

    @property
    def process_groups(self):
        return self.analysis_inst.aux["process_groups"]

    @cached_property
    def legacy_processes(self):
        if self.is_config_pgroup:
            return self.process_groups[self.process_group[0]]
        else:
            return self.process_group

    def getPGroot(self, process, process_group=None):
        if process_group is None:
            process_group = self.legacy_processes
        for _, p in process.walk_parent_processes(include_self=True):
            if any(p is self.analysis_inst.processes.get(pg) for pg in process_group):
                return p

    @property
    def legacy_processes_hash(self):
        return hashlib.sha256("".join(sorted(self.legacy_processes)).encode("utf-8")).hexdigest()

    def store_parts(self):
        return super().store_parts() + (self.legacy_processes_hash,)


class CreateIssueMixin:
    create_issue = luigi.BoolParameter()
    issue_title = luigi.Parameter(default="")


class CombinedMixin:
    years = law.CSVParameter(default=["2016", "2017", "2018", "run2"])

    def store_parts(self):
        return super().store_parts() + (",".join(sorted(self.years)),)


class CMSLabelMixin:
    cms_label = luigi.ChoiceParameter(
        choices=["default", "pas", "paper", "private"], default="default"
    )

    @property
    def cms_label_suffix(self):
        return dict(
            default="Work in progress", pas="Preliminary", paper="", private="Private Work"
        )[self.cms_label]

    @property
    def outmap(self):
        return {
            f"plot_{self.cms_label}.pdf": f"*{self.cms_label}.pdf",
            f"plot_{self.cms_label}.png": f"*{self.cms_label}.png",
            f"plot_{self.cms_label}.C": f"*{self.cms_label}.C",
        }

    @property
    def _cms_label_cmd(self):
        cmd = ["--plot-postfix", self.cms_label]
        if self.cms_label == "paper":
            cmd += ["--paper", "True"]
        elif self.cms_label == "pas":
            cmd += ["--summary", "True"]
        return cmd
