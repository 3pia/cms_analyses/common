#!/bin/bash
krenew -H 60 >&2 || { kinit -r 10d "$DHA_GRID_USER@CERN.CH" >&2 && krenew -b -K 60 >&2 ; } || exit 123
DHI_REPO=${DHI_REPO:-repos/inference/}
CMD="cd ${DHI_REPO@Q} && . setup.sh >&2 && export DHI_LOCAL_SCHEDULER=False DHI_STORE=\"/eos/user/b/befische/DiHiggs\" DHI_STORE_JOBS=\"/afs/cern.ch/work/m/mfackeld/DiHiggs/data/store/\" DHI_STORE_BUNDLES=\"/eos/user/b/befische/DiHiggs\" && exec law ${@@Q}"
CMD="krenew -t -- bash -c ${CMD@Q}"
krenew -- ssh -tq lxplus.cern.ch "$CMD"
