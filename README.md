# di-Higgs Analysis

### Infos on git


##### Install pre-commit hooks

```
pip install pre-commit # or brew install pre-commit on macOS
pre-commit install # Will install a pre-commit hook into the git repo
```


##### `[user]` config

Make sure to have your name and email as registered at CERN in the `.git/config` of the cloned repository. Example:

```
[user]
    name = My Name
    email = my.mail@cern.ch
```


##### LFS

This repository has git-lfs enabled. Therefore, after cloning the repository, run

```shell
git lfs install
```

once.


### Setup & Tasks

##### Creating dask certificate

`openssl req -x509 -sha256 -nodes -days 3650 -newkey rsa:2048 -keyout tls_client_key.pem -out tls_ca_file.pem`


### Fix Kerberos ticket and open ssh connection

1. `ssh -o exit lxplus`
2. `dhi_law.sh --help` (triggers `kinit`)
