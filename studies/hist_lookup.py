import numpy as np
import numba
from numba import cuda

from coffea.lookup_tools.dense_lookup import dense_lookup
import boost_histogram as bh


np.random.seed(42)
test_pt = np.random.random(1000)
test_eta = np.random.random(1000)

# coffea
lookup_coffea = dense_lookup(np.ones(shape=(3, 4)), (np.linspace(0, 1, 4), np.linspace(0, 1, 5)))
"""
In [1]: %timeit lookup_coffea(test_pt, test_eta)
88.8 µs ± 9.13 µs per loop (mean ± std. dev. of 7 runs, 10000 loops each)
"""

# boost
lookup_boost = bh.Histogram(bh.axis.Regular(3, 0, 1), bh.axis.Regular(4, 0, 1))
lookup_boost[...] = 1.0
lookup_boost_view = lookup_boost.view()
"""
In [2]: %timeit lookup_boost_view[lookup_boost.axes.index(test_pt, test_eta)]
31.7 µs ± 1.39 µs per loop (mean ± std. dev. of 7 runs, 10000 loops each)
"""
from IPython import embed

embed()
