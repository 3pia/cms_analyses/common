#!/usr/bin/env python
# coding: utf-8
# flake8: noqa

"""
Defintion of the campaign and datasets for CMS.
"""

import scinum as sn

import config.common as common
import utils.aci as aci
from config.util import init_campaign
from tasks.corrections.jets import make_files as jec_files

# campaign
campaign = aci.Campaign("Run2_pp_13TeV_2018", 2, ecm=13)

"""
Details: https://twiki.cern.ch/twiki/bin/viewauth/CMS/TopSystematics#TOP_Systematic_Uncertainties_Run

UE (underlying_event):
    Directly inferred from up/down variations of the datasets

mtop (top mass variation, 3 GeV shift):
    Up = 175.5 GeV
    Down = 169.5 GeV
    (assuming Up and Down histograms are normalized to nominal)

CR (color reconnection):
    Up = nominal + abs(nominal - max{GluonMove, QCDbased, erdON})
    Down = nominal - abs(nominal - max{GluonMove, QCDbased, erdON})
    (assuming that the GluonMove, QCDbased and erdON histograms are normalized to the same integral as the nominal histogram)

HDamp (ME-PS matching scale):
    Directly inferred from up/down variations of the datasets

dipoleRecoilOn (HH VBF):
    see: https://gitlab.cern.ch/cms-hh-bbww/cms-hh-to-bbww/-/blob/master/Legacy/DipoleRecoil.md
"""

init_campaign(
    campaign,
    __name__,
    __file__,
    ds_shifts={
        "TTTo2L2Nu": {
            # UE
            "TuneCP5Down": dict(
                keys=[
                    "/TTTo2L2Nu_TuneCP5down_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM",
                    "/TTTo2L2Nu_TuneCP5down_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21_ext1-v1/NANOAODSIM",
                    "/TTTo2L2Nu_TuneCP5down_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21_ext2-v1/NANOAODSIM",
                ]
            ),
            "TuneCP5Up": dict(
                keys=[
                    "/TTTo2L2Nu_TuneCP5up_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM",
                    "/TTTo2L2Nu_TuneCP5up_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21_ext1-v1/NANOAODSIM",
                    "/TTTo2L2Nu_TuneCP5up_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21_ext2-v1/NANOAODSIM",
                ]
            ),
            # mtop
            "mtopDown": dict(
                keys=[
                    "/TTTo2L2Nu_mtop169p5_TuneCP5_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM",
                    "/TTTo2L2Nu_mtop169p5_TuneCP5_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21_ext1-v1/NANOAODSIM",
                    "/TTTo2L2Nu_mtop169p5_TuneCP5_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21_ext2-v1/NANOAODSIM",
                ]
            ),
            "mtopUp": dict(
                keys=[
                    "/TTTo2L2Nu_mtop175p5_TuneCP5_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM",
                    "/TTTo2L2Nu_mtop175p5_TuneCP5_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21_ext1-v1/NANOAODSIM",
                    "/TTTo2L2Nu_mtop175p5_TuneCP5_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21_ext2-v1/NANOAODSIM",
                ]
            ),
            # color reconnection
            "GluonMove": dict(
                keys=[
                    "/TTTo2L2Nu_TuneCP5CR2_GluonMove_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM",
                    "/TTTo2L2Nu_TuneCP5CR2_GluonMove_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21_ext1-v1/NANOAODSIM",
                ]
            ),
            "QCDbased": dict(
                keys=[
                    "/TTTo2L2Nu_TuneCP5CR1_QCDbased_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM",
                    "/TTTo2L2Nu_TuneCP5CR1_QCDbased_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21_ext1-v1/NANOAODSIM",
                ]
            ),
            # erdON
            "erdON": dict(
                keys=[
                    "/TTTo2L2Nu_TuneCP5_erdON_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM",
                    "/TTTo2L2Nu_TuneCP5_erdON_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21_ext1-v1/NANOAODSIM",
                    "/TTTo2L2Nu_TuneCP5_erdON_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21_ext2-v1/NANOAODSIM",
                ]
            ),
            # hdamp
            "HDampDown": dict(
                keys=[
                    "/TTTo2L2Nu_hdampDOWN_TuneCP5_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM",
                    "/TTTo2L2Nu_hdampDOWN_TuneCP5_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21_ext1-v1/NANOAODSIM",
                    "/TTTo2L2Nu_hdampDOWN_TuneCP5_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21_ext2-v1/NANOAODSIM",
                ]
            ),
            "HDampUp": dict(
                keys=[
                    "/TTTo2L2Nu_hdampUP_TuneCP5_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM",
                    "/TTTo2L2Nu_hdampUP_TuneCP5_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21_ext1-v1/NANOAODSIM",
                    "/TTTo2L2Nu_hdampUP_TuneCP5_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21_ext2-v1/NANOAODSIM",
                ]
            ),
        },
        "TTToHadronic": {
            # UE
            "TuneCP5Down": "/TTToHadronic_TuneCP5down_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM",
            "TuneCP5Up": "/TTToHadronic_TuneCP5up_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM",
            # mtop
            "mtopDown": "/TTToHadronic_mtop169p5_TuneCP5_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM",
            "mtopUp": "/TTToHadronic_mtop175p5_TuneCP5_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM",
            # color reconnection
            "GluonMove": "/TTToHadronic_TuneCP5CR2_GluonMove_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM",
            "QCDbased": "/TTToHadronic_TuneCP5CR1_QCDbased_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM",
            "erdON": "/TTToHadronic_TuneCP5_erdON_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM",
            # hdamp
            "HDampDown": "/TTToHadronic_hdampDOWN_TuneCP5_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM",
            "HDampUp": "/TTToHadronic_hdampUP_TuneCP5_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM",
        },
        "TTToSemiLeptonic": {
            # UE
            "TuneCP5Down": "/TTToSemiLeptonic_TuneCP5down_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM",
            "TuneCP5Up": "/TTToSemiLeptonic_TuneCP5up_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM",
            # mtop
            "mtopDown": dict(
                keys=[
                    "/TTToSemiLeptonic_mtop169p5_TuneCP5_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM",
                    "/TTToSemiLeptonic_mtop169p5_TuneCP5_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21_ext1-v1/NANOAODSIM",
                ]
            ),
            "mtopUp": dict(
                keys=[
                    "/TTToSemiLeptonic_mtop175p5_TuneCP5_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM",
                    "/TTToSemiLeptonic_mtop175p5_TuneCP5_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21_ext1-v1/NANOAODSIM",
                ]
            ),
            # color reconnection
            "GluonMove": "/TTToSemiLeptonic_TuneCP5CR2_GluonMove_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM",
            "QCDbased": "/TTToSemiLeptonic_TuneCP5CR1_QCDbased_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM",
            "erdON": "/TTToSemiLeptonic_TuneCP5_erdON_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM",
            # hdamp
            "HDampDown": "/TTToSemiLeptonic_hdampDOWN_TuneCP5_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM",
            "HDampUp": "/TTToSemiLeptonic_hdampUP_TuneCP5_13TeV-powheg-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM",
        },
        "VBFHHTo2B2Tau_CV_1_C2V_1_C3_1": {
            "dipoleRecoilOn": "/VBFHHTo2B2Tau_CV_1_C2V_1_C3_1_dipoleRecoilOn-TuneCP5_PSweights_13TeV-madgraph-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM"
        },
        "VBFHHTo2B2Tau_CV_1_C2V_2_C3_1": {
            "dipoleRecoilOn": " /VBFHHTo2B2Tau_CV_1_C2V_2_C3_1_dipoleRecoilOn-TuneCP5_PSweights_13TeV-madgraph-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM"
        },
        "VBFHHTo2B2VTo2L2Nu_CV_1_C2V_1_C3_1": {
            "dipoleRecoilOn": "/VBFHHTo2B2VTo2L2Nu_CV_1_C2V_1_C3_1_dipoleRecoilOn-TuneCP5_PSweights_13TeV-madgraph-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM"
        },
        "VBFHHTo2B2VTo2L2Nu_CV_1_C2V_2_C3_1": {
            "dipoleRecoilOn": "/VBFHHTo2B2VTo2L2Nu_CV_1_C2V_2_C3_1_dipoleRecoilOn-TuneCP5_PSweights_13TeV-madgraph-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM"
        },
        "VBFHHTo2B2WToLNu2J_CV_1_C2V_1_C3_1": {
            "dipoleRecoilOn": "/VBFHHTo2B2WToLNu2J_CV_1_C2V_1_C3_1_dipoleRecoilOn-TuneCP5_PSweights_13TeV-madgraph-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM"
        },
        "VBFHHTo2B2WToLNu2J_CV_1_C2V_2_C3_1": {
            "dipoleRecoilOn": "/VBFHHTo2B2WToLNu2J_CV_1_C2V_2_C3_1_dipoleRecoilOn-TuneCP5_PSweights_13TeV-madgraph-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM"
        },
        "VBF_HH_CV_1_C2V_1_C3_1": {
            "dipoleRecoilOn": "/VBF_HH_CV_1_C2V_1_C3_1-TuneCP5_PSweights_13TeV-madgraph-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM",
        },
        "VBF_HH_CV_1_C2V_2_C3_1": {
            "dipoleRecoilOn": "/VBF_HH_CV_1_C2V_2_C3_1-TuneCP5_PSweights_13TeV-madgraph-pythia8/RunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1/NANOAODSIM",
        },
    },
)

# https://twiki.cern.ch/twiki/bin/view/CMS/TWikiLUM#SummaryTable
campaign.aux["lumi"] = 59740.0

campaign.aux["pileup_lfn_scenario_hint"] = dict(new_pmx="new_pmx")

# https://twiki.cern.ch/twiki/bin/viewauth/CMS/PileupJSONFileforData#Pileup_JSON_Files_For_Run_II
campaign.aux["min_bias_xsec"] = sn.Number(69.2, (sn.Number.REL, 0.046))  # mb


# Jec/Jes/Jer

campaign.aux["jes_version"] = {
    "A": "Autumn18_RunA_V19_DATA",
    "B": "Autumn18_RunB_V19_DATA",
    "C": "Autumn18_RunC_V19_DATA",
    "D": "Autumn18_RunD_V19_DATA",
    "mc": "Autumn18_V19_MC",
}

campaign.aux["jes_levels"] = {
    "data": ["L1FastJet", "L2Relative", "L3Absolute", "L2L3Residual"],
    "mc": ["L1FastJet", "L2Relative", "L3Absolute"],
}

# reduced set
campaign.aux["junc_format"] = "RegroupedV2_{version}_UncertaintySources_AK4PFchs"

# JER
campaign.aux["jer_version"] = "Autumn18_V7"
campaign.aux["jer_type"] = "PtResolution"


files = {
    "default": {
        "POG_lumi_file": "/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions18/13TeV/ReReco/Cert_314472-325175_13TeV_17SeptEarlyReReco2018ABC_PromptEraD_Collisions18_JSON.txt",  # https://hypernews.cern.ch/HyperNews/CMS/get/luminosity/749.html
        "POG_pileup_file": "/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions18/13TeV/PileUp/pileup_latest.txt",
        "Tallinn_gf_hh_denom": "https://github.com/HEP-KBFI/hh-bbww/raw/master/data/denom_2018.root",
        **common.files["default"],
    },
    "histo": {
        "electron_sf_files": {  # https://twiki.cern.ch/twiki/bin/view/CMS/EgammaIDRecipesRun2#94X_series_Fall17V2_Scale_factor
            # Standard CMS
            "POG_reco": "/eos/user/d/dnoll/HH/sf/electron/egammaEffi.txt_EGM2D_updatedAll.root",
            "POG_id": "/eos/user/d/dnoll/HH/sf/electron/2018_ElectronMVA90.root",
            # Fakeable stuff + multilepton MVA ID
            # electron identification is done in 3 steps:
            # - nothing to reco / or reco efficiency (reco)
            # - reco to something (id_loose_01) [something is defined arbitrarily due to technical reasons]
            # - something to loose (id_loose_02) [something is defined arbitrarily due to technical reasons]
            # all these should be merged into one SF accoding to https://gitlab.cern.ch/cms-hh-bbww/cms-hh-to-bbww/-/blob/master/Legacy/data_to_mc_corrections.md#lepton-id-sf-uncertainties
            "Tallinn_reco": "/afs/cern.ch/user/v/veelken/public/ttHAnalysis/leptonSF/2018/el_scaleFactors_gsf.root",
            "Tallinn_id_loose_01": "/afs/cern.ch/user/b/balvarez/work/public/ttHAnalysis/TnP_loose_ele_2018.root",
            "Tallinn_id_loose_02": "/afs/cern.ch/user/b/balvarez/work/public/ttHAnalysis/TnP_loosettH_ele_2018.root",
            "Tallinn_tth_tight": "/afs/cern.ch/user/b/balvarez/work/public/ttHAnalysis/TnP_ttH_ele_2018_3l/passttH/egammaEffi.txt_EGM2D.root",
            "Tallinn_tth_tight_error": "/afs/cern.ch/user/k/kaehatah/public/leptonIDSFerror/lepMVAEffSF_e_error_2018.root",
            "Tallinn_tth_relaxed": "/afs/cern.ch/user/k/kaehatah/public/leptonSF_recomp/lepMVAEffSF_e_2018_recomp.root",
        },
        "muon_sf_files": {  # https://twiki.cern.ch/twiki/bin/view/CMS/MuonReferenceEffs2017
            # Standard CMS
            "POG_low_pt_id": "/eos/user/d/dnoll/HH/sf/muon/RunABCD_SF_ID_lowpt_2018.root",
            "POG_iso": "/eos/user/m/mfackeld/sf_2018/RunABCD_SF_ISO_DESY_2018.root",
            "POG_id": "/eos/user/m/mfackeld/sf_2018/RunABCD_SF_ID_DESY_2018.root",
            # Fakeable stuff + multilepton MVA ID
            "Tallinn_idiso_loose": "/afs/cern.ch/user/b/balvarez/work/public/ttHAnalysis/TnP_loose_muon_2018.root",
            "Tallinn_tth_tight": "/afs/cern.ch/user/b/balvarez/work/public/ttHAnalysis/TnP_ttH_muon_2018_3l/passttH/egammaEffi.txt_EGM2D.root",
            "Tallinn_tth_tight_error": "/afs/cern.ch/user/k/kaehatah/public/leptonIDSFerror/lepMVAEffSF_m_error_2018.root",
            "Tallinn_tth_relaxed": "/afs/cern.ch/user/k/kaehatah/public/leptonSF_recomp/lepMVAEffSF_m_2018_recomp.root",
        },
        "trigger_sf_files": {
            # Standard CMS
            "ttHbb_dilepton": "/eos/user/d/dnoll/HH/sf/trigger/tth_dileptonic_2DscaleFactors_MC_PU-lep_SFs_withSysts_2018ABCD_20-09-20_updated_axis_limits.root",
            # Fakeable stuff + multilepton MVA ID
            "Tallinn_dilepton": "/eos/user/m/mfackeld/sf_2018/trigger_sf_2018.root",
        },
        "trigger_sf_files_1d": {
            "Tallinn_sl_electron_1d": "/eos/user/m/mfackeld/sf_2018/Electron_Run2018_Ele32orEle35.root",
            "Tallinn_sl_muon_1d": "/eos/user/m/mfackeld/sf_2018/Muon_Run2018_IsoMu24orIsoMu27.root",
        },
        "jet_pu_id": {
            "POG_eff": "/eos/user/m/mfackeld/sf_2017/effcyPUID_81Xtraining.root",
            "POG_sf": "/eos/user/m/mfackeld/sf_2017/scalefactorsPUID_81Xtraining.root",
        },
        "fake": {
            "Tallinn_fakerates_sl": "/afs/cern.ch/user/v/veelken/public/leptonFR/FR_lep_mva_hh_bbWW_wFullSyst_2018_KBFI_2021Feb3_wCERNUncs2_FRErrTheshold_0p01.root",
            "Tallinn_fakerates_dl": "/afs/cern.ch/user/k/kaehatah/public/leptonFR/FR_lep_mva_hh_multilepton_wFullSyst_2018_KBFI_2020Dec21_wCERNUncs2_FRErrTheshold_0p01.root",
        },
    },
    "btag": {
        "deepjet": {
            "POG_full": "https://twiki.cern.ch/twiki/pub/CMS/BtagRecommendation102X/DeepJet_102XSF_V2.csv",
            "POG_reduced": "https://twiki.cern.ch/twiki/pub/CMS/BtagRecommendation102X/DeepJet_102XSF_V2_JESreduced.csv",  # "/eos/user/m/mfackeld/sf_2018/deepjet_2018.csv",
        },
        "subjet_deepb": "https://twiki.cern.ch/twiki/pub/CMS/BtagRecommendation102X/subjet_DeepCSV_102XSF_V1.csv",
    },
}

# add jec files
files.update(jec_files(campaign))

campaign.aux["files"] = files

# store b-tagger working points
campaign.aux["working_points"] = {
    "deepjet": {
        "loose": 0.0494,
        "medium": 0.2770,
        "tight": 0.7264,
    },
    "subjet_deepb": {
        "loose": 0.1241,
        "medium": 0.4184,
    },
}

# MET filters
campaign.aux["met_filters"] = {
    "data": [
        "goodVertices",
        "globalSuperTightHalo2016Filter",
        "HBHENoiseFilter",
        "HBHENoiseIsoFilter",
        "EcalDeadCellTriggerPrimitiveFilter",
        "BadPFMuonFilter",
        "eeBadScFilter",
    ],
    "mc": [
        "goodVertices",
        "globalSuperTightHalo2016Filter",
        "HBHENoiseFilter",
        "HBHENoiseIsoFilter",
        "EcalDeadCellTriggerPrimitiveFilter",
        "BadPFMuonFilter",
    ],
}

campaign.aux["jes_sources"] = [
    "Absolute",
    "Absolute_2018",
    "BBEC1",
    "BBEC1_2018",
    "EC2",
    "EC2_2018",
    "FlavorQCD",
    "HF",
    "HF_2018",
    "RelativeBal",
    "RelativeSample_2018",
]

campaign.aux["PDFSet"] = "NNPDF31_nnlo_hessian_pdfas"
