#!/usr/bin/env python
# coding: utf-8

"""
Physics processes.
If not stated otherwise, cross sections are given in pb.
Values taken from:
- https://twiki.cern.ch/twiki/bin/view/CMS/StandardModelCrossSectionsat13TeVInclusive?rev=18
- https://twiki.cern.ch/twiki/bin/view/CMS/SummaryTable1G25ns?rev=151
VH (b-enriched):
- CMS AN-2019/229 v6 (p.6-8)
"""

import re
from math import inf

import scinum as sn

import utils.aci as aci
from config.constants import *
from utils.aci import rgb


def renumerate(iterable, start=0, step=1):
    assert step
    for i, v in enumerate(iterable):
        yield start + step * i, v


def find_process_key(keys):
    for key in keys:
        try:
            get(key)
        except ValueError:
            continue
        else:
            return key


# fmt: off

ALL = []

Data = aci.Process(name="data", id=0, is_data=True, label=r"Data", color=(0, 0, 0), processes=[
    aci.Process(name="data_dl", id=1, is_data=True, label=r"Data", processes=[
        aci.Process(name="data_ee", id=3, is_data=True, label=r"Data"),
        aci.Process(name="data_emu", id=4, is_data=True, label=r"Data"),
        aci.Process(name="data_mumu", id=5, is_data=True, label=r"Data"),
    ]),
    aci.Process(name="data_sl", id=2, is_data=True, label=r"Data", processes=[
        aci.Process(name="data_e", id=6, is_data=True, label=r"Data"),
        aci.Process(name="data_mu", id=7, is_data=True, label=r"Data"),
    ]),
    aci.Process(name="data_jet", id=8, is_data=True, label=r"Data"),
    aci.Process(name="data_btag", id=9, is_data=True, label=r"Data"),
])
ALL.append(Data)

# fake processes, emitted by FitDiagnostics
FitdiagnosticsGroup = aci.Process(name="fitdiagnostics_group", processes=[
    aci.Process(
        name="data_err",
        id=990,
        is_data=True,
        aux=dict(style=dict(marker="")),
        label="",
    ),
    aci.Process(name="total", id=991),
    aci.Process(name="total_background", id=992),
    aci.Process(name="total_signal", id=993),
])
ALL.append(FitdiagnosticsGroup)

# fake processes, emitted by PlotSysts
ALL.append(aci.Process(name="sum_background", id=994))

# fmt: off
node2txt = {0: "SM", 14: "box"}
vbf_nlo = [
    ("qqHH_%s" % v.replace("C3_", "kl_"), re.sub(
        "C([^ ]+) (\d+(?: \d+)?)",
        lambda m: f"$c_{{{m[1]}}}={m[2].replace(' ', '.')}$",
        v.replace("_", " ")
    ).replace("$ $", r",\,"), vxs)
    for v, vxs in HHxs["vbf_n3lo_2017_2018"].items()
]
DiHiggs = aci.Process(name="diHiggs", id=1000, label=r"di-Higgs", label_short="HH", processes=[
    aci.Process(name="HH_%s" % decay, id=id, label=dlabel, processes=[
        aci.Process(name="HH_%s_%s" % (decay, prod), id=ip, label=r"%s, %s" % (plabel, dlabel), processes=[
            aci.Process(name="HH_%s_%s_LO" % (decay, prod), id=ip + 1, label=r"%s, %s (LO)" % (plabel, dlabel), processes=[
                aci.Process(
                    name="%sToHHTo%s_node_%s" % (prod, decay, node2txt.get(i, i)),
                    id=ip + 2 + i,
                    label=r"%s, %s (LO) - $%s$" % (plabel, dlabel, node2txt.get(i, "BMS_%d" % i)),
                    xsecs={13: br}, #if i else (br * xs)}, # should be 1pb x BR for fitting
                    aux=dict(gfHHparams=gfHHparams[i]) if i < len(gfHHparams) else None,
                    **({} if i else SMkwargs)
                )
                for i in range(15)
            ]),
            aci.Process(name="HH_%s_%s_NLO" % (decay, prod), id=ip + 20, label=r"%s, %s (NLO)" % (plabel, dlabel), label_short="HH(%s)" % prod, processes=[
                aci.Process(
                    name="%s_%s" % (vname, decay),
                    id=iv,
                    label=r"%s, %s (NLO) - %s" % (plabel, dlabel, vlabel),
                    xsecs={13: br * vxs * k},
                )
                for iv, (vname, vlabel, vxs) in renumerate(vnlo, ip + 21)
            ]),
        ] + [
            aci.Process(name="HH_%s_%s_%s" % (decay, prod, reso), id=ir, processes=[
                aci.Process(
                    name="%sTo%sToHHTo%s_M-%d_narrow" % (prod, reso, decay, mass),
                    id=im,
                    xsecs={13: br},
                )
                for im, mass in renumerate(HHres["masses"], ir + 1)
            ])
            for ir, reso in renumerate(HHres["resonance"], ip + 30, 30)
        ])
        for ip, (prod, plabel, xs, k, vnlo) in renumerate([
            ("GluGlu", r"$gg \rightarrow HH$", HHxs["gf_nnlo_gdocs"][1], 1.0, [
                (
                    "ggHH_kl_%s_kt_1" % ("%.2f" % v).rstrip("0").rstrip(".").replace(".", "p"),
                    r"$\kappa_\lambda = %.2f$" % v,
                    vxs
                ) for v, vxs in HHxs["gf_nnlo_gdocs"].items()
            ]),
            ("VBF", r"$VBF \rightarrow HH$", sn.Number(
                # https://twiki.cern.ch/twiki/bin/view/LHCPhysics/LHCHXSWGHH?rev=58#HHjj_VBF
                1.723e-3,
                {
                    "scale": ("rel", 0.03, 0.04),
                    "pdf_alphas_s": ("rel", 0.021),
                }
            ), 1.0, vbf_nlo),
        ], id + 1, 100)
    ])
    for id, (decay, dlabel, br, SMkwargs) in renumerate([
        ("2B2WToLNu2J", r"$H \rightarrow b\bar{b}$, $H \rightarrow WW$ (SL)", BR_HH_BBWW_SL, dict(color=(244, 67, 54))),
        ("2B2VTo2L2Nu", r"$H \rightarrow b\bar{b}$, $H \rightarrow VV$ (DL)", BR_HH_BBVV_DL, dict()),
        ("2B2Tau", r"$H \rightarrow b\bar{b}$, $H \rightarrow \tau\tau$", BR_HH_BBTAUTAU, dict()),
        ("4B", r"$HH \rightarrow b\bar{b}b\bar{b}$", BR_HH_BBBB, dict())
    ], 1001, 200)
] + [
    aci.Process(name="VBF_HH", id=1990, label=r"$VBF \rightarrow HH$ (NLO)", processes=[
        aci.Process(
            name=vname,
            id=iv,
            label=r"$VBF \rightarrow HH$ (NLO) - %s" % vlabel,
            xsecs={13: sn.Number(vxs)},
        )
        for iv, (vname, vlabel, vxs) in renumerate(vbf_nlo, 1991)
    ])
])
ALL.append(DiHiggs)

# https://twiki.cern.ch/twiki/bin/view/LHCPhysics/CERNYellowReportPageAt13TeV (xsec + uncertainties)
SingleH = aci.Process(name="H", id=9000, label=r"(SM) Higgs", label_short="H", color=rgb(120, 33, 80), processes=[
    aci.Process(name="ggH", id=9100, label=r"ggF (SM) Higgs", xsecs={13: sn.Number(48.58, {"theory_gauss": ("rel", 0.039), "pdf": ("rel", 0.019), "alpha_s": ("rel", 0.026)})}, processes=[
        aci.Process(name="GluGluHToTauTau_M125", id=9101, label=r"$ggH \rightarrow \tau\tau$", xsecs={13: sn.Number(3.046900)}),
        aci.Process(name="GluGluHToMuMu_M_125", id=9102, label=r"$ggH \rightarrow \mu\mu$", xsecs={13: sn.Number(0.010571)}),
        aci.Process(name="GluGluHToBB_M125", id=9103, label=r"$ggH \rightarrow bb$", xsecs={13: sn.Number(28.293000)}),
        aci.Process(name="GluGluHToGG_M125", id=9104, label=r"$ggH \rightarrow gg$", xsecs={13: sn.Number(0.110280)}),
        aci.Process(name="GluGluHToZZTo4L_M125", id=9105, label=r"$ggH \rightarrow ZZ \rightarrow 4\ell$", xsecs={13: sn.Number(0.012970)}),
        aci.Process(name="GluGluHToZZTo2L2Q_M125", id=9106, label=r"$ggH \rightarrow ZZ \rightarrow \ell\ell qq$", xsecs={13: sn.Number(0.179630)}),
        aci.Process(name="GluGluHToWWToLNuQQ_M125_NNPDF31", id=9107, label=r"$ggH \rightarrow WW \rightarrow \ell\nu qq$", xsecs={13: sn.Number(4.562100)}),
        aci.Process(name="GluGluHToWWTo2L2Nu_M125", id=9108, label=r"$ggH \rightarrow WW \rightarrow \ell\ell \nu\nu$", xsecs={13: sn.Number(1.103300)}),
    ]),
    aci.Process(name="VBFH", id=9200, label=r"VBF (SM) Higgs", xsecs={13: sn.Number(3.782, {"scale": ("rel", 0.004, 0.003), "pdf": ("rel", 0.021), "alpha_s": ("rel", 0.005)})}, processes=[
        aci.Process(name="VBFHToTauTau_M125", id=9201, label=r"$VBFH \rightarrow \tau\tau$", xsecs={13: sn.Number(0.237200)}),
        aci.Process(name="VBFHToMuMu_M_125", id=9202, label=r"$VBFH \rightarrow \mu\mu$", xsecs={13: sn.Number(0.000823)}),
        aci.Process(name="VBFHToBB_M_125", id=9203, label=r"$VBFH \rightarrow bb$", xsecs={13: sn.Number(2.202600)}),
        aci.Process(name="VBFHToGG_M125", id=9204, label=r"$VBFH \rightarrow gg$", xsecs={13: sn.Number(0.008585)}),
        aci.Process(name="VBF_HToZZTo4L_M125", id=9205, label=r"$VBFH \rightarrow ZZ \rightarrow 4\e\ell\ell$", xsecs={13: sn.Number(0.001010)}),
        aci.Process(name="VBFHToWWToLNuQQ_M125_NNPDF31", id=9206, label=r"$VBFH \rightarrow WW \rightarrow \ell\nu qq$", xsecs={13: sn.Number(0.355170)}),
        aci.Process(name="VBFHToWWTo2L2Nu_M125", id=9207, label=r"$VBFH \rightarrow WW \rightarrow \ell\ell \nu\nu$", xsecs={13: sn.Number(0.085894)}),
    ]),
    aci.Process(name="VH", id=9300, label=r"VH", processes=[
        aci.Process(name="VHToNonbb_M125", id=9301, label=r"VHToNonbb_M125", xsecs={13: sn.Number(0.942500)}),

        aci.Process(name="ZH", id=9400, label=r"ZH", color=(244, 67, 54), xsecs={13: sn.Number(8.839, {"scale": ("rel", 0.038, 0.031), "pdf": ("rel", 0.013), "alpha_s": ("rel", 0.009)})}, processes=[
            aci.Process(name="ZH_DY", id=9410, label=r"$ZH (DY)",color=(255, 130, 51), processes=[
                aci.Process(name="ZH_bbll", id=9411, label=r"$H \rightarrow b\bar{b}$, $Z \rightarrow \ell\ell$", xsecs={13: sn.Number(0.051980),}),
                aci.Process(name="ZH_bbnunu", id=9412, label=r"$H \rightarrow b\bar{b}$, $Z \rightarrow \nu\nu$", xsecs={13: sn.Number(0.09322),}),
            ]),
            aci.Process(name="ggZH", id=9420, label=r"$gg \rightarrow ZH$", color=(255, 102, 102), processes=[
                aci.Process(name="ggZH_bbll", id=9421, label=r"$gg \rightarrow ZH, H \rightarrow b\bar{b}$, $Z \rightarrow \ell\ell$", xsecs={13: sn.Number(0.00720),}),
                aci.Process(name="ggZH_bbnunu", id=9422, label=r"$gg \rightarrow ZH, H \rightarrow b\bar{b}$, $Z \rightarrow \nu\nu$", xsecs={13: sn.Number(0.01437),}),
            ]),
            aci.Process(name="ZHToTauTau_M125", id=9413, label=r"ZHToTauTau_M125", xsecs={13: sn.Number(0.055440)}),
            aci.Process(name="ZH_HToBB_ZToQQ_M125", id=9414, label=r"$H \rightarrow b\bar{b}$, $Z \rightarrow q\bar{q}$", xsecs={13: sn.Number(0.5242),}), # AN2019_250_v6 & XSDB
        ]),

        aci.Process(name="WH", id=9500, label=r"WH", color=(244, 67, 54), xsecs={13: sn.Number(1.373, {"scale": ("rel", 0.005, 0.007), "pdf": ("rel", 0.017), "alpha_s": ("rel", 0.009)})}, processes=[
            # https://cms-gen-dev.cern.ch/xsdb/?searchQuery=DAS=WplusH_HToBB_WToLNu_M125_13TeV_powheg_pythia8
            #aci.Process(name="WplusH_bblnu", 9501, label=r"$H \rightarrow b\bar{b}$, $W^+ \rightarrow \ell^+\nu$", xsecs={13: sn.Number(0.2791, 0.004461),}),
            aci.Process(name="WplusH_HToBB_WToLNu_M125", id=9502, label=r"$H \rightarrow b\bar{b}$, $W^+ \rightarrow \ell^+\nu$", xsecs={13: sn.Number(0.159500)}),
            # https://cms-gen-dev.cern.ch/xsdb/?columns=67108863&currentPage=0&ordDirection=1&ordFieldName=total_uncertainty&pageSize=10&searchQuery=DAS%3DWminusH_HToBB_WToLNu_M125_13TeV_powheg_pythia8
            #aci.Process(name="WminusH_bblnu", 9503, label=r"$H \rightarrow b\bar{b}$, $W^- \rightarrow \ell^-\nu$", xsecs={13: sn.Number(0.1733, 0.0003644),}),
            aci.Process(name="WminusH_HToBB_WToLNu_M125", id=9504, label=r"$H \rightarrow b\bar{b}$, $W^- \rightarrow \ell^-\nu$", xsecs={13: sn.Number(0.101200)}),
            # # AN2019_250_v6 & XSDB
            aci.Process(name="WplusH_HToBB_WToQQ_M125", id=9505, label=r"$H \rightarrow b\bar{b}$, $W^+ \rightarrow q\bar{q}$", xsecs={13: sn.Number(0.5716)}),
            aci.Process(name="WminusH_HToBB_WToQQ_M125", id=9506, label=r"$H \rightarrow b\bar{b}$, $W^+ \rightarrow q\bar{q}$", xsecs={13: sn.Number(0.3587)}),
        ]),

    ]),
    aci.Process(name="ttH", id=9600,  xsecs={13: sn.Number(0.5071, {"scale": ("rel", 0.058, 0.092), "pdf": ("rel", 0.03), "alpha_s": ("rel", 0.03)})}, processes=[
        aci.Process(name="ttHJetToNonbb_M125", id=9601, label="ttHJetToNonbb_M125", xsecs={13: sn.Number(0.211800)}),
        aci.Process(name="ttHJetTobb_M125", id=9602, label="ttHJetTobb_M125", xsecs={13: sn.Number(0.295300)}),
    ]),
    aci.Process(name="TH", id=9700, processes=[
        aci.Process(name="THQ_ctcvcp_4f_Hincl", id=9701, label="THQ_ctcvcp_4f_Hincl", xsecs={13: sn.Number(0.070960)}),
        aci.Process(name="THW_ctcvcp_5f_Hincl", id=9702, label="THW_ctcvcp_5f_Hincl", xsecs={13: sn.Number(0.015610)}),
    ]),

    aci.Process(name="HZJ_HToWW_M125", id=9801, label="HZJ_HToWW_M125", xsecs={13: sn.Number(0.188900)}),
    aci.Process(name="ttVH", id=9900, label=r"ttVH", color=rgb(170, 170, 100), processes=[
        aci.Process(name="TTWH", id=9901, label=r"TTWH", xsecs={13: sn.Number(0.001582)}),
        aci.Process(name="TTZH", id=9902, label=r"TTZH", xsecs={13: sn.Number(0.001535)}),
    ])
])
ALL.append(SingleH)

ALL.append(aci.Process(name="TTHH", label="TTHH", xsecs={13: sn.Number(0.000000)}))

xs_tt13 = sn.Number(831.76, {"scale": (19.77, 29.20), "pdf": 35.06, "mtop": (23.18, 22.45)}) # 172.5 GeV
TT = aci.Process(name="tt", id=2000, label=r"$t\bar{t}$ + Jets", label_short="TT", color=rgb(117, 171, 79), xsecs={13: xs_tt13}, processes=[
    aci.Process(name="tt_sl", id=2100, label=r"$t\bar{t}$ + Jets, SL", xsecs={13: xs_tt13 * BR_WW_SL}),
    aci.Process(name="tt_dl", id=2200, label=r"$t\bar{t}$ + Jets, DL", xsecs={13: xs_tt13 * BR_WW_DL}),
    aci.Process(name="tt_fh", id=2300, label=r"$t\bar{t}$ + Jets, FH", xsecs={13: xs_tt13 * BR_WW_FH}),
])
del xs_tt13
ALL.append(TT)

# https://twiki.cern.ch/twiki/bin/view/CMS/SummaryTable1G25ns?rev=153#DY_Z
DY = aci.Process(name="dy", id=3000, label=r"Drell-Yan", color=rgb(40, 53, 135), processes=[
    aci.Process(name="dy_lep", id=3100, label=r"Drell-Yan, $Z \rightarrow ll$", processes=[
        # NLO
        aci.Process(name="dy_lep_5To50", id=3101, label=r"Drell-Yan, $5 \leq m_{ll} \leq 50$", xsecs={13: sn.Number(71310, {"scale": 70})}, aux=dict(exclusive="dy_to50")),
        aci.Process(name="dy_lep_10To50", id=3102, label=r"Drell-Yan, $10 \leq m_{ll} \leq 50$", xsecs={13: sn.Number(18610)}, aux=dict(exclusive="dy_to50")),
        aci.Process(name="dy_lep_50ToInf", id=3103, label=r"Drell-Yan, $m_{ll} > 50$", xsecs={13: sn.Number(6077.22, {"integration": 1.49, "pdf": 14.78, "scale": ("rel", 0.02)})}, aux=dict(exclusive="dy_50to")),
        aci.Process(name="DYJetsToLL_M_4to50", id=3110, label=r"Drell-Yan, $4 \leq M \leq 50$", aux=dict(exclusive="dy_to50"), processes=[
            aci.Process(name="DYJetsToLL_M_4to50_HT_70to100", id=3111, label=r"Drell-Yan, HT70to100", xsecs={13: sn.Number(172.340000)}),
            aci.Process(name="DYJetsToLL_M_4to50_HT_100to200", id=3112, label=r"Drell-Yan, HT100to200", xsecs={13: sn.Number(204.0),}),
            aci.Process(name="DYJetsToLL_M_4to50_HT_200to400", id=3113, label=r"Drell-Yan, HT200to400", xsecs={13: sn.Number(54.39),}),
            aci.Process(name="DYJetsToLL_M_4to50_HT_400to600", id=3114, label=r"Drell-Yan, HT400to600", xsecs={13: sn.Number(5.697),}),
            aci.Process(name="DYJetsToLL_M_4to50_HT_600toInf", id=3115, label=r"Drell-Yan, HT600toInf", xsecs={13: sn.Number(1.85),}),
        ]),
        aci.Process(name="dy_lep_nj", id=3120, label=r"Drell-Yan, $n Jets$", color=(76, 175, 80), aux=dict(binned={"Njets"}, exclusive="dy_50to"), processes=[
            aci.Process(name="dy_lep_0j", id=3121, label=r"Drell-Yan, $0 Jets$", xsecs={13: sn.Number(4758.9)}),
            aci.Process(name="dy_lep_1j", id=3122, label=r"Drell-Yan, $1 Jets$", xsecs={13: sn.Number(929.1)}),
            aci.Process(name="dy_lep_2j", id=3123, label=r"Drell-Yan, $2 Jets$", xsecs={13: sn.Number(337.1)}),
        ]),
        # LO
        aci.Process(name="DYJetsToLL_M_50_incl", id=3130, label=r"Drell-Yan, M50", aux=dict(binned={"Vpt"}, exclusive="dy_50to"), xsecs={13: sn.Number(5343.0),}),
        aci.Process(name="DYJetsToLL_M_50_HT", id=3140, label=r"Drell-Yan, M50 HT-binned", aux=dict(binned={"HT", "Vpt"}, exclusive="dy_50to"), processes=[
            aci.Process(name="DYJetsToLL_M_50_HT_70to100", id=3141, label=r"Drell-Yan, M50 HT70to100", xsecs={13: sn.Number(169.9, {"total": 0.5}) * 1.23}),
            aci.Process(name="DYJetsToLL_M_50_HT_100to200", id=3142, label=r"Drell-Yan, M50 HT100to200", xsecs={13: sn.Number(147.40, {"total": 0.09}) * 1.23}),
            aci.Process(name="DYJetsToLL_M_50_HT_200to400", id=3143, label=r"Drell-Yan, M50 HT200to400", xsecs={13: sn.Number(40.99, {"total": 0.04}) * 1.23}),
            aci.Process(name="DYJetsToLL_M_50_HT_400to600", id=3144, label=r"Drell-Yan, M50 HT400to600", xsecs={13: sn.Number(5.678, {"total": 0.005}) * 1.23}),
            aci.Process(name="DYJetsToLL_M_50_HT_600to800", id=3145, label=r"Drell-Yan, M50 HT600to800", xsecs={13: sn.Number(1.367) * 1.23}),
            aci.Process(name="DYJetsToLL_M_50_HT_800to1200", id=3146, label=r"Drell-Yan, M50 HT800to1200", xsecs={13: sn.Number(0.6304) * 1.23}),
            aci.Process(name="DYJetsToLL_M_50_HT_1200to2500", id=3147, label=r"Drell-Yan, M50 HT1200to2500", xsecs={13: sn.Number(0.1514) * 1.23}),
            aci.Process(name="DYJetsToLL_M_50_HT_2500toInf", id=3148, label=r"Drell-Yan, M50 HT2500toInf", xsecs={13: sn.Number(0.003565) * 1.23}),
        ]),
        aci.Process(name="DYnJetsToLL_M_50", id=3150, label=r"Drell-Yan, $n Jets$ M50", aux=dict(binned={"Njets"}, exclusive="dy_50to"), processes=[
            aci.Process(name="DY1JetsToLL_M_50", id=3151, label=r"Drell-Yan, $1 Jets$ M50", xsecs={13: sn.Number(998.61)}),
            aci.Process(name="DY2JetsToLL_M_50", id=3152, label=r"Drell-Yan, $2 Jets$ M50", xsecs={13: sn.Number(349.25)}),
            aci.Process(name="DY3JetsToLL_M_50", id=3153, label=r"Drell-Yan, $3 Jets$ M50", xsecs={13: sn.Number(127.52)}),
            aci.Process(name="DY4JetsToLL_M_50", id=3154, label=r"Drell-Yan, $4 Jets$ M50", xsecs={13: sn.Number(50.039)}),
        ]),
        # ?O
        aci.Process(name="DYJetsToLL_M_5to50", id=3160, label=r"Drell-Yan, $5 \leq M \leq 50$", aux=dict(exclusive="dy_to50"), processes=[
            aci.Process(name="DYJetsToLL_M_5to50_HT_70to100", id=3161, label=r"Drell-Yan, HT70to100", xsecs={13: sn.Number(301.2, {"total": 0.8})}),
            aci.Process(name="DYJetsToLL_M_5to50_HT_100to200", id=3162, label=r"Drell-Yan, HT100to200", xsecs={13: sn.Number(224.2, {"total": 5.7})}),
            aci.Process(name="DYJetsToLL_M_5to50_HT_200to400", id=3163, label=r"Drell-Yan, HT200to400", xsecs={13: sn.Number(37.2, {"total": 1.1})}),
            aci.Process(name="DYJetsToLL_M_5to50_HT_400to600", id=3164, label=r"Drell-Yan, HT400to600", xsecs={13: sn.Number(3.581, {"total": 0.118})}),
            aci.Process(name="DYJetsToLL_M_5to50_HT_600toInf", id=3165, label=r"Drell-Yan, HT600toInf", xsecs={13: sn.Number(1.124, {"total": 0.038})}),
        ]),
        aci.Process(name="DYnJetsToLL_M_10to50", id=3170, label=r"Drell-Yan, $n Jets$ $10 \leq M \leq 50$", aux=dict(exclusive="dy_to50"), processes=[
            aci.Process(name="DY1JetsToLL_M_10to50", id=3171, label=r"Drell-Yan, $1 Jets$ M10To50", xsecs={13: sn.Number(859.680000)}),
            aci.Process(name="DY2JetsToLL_M_10to50", id=3172, label=r"Drell-Yan, $2 Jets$ M10To50", xsecs={13: sn.Number(455.250000)}),
            aci.Process(name="DY3JetsToLL_M_10to50", id=3173, label=r"Drell-Yan, $3 Jets$ M10To50", xsecs={13: sn.Number(111.810000)}),
            aci.Process(name="DY4JetsToLL_M_10to50", id=3174, label=r"Drell-Yan, $4 Jets$ M10To50", xsecs={13: sn.Number(43.120000)}),
        ]),
        aci.Process(name="DYBJetsToLL_M-50", id=3180, label=r"Drell-Yan, $b Jets$ M50", aux=dict(binned={"Vpt", "Njets", "Nb", "nGen2bStatus"}, exclusive="dy_50to"), processes=[
            # xsec (pb): 2018=3.206, 2017=3.224, 2016=3.027; k-factor: 1.23
            aci.Process(name="DYBJetsToLL_M-50_Zpt-100to200", id=3190, label=r"Drell-Yan, $b Jets$ M50 Zpt100to200", processes=[
                aci.Process(name="DYBJetsToLL_M-50_Zpt-100to200__2016", id=3191, xsecs={13: sn.Number(3.027) * 1.23}),
                aci.Process(name="DYBJetsToLL_M-50_Zpt-100to200__2017", id=3192, xsecs={13: sn.Number(3.224) * 1.23}),
                aci.Process(name="DYBJetsToLL_M-50_Zpt-100to200__2018", id=3193, xsecs={13: sn.Number(3.206) * 1.23}),
            ]),
            # xsec (pb): 2018=0.3304, 2017=0.3298, 2016=0.297; k-factor: 1.23
            aci.Process(name="DYBJetsToLL_M-50_Zpt-200toInf", id=3200, label=r"Drell-Yan, $b Jets$ M50 Zpt200toInf", processes=[
                aci.Process(name="DYBJetsToLL_M-50_Zpt-200toInf__2016", id=3201, xsecs={13: sn.Number(0.297) * 1.23}),
                aci.Process(name="DYBJetsToLL_M-50_Zpt-200toInf__2017", id=3202, xsecs={13: sn.Number(0.3298) * 1.23}),
                aci.Process(name="DYBJetsToLL_M-50_Zpt-200toInf__2018", id=3203, xsecs={13: sn.Number(0.3304) * 1.23}),
            ]),
        ]),
        aci.Process(name="DYJetsToLL_BGenFilter_M-50", id=3210, label=r"Drell-Yan, BGenFilter M50", aux=dict(binned={"Vpt", "Njets", "Nb", "nGen2bStatus"}, exclusive="dy_50to"), processes=[
            # xsec (pb): 2018=2.662, 2017=2.671, 2016=3.41; k-factor: 1.23
            aci.Process(name="DYJetsToLL_BGenFilter_Zpt-100to200_M-50", id=3220, label=r"Drell-Yan, BGenFilter M50 Zpt100to200", processes=[
                aci.Process(name="DYJetsToLL_BGenFilter_Zpt-100to200_M-50__2016", id=3221, xsecs={13: sn.Number(3.41) * 1.23}),
                aci.Process(name="DYJetsToLL_BGenFilter_Zpt-100to200_M-50__2017", id=3222, xsecs={13: sn.Number(2.671) * 1.23}),
                aci.Process(name="DYJetsToLL_BGenFilter_Zpt-100to200_M-50__2018", id=3223, xsecs={13: sn.Number(2.662) * 1.23}),
            ]),
            # xsec (pb): 2018=0.3949, 2017=0.3934, 2016=0.5082; k-factor: 1.23
            aci.Process(name="DYJetsToLL_BGenFilter_Zpt-200toInf_M-50", id=3230, label=r"Drell-Yan, BGenFilter M50 Zpt200toInf", processes=[
                aci.Process(name="DYJetsToLL_BGenFilter_Zpt-200toInf_M-50__2016", id=3231, xsecs={13: sn.Number(0.5082) * 1.23}),
                aci.Process(name="DYJetsToLL_BGenFilter_Zpt-200toInf_M-50__2017", id=3232, xsecs={13: sn.Number(0.3934) * 1.23}),
                aci.Process(name="DYJetsToLL_BGenFilter_Zpt-200toInf_M-50__2018", id=3233, xsecs={13: sn.Number(0.3949) * 1.23}),
            ]),
        ]),
    ]),
])
ALL.append(DY)


# z -> nunu + Jets
ZJets = aci.Process(name="zjets_nunu", id=3500, label=r"$Z \rightarrow \nu\nu + jets$", color=rgb(36, 52, 137), processes=[
    aci.Process(name="zjets_nunu_ht", id=3510, label=r"ZJets", processes=[
        aci.Process(name="zjets_nunu_100to200", id=3511, label=r"zjets_nunu_100to200", xsecs={13: sn.Number(304.5)}),
        aci.Process(name="zjets_nunu_200to400", id=3512, label=r"zjets_nunu_200to400", xsecs={13: sn.Number(91.85)}),
        aci.Process(name="zjets_nunu_400to600", id=3513, label=r"zjets_nunu_400to600", xsecs={13: sn.Number(13.11)}),
        aci.Process(name="zjets_nunu_600to800", id=3514, label=r"zjets_nunu_600to800", xsecs={13: sn.Number(3.257)}),
        aci.Process(name="zjets_nunu_800to1200", id=3515, label=r"zjets_nunu_800to1200", xsecs={13: sn.Number(1.499)}),
        aci.Process(name="zjets_nunu_1200to2500", id=3516, label=r"zjets_nunu_1200to2500", xsecs={13: sn.Number(0.343)}),
        aci.Process(name="zjets_nunu_2500toInf", id=3517, label=r"zjets_nunu_2500toInf", xsecs={13: sn.Number(0.005146)}),
    ]),
    aci.Process(name="zbjets_nunu", id=3520, label=r"ZBJets", processes=[
        aci.Process(name="zbjets_nunu_100to200", id=3521, label=r"zbjets_nunu_100to200", xsecs={13: sn.Number(6.209)}),
        aci.Process(name="zbjets_nunu_200toInf", id=3522, label=r"zbjets_nunu_200toInf", xsecs={13: sn.Number(0.6286)}),
    ]),
    aci.Process(name="zjets_nunu_bgen", id=3530, label=r"ZJets bGenFilter", processes=[
        aci.Process(name="ZJetsToNuNu_BGenFilter_Zpt_100to200", id=3531, label=r"zjets_nunu_bGenFilter_100to200", xsecs={13: sn.Number(1.689)}),
        aci.Process(name="ZJetsToNuNu_BGenFilter_Zpt_200toInf", id=3532, label=r"zjets_nunu_bGenFilter_200toInf", xsecs={13: sn.Number(0.2476)}),
    ]),
])
ALL.append(ZJets)


VV = aci.Process(name="vv", id=4000, color=rgb(55, 126, 184), label=r"Di-boson", processes=[
    aci.Process(name="ww", id=4100, label=r"WW", xsecs={13: sn.Number(118.7, {"scale": ("rel", 0.025, 0.022), "pdf": ("rel", 0.046)})}, processes=[
        aci.Process(name="WWTo2L2Nu_NNPDF31", id=4101, label=r"WWTo2L2Nu_NNPDF31", xsecs={13: sn.Number(12.200000)}),
        aci.Process(name="WWTo2L2Nu_DoubleScattering", id=4102, label=r"WWTo2L2Nu_DoubleScattering", xsecs={13: sn.Number(0.223200)}),
        aci.Process(name="WWToLNuQQ_NNPDF31", id=4103, label="WWToLNuQQ_NNPDF31", xsecs={13: sn.Number(50.450000)}),
    ]),
    aci.Process(name="zz", id=4200, label=r"ZZ", processes=[
        aci.Process(name="ZZTo4L", id=4201, label=r"ZZTo4L", xsecs={
            13: sn.Number(1.256),
            # 13: sn.Number(0.0719, {"scale": 0.0023, "pdf": 0.0027}) * ((N_LEPS / BR_Z_CLEP) ** 2 / 2.0),
        }),
        aci.Process(name="ZZTo2L2Q", id=4202, label=r"ZZTo2L2Q", xsecs={13: sn.Number(5.520000)}),
        # from AN2019_250_v6, strange: 2016 listed as 6.912
        aci.Process(name="ZZTo4bQ01j_5f", id=4203, label=r"ZZTo4B", xsecs={13: sn.Number(0.3682)})
    ]),
    aci.Process(name="wz", id=4300, label=r"WZ", processes=[
        aci.Process(name="WZTo3LNu", id=4301, label=r"WZTo3LNu", xsecs={
            13: sn.Number(4.430000),
            # 13: sn.Number.add(
            #     sn.Number(0.106, {"scale": 0.0036, "pdf": 0.0050}),
            #     sn.Number(0.0663, {"scale": 0.0029, "pdf": 0.0032}),
            #     rho=1,
            # ) * (N_LEPS / BR_W_LEP * N_LEPS / BR_Z_CLEP),
        }),
        aci.Process(name="WZTo2L2Q", id=4302, label=r"WZTo2L2Q", xsecs={13: sn.Number(5.600000)}),
    ]),
    aci.Process(name="wg", id=4400, label=r"WG", processes=[
        aci.Process(name="WGToLNuG", id=4401, label=r"WGToLNuG", xsecs={13: sn.Number(464.800000)}),
        aci.Process(name="WGToLNuG_01J_5f", id=4402, label="WGToLNuG_01J_5f", xsecs={13: sn.Number(191.600000)}),
    ]),
    aci.Process(name="zg", id=4500, label=r"ZG", processes=[
        aci.Process(name="ZGToLLG_01J_5f", id=4501, label=r"ZGToLLG_01J_5f", xsecs={13: sn.Number(55.590000)}),
        aci.Process(name="ZGTo2LG", id=4502, label=r"ZGTo2LG", xsecs={13: sn.Number(123.900000)}),
    ]),
])
ALL.append(VV)


# https://twiki.cern.ch/twiki/bin/view/CMS/SummaryTable1G25ns?rev=153#Triboson + xsdb unc.
VVV = aci.Process(name="vvv", id=4600, label=r"Tri-boson", color=rgb(152, 78, 163), processes=[
    aci.Process(name="WWW_4F", id=4601, label=r"WWW", xsecs={13: sn.Number(0.2086, {"total": 0.0002024})}),
    aci.Process(name="WWZ_4F", id=4602, label=r"WWZ", xsecs={13: sn.Number(0.1651, {"total": 0.0001724})}),
    aci.Process(name="WZZ", id=4603, label=r"WZZ", xsecs={13: sn.Number(0.05565, {"total": 5.5e-05})}),
    aci.Process(name="ZZZ", id=4604, label=r"ZZZ", xsecs={13: sn.Number(0.01398, {"total": 1.496e-05})}),
    aci.Process(name="WZG", id=4605, label=r"WZG", xsecs={13: sn.Number(0.04123, {"total": 4.251e-05})}),
])
ALL.append(VVV)


xs_tW = sn.Number(71.7, dict(scale=1.80, pdf=3.40))
ST = aci.Process(name="st", id=5000, color=rgb(244, 216, 44), label=r"Single top", label_short="ST", processes=[
    aci.Process(name="st_t_channel_top", id=5010, label=r"Single top (t-channel)", xsecs={
        13: sn.Number(136.02, dict(
            scale=(4.09, 2.92),
            pdf_alphas_s=(3.52, 3.52),
            total=(5.40, 4.57),
            mass=(1.11, 1.11),
            Ebeam=0.24,
        ))}),
    aci.Process(name="st_t_channel_antitop", id=5020, label=r"Single anti-top (t-channel)", xsecs={
        13: sn.Number(80.95, dict(
            scale=(2.53, 1.71),
            pdf_alphas_s=(3.18, 3.18),
            total=(4.06, 3.61),
            mass=(0.71, 0.70),
            Ebeam=0.16,
        ))}),
    # https://cms-gen-dev.cern.ch/xsdb/?searchQuery=DAS%3DST_s-channel_4f_leptonDecays_13TeV_%2A
    aci.Process(name="st_s_channel", id=5030, label=r"Single top (s-channel)", xsecs={13: sn.Number(3.365, dict(total=0.004584))}),
    aci.Process(name="st_tW_top", id=5040, label=r"tW", xsecs={13: xs_tW / 2}),
    aci.Process(name="st_tW_antitop", id=5050, label=r"$\overline{t}W$", xsecs={13: xs_tW / 2}, processes=[
        # https://cms-gen-dev.cern.ch/xsdb/?searchQuery=DAS=ST_tWll_5f_LO_13TeV-MadGraph-pythia8
        aci.Process(name="ST_tWll_5f_LO", id=5051, label=r"$\overline{t}W \rightarrow ll$", xsecs={13: sn.Number(0.01104, dict(total=5.01e-06))})
    ]),
])
ALL.append(ST)


WJets = aci.Process(name="wjets", id=6000, label=r"W + Jets", color=rgb(199, 21, 133), processes=[
    aci.Process(name="wjets_lep", id=6100, label=r"W + Jets, $W \rightarrow l \nu$", color=(199, 21, 133), processes=[
        # https://twiki.cern.ch/twiki/bin/viewauth/CMS/SummaryTable1G25ns#W_jets
        aci.Process(name="WJetsToLNu", id=6101, label=r"W + Jets, $W \rightarrow l \nu$", aux=dict(exclusive="wjets_lep"), xsecs={
            13: sn.Number(20508.9, {"scale": (165.7, 88.2), "pdf": 770.9}) * N_LEPS
            # 13: sn.Number(61526.7, {"scale": (497.1, 264.6), "pdf": 2312.7})
        }),
        aci.Process(name="WnJetsToLNu", id=6200, label=r"WnJetsToLNu", aux=dict(binned={"Njets"}, exclusive="wjets_lep"), processes=[
            aci.Process(name="W1JetsToLNu", id=6201, label=r"W1JetsToLNu", xsecs={13: sn.Number(9442.490000)}),
            aci.Process(name="W2JetsToLNu", id=6202, label=r"W2JetsToLNu", xsecs={13: sn.Number(3252.490000)}),
            aci.Process(name="W3JetsToLNu", id=6203, label=r"W3JetsToLNu", xsecs={13: sn.Number(1153.420000)}),
            aci.Process(name="W4JetsToLNu", id=6204, label=r"W4JetsToLNu", xsecs={13: sn.Number(634.050000)}),
        ]),
        aci.Process(name="WJetsToLNu_HT", id=6300, label=r"WJetsToLNu_HT", aux=dict(binned={"HT"}, exclusive="wjets_lep"), processes=[
            aci.Process(name="WJetsToLNu_HT_70To100", id=6301, label=r"WJetsToLNu_HT_70To100", xsecs={13: sn.Number(1504.920000)}),
            aci.Process(name="WJetsToLNu_HT_100To200", id=6302, label=r"WJetsToLNu_HT_100To200", xsecs={13: sn.Number(1625.080000)}),
            aci.Process(name="WJetsToLNu_HT_200To400", id=6303, label=r"WJetsToLNu_HT_200To400", xsecs={13: sn.Number(477.960000)}),
            aci.Process(name="WJetsToLNu_HT_400To600", id=6304, label=r"WJetsToLNu_HT_400To600", xsecs={13: sn.Number(67.441000)}),
            aci.Process(name="WJetsToLNu_HT_600To800", id=6305, label=r"WJetsToLNu_HT_600To800", xsecs={13: sn.Number(15.096000)}),
            aci.Process(name="WJetsToLNu_HT_800To1200", id=6306, label=r"WJetsToLNu_HT_800To1200", xsecs={13: sn.Number(6.362600)}),
            aci.Process(name="WJetsToLNu_HT_1200To2500", id=6307, label=r"WJetsToLNu_HT_1200To2500", xsecs={13: sn.Number(1.265800)}),
            aci.Process(name="WJetsToLNu_HT_2500ToInf", id=6308, label=r"WJetsToLNu_HT_2500ToInf", xsecs={13: sn.Number(0.009405)}),
        ]),
        aci.Process(name="WBJetsToLNu_Wpt", id=6400, label=r"WBJetsToLNu_Wpt", aux=dict(binned={"Vpt", "Njets", "Nb", "nGen2bStatus"}, exclusive="wjets_lep"), processes=[
            # xsec (pb): 2018=5.527, 2017=5.542, 2016=5.542 <- missing in VH AN; k-factor: 1.21
            aci.Process(name="WBJetsToLNu_Wpt-100to200", id=6401, label=r"WBJetsToLNu_Wpt 100to200", processes=[
                aci.Process(name="WBJetsToLNu_Wpt-100to200__2016", id=6402, xsecs={13: sn.Number(5.542) * 1.21}),
                aci.Process(name="WBJetsToLNu_Wpt-100to200__2017", id=6403, xsecs={13: sn.Number(5.542) * 1.21}),
                aci.Process(name="WBJetsToLNu_Wpt-100to200__2018", id=6404, xsecs={13: sn.Number(5.527) * 1.21}),
            ]),
            # xsec (pb): 2018=0.7996, 2017=0.801, 2016=0.8524; k-factor: 1.21
            aci.Process(name="WBJetsToLNu_Wpt-200toInf", id=6410, label=r"WBJetsToLNu_Wpt 200toInf", processes=[
                aci.Process(name="WBJetsToLNu_Wpt-200toInf__2016", id=6411, xsecs={13: sn.Number(0.8524) * 1.21}),
                aci.Process(name="WBJetsToLNu_Wpt-200toInf__2017", id=6412, xsecs={13: sn.Number(0.801) * 1.21}),
                aci.Process(name="WBJetsToLNu_Wpt-200toInf__2018", id=6413, xsecs={13: sn.Number(0.7996) * 1.21}),
            ]),
        ]),
        aci.Process(name="WJetsToLNu_BGenFilter_Wpt", id=6500, label=r"WJetsToLNu_Wpt, BGenFilter", aux=dict(binned={"Vpt", "Njets", "Nb", "nGen2bStatus"}, exclusive="wjets_lep"), processes=[
            # xsec (pb): 2018=20.49, 2017=20.56, 2016=26.1; k-factor: 1.21
            aci.Process(name="WJetsToLNu_BGenFilter_Wpt-100to200", id=6501, label=r"WJetsToLNu_Wpt, BGenFilter Wpt100to200", processes=[
                aci.Process(name="WJetsToLNu_BGenFilter_Wpt-100to200__2016", id=6502, xsecs={13: sn.Number(26.1) * 1.21}),
                aci.Process(name="WJetsToLNu_BGenFilter_Wpt-100to200__2017", id=6503, xsecs={13: sn.Number(20.56) * 1.21}),
                aci.Process(name="WJetsToLNu_BGenFilter_Wpt-100to200__2018", id=6504, xsecs={13: sn.Number(20.49) * 1.21}),
            ]),
            # xsec (pb): 2018=2.935, 2017=2.936, 2016=3.545; k-factor: 1.21
            aci.Process(name="WJetsToLNu_BGenFilter_Wpt-200toInf", id=6510, label=r"WJetsToLNu_Wpt, BGenFilter Wpt200toInf", processes=[
                aci.Process(name="WJetsToLNu_BGenFilter_Wpt-200toInf__2016", id=6511, xsecs={13: sn.Number(3.545) * 1.21}),
                aci.Process(name="WJetsToLNu_BGenFilter_Wpt-200toInf__2017", id=6512, xsecs={13: sn.Number(2.936) * 1.21}),
                aci.Process(name="WJetsToLNu_BGenFilter_Wpt-200toInf__2018", id=6513, xsecs={13: sn.Number(2.935) * 1.21}),
            ]),
        ]),
        # https://gitlab.cern.ch/cms-hh-bbww/cms-hh-to-bbww/-/blob/master/Legacy/datasets.md#510-nlo-wjets-samples
        aci.Process(name="WJetsToLNu_nJ", id=6600, label=r"WJetsToLNu_nJ", aux=dict(binned={"Njets"}, exclusive="wjets_lep"), processes=[
            aci.Process(name="WJetsToLNu_0J", id=6601, label=r"WJetsToLNu_0J", xsecs={13: sn.Number(50062)}),
            aci.Process(name="WJetsToLNu_1J", id=6602, label=r"WJetsToLNu_1J", xsecs={13: sn.Number(8331)}),
            aci.Process(name="WJetsToLNu_2J", id=6603, label=r"WJetsToLNu_2J", xsecs={13: sn.Number(3133)}),
        ]),
    ]),
])
ALL.append(WJets)


# https://arxiv.org/pdf/1610.07922.pdf (page 160, table 40)
TTV = aci.Process(name="ttV", id=7000, label=r"ttV", color=rgb(150, 150, 20), processes=[
    aci.Process(name="ttW", id=7100, label=r"ttW", processes=[
        aci.Process(name="TTWJetsToLNu", id=7101, label=r"tt W+jets (l nu)", xsecs={13: sn.Number(0.196000, {"scale": ("rel", 0.129, 0.115), "pdf": ("rel", 0.02, 0.02), "alpha_s": ("rel", 0.027, 0.027)})}),
        aci.Process(name="TTWJetsToQQ", id=7102, label=r"tt W+jets (2q)", xsecs={13: sn.Number(0.404900, {"scale": ("rel", 0.129, 0.115), "pdf": ("rel", 0.02, 0.02), "alpha_s": ("rel", 0.027, 0.027)})}),
    ]),
    aci.Process(name="ttZ", id=7200, label=r"ttZ", processes=[
        aci.Process(name="TTZToLL_M_1to10", id=7201, label=r"TTZToLL_M_1to10", xsecs={13: sn.Number(0.082200, {"scale": ("rel", 0.096, 0.113), "pdf": ("rel", 0.028, 0.028), "alpha_s": ("rel", 0.0278, 0.028)})}),
        aci.Process(name="TTZToLLNuNu_M_10", id=7203, label=r"tt Z (2l 2nu)", xsecs={13: sn.Number(0.281400, {"scale": ("rel", 0.096, 0.113), "pdf": ("rel", 0.028, 0.028), "alpha_s": ("rel", 0.0278, 0.028)})}),
        aci.Process(name="TTZToQQ", id=7204, label=r"tt Z (2q)", xsecs={13: sn.Number(0.586800, {"scale": ("rel", 0.096, 0.113), "pdf": ("rel", 0.028, 0.028), "alpha_s": ("rel", 0.0278, 0.028)})}),

    ]),
])
ALL.append(TTV)


TTVV = aci.Process(name="ttVV", id=7300, label=r"ttVV", color=rgb(200, 200, 20), processes=[
    aci.Process(name="TTWW", id=7301, label=r"TTWW", xsecs={13: sn.Number(0.006981)}),
])
ALL.append(TTVV)


# https://cms-gen-dev.cern.ch/xsdb/?pageSize=50&searchQuery=DAS%3DQCD_HT%5B%5E_%5D%2Bto%5B%5E_%5D%2B_TuneCUETP8M1_13TeV-madgraphMLM-pythia8
# use RunIISummer16MiniAODv2 values
QCD = aci.Process(name="QCD", id=8000, label=r"QCD", color=rgb(0, 102, 102), processes=[
    aci.Process(name="QCD_HT50to100", id=8001, label=r"QCD HT 50-100", xsecs={13: sn.Number(246300000.0)}),
    aci.Process(name="QCD_HT100to200", id=8002, label=r"QCD HT 100-200", xsecs={13: sn.Number(28060000.0)}),
    aci.Process(name="QCD_HT200to300", id=8003, label=r"QCD HT 200-300", xsecs={13: sn.Number(1710000.0)}),
    aci.Process(name="QCD_HT300to500", id=8004, label=r"QCD HT 300-500", xsecs={13: sn.Number(347500.0)}),
    aci.Process(name="QCD_HT500to700", id=8005, label=r"QCD HT 500-700", xsecs={13: sn.Number(32060.0)}),
    aci.Process(name="QCD_HT700to1000", id=8006, label=r"QCD HT 700-1000", xsecs={13: sn.Number(6829.0)}),
    aci.Process(name="QCD_HT1000to1500", id=8007, label=r"QCD HT 1000-1500", xsecs={13: sn.Number(1207.0)}),
    aci.Process(name="QCD_HT1500to2000", id=8008, label=r"QCD HT 1500-2000", xsecs={13: sn.Number(120.0)}),
    aci.Process(name="QCD_HT2000toInf", id=8009, label=r"QCD HT 2000-Inf", xsecs={13: sn.Number(25.25)}),
])
ALL.append(QCD)


QCD_BGenFilter = aci.Process(name="QCD_BGenFilter", id=8100, label=r"QCD BGenFilter", processes=[
    # https://cms-gen-dev.cern.ch/xsdb/?searchQuery=DAS%3DQCD_HT%5B%5E_%5D%2Bto%5B%5E_%5D%2B_BGenFilter_TuneCP5_13TeV-madgraph-pythia8
    aci.Process(name="QCD_HT100to200_BGenFilter", id=8101, label="QCD_HT100to200_BGenFilter", xsecs={13: sn.Number(1275000.0)}),
    # https://cms-gen-dev.cern.ch/xsdb/?searchQuery=DAS%3DQCD_HT[^_]%2Bto[^_]%2B_BGenFilter_TuneCUETP8M1_13TeV-madgraphMLM-pythia8
    aci.Process(name="QCD_HT200to300_BGenFilter", id=8102, label="QCD_HT200to300_BGenFilter", xsecs={13: sn.Number(154400.0)}),
    aci.Process(name="QCD_HT300to500_BGenFilter", id=8103, label="QCD_HT300to500_BGenFilter", xsecs={13: sn.Number(37680.0)}),
    aci.Process(name="QCD_HT500to700_BGenFilter", id=8104, label="QCD_HT500to700_BGenFilter", xsecs={13: sn.Number(4141.0)}),
    aci.Process(name="QCD_HT700to1000_BGenFilter", id=8105, label="QCD_HT700to1000_BGenFilter", xsecs={13: sn.Number(978.4)}),
    aci.Process(name="QCD_HT1000to1500_BGenFilter", id=8106, label="QCD_HT1000to1500_BGenFilter", xsecs={13: sn.Number(189.4)}),
    aci.Process(name="QCD_HT1500to2000_BGenFilter", id=8107, label="QCD_HT1500to2000_BGenFilter", xsecs={13: sn.Number(20.35)}),
    aci.Process(name="QCD_HT2000toInf_BGenFilter", id=8108, label="QCD_HT2000toInf_BGenFilter", xsecs={13: sn.Number(4.463)}),
])
ALL.append(QCD_BGenFilter)


# https://cms-gen-dev.cern.ch/xsdb/?searchQuery=DAS%3DQCD_Pt-%5B%5E_%5D%2Bto%5B%5E_%5D%2B_EMEnriched_TuneCUETP8M1_13TeV_pythia8
QCD_EMEnriched=aci.Process(name="QCD_EMEnriched", id=8200, label=r"QCD EMEnriched", processes=[
    aci.Process(name="QCD_Pt_15to20_EMEnriched", id=9999, label="QCD_Pt_15to20_EMEnriched", xsecs={13: sn.Number(1601000.0)}),
    aci.Process(name="QCD_Pt_20to30_EMEnriched", id=8201, label="QCD_Pt_20to30_EMEnriched", xsecs={13: sn.Number(5533000.0)}),
    aci.Process(name="QCD_Pt_30to50_EMEnriched", id=8202, label="QCD_Pt_30to50_EMEnriched", xsecs={13: sn.Number(6953000.0)}),
    aci.Process(name="QCD_Pt_50to80_EMEnriched", id=8203, label="QCD_Pt_50to80_EMEnriched", xsecs={13: sn.Number(0.0)}),
    aci.Process(name="QCD_Pt_80to120_EMEnriched", id=8204, label="QCD_Pt_80to120_EMEnriched", xsecs={13: sn.Number(417400.0)}),
    aci.Process(name="QCD_Pt_120to170_EMEnriched", id=8205, label="QCD_Pt_120to170_EMEnriched", xsecs={13: sn.Number(75840.0)}),
    aci.Process(name="QCD_Pt_170to300_EMEnriched", id=8206, label="QCD_Pt_170to300_EMEnriched", xsecs={13: sn.Number(18830.0)}),
    aci.Process(name="QCD_Pt_300toInf_EMEnriched", id=8207, label="QCD_Pt_300toInf_EMEnriched", xsecs={13: sn.Number(1221.0)}),
])
ALL.append(QCD_EMEnriched)


# https://cms-gen-dev.cern.ch/xsdb/?searchQuery=DAS%3DQCD_Pt-%5B%5E_%5D%2Bto%5B%5E_%5D%2B_MuEnrichedPt5_TuneCUETP8M1_13TeV_pythia8
QCD_MuEnrichedPt5 = aci.Process(name="QCD_MuEnrichedPt5", id=8300, label=r"QCD MuEnrichedPt5", processes=[
    aci.Process(name="QCD_Pt_15to20_MuEnrichedPt5", id=8301, label="QCD_Pt_15to20_MuEnrichedPt5", xsecs={13: sn.Number(3616000.0)}),
    aci.Process(name="QCD_Pt_20to30_MuEnrichedPt5", id=8302, label="QCD_Pt_20to30_MuEnrichedPt5", xsecs={13: sn.Number(3160000.0)}),
    aci.Process(name="QCD_Pt_30to50_MuEnrichedPt5", id=8303, label="QCD_Pt_30to50_MuEnrichedPt5", xsecs={13: sn.Number(1662000.0)}),
    aci.Process(name="QCD_Pt_50to80_MuEnrichedPt5", id=8304, label="QCD_Pt_50to80_MuEnrichedPt5", xsecs={13: sn.Number(452200.0)}),
    aci.Process(name="QCD_Pt_80to120_MuEnrichedPt5", id=8305, label="QCD_Pt_80to120_MuEnrichedPt5", xsecs={13: sn.Number(106500.0)}),
    aci.Process(name="QCD_Pt_120to170_MuEnrichedPt5", id=8306, label="QCD_Pt_120to170_MuEnrichedPt5", xsecs={13: sn.Number(25700.0)}),
    aci.Process(name="QCD_Pt_170to300_MuEnrichedPt5", id=8307, label="QCD_Pt_170to300_MuEnrichedPt5", xsecs={13: sn.Number(8683.0)}),
    aci.Process(name="QCD_Pt_300to470_MuEnrichedPt5", id=8308, label="QCD_Pt_300to470_MuEnrichedPt5", xsecs={13: sn.Number(797.3)}),
    aci.Process(name="QCD_Pt_470to600_MuEnrichedPt5", id=8309, label="QCD_Pt_470to600_MuEnrichedPt5", xsecs={13: sn.Number(79.25)}),
    aci.Process(name="QCD_Pt_600to800_MuEnrichedPt5", id=8310, label="QCD_Pt_600to800_MuEnrichedPt5", xsecs={13: sn.Number(25.25)}),
    aci.Process(name="QCD_Pt_800to1000_MuEnrichedPt5", id=8311, label="QCD_Pt_800to1000_MuEnrichedPt5", xsecs={13: sn.Number(4.723)}),
    aci.Process(name="QCD_Pt_1000toInf_MuEnrichedPt5", id=8312, label="QCD_Pt_1000toInf_MuEnrichedPt5", xsecs={13: sn.Number(1.613)}),
])
ALL.append(QCD_MuEnrichedPt5)


Rare = aci.Process(name="rare", id=20000, label=r"Rare", color=rgb(255, 140, 0), processes=[
    aci.Process(name="TTGJets", id=20001, label=r"TTGJets", xsecs={13: sn.Number(4.215000)}),
    aci.Process(name="TGJets_leptonDecays", id=20002, label=r"TGJets_leptonDecays", xsecs={13: sn.Number(1.018000)}),
    aci.Process(name="tZq_ll_4f", id=20003, label=r"tZq_ll_4f", xsecs={13: sn.Number(0.073580)}),
    aci.Process(name="TTTT", id=20004, label=r"TTTT", xsecs={13: sn.Number(0.008213)}),
    aci.Process(name="WpWpJJ_EWK_QCD", id=20005, label=r"WpWpJJ_EWK_QCD", xsecs={13: sn.Number(0.049260)}),
])
ALL.append(Rare)

# fmt: on


if __name__ == "__main__":
    from argparse import ArgumentParser

    ap = ArgumentParser(description="list process in the config")
    ap.add_argument("-l", "--label", action="store_true")
    ap.add_argument("-s", "--short", "--short-label", action="store_true")

    args = ap.parse_args()

    for process in ALL:
        for depth, proc in process.walk_processes(include_self=True):
            prefix = ("" if depth else "\n") + "| " * depth + "> "
            if args.label:
                value = proc.label
            elif args.short:
                value = proc.label_short
            else:
                value = proc.xsecs.get(13, "-")
            print("%-50s %s" % (prefix + proc.name, value))
