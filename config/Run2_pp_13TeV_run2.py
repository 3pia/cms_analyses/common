#!/usr/bin/env python
# coding: utf-8
# flake8: noqa

"""
Defintion of the campaign and datasets for CMS.
"""

import utils.aci as aci

# campaign
campaign = aci.Campaign("Run2_pp_13TeV_run2", 3, ecm=13)

# https://twiki.cern.ch/twiki/bin/view/CMS/TWikiLUM#SummaryTable
campaign.aux["lumi"] = 137580.0
campaign.aux["year"] = "Run2"
