# coding: utf-8

# comment out the following line when: https://github.com/riga/law/pull/112 and https://github.com/riga/law/pull/113 are merged
# and rich logging should be used in favor of standard law logging
# import law

# law.contrib.load("rich")
# law.rich.replace_console_handlers(show_time=False)

import atexit
import warnings

import line_profiler

tprofile = line_profiler.LineProfiler()

import memory_profiler

mprofile = memory_profiler.LineProfiler()

# use MALLOC_MMAP_THRESHOLD_=16384 do get proper memory readings


def dump_stats():
    if line_profiler and tprofile.get_stats().timings:
        tprofile.print_stats(output_unit=1e-3, stripzeros=True)
    if memory_profiler and mprofile.code_map:
        memory_profiler.show_results(mprofile)


atexit.register(dump_stats)

__builtins__["tprofile"] = tprofile
__builtins__["mprofile"] = mprofile


import order as od

od.Dataset._instances.soft_checking = True
od.Variable._instances.soft_checking = True
od.Category._instances.soft_checking = True
od.Config._instances.soft_checking = True
od.Channel._instances.soft_checking = True

warnings.filterwarnings("ignore", message=r".*already exists in the uniqueness context.*")


import matplotlib.pyplot as plt

plt.rcParams.update({"font.size": 12})
