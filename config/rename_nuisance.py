# coding: utf-8

"""
This files only purpose is to be copied to lxplus and used with inference tools for pretty Pulls & Impacts nuisance label plots!
"""

import re

final_state_map = {"dl": "DL", "sl": "SL"}

categories_sl = {
    "all_incl_sr_prompt_wjets_other": "incl. W+Jets + Other",
    "all_incl_sr_prompt_dnn_node_wjets": "incl. W+Jets",
    "all_incl_sr_prompt_dnn_node_class_other": "incl. Other",
    "all_boosted_sr_prompt_dnn_node_class_HHGluGlu_NLO": "boost. HH(GGF)",
    "all_boosted_sr_prompt_dnn_node_class_HHVBF_NLO": "boost. HH(VBF)",
    "all_boosted_sr_prompt_tophiggs": "boost. Top + Higgs",
    "all_resolved_sr_prompt_tophiggs": "res. #geq 1b Top+Higgs",
    "all_resolved_1b_sr_prompt_dnn_node_class_HHGluGlu_NLO": "res. = 1b HH(GGF)",
    "all_resolved_1b_sr_prompt_dnn_node_class_HHVBF_NLO": "res. = 1b HH(VBF)",
    "all_resolved_2b_sr_prompt_dnn_node_class_HHGluGlu_NLO": "res. #geq 2b HH(GGF)",
    "all_resolved_2b_sr_prompt_dnn_node_class_HHVBF_NLO": "res. #geq 2b HH(VBF)",
}

categories_dl = {
    "all_incl_sr_grp_dy_vv": "incl. DY + Multiboson",
    "all_boosted_1b_sr_dnn_node_class_HHGluGlu_NLO": "boost. HH(GGF)",
    "all_boosted_1b_sr_dnn_node_class_HHVBF_NLO": "boost. HH(VBF)",
    "all_boosted_1b_sr_grp_other_top": "boost. Top + Other",
    "all_resolved_sr_grp_other_top": "res. #geq 1b Top + Other",
    "all_resolved_1b_sr_dnn_node_class_HHGluGlu_NLO": "res. = 1b HH(GGF)",
    "all_resolved_1b_sr_dnn_node_class_HHVBF_NLO": "res. = 1b HH(VBF)",
    "all_resolved_2b_sr_dnn_node_class_HHGluGlu_NLO": "res. #geq 2b HH(GGF)",
    "all_resolved_2b_sr_dnn_node_class_HHVBF_NLO": "res. #geq 2b HH(VBF)",
}


categories_map = categories_sl.copy()
categories_map.update(categories_dl)

higgs_procs = {
    "hww": "H #rightarrow WW",
    "hzz": "H #rightarrow ZZ",
    "hbb": "H #rightarrow bb",
    "htt": "H #rightarrow #tau#tau",
    "ggH_hbb": "H #rightarrow bb (ggF)",
    "qqH_hbb": "H #rightarrow bb (VBF)",
    "WH_hbb": "WH #rightarrow bb",
    "ZH_hbb": "ZH #rightarrow bb",
    "ttH_hbb": "ttH #rightarrow bb",
    "ggH_hww": "H #rightarrow WW (ggF)",
    "qqH_hww": "H #rightarrow WW (VBF)",
}

lep_channels = {
    "e": "e",
    "m": "#mu",
    "mu": "#mu",
    "ee": "ee",
    "mm": "#mu#mu",
    "mumu": "#mu#mu",
    "emu": "e#mu",
}


def rename_nuisance(name):
    # qqHH_pythiaDipoleOn
    if name == "qqHH_pythiaDipoleOn":
        return "HH (VBF) dipole-recoil"

    # UE
    if name == "underlying_event":
        return "Underlying event"

    # HDamp
    if name == "hdamp":
        return "ME - PS matching scale"

    # CR
    if name in ("GluonMove", "QCDbased", "erdON"):
        return "Color reconnection ({})".format(name)

    # top pt reweighting
    if name == "CMS_top_pT_reweighting":
        return r"Top p#scale[0.6]{T} reweighting"

    # total_VVV
    if name == "total_VVV":
        return "VVV xsec unc. (total)"

    # integration_dy
    if name == "integration_dy":
        return "DY xsec unc. (integration)"

    # alpha_s
    if name == "alpha_s":
        return r"Strong coupling constant (#alpha#scale[0.6]{s})"

    # m_top_unc_tt
    if name == "m_top_unc_tt":
        return "Top mass unc."

    # EW corrections
    # e.g.: EW_corr_ttZ
    m = re.match(r"EW_corr_(.+)", name)
    if m:
        return "EWK corrections ({})".format(m.group(1))

    # mc stats?
    m = re.match(r"^prop_binbbww_(.+)__(201[0-9])_(.+)_bin(\d{1,3})(|_(.+))$", name)
    if m:
        final_state = final_state_map[m.group(1)]
        year = int(m.group(2))
        cat = categories_map.get(m.group(3), m.group(3))
        b = int(m.group(4))
        proc = m.group(5) or ""
        if proc.startswith("_"):
            proc = proc[1:]
        if proc:
            # simplify HH process names
            m_bbtt_ggf = re.match("^ggHH_kl_([^_]+)_kt_([^_]+)(_hbbhtt)?$", proc)
            m_bbtt_vbf = re.match("^qqHH_CV_([^_]+)_C2V_([^_]+)_(kl|C3)_([^_]+)(_hbbhtt)?$", proc)
            m_bbvv_ggf = re.match("^ggHH_kl_([^_]+)_kt_([^_]+)(_hbbhww)?$", proc)
            m_bbvv_vbf = re.match("^qqHH_CV_([^_]+)_C2V_([^_]+)_(kl|C3)_([^_]+)(_hbbhww)?$", proc)
            if m_bbtt_ggf:
                proc = "ggHH (bb#tau#tau)#scale[0.6]{{ (kl={}, kt={})}}".format(
                    m_bbtt_ggf.group(1), m_bbtt_ggf.group(2)
                )
            elif m_bbtt_vbf:
                proc = "qqHH (bb#tau#tau)#scale[0.6]{{ (k2V={}, kV={}, kl={})}}".format(
                    m_bbtt_vbf.group(2), m_bbtt_vbf.group(1), m_bbtt_vbf.group(4)
                )
            elif m_bbvv_ggf:
                proc = "ggHH (bbVV)#scale[0.6]{{ (kl={}, kt={})}}".format(
                    m_bbvv_ggf.group(1), m_bbvv_ggf.group(2)
                )
            elif m_bbvv_vbf:
                proc = "qqHH (bbVV)#scale[0.6]{{ (k2V={}, kV={}, kl={})}}".format(
                    m_bbvv_vbf.group(2), m_bbvv_vbf.group(1), m_bbvv_vbf.group(4)
                )
            proc = higgs_procs.get(proc, proc)
            if proc == "Other_bbWW":
                proc = "Other"
            proc += " "
        return "MC stat, {} {} {}, {}bin {}".format(
            final_state,
            year,
            cat,
            proc,
            b + 1,
        )

    # Lumi
    # e.g.: lumi_13TeV_correlated
    m = re.match(r"^lumi_13TeV_(.+)$", name)
    if m:
        year = {"1718": "2017 & 2018", "correlated": "Run 2"}.get(m.group(1), m.group(1))
        return "Luminosity {}".format(year)

    # pileup
    # e.g.: CMS_pileup_2017
    m = re.match(r"^CMS_pileup_(201[0-9])$", name)
    if m:
        return "Pileup {}".format(int(m.group(1)))

    if name == "CMS_pileup":
        return "Pileup"

    # BR
    # e.g.: BR_hww
    m = re.match(r"^BR_(.+)$", name)
    if m:
        proc = higgs_procs[m.group(1)]
        return "BR ({})".format(proc)

    # JES
    # e.g.: CMS_scale_j_RelBal
    m = re.match(r"^CMS_scale_j_(.+)$", name)
    if m:
        m1 = re.match(r"^(.+)_(201[0-9])$", m.group(1))
        if m1:
            return "JES ({}, {})".format(m1.group(1), int(m1.group(2)))
        else:
            return "JES ({})".format(m.group(1))

    if name == "CMS_HEM_2018":
        return "JES (HEM, 2018)"

    # JER
    # e.g.: CMS_res_j_2016
    m = re.match(r"^CMS_res_j_(201[0-9])$", name)
    if m:
        return "JER {}".format(m.group(1))

    # PU Jet ID
    # e.g.: CMS_eff_j_PUJET_mistag_2017
    m = re.match(r"^CMS_eff_j_PUJET_(mistag|id)_(201[0-9])$", name)
    if m:
        syst = {"mistag": "mistag", "id": "ID"}.get(m.group(1))
        year = int(m.group(2))
        return "Pileup Jet ({}) {}".format(syst, year)

    # unclustered energy
    # e.g.: CMS_unclusteredEnergy_2017
    m = re.match(r"^CMS_unclusteredEnergy_(201[0-9])$", name)
    if m:
        return "Unclustered energy (MET) {}".format(int(m.group(1)))

    # B-tagging
    # e.g.: CMS_btag_LF_2016_2017_2018
    m = re.match(
        r"^CMS_btag_(LF|HF|lfstats1|lfstats2|hfstats1|hfstats2|subjet|cferr1|cferr2)_(.+)$", name
    )
    if m:
        syst = {
            "LF": "light flav.",
            "HF": "heavy flav.",
            "lfstats1": "light flav. stats 1",
            "lfstats2": "light flav. stats 2",
            "hfstats1": "heavy flav. stats 1",
            "hfstats2": "heavy flav. stats 2",
            "cferr1": "charm flav. err 1",
            "cferr2": "charm flav. err 2",
        }.get(m.group(1), m.group(1))
        year = {"2016_2017_2018": "Run 2"}.get(m.group(2), m.group(2))
        return "B-tagging ({}) {}".format(syst, year)
    # ad-hoc fix, forgot to properly rename on datacard level for SL 2018...
    if name == "btagWeight_subjet":
        return "B-tagging (subjet) 2018"

    # DY estimation?
    # NCC
    # Signal category lnNs
    # e.g.: CMS_bbww_dl_DY_all_resolved_1b_sr_dnn_node_class_HHVBF_NLO_2016
    m = re.match(r"^CMS_bbww_dl_DY_(all.+)_(201[0-9])$", name)
    if m:
        cat = categories_dl[m.group(1)]
        year = int(m.group(2))
        return "DY est. ncc, {} {}".format(cat, year)

    # Background categories
    # e.g.: CMS_bbww_dl_DY_ncc3_all_incl_sr_grp_dy_vv_2016
    m = re.match(r"^CMS_bbww_dl_DY_ncc([0-9])_(.+)_(201[0-9])$", name)
    if m:
        p = m.group(1)
        cat = categories_dl[m.group(2)]
        year = int(m.group(3))
        return "DY est. ncc ({}), {} {}".format(p, cat, year)

    # DY estimation uncertainties:
    # e.g.: CMS_bbww_dl_DY_resolved1b_2016
    m = re.match(r"^CMS_bbww_dl_DY_(resolved1b|resolved2b|boost)_(201[0-9])$", name)
    if m:
        cats = {"resolved1b": "res. = 1b", "resolved2b": "res. #geq 2b", "boost": "boost."}
        cat = cats[m.group(1)]
        year = int(m.group(2))
        return "DY est., {} {}".format(cat, year)

    # Fakes
    # e.g.: CMS_bbww_dl_FakeRate_e_nonclosure_2016
    m = re.match(r"^CMS_bbww_(dl|sl)_FakeRate_(e|m|mu)_(.+)_(201[0-9])$", name)
    if m:
        ana = final_state_map[m.group(1)]
        lep = lep_channels[m.group(2)]
        if lep == "e":
            syst = {
                "BVVL": r"Barrel p#scale[0.6]{T} < 25",
                "BVL": r"Barrel 25 < p#scale[0.6]{T} < 35",
                "BL": r"Barrel 35 < p#scale[0.6]{T} < 45",
                "BM": r"Barrel 45 < p#scale[0.6]{T} < 65",
                "BH": r"Barrel p#scale[0.6]{T} > 65",
                "EVVL": r"Endcap p#scale[0.6]{T} < 25",
                "EVL": r"Endcap 25 < p#scale[0.6]{T} < 35",
                "EL": r"Endcap 35 < p#scale[0.6]{T} < 45",
                "EM": r"Endcap 45 < p#scale[0.6]{T} < 65",
                "EH": r"Endcap p#scale[0.6]{T} > 65",
            }

        else:
            syst = {
                "BVVVL": r"Barrel p#scale[0.6]{T} < 15",
                "BVVL": r"Barrel 15 < p#scale[0.6]{T} < 20",
                "BVL": r"Barrel 20 < p#scale[0.6]{T} < 32",
                "BL": r"Barrel 32 < p#scale[0.6]{T} < 45",
                "BM": r"Barrel 45 < p#scale[0.6]{T} < 65",
                "BH": r"Barrel p#scale[0.6]{T} > 65",
                "EVVVL": r"Endcap p#scale[0.6]{T} < 15",
                "EVVL": r"Endcap 15 < p#scale[0.6]{T} < 20",
                "EVL": r"Endcap 20 < p#scale[0.6]{T} < 32",
                "EL": r"Endcap 32 < p#scale[0.6]{T} < 45",
                "EM": r"Endcap 45 < p#scale[0.6]{T} < 65",
                "EH": r"Endcap p#scale[0.6]{T} > 65",
            }

        syst = syst.get(m.group(3), m.group(3))
        year = int(m.group(4))
        return "Fakes est. ({}) {}, {} {}".format(ana, lep, syst, year)

    # NCC
    # e.g.: CMS_bbww_sl_FakeRate_nc_nom_mu_all_resolved_2b_sr_prompt_dnn_node_class_HHVBF_NLO_2017
    m = re.match(r"^CMS_bbww_sl_FakeRate_nc_(nom|slope)_(e|m|mu)_(.+)_(201[0-9])$", name)
    if m:
        kind = {"nom": "nom."}.get(m.group(1), m.group(1))
        lep = lep_channels[m.group(2)]
        cat = categories_sl[m.group(3)]
        year = int(m.group(4))
        return "Fakes est. {} ncc (SL) {}, {} {}".format(kind, lep, cat, year)

    # lepton effs.
    # e.g.: CMS_eff_e_id_loose_2017
    m = re.match(r"^CMS_eff_(e|m|mu)_id_(loose|tth|tight)_(201[0-9])$", name)
    if m:
        lep = lep_channels[m.group(1)]
        wp = {"loose": "loose", "tth": "ttH", "tight": "tight"}[m.group(2)]
        year = int(m.group(3))
        return "Lepton ({}) eff. ({}) {}".format(lep, wp, year)

    # Trigger effs.
    # e.g.: CMS_bbww_sl_TriggerWeight_e_2017
    m = re.match(r"^CMS_bbww_(sl|dl)_TriggerWeight_(e|m|mu|ee|mm|mumu|emu)_(201[0-9])$", name)
    if m:
        ana = final_state_map[m.group(1)]
        lep = lep_channels[m.group(2)]
        year = int(m.group(3))
        return "Trigger eff. ({}) {} {}".format(ana, lep, year)

    # Parton shower
    # e.g.: ps_isr_TT
    m = re.match(r"^ps_(isr|fsr)_(.+)$", name)
    if m:
        syst = {"isr": "ISR", "fsr": "FSR"}[m.group(1)]
        return "Parton shower ({}) {}".format(syst, m.group(2))

    # Prefiring
    # e.g.: CMS_l1_ecal_prefiring_2017
    m = re.match(r"^CMS_l1_ecal_prefiring_(201[0-9])$", name)
    if m:
        year = int(m.group(1))
        return "L1 ECAL Prefiring ({})".format(year)

    # QCDScale
    # e.g.: QCDscale_V
    if name.startswith("QCDscale_"):
        _, p = tuple(name.split("_", 1))
        return "QCD scale ({})".format(p)

    # THU_HH
    if name == "THU_HH":
        return "QCD scale (ggHH + mtop)"

    # Higgs PDF
    # e.g.: pdf_Higgs_qqHH
    if name.startswith("pdf_Higgs_"):
        _, _, p = tuple(name.split("_"))
        return "PDF (Higgs, {})".format(p)
    if name == "pdf_gg":
        return "PDF (gluon-induced)"
    if name == "pdf_qqbar":
        return "PDF (quark-induced)"

    # scale
    # e.g.: scale_VV
    if name.startswith("scale_"):
        _, p = tuple(name.split("_", 1))
        return "Renormalization scale ({})".format(higgs_procs.get(p, p))  # is envelope

    # custom normalisation systematics
    customs = {
        # SL
        "CMS_bbww_sl_st_norm": "ST norm. (SL, 20%)",
        "CMS_bbww_sl_tt_norm": "TT norm. (SL, 20%)",
        "CMS_bbww_sl_tt_norm_boost": "TT norm. boost. (SL, 20%)",
        "CMS_bbww_sl_st_norm_boost": "ST norm. boost. (SL, 20%)",
        "CMS_bbww_sl_ST_norm": "ST norm.",
        "CMS_bbww_sl_TT_norm": "TT norm.",
        "CMS_bbww_sl_WJets_norm": "W+Jets norm.",
        "CMS_bbww_sl_fakes_norm_2016": "Fakes norm. (SL, 50%) 2016",
        "CMS_bbww_sl_fakes_norm_2017": "Fakes norm. (SL, 50%) 2017",
        "CMS_bbww_sl_fakes_norm_2018": "Fakes norm. (SL, 50%) 2018",
        "CMS_bbww_sl_wjets_norm": "W+Jets norm. (SL, 20%)",
        # DL
        "CMS_bbww_dl_st_norm": "ST norm. (DL, 20%)",
        "CMS_bbww_dl_tt_norm": "TT norm. (DL, 20%)",
        "CMS_bbww_dl_tt_norm_boost": "TT norm. boost. (DL, 20%)",
        "CMS_bbww_dl_st_norm_boost": "ST norm. boost. (DL, 20%)",
        "CMS_bbww_dl_ST_norm": "ST norm.",
        "CMS_bbww_dl_TT_norm": "TT norm.",
        "CMS_bbww_dl_fakes_norm_2016": "Fakes norm. (DL, 30%) 2016",
        "CMS_bbww_dl_fakes_norm_2017": "Fakes norm. (DL, 30%) 2017",
        "CMS_bbww_dl_fakes_norm_2018": "Fakes norm. (DL, 30%) 2018",
        # global, deprecated
        "CMS_bbww_st_norm": "ST norm. (20%)",
        "CMS_bbww_tt_norm": "TT norm. (20%)",
        "CMS_bbww_wjets_norm": "W+Jets norm. (20%)",
    }
    if name in customs:
        return customs[name]

    return name
