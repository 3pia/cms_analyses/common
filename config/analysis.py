# coding: utf-8

from config.common import analysis
from config.Run2_pp_13TeV_2016 import campaign as run_2016
from config.Run2_pp_13TeV_2017 import campaign as run_2017
from config.Run2_pp_13TeV_2018 import campaign as run_2018

analysis.campaigns.add(run_2016)
analysis.campaigns.add(run_2017)
analysis.campaigns.add(run_2018)
