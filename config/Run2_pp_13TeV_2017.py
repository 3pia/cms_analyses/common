#!/usr/bin/env python
# coding: utf-8
# flake8: noqa

"""
Defintion of the campaign and datasets for CMS.
"""

import scinum as sn

import config.common as common
import utils.aci as aci
from config.util import init_campaign
from tasks.corrections.jets import make_files as jec_files

# campaign
campaign = aci.Campaign("Run2_pp_13TeV_2017", 1, ecm=13)

"""
Details: https://twiki.cern.ch/twiki/bin/viewauth/CMS/TopSystematics#TOP_Systematic_Uncertainties_Run

UE (underlying_event):
    Directly inferred from up/down variations of the datasets

mtop (top mass variation, 3 GeV shift):
    Up = 175.5 GeV
    Down = 169.5 GeV
    (assuming Up and Down histograms are normalized to nominal)

CR (color reconnection):
    Up = nominal + abs(nominal - max{GluonMove, QCDbased, erdON})
    Down = nominal - abs(nominal - max{GluonMove, QCDbased, erdON})
    (assuming that the GluonMove, QCDbased and erdON histograms are normalized to the same integral as the nominal histogram)

HDamp (ME-PS matching scale):
    Directly inferred from up/down variations of the datasets

dipoleRecoilOn (HH VBF):
    see: https://gitlab.cern.ch/cms-hh-bbww/cms-hh-to-bbww/-/blob/master/Legacy/DipoleRecoil.md
"""

init_campaign(
    campaign,
    __name__,
    __file__,
    ds_shifts={
        "TTTo2L2Nu": {
            # UE
            "TuneCP5Down": "/TTTo2L2Nu_TuneCP5down_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_new_pmx_102X_mc2017_realistic_v8-v1/NANOAODSIM",
            "TuneCP5Up": "/TTTo2L2Nu_TuneCP5up_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_new_pmx_102X_mc2017_realistic_v8-v1/NANOAODSIM",
            # mtop
            "mtopDown": "/TTTo2L2Nu_mtop169p5_TuneCP5_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_new_pmx_102X_mc2017_realistic_v8-v1/NANOAODSIM",
            "mtopUp": "/TTTo2L2Nu_mtop175p5_TuneCP5_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_new_pmx_102X_mc2017_realistic_v8-v1/NANOAODSIM",
            # color reconnection
            "GluonMove": dict(
                keys=[
                    "/TTTo2L2Nu_TuneCP5CR2_GluonMove_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8-v1/NANOAODSIM",
                    "/TTTo2L2Nu_TuneCP5CR2_GluonMove_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8_ext1-v1/NANOAODSIM",
                    "/TTTo2L2Nu_TuneCP5CR2_GluonMove_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8_ext2-v1/NANOAODSIM",
                ]
            ),
            "QCDbased": dict(
                keys=[
                    "/TTTo2L2Nu_TuneCP5CR1_QCDbased_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8-v1/NANOAODSIM",
                    "/TTTo2L2Nu_TuneCP5CR1_QCDbased_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8_ext1-v1/NANOAODSIM",
                    "/TTTo2L2Nu_TuneCP5CR1_QCDbased_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8_ext2-v1/NANOAODSIM",
                ]
            ),
            # erdON
            "erdON": dict(
                keys=[
                    "/TTTo2L2Nu_TuneCP5_erdON_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8-v1/NANOAODSIM",
                    "/TTTo2L2Nu_TuneCP5_erdON_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8_ext1-v1/NANOAODSIM",
                    "/TTTo2L2Nu_TuneCP5_erdON_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8_ext2-v1/NANOAODSIM",
                ]
            ),
            # hdamp
            "HDampDown": "/TTTo2L2Nu_hdampDOWN_TuneCP5_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_new_pmx_102X_mc2017_realistic_v8-v1/NANOAODSIM",
            "HDampUp": "/TTTo2L2Nu_hdampUP_TuneCP5_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_new_pmx_102X_mc2017_realistic_v8-v1/NANOAODSIM",
        },
        "TTToHadronic": {
            # UE
            "TuneCP5Down": "/TTToHadronic_TuneCP5down_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_new_pmx_102X_mc2017_realistic_v8-v1/NANOAODSIM",
            "TuneCP5Up": "/TTToHadronic_TuneCP5up_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_new_pmx_102X_mc2017_realistic_v8-v1/NANOAODSIM",
            # mtop
            "mtopDown": "/TTToHadronic_mtop169p5_TuneCP5_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8-v1/NANOAODSIM",
            "mtopUp": "/TTToHadronic_mtop175p5_TuneCP5_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_new_pmx_102X_mc2017_realistic_v8-v1/NANOAODSIM ",
            # color reconnection
            "GluonMove": "/TTToHadronic_TuneCP5CR2_GluonMove_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8-v1/NANOAODSIM",
            "QCDbased": "/TTToHadronic_TuneCP5CR1_QCDbased_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8-v1/NANOAODSIM",
            "erdON": "/TTToHadronic_TuneCP5_erdON_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8-v1/NANOAODSIM",
            # hdamp
            "HDampDown": "/TTToHadronic_hdampDOWN_TuneCP5_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_new_pmx_102X_mc2017_realistic_v8-v1/NANOAODSIM",
            "HDampUp": "/TTToHadronic_hdampUP_TuneCP5_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8-v1/NANOAODSIM",
        },
        "TTToSemiLeptonic": {
            # UE
            "TuneCP5Down": "/TTToSemiLeptonic_TuneCP5down_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_new_pmx_102X_mc2017_realistic_v8-v1/NANOAODSIM",
            "TuneCP5Up": "/TTToSemiLeptonic_TuneCP5up_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_new_pmx_102X_mc2017_realistic_v8-v1/NANOAODSIM",
            # mtop
            "mtopDown": "/TTToSemiLeptonic_mtop169p5_TuneCP5_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_new_pmx_102X_mc2017_realistic_v8-v1/NANOAODSIM",
            "mtopUp": "/TTToSemiLeptonic_mtop175p5_TuneCP5_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_new_pmx_102X_mc2017_realistic_v8-v1/NANOAODSIM",
            # color reconnection
            "GluonMove": "/TTToSemiLeptonic_TuneCP5CR2_GluonMove_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8-v1/NANOAODSIM",
            "QCDbased": "/TTToSemiLeptonic_TuneCP5CR1_QCDbased_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8-v1/NANOAODSIM",
            "erdON": "/TTToSemiLeptonic_TuneCP5_erdON_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8-v1/NANOAODSIM",
            # hdamp
            "HDampDown": "/TTToSemiLeptonic_hdampDOWN_TuneCP5_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8-v1/NANOAODSIM",
            "HDampUp": "/TTToSemiLeptonic_hdampUP_TuneCP5_PSweights_13TeV-powheg-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_new_pmx_102X_mc2017_realistic_v8-v1/NANOAODSIM",
        },
        "VBFHHTo2B2Tau_CV_1_C2V_1_C3_1": {
            "dipoleRecoilOn": "/VBFHHTo2B2Tau_CV_1_C2V_1_C3_1_dipoleRecoilOn-TuneCP5_PSweights_13TeV-madgraph-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8-v1/NANOAODSIM"
        },
        "VBFHHTo2B2Tau_CV_1_C2V_2_C3_1": {
            "dipoleRecoilOn": "/VBFHHTo2B2Tau_CV_1_C2V_2_C3_1_dipoleRecoilOn-TuneCP5_PSweights_13TeV-madgraph-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8-v1/NANOAODSIM"
        },
        "VBFHHTo2B2VTo2L2Nu_CV_1_C2V_1_C3_1": {
            "dipoleRecoilOn": "/VBFHHTo2B2VTo2L2Nu_CV_1_C2V_1_C3_1_dipoleRecoilOn-TuneCP5_PSweights_13TeV-madgraph-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8-v1/NANOAODSIM"
        },
        "VBFHHTo2B2VTo2L2Nu_CV_1_C2V_2_C3_1": {
            "dipoleRecoilOn": "/VBFHHTo2B2VTo2L2Nu_CV_1_C2V_2_C3_1_dipoleRecoilOn-TuneCP5_PSweights_13TeV-madgraph-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8-v1/NANOAODSIM"
        },
        "VBFHHTo2B2WToLNu2J_CV_1_C2V_1_C3_1": {
            "dipoleRecoilOn": "/VBFHHTo2B2WToLNu2J_CV_1_C2V_1_C3_1_dipoleRecoilOn-TuneCP5_PSweights_13TeV-madgraph-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8-v1/NANOAODSIM"
        },
        "VBFHHTo2B2WToLNu2J_CV_1_C2V_2_C3_1": {
            "dipoleRecoilOn": "/VBFHHTo2B2WToLNu2J_CV_1_C2V_2_C3_1_dipoleRecoilOn-TuneCP5_PSweights_13TeV-madgraph-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8-v1/NANOAODSIM"
        },
        "VBF_HH_CV_1_C2V_1_C3_1": {
            "dipoleRecoilOn": "/VBF_HH_CV_1_C2V_1_C3_1-TuneCP5_PSweights_13TeV-madgraph-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8-v1/NANOAODSIM"
        },
        "VBF_HH_CV_1_C2V_2_C3_1": {
            "dipoleRecoilOn": "/VBF_HH_CV_1_C2V_2_C3_1-TuneCP5_PSweights_13TeV-madgraph-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8-v1/NANOAODSIM"
        },
    },
)
# find_ds_shifts(
#     campaign,
#     shifts={
#         "TuneCP5Up": "TuneCP5",
#         "TuneCP5Down": "TuneCP5",
#     },
#     queries=[
#         "/*_TuneCP5*13TeV*/RunIIFall17NanoAODv6-PU2017_12Apr2018_Nano25Oct2019_*/NANOAODSIM",
#     ],
# )

# https://twiki.cern.ch/twiki/bin/view/CMS/TWikiLUM#SummaryTable
campaign.aux["lumi"] = 41530.0

campaign.aux["pileup_lfn_scenario_hint"] = dict(new_pmx="new_pmx")


# https://twiki.cern.ch/twiki/bin/viewauth/CMS/PileupJSONFileforData#Pileup_JSON_Files_For_Run_II
campaign.aux["min_bias_xsec"] = sn.Number(69.2, (sn.Number.REL, 0.046))  # mb


# Jec/Jes/Jer

campaign.aux["jes_version"] = {
    "B": "Fall17_17Nov2017B_V32_DATA",
    "C": "Fall17_17Nov2017C_V32_DATA",
    "D": "Fall17_17Nov2017DE_V32_DATA",
    "E": "Fall17_17Nov2017DE_V32_DATA",
    "F": "Fall17_17Nov2017F_V32_DATA",
    "mc": "Fall17_17Nov2017_V32_MC",
}


campaign.aux["jes_levels"] = {
    "data": ["L1FastJet", "L2Relative", "L3Absolute", "L2L3Residual"],
    "mc": ["L1FastJet", "L2Relative", "L3Absolute"],
}

# reduced set
campaign.aux["junc_format"] = "RegroupedV2_{version}_UncertaintySources_AK4PFchs"

# JER
campaign.aux["jer_version"] = "Fall17_V3"
campaign.aux["jer_type"] = "PtResolution"


files = {
    "default": {
        "POG_lumi_file": "/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions17/13TeV/ReReco/Cert_294927-306462_13TeV_EOY2017ReReco_Collisions17_JSON_v1.txt",  # https://hypernews.cern.ch/HyperNews/CMS/get/luminosity/749.html
        "POG_pileup_file": "/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions17/13TeV/PileUp/pileup_latest.txt",
        "Tallinn_gf_hh_denom": "https://github.com/HEP-KBFI/hh-bbww/raw/master/data/denom_2018.root",
        **common.files["default"],
    },
    "histo": {
        "electron_sf_files": {  # https://twiki.cern.ch/twiki/bin/view/CMS/EgammaIDRecipesRun2#94X_series_Fall17V2_Scale_factor
            # Standard CMS
            "POG_reco_high": "/eos/user/m/mfackeld/sf_2017/egammaEffi.txt_EGM2D_runBCDEF_passingRECO.root",
            "POG_reco_low": "/eos/user/m/mfackeld/sf_2017/egammaEffi.txt_EGM2D_runBCDEF_passingRECO_lowEt.root",
            "POG_id": "/eos/user/m/mfackeld/sf_2017/2017_ElectronMVA90.root",
            # Fakeable stuff + multilepton MVA ID
            # electron identification is done in 3 steps:
            # - nothing to reco / or reco efficiency (reco_low & reco_high)
            # - reco to something (id_loose_01) [something is defined arbitrarily due to technical reasons]
            # - something to loose (id_loose_02) [something is defined arbitrarily due to technical reasons]
            # all these should be merged into one SF accoding to https://gitlab.cern.ch/cms-hh-bbww/cms-hh-to-bbww/-/blob/master/Legacy/data_to_mc_corrections.md#lepton-id-sf-uncertainties
            "Tallinn_reco_low": "/afs/cern.ch/user/k/kaehatah/public/leptonIDSF/2017/el_scaleFactors_gsf_ptLt20.root",
            "Tallinn_reco_high": "/afs/cern.ch/user/k/kaehatah/public/leptonIDSF/2017/el_scaleFactors_gsf_ptGt20.root",
            "Tallinn_id_loose_01": "/afs/cern.ch/user/b/balvarez/work/public/ttHAnalysis/TnP_loose_ele_2017.root",
            "Tallinn_id_loose_02": "/afs/cern.ch/user/b/balvarez/work/public/ttHAnalysis/TnP_loosettH_ele_2017.root",
            "Tallinn_tth_tight": "/afs/cern.ch/user/b/balvarez/work/public/ttHAnalysis/TnP_ttH_ele_2017_3l/passttH/egammaEffi.txt_EGM2D.root",
            "Tallinn_tth_tight_error": "/afs/cern.ch/user/k/kaehatah/public/leptonIDSFerror/lepMVAEffSF_e_error_2017.root",
            "Tallinn_tth_relaxed": "/afs/cern.ch/user/k/kaehatah/public/leptonSF_recomp/lepMVAEffSF_e_2017_recomp.root",
        },
        "muon_sf_files": {  # https://twiki.cern.ch/twiki/bin/view/CMS/MuonReferenceEffs2017
            # Standard CMS
            "POG_low_pt_id": "/eos/user/m/mfackeld/sf_2017/RunBCDEF_SF_ID_JPsi_syst.root",
            "POG_iso": "/eos/user/m/mfackeld/sf_2017/RunBCDEF_SF_ISO_DESY_2017.root",
            "POG_id": "/eos/user/m/mfackeld/sf_2017/RunBCDEF_SF_ID_DESY_2017.root",
            # Fakeable stuff + multilepton MVA ID
            "Tallinn_idiso_loose": "/afs/cern.ch/user/b/balvarez/work/public/ttHAnalysis/TnP_loose_muon_2017.root",
            "Tallinn_tth_tight": "/afs/cern.ch/user/b/balvarez/work/public/ttHAnalysis/TnP_ttH_muon_2017_3l/passttH/egammaEffi.txt_EGM2D.root",
            "Tallinn_tth_tight_error": "/afs/cern.ch/user/k/kaehatah/public/leptonIDSFerror/lepMVAEffSF_m_error_2017.root",
            "Tallinn_tth_relaxed": "/afs/cern.ch/user/k/kaehatah/public/leptonSF_recomp/lepMVAEffSF_m_2017_recomp.root",
        },
        "trigger_sf_files": {
            # Standard CMS
            "ttHbb_dilepton": "/eos/user/m/mfackeld/sf_2017/tth_dileptonic_2DscaleFactors_MC_PU-lep_SFs_withSysts_2017BCDEF_24-06-20_updated_axis_limits.root",
            "ttHbb_single_e": "/eos/user/m/mfackeld/sf_2017/SingleEG_JetHT_Trigger_Scale_Factors_ttHbb2017_v3.root",
            "ttHbb_single_mu": "/eos/user/m/mfackeld/sf_2017/EfficienciesAndSF_RunBtoF_Nov17Nov2017.root",
            # Fakeable stuff + multilepton MVA ID
            "Tallinn_dilepton": "/eos/user/m/mfackeld/trigger_sf/trigger_sf_2017.root",
        },
        "trigger_sf_files_1d": {
            "Tallinn_sl_electron_1d": "/eos/user/m/mfackeld/sf_2017/Electron_Ele32orEle35_eff.root",
            "Tallinn_sl_muon_1d": "/eos/user/m/mfackeld/sf_2017/Muon_IsoMu24orIsoMu27_eff.root",
        },
        "l1_ecal_prefiring": [
            "/eos/user/m/mfackeld/sf_2017/L1prefiring_jetempt_2017BtoF.root",
            "/eos/user/m/mfackeld/sf_2017/L1prefiring_jetpt_2017BtoF.root",
            "/eos/user/m/mfackeld/sf_2017/L1prefiring_photonpt_2017BtoF.root",
        ],
        "jet_pu_id": {
            "POG_eff": "/eos/user/m/mfackeld/sf_2017/effcyPUID_81Xtraining.root",
            "POG_sf": "/eos/user/m/mfackeld/sf_2017/scalefactorsPUID_81Xtraining.root",
        },
        "fake": {
            "Tallinn_fakerates_sl": "/afs/cern.ch/user/v/veelken/public/leptonFR/FR_lep_mva_hh_bbWW_wFullSyst_2017_KBFI_2021Feb3_wCERNUncs2_FRErrTheshold_0p01.root",
            "Tallinn_fakerates_dl": "/afs/cern.ch/user/k/kaehatah/public/leptonFR/FR_lep_mva_hh_multilepton_wFullSyst_2017_KBFI_2020Dec21_wCERNUncs2_FRErrTheshold_0p01.root",
        },
    },
    "btag": {
        "deepjet": {
            "POG_full": "https://twiki.cern.ch/twiki/pub/CMS/BtagRecommendation94X/DeepFlavour_94XSF_V4_B_F.csv",
            "POG_reduced": "https://twiki.cern.ch/twiki/pub/CMS/BtagRecommendation94X/DeepFlavour_94XSF_V4_B_F_JESreduced.csv",  # "/eos/user/m/mfackeld/btagsf_reduced_jes/deepjet_2017.csv",
        },
        "subjet_deepb": "https://twiki.cern.ch/twiki/pub/CMS/BtagRecommendation94X/subjet_DeepCSV_94XSF_V4_B_F_v2.csv",
    },
}

# add jec files
files.update(jec_files(campaign))

campaign.aux["files"] = files


# store b-tagger working points
campaign.aux["working_points"] = {
    "deepjet": {
        "loose": 0.0521,
        "medium": 0.3033,
        "tight": 0.7489,
    },
    "subjet_deepb": {
        "loose": 0.1522,
        "medium": 0.4941,
    },
}

# MET filters
campaign.aux["met_filters"] = {
    "data": [
        "goodVertices",
        "globalSuperTightHalo2016Filter",
        "HBHENoiseFilter",
        "HBHENoiseIsoFilter",
        "EcalDeadCellTriggerPrimitiveFilter",
        "BadPFMuonFilter",
        "eeBadScFilter",
    ],
    "mc": [
        "goodVertices",
        "globalSuperTightHalo2016Filter",
        "HBHENoiseFilter",
        "HBHENoiseIsoFilter",
        "EcalDeadCellTriggerPrimitiveFilter",
        "BadPFMuonFilter",
    ],
}

campaign.aux["b_tagger"] = "deepjet"

campaign.aux["jes_sources"] = [
    "Absolute",
    "Absolute_2017",
    "BBEC1",
    "BBEC1_2017",
    "EC2",
    "EC2_2017",
    "FlavorQCD",
    "HF",
    "HF_2017",
    "RelativeBal",
    "RelativeSample_2017",
]

campaign.aux["PDFSet"] = "NNPDF31_nnlo_hessian_pdfas"
