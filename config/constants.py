# coding: utf-8

"""
Physics constants.
"""


import numpy as np
import scinum as sn
from scipy.stats import chi2, norm

# constants
N_LEPS = sn.Number(3)
Z_MASS = sn.Number(91.1876, {"z_mass": 0.0021})
W_MASS = sn.Number(80.385)
H_MASS = sn.Number(125.35)

# Higgs BR
BR_H_BB = sn.Number(0.5824, {"br_hbb": ("rel", 0.0124, 0.0127)})
BR_H_WW = sn.Number(0.2137, {"br_hww": ("rel", 0.0154, 0.0153)})
BR_H_GG = sn.Number(0.08187, {"br_hgg": ("rel", 0.034, 0.0341)})
BR_H_TT = sn.Number(0.06272, {"br_htt": ("rel", 0.0117, 0.0116)})
BR_H_CC = sn.Number(0.02891, {"br_hcc": ("rel", 0.012, 0.012)})
BR_H_ZZ = sn.Number(0.0262, {"br_hzz": ("rel", 0.015, 0.015)})
BR_H_YY = sn.Number(0.00227, {"br_hyy": ("rel", 0.0173, 0.0172)})
BR_H_ZY = sn.Number(0.001533, {"br_hzy": ("rel", 0.0571, 0.0571)})
BR_H_MM = sn.Number(0.0002176, {"br_hzy": ("rel", 0.0123, 0.0123)})

# W BR
BR_W_HAD = sn.Number(0.6741, {"br_whad": 0.0027})
BR_W_LEP = 1 - BR_W_HAD
BR_WW_SL = 2 * BR_W_HAD.mul(BR_W_LEP, rho=-1, inplace=False)
BR_WW_DL = BR_W_LEP**2
BR_WW_FH = BR_W_HAD**2

# Z BR
BR_Z_CLEP = sn.Number(0.033658, {"br_z_clep": 0.000023}) * N_LEPS
BR_Z_NUNU = sn.Number(0.20, {"br_z_nunu": 0.00055})
BR_ZZ_LLNUNU = 2 * BR_Z_CLEP * BR_Z_NUNU

BR_HH_BBZZ_DL = 2 * BR_H_BB * BR_H_ZZ * BR_ZZ_LLNUNU
BR_HH_BBWW_SL = 2 * BR_H_BB * BR_H_WW * BR_WW_SL
BR_HH_BBWW_DL = 2 * BR_H_BB * BR_H_WW * BR_WW_DL
BR_HH_BBVV_DL = BR_HH_BBWW_DL + BR_HH_BBZZ_DL
BR_HH_BBTAUTAU = sn.Number(0.073056)
BR_HH_BBBB = BR_H_BB**2

HHres = dict(
    prod=("GluGlu", "VBF"),
    resonance=("Radion", "BulkGraviton"),
    masses=(
        250,
        260,
        270,
        280,
        300,
        320,
        340,
        350,
        400,
        450,
        500,
        550,
        600,
        650,
        700,
        750,
        800,
        850,
        900,
        1000,
        1200,
        1250,
        1500,
        1750,
        2000,
        2500,
        3000,
    ),
    decays=(
        ("2B2VTo2L2Nu", BR_HH_BBWW_DL),
        ("2B2Tau", BR_HH_BBTAUTAU),
        ("4B", BR_HH_BBBB),
        ("2B2WToLNu2J", BR_HH_BBWW_SL),
    ),
)

HHxs = dict(
    # outdated: https://gitlab.cern.ch/cms-hh-bbww/cms-hh-to-bbww/blob/00b78fa4/Legacy/datasets.md#5312-cross-sections
    #
    # https://docs.google.com/spreadsheets/d/1XQQsN4rl3xGDa35W516TwKyadccpfoT7M1mFGxZ4UjQ/edit#gid=1223976475
    # scale uncertainties will be added by HH model directly, kl-dependent k-factors too.
    gf_nnlo_gdocs={  # kl -> xs
        0: sn.Number(
            0.069725,
            {
                # "scale": ("rel", 0.024, 0.061),
                "pdf": ("rel", 0.021),
                "alpha_s": ("rel", 0.021),
                "mtop": ("rel", 0.026),
            },
        ),
        1: sn.Number(
            0.031047,
            {
                # "scale": ("rel", 0.022, 0.05),
                "pdf": ("rel", 0.021),
                "alpha_s": ("rel", 0.021),
                "mtop": ("rel", 0.026),
            },
        ),
        2.45: sn.Number(
            0.013124,
            {
                # "scale": ("rel", 0.023, 0.051),
                "pdf": ("rel", 0.021),
                "alpha_s": ("rel", 0.021),
                "mtop": ("rel", 0.026),
            },
        ),
        5: sn.Number(
            0.091172,
            {
                # "scale": ("rel", 0.049, 0.088),
                "pdf": ("rel", 0.021),
                "alpha_s": ("rel", 0.021),
                "mtop": ("rel", 0.026),
            },
        ),
    },
    # https://twiki.cern.ch/twiki/bin/view/LHCPhysics/LHCHXSWGHH#Latest_recommendations_for_gluon
    gf_nnlo_ftapprox={
        0: sn.Number(
            0.07038,
            {
                "scale": ("rel", 0.024, 0.061),
                "pdf": ("rel", 0.021),
                "alpha_s": ("rel", 0.021),
                "mtop": ("rel", 0.026),
            },
        ),
        1: sn.Number(
            0.03105,
            {
                "scale": ("rel", 0.022, 0.05),
                "pdf": ("rel", 0.021),
                "alpha_s": ("rel", 0.021),
                "mtop": ("rel", 0.026),
            },
        ),
        2.45: sn.Number(
            0.01310,
            {
                "scale": ("rel", 0.023, 0.051),
                "pdf": ("rel", 0.021),
                "alpha_s": ("rel", 0.021),
                "mtop": ("rel", 0.026),
            },
        ),
        5: sn.Number(
            0.09482,
            {
                "scale": ("rel", 0.049, 0.088),
                "pdf": ("rel", 0.021),
                "alpha_s": ("rel", 0.021),
                "mtop": ("rel", 0.026),
            },
        ),
    },
    vbf_lo=dict(
        CV_1_C2V_1_C3_0=0.004454,
        CV_1_C2V_1_C3_1=0.001668,
        CV_1_C2V_1_C3_2=0.001375,
        CV_1_C2V_2_C3_1=0.01374,
        CV_1_5_C2V_1_C3_1=0.0638,
        CV_0_5_C2V_1_C3_1=0.01046,
        CV_1_C2V_0_C3_1=0.02617,
    ),
    # https://docs.google.com/spreadsheets/d/1XQQsN4rl3xGDa35W516TwKyadccpfoT7M1mFGxZ4UjQ/edit#gid=1223976475
    vbf_n3lo_2016=dict(
        CV_1_C2V_1_C3_0=0.0045915,
        CV_1_C2V_1_C3_1=0.0017260,
        CV_1_C2V_1_C3_2=0.0014306,
        CV_1_C2V_2_C3_1=0.0143923,
        CV_1_5_C2V_1_C3_1=0.0663340,
        CV_0_5_C2V_1_C3_1=0.0108778,
        CV_1_C2V_0_C3_1=0.0272322,
    ),
    vbf_n3lo_2017_2018=dict(
        CV_1_C2V_1_C3_0=0.0046089,
        CV_1_C2V_1_C3_1=0.0017260,
        CV_1_C2V_1_C3_2=0.0014228,
        CV_1_C2V_2_C3_1=0.0142178,
        CV_1_5_C2V_1_C3_1=0.0660185,
        CV_0_5_C2V_1_C3_1=0.0108237,
        CV_1_C2V_0_C3_1=0.0270800,
    ),
)

# https://github.com/cms-hh/HHStatAnalysis/blob/ba28eff0668885507019e07d6526ea1bc7165981/AnalyticalModels/python/NonResonantModel.py#L134-L138
# https://gitlab.cern.ch/hh/eft-benchmarks/-/blob/master/README.md#benchmarks-of-interest
gfHHparams = np.array(
    [  # kl, kt, c2, cg, c2g
        (1.0, 1.0, 0.0, 0.0, 0.0),  # SM
        (7.5, 1.0, -1.0, 0.0, 0.0),  # BM 1
        (1.0, 1.0, 0.5, -0.8, 0.6),
        (1.0, 1.0, -1.5, 0.0, -0.8),
        (-3.5, 1.5, -3.0, 0.0, 0.0),
        (1.0, 1.0, 0.0, 0.8, -1.0),
        (2.4, 1.0, 0.0, 0.2, -0.2),
        (5.0, 1.0, 0.0, 0.2, -0.2),
        (15.0, 1.0, 0.0, -1.0, 1.0),
        (1.0, 1.0, 1.0, -0.6, 0.6),
        (10.0, 1.5, -1.0, 0.0, 0.0),
        (2.4, 1.0, 0.0, 1.0, -1.0),
        (15.0, 1.0, 1.0, 0.0, 0.0),  # BM 12
    ],
    dtype=[(l, float) for l in ["kl", "kt", "c2", "cg", "c2g"]],
)
# https://link.springer.com/content/pdf/10.1007/JHEP03(2020)091.pdf Table 2
# https://gitlab.cern.ch/hh/eft-benchmarks/-/blob/master/README.md#benchmarks-of-interest
# we dont have samples for those, but we can reweight to them
gfHHparamsNoSample = np.array(
    [  # kl, kt, c2, cg, c2g
        (1.0, 1.0, 0.5, 0.8 / 3.0, 0.0),  # 8a, belongs to the 12 BMs from 'gfHHparams'
        (3.94, 0.94, -1.0 / 3.0, 0.75, -1.0),  # BM1
        (6.84, 0.61, 1.0 / 3.0, 0.0, 1.0),
        (2.21, 1.05, -1.0 / 3.0, 0.75, -1.5),
        (2.79, 0.61, 1.0 / 3.0, -0.75, -0.5),
        (3.95, 1.17, -1.0 / 3.0, 0.25, 1.5),
        (5.68, 0.83, 1.0 / 3.0, -0.75, -1.0),
        (-0.1, 0.94, 1.0, 0.25, 0.5),  # BM 7
    ],
    dtype=[(l, float) for l in ["kl", "kt", "c2", "cg", "c2g"]],
)


# https://en.wikipedia.org/wiki/Poisson_distribution#Confidence_interval
# poisson confidence interval (1-sigma, upper) for lambda if oberservation was 0
LAMBDA0 = chi2.ppf(1 - (1 - (norm.cdf(1) * 2 - 1)) / 2, 2) / 2
